-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-07-2017 a las 04:07:24
-- Versión del servidor: 5.7.16-log
-- Versión de PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mijoven7d`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividades`
--

CREATE TABLE `actividades` (
  `id` int(11) NOT NULL,
  `f_inicio` date DEFAULT NULL,
  `f_final` date DEFAULT NULL,
  `mes` varchar(100) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `descripcion` mediumtext,
  `puntuacion` int(11) DEFAULT NULL,
  `tipo_a_id` int(11) DEFAULT NULL,
  `circular_id` int(11) DEFAULT NULL,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `actividades`
--

INSERT INTO `actividades` (`id`, `f_inicio`, `f_final`, `mes`, `nombre`, `descripcion`, `puntuacion`, `tipo_a_id`, `circular_id`, `user_id`, `created_at`, `updated_at`) VALUES
(20, NULL, NULL, NULL, 'Plan de sostenimiento', 'La entrega de un plan de sostenimiento mensual, los 10 de cada vez, al entregarlo puntual recibira su puntuacion completa de lo contrario tendra solo 50 ptos, y de no entregarlo tendrá 0 ptos.', 80, 1, 1, 5, '2017-05-29 02:29:11', '2017-05-29 02:29:11'),
(21, '2017-05-23', '2017-05-30', 'mayo', 'Campañas ministeriales', 'Los clubes deben participar de las campañas ministeriales, con uniforme de gala, subir fotos... tendran sus 500 ptos al realizar la actividad de lo contrario tendra 0 ptos.', 500, 3, 1, 5, '2017-05-29 03:11:03', '2017-05-29 03:11:04'),
(22, NULL, NULL, NULL, 'Ministerio mensual', 'Un ministerio fuera de serie, consiste en visitar casa de abuelos, retenes, orfanatos, entre otras, el informe de estas actividades a los 10 de cada mes tendrá una puntuación total de 75 ptos de lo contrario el club no recibirá su puntuación.', 75, 1, 1, 5, '2017-05-29 03:53:59', '2017-05-29 03:54:00'),
(23, NULL, NULL, NULL, 'Gpss', 'El club deberá informar las reuniones a sus grupos pequeños salvacion y servicio a los 10 de cada mes, fotos no con poses, sino en acción, el club q envié su informe a la fecha recibe la puntuación de lo contrario no la tendrá.', 400, 1, 1, 5, '2017-05-29 03:57:04', '2017-05-29 03:57:06'),
(28, NULL, NULL, NULL, 'Informe mensual', 'Entregar informe mensualmente a mas tardar los 10 de cada mes! de lo contrario tendrá la mitad de la puntuación.', 100, 1, 1, 5, '2017-06-06 06:35:51', '2017-06-06 06:35:51'),
(29, NULL, NULL, NULL, 'iwieº', 'jhjwhejhj jhjh', 100, 1, 3, 5, '2017-06-07 06:06:11', '2017-06-07 06:06:12'),
(31, '2017-11-21', NULL, 'Noviembre', 'Pagos', '<p>Realizar el pago antes del 21 de de noviembre y tendrá la puntuación completa, de lo contrario tendrá la mitad de la puntuación</p>', 2000, 2, 1, 5, '2017-07-19 18:49:59', '2017-07-19 18:49:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `act_modulo`
--

CREATE TABLE `act_modulo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `modulo_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `act_modulo`
--

INSERT INTO `act_modulo` (`id`, `nombre`, `modulo_id`, `created_at`, `updated_at`) VALUES
(1, 'Miembros participando en la sociedad JA', 1, '2017-05-05 01:40:44', '2017-05-05 01:40:44'),
(2, 'Miembros asistentes al club', 1, '2017-05-05 01:40:44', '2017-05-05 01:40:44'),
(3, 'Miembros participando en año bíblico', 1, '2017-05-05 01:40:44', '2017-05-05 01:40:44'),
(4, 'Grupo de oración intercesora', 1, '2017-05-05 01:40:44', '2017-05-05 01:40:44'),
(5, 'Grupos GPSS activos trabajando', 1, '2017-05-05 01:40:44', '2017-05-05 01:40:44'),
(6, 'Semanas de oración JA', 1, '2017-05-05 01:40:44', '2017-05-05 01:40:44'),
(7, 'Retiros espirituales', 1, '2017-05-05 01:40:44', '2017-05-05 01:40:44'),
(8, 'Campamentos Juveniles', 1, '2017-05-05 01:40:44', '2017-05-05 01:40:44'),
(9, 'Paseos y Caminatas', 1, '2017-05-05 01:40:44', '2017-05-05 01:40:44'),
(10, 'Ferias JA GAAM y Deportivas', 1, '2017-05-05 01:40:44', '2017-05-05 01:40:44'),
(11, 'Fogatas y/o aniversarios', 1, '2017-05-05 01:40:44', '2017-05-05 01:40:44'),
(12, 'Lectura del Libro "Servicio Cristiano"//PATRIARCAS Y PROFETAS', 2, '2017-05-05 01:40:44', '2017-05-05 01:40:44'),
(13, 'Clases y especialidad J.A realizadas', 2, '2017-05-05 01:40:45', '2017-05-05 01:40:45'),
(14, 'Ceremonias de investidura', 2, '2017-05-05 01:40:45', '2017-05-05 01:40:45'),
(15, 'Feria de Conquistadores y aventureros', 2, '2017-05-05 01:40:45', '2017-05-05 01:40:45'),
(16, 'Reunión y capacitación de liderazgo', 2, '2017-05-05 01:40:45', '2017-05-05 01:40:45'),
(17, 'Miembros activos en Mision trabaJA', 3, '2017-05-05 01:40:45', '2017-05-05 01:40:45'),
(18, 'Predicadores Juveniles "GANA CON JESÚS"', 3, '2017-05-05 01:40:45', '2017-05-05 01:40:45'),
(19, 'Cantidad de hermanos bautizados en el mes', 3, '2017-05-05 01:40:45', '2017-05-05 01:40:45'),
(20, 'estudios Bíblicos', 3, '2017-05-05 01:40:45', '2017-05-05 01:40:45'),
(21, 'cantatas de testificación', 3, '2017-05-05 01:40:45', '2017-05-05 01:40:45'),
(22, 'Evangelismo por internet (páginas web) RETO', 3, '2017-05-05 01:40:45', '2017-05-05 01:40:45'),
(23, 'Contactos misioneros', 3, '2017-05-05 01:40:45', '2017-05-05 01:40:45'),
(24, 'Recolección', 3, '2017-05-05 01:40:45', '2017-05-05 01:40:45'),
(25, 'Campañas Evangelísticas // CAMPAÑAS MINISTERIALES', 3, '2017-05-05 01:40:45', '2017-05-05 01:40:45'),
(26, 'Marchas de Salud', 3, '2017-05-05 01:40:45', '2017-05-05 01:40:45'),
(27, 'Miembros en bandas marciales', 3, '2017-05-05 01:40:45', '2017-05-05 01:40:45'),
(28, 'Miembros en la orquesta adventista', 3, '2017-05-05 01:40:45', '2017-05-05 01:40:45'),
(29, 'Servicio a la comunidad (último domingo de cada mes)', 3, '2017-05-05 01:40:45', '2017-05-05 01:40:45'),
(30, '5 Días para dejar de fumar', 3, '2017-05-05 01:40:45', '2017-05-05 01:40:45'),
(31, 'Impacto JA "Cerca de Ti"', 3, '2017-05-05 01:40:45', '2017-05-05 01:40:45'),
(32, '"Flores para ti Mamá"', 3, '2017-05-05 01:40:45', '2017-05-05 01:40:45'),
(33, '"Operación balsamo"', 3, '2017-05-05 01:40:45', '2017-05-05 01:40:45'),
(34, 'Reuniones con S.U.V.A.', 4, '2017-05-05 01:40:46', '2017-05-05 01:40:46'),
(35, 'Estudios Bíblicos con Universitarios', 4, '2017-05-05 01:40:46', '2017-05-05 01:40:46'),
(36, 'Actividades especiales', 4, '2017-05-05 01:40:46', '2017-05-05 01:40:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargos`
--

CREATE TABLE `cargos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cargos`
--

INSERT INTO `cargos` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'director(a)', '2017-03-02 21:57:46', '2017-03-02 21:57:46'),
(2, 'sub-director(a)', '2017-03-02 21:57:46', '2017-03-02 21:57:46'),
(3, 'tesorero(a)', '2017-03-02 21:57:47', '2017-03-02 21:57:47'),
(4, 'Consejero(a)', '2017-03-02 21:57:47', '2017-03-02 21:57:47'),
(5, 'Anciano responsable', '2017-03-02 21:57:47', '2017-03-02 21:57:47'),
(6, 'Lider responsable', '2017-03-02 21:57:47', '2017-03-02 21:57:47'),
(7, 'Lider asociado responsable', '2017-03-02 21:57:47', '2017-03-02 21:57:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `nombre`, `tipo`, `created_at`, `updated_at`) VALUES
(1, 'Aventureros', 1, '2017-02-21 20:23:32', '2017-02-21 20:23:32'),
(2, 'Conquistadores', 1, '2017-02-21 20:23:33', '2017-02-21 20:23:33'),
(3, 'Guías Mayores', 1, '2017-02-21 20:23:33', '2017-02-21 20:23:33'),
(4, 'Jóvenes', 2, '2017-02-21 20:23:33', '2017-02-21 20:23:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `circulares`
--

CREATE TABLE `circulares` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `sub_titulo` varchar(255) DEFAULT NULL,
  `destinario` text,
  `asunto` mediumtext,
  `fecha` varchar(150) DEFAULT NULL,
  `introduccion` text,
  `recomendacion` text,
  `reconocimiento` text,
  `observacion` text,
  `categoria_id` int(11) DEFAULT NULL,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `circulares`
--

INSERT INTO `circulares` (`id`, `titulo`, `sub_titulo`, `destinario`, `asunto`, `fecha`, `introduccion`, `recomendacion`, `reconocimiento`, `observacion`, `categoria_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'II CAMPOREE DE CONQUISTADORES Y GUIAS MAYORES', 'Circular 1', NULL, NULL, 'Marzo 26-31, 2018', 'Querida amig@:\n\n¡Bienvenid@ a esta nueva y ruta hacia nuestro Camporee 2018! Ha sido maravilloso sentir la emoción y el fervor en nuestros chicos luego de nuestro pasado Camporee “Los Elegidos: De la Esclavitud a la Libertad”, y las decisiones que están marcando sus vidas para siempre. Es impresionante notar el entusiasmo, las ganas de continuar con ese sueño maravilloso que comenzó hace pocos días en Guaranache y que está contagiando cada rincón de MVNOr. Es una evidencia que estamos cumpliendo con el Plan de Dios. Sólo así Dios irá con nosotros, y seguirá guiando nuestro destino. Bendito sea su Nombre y Su Misericordia. Le rogamos juntos que nos mantenga firmes, fieles, y muy valientes y que trace nuestro destino hasta el Cielo!\nCon esta pasión en la sangre, les comparto la primera información para nuestro próximo camporee.\n\nInscripción:\n Club: Bs. 5.000,00 Cada uno.\n Si deseas participar del Camporee 2018 como club de Conquistadores y Guías Mayores, inscribirás los dos clubes. Si no lo haces, no podrás optar a la premiación respectiva, ni recibir su tabla de desempeño. Te animo a que inscribas ambos y busques los miembros quienes formarán parte de tu club!\n Fecha de inscripción: Lunes 24 de Abril, 2017, en la oficina de la Misión. Puedes hacerlo presencial o bien sea enviando una copia de la transferencia/depósito al correo jovenesnororiente@gmail.com y llamando al (0426 592 22 20) para reportar la operación. Nota: el depósito original ha de entregarse en la Misión, pero para efectos de inscripción se puede enviar una copia mientras se lleva el voucher original a la oficina de la Misión.\n NOTA IMPORTANTE: Una vez que la zona haya alcanzado la meta de inscripción de los clubes participantes, se le dará la oportunidad de elegir el lugar de campamento. Así que ¡A todos nos interesa motivar a que se cumpla la meta de inscripción de los clubes por zona! Quizá podamos elegir un terreno con un poco más de sombra, más cerca al auditorio, etc. etc. siempre hay beneficios al inscribirnos con tiempo!\n NOTA IMPORTANTE 2: La cuota de Bs. 5.000,00 está sujeta a cambios a partir del lunes 3 de Julio del mes en curso.\n Acampante: Se establecerá a partir del mes de enero, 2018. Esto quiere decir que trabajarán durante este año para cubrir los gastos del club.\n Seguro: Se establecerá a partir de Febrer 2018.\nMETA: Llevar al menos 560 Conquistadores (112 visitas y 448 conquis), 560 GM (112 visitas y 448 GM), 56 iglesias asistentes (equivalente al 60% del total de las congregaciones de nuestro campo) y 225 investiduras en el Camporee con la ayuda de Dios. Para lograr este fin hemos dividido las metas por zona (hemos además dividido, sólo para efectos de los Ministerios Juveniles, la zona 2 –Zona 2A compuesta por los distritos de Clarines, Barcelona 1 y Barcelona 2 – y zona 2B – compuesta por los distritos Pto la Cruz 1, Pto. La Cruz 2 y Metropolitano). Estas metas se han colocado calculando sólo 10 miembros (8 ASD y 2 visitas), al igual que cada club de GM. Esperamos que cada director se desafíe a más, tal como es la gente de oriente!', NULL, 'Todos, todos los clubes que logren con ayuda de Dios la categoría de “Honorable” recibirán premiación durante el Camporee. Asimismo, se entregarán premios a los siguientes clubes:\n Club novato del Año: Se otorgará al club que asiste por primera vez, en función de su desempeño DURANTE el camporee. También puede optar, si lo desea, por cualquiera de los escalafones anuales.\n Club regreso del año: Se otorgará al club que asistió al Camporee junto con la AVOR el año 2015, pero no asistió al camporee en Guaranache, siendo ya un club organizado. También puede optar, si lo desea, por cualquiera de los escalafones anuales.\n Club Amigable: Se realizará una votación general donde se elegirá aquel club que se gane el cariño de los demás por su espíritu de servicio, humildad y compañerismo hacia los demas clubes !Ya pueden comenzar a realizar actos de bondad para iniciar la campana por obtener este galardon!\n„Ï Club Misionero del Ano: Se otorgara en funcion de la entrega del informe misionero que entregaran durante la inscripcion. Los parametros se entregaran muy pronto. Sera una premiacion especial, basada en el trabajo evangelistico de tal club.\n„Ï Club Pionero del ano: Se entregara un premio especial a aquel club que colabore y asesore de manera activa en la organizacion de un club nuevo. Tendra una premiacion especial.\n„Ï Desempeno de zonas: De igual modo, daremos una premiacion especial a la zona cuyo desempeno de sus clubes le ayude a obtener la mayor puntuacion durante todo el ano, incluyendo el camporee. Esto se lograra si los clubes de sus zonas se mantienen luchando por la mas alta puntuacion, ¡§Honorable¡¨.\nComo puedes ver, hay oportunidad de alcanzar al menos un premio. Sólo tienes que esforzarte con tu club de cumplir CADA REQUISITO y por la gracia de Dios trabajar, desde el domingo 23 de Abril.', 'Bienvenido una vez más a la aventura, al maravilloso viaje que emprenderemos junto al Pueblo de Israel desde la ribera del Río Jordán hasta conquistar el último rincón de Canaán, la Tierra Prometida!\nDios te bendiga. Oro por ti, y estoy siempre a tu orden. Con mucho cariño,', 2, 5, '2017-05-04 06:33:50', '2017-05-04 06:33:50'),
(2, 'idsdjskdj', 'kwdjkdjkdkkk', 'kjdkjsfkj', 'kdkfdjk', 'skdkdkjk', 'kjkwjkejk', NULL, NULL, NULL, 1, 3, '2017-05-16 18:56:43', '2017-05-16 18:56:44'),
(3, 'hola que tal?', 'iwuiu', NULL, 'iiuiiui', NULL, NULL, NULL, NULL, NULL, 1, 5, '2017-05-21 22:28:46', '2017-05-21 22:28:47'),
(4, 'kjkjkjkjk', 'kkjkdjkdj', 'jkwqjkj', 'jajshfhjf', '89-sskjkjj', 'skdjskdfjkj', 'jkjkjkj', 'jkjkjkjk', 'jkjkjkj', 4, 3, '2017-05-22 01:05:03', '2017-05-22 01:05:04'),
(5, 'esta es una circular para guías mayores', 'fjfdjdfjhdjfhjdhfjhfjhf', NULL, 'jhsjdhjshfj', '30 de marzo-7 de abril', 'jxhjhdjfj', 'jdhjhfj', 'djhfjjsdfj', NULL, 3, 5, '2017-05-30 22:01:31', '2017-05-30 22:01:32'),
(6, 'III Camporee de Conquistadores y Guías Mayores', NULL, NULL, NULL, 'Abril 27- 07 de Marzo', 'djhjdhf dfjdssjdk sahjdsaj', 'jhdj sdfjdfjds dfhdjh', 'jdvjjdf dfjhdjhdjf djfdjf', 'djjdj fjdjhdjf djfhjdfj', 2, 5, '2017-06-01 20:17:44', '2017-06-01 20:17:47'),
(8, 'addh', NULL, NULL, NULL, '21 de marzo - 28 de marzo', NULL, NULL, NULL, NULL, 4, 5, '2017-07-07 22:00:57', '2017-07-07 22:00:57'),
(9, 'jkkj', NULL, NULL, NULL, 'kjkjk', NULL, NULL, NULL, NULL, 1, 5, '2017-07-07 22:01:35', '2017-07-07 22:01:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clubes`
--

CREATE TABLE `clubes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `iglesia_id` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clubes`
--

INSERT INTO `clubes` (`id`, `nombre`, `telefono`, `categoria_id`, `iglesia_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Sol de Oriente', '923847356', 2, 1, 4, '2017-02-15 06:22:47', '2017-02-15 06:22:47'),
(2, 'Cajigal', '899289382', 2, 2, 7, '2017-06-20 21:42:15', '2017-06-20 21:42:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directivas`
--

CREATE TABLE `directivas` (
  `id` int(11) NOT NULL,
  `club_id` int(11) DEFAULT NULL,
  `persona_id` int(11) DEFAULT NULL,
  `cargo_id` int(11) DEFAULT NULL,
  `grupo_id` int(11) DEFAULT NULL,
  `iglesia_id` int(11) DEFAULT NULL,
  `t` varchar(4) DEFAULT NULL,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `distritos`
--

CREATE TABLE `distritos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `zona_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `distritos`
--

INSERT INTO `distritos` (`id`, `nombre`, `zona_id`, `created_at`, `updated_at`) VALUES
(1, 'Barcelona I', 2, '2017-02-21 20:28:46', '2017-02-21 23:48:07'),
(2, 'Barcelona II', 2, '2017-06-20 21:12:26', '2017-06-20 21:12:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos`
--

CREATE TABLE `grupos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `club_id` int(11) DEFAULT NULL,
  `iglesia_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `grupos`
--

INSERT INTO `grupos` (`id`, `nombre`, `direccion`, `club_id`, `iglesia_id`, `created_at`, `updated_at`) VALUES
(1, 'Jóvenes Instructores', 'calle maturin', 1, NULL, '2017-02-20 01:11:25', '2017-02-20 01:11:25'),
(2, 'Mujeres triunfadoras', 'calle maturin', 1, NULL, '2017-02-20 01:11:25', '2017-02-20 01:11:25'),
(4, 'Soldado soy de Jesús', 'Pica de Maurica', 1, NULL, '2017-04-04 05:11:17', '2017-04-04 05:11:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `identidad`
--

CREATE TABLE `identidad` (
  `id` int(11) NOT NULL,
  `logo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `direccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `club_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `iglesias`
--

CREATE TABLE `iglesias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `direccion` varchar(200) DEFAULT NULL,
  `tipo_id` int(11) DEFAULT NULL,
  `distrito_id` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `iglesias`
--

INSERT INTO `iglesias` (`id`, `nombre`, `direccion`, `tipo_id`, `distrito_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'central de Barcelona', 'Av. Contry Club Calle maturin', 1, 1, 6, '2017-02-21 20:28:48', '2017-02-21 20:28:48'),
(2, 'La aduana', 'Aduana calle 1', 1, 2, NULL, '2017-06-20 21:41:48', '2017-06-20 21:41:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `informes`
--

CREATE TABLE `informes` (
  `id` int(11) NOT NULL,
  `mes` varchar(50) DEFAULT NULL,
  `descripcion` text,
  `modulo_id` int(11) NOT NULL,
  `club_id` int(11) DEFAULT NULL,
  `actividad_id` int(11) DEFAULT NULL,
  `iglesia_id` int(11) DEFAULT NULL,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `informes`
--

INSERT INTO `informes` (`id`, `mes`, `descripcion`, `modulo_id`, `club_id`, `actividad_id`, `iglesia_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Julio', 'kj fkdjfkd fkdfjdk fkdjff fjdk', 1, 1, NULL, NULL, 4, '2017-07-26 06:56:20', '2017-07-26 06:56:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `informe_act`
--

CREATE TABLE `informe_act` (
  `id` int(11) NOT NULL,
  `informe_id` int(11) DEFAULT NULL,
  `actmod_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `informe_act`
--

INSERT INTO `informe_act` (`id`, `informe_id`, `actmod_id`, `created_at`, `updated_at`) VALUES
(8, 1, 1, '2017-07-26 06:56:20', '2017-07-26 06:56:20'),
(9, 1, 2, '2017-07-26 06:56:20', '2017-07-26 06:56:20'),
(10, 1, 3, '2017-07-26 06:56:20', '2017-07-26 06:56:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscripciones`
--

CREATE TABLE `inscripciones` (
  `id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `monto` decimal(7,2) DEFAULT NULL,
  `monto_miembro` decimal(7,2) DEFAULT NULL,
  `monto_acompanante` decimal(7,2) DEFAULT NULL,
  `monto_nino` decimal(7,2) DEFAULT NULL,
  `seguro` decimal(7,2) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `club_id` int(11) DEFAULT NULL,
  `iglesia_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `inscripciones`
--

INSERT INTO `inscripciones` (`id`, `fecha`, `monto`, `monto_miembro`, `monto_acompanante`, `monto_nino`, `seguro`, `categoria_id`, `club_id`, `iglesia_id`, `created_at`, `updated_at`) VALUES
(1, '2017-04-13', '700.00', '678.00', '654.00', '343.00', '1000.00', 1, NULL, NULL, '2017-03-17 01:40:00', '2017-04-28 00:07:23'),
(2, '2017-03-23', '89898.00', '8989.00', '98989.00', '989.00', '6000.00', 2, NULL, NULL, '2017-03-17 01:40:15', '2017-03-17 01:40:16'),
(3, '2017-04-27', '2000.00', '67678.00', '8777.00', '77776.00', '67676.00', 3, NULL, NULL, '2017-03-26 05:58:39', '2017-03-26 05:58:40'),
(6, '2017-03-13', '78787.00', '6767.00', '7767.00', '5545.00', '5454.00', 1, NULL, NULL, '2017-03-29 07:08:16', '2017-03-29 07:08:16'),
(11, '2017-03-21', '5656.00', '6556.00', '5656.00', '65656.00', '665.00', 3, NULL, NULL, '2017-03-30 20:40:20', '2017-03-30 20:40:21'),
(12, '2017-03-22', '6565.00', '5656.00', '6565.00', '87878.00', '878.00', 2, NULL, NULL, '2017-03-31 23:14:53', '2017-03-31 23:14:54'),
(23, '2017-05-11', '800.00', '80.00', '89.00', '88.00', '0.00', 4, NULL, NULL, '2017-05-09 06:06:29', '2017-05-09 06:06:29'),
(24, '2017-05-17', '89.00', '9.00', '9.00', '9.00', '9.00', 4, NULL, NULL, '2017-05-09 06:09:58', '2017-05-09 06:09:58'),
(34, '2017-05-02', '889.00', '9899.00', '8989.00', '9898.00', '9899.00', 1, NULL, NULL, '2017-05-14 18:06:31', '2017-05-14 18:06:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `investiduras`
--

CREATE TABLE `investiduras` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `t` varchar(2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `investiduras`
--

INSERT INTO `investiduras` (`id`, `nombre`, `t`, `created_at`, `updated_at`) VALUES
(1, 'Corderitos', 'a', NULL, NULL),
(2, 'Castorcitos', 'a', NULL, NULL),
(3, 'Abejitas industriosas', 'a', NULL, NULL),
(4, 'Rayos de sol', 'a', NULL, NULL),
(5, 'Constructor', 'a', NULL, NULL),
(6, 'Manos ayudadoras', 'a', NULL, NULL),
(7, 'Amigo', 'c', NULL, NULL),
(8, 'Amigo de senda', 'c', NULL, NULL),
(9, 'Compañero', 'c', NULL, NULL),
(10, 'Compañero de caminata', 'c', NULL, NULL),
(11, 'Explorador', 'c', NULL, NULL),
(12, 'Explorador de selva', 'c', NULL, NULL),
(13, 'Orientador', 'c', NULL, NULL),
(14, 'Orientador de expedición', 'c', NULL, NULL),
(15, 'Viajero', 'c', NULL, NULL),
(16, 'Viajero avanzado', 'c', NULL, NULL),
(17, 'Guía', 'c', NULL, NULL),
(18, 'Guía anvanzado', 'c', NULL, NULL),
(19, 'Guía mayor', 'c', NULL, NULL),
(20, 'Líder juvenil', 'c', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lecciones`
--

CREATE TABLE `lecciones` (
  `id` int(11) NOT NULL,
  `numero` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `lecciones`
--

INSERT INTO `lecciones` (`id`, `numero`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 2, NULL, NULL),
(3, 3, NULL, NULL),
(4, 4, NULL, NULL),
(5, 5, NULL, NULL),
(6, 6, NULL, NULL),
(7, 7, NULL, NULL),
(8, 8, NULL, NULL),
(9, 9, NULL, NULL),
(10, 10, NULL, NULL),
(11, 12, NULL, NULL),
(12, 13, NULL, NULL),
(13, 14, NULL, NULL),
(14, 15, NULL, NULL),
(15, 15, NULL, NULL),
(16, 16, NULL, NULL),
(17, 17, NULL, NULL),
(18, 18, NULL, NULL),
(19, 19, NULL, NULL),
(20, 20, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `miembros`
--

CREATE TABLE `miembros` (
  `id` int(11) NOT NULL,
  `persona_id` int(11) DEFAULT NULL,
  `club_id` int(11) DEFAULT NULL,
  `donante` tinyint(1) DEFAULT NULL,
  `voluntario` tinyint(1) DEFAULT NULL,
  `ministerio` varchar(255) DEFAULT NULL,
  `curso` varchar(255) DEFAULT NULL,
  `categoria` varchar(100) DEFAULT NULL,
  `tipo_sangre` varchar(10) DEFAULT NULL,
  `necesidad` mediumtext,
  `observacion` mediumtext,
  `t` varchar(2) DEFAULT NULL,
  `grupo_id` int(11) DEFAULT NULL,
  `iglesia_id` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `miembros`
--

INSERT INTO `miembros` (`id`, `persona_id`, `club_id`, `donante`, `voluntario`, `ministerio`, `curso`, `categoria`, `tipo_sangre`, `necesidad`, `observacion`, `t`, `grupo_id`, `iglesia_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 0, 0, 'Coro', NULL, NULL, 'o+', NULL, NULL, 'o', NULL, NULL, 4, '2017-07-19 01:03:59', '2017-07-19 01:03:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `miembro_investidura`
--

CREATE TABLE `miembro_investidura` (
  `id` int(11) NOT NULL,
  `miembro_id` int(11) DEFAULT NULL,
  `investidura_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `miembro_investidura`
--

INSERT INTO `miembro_investidura` (`id`, `miembro_id`, `investidura_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2017-07-19 01:03:59', '2017-07-19 01:03:59'),
(2, 1, 2, '2017-07-19 01:03:59', '2017-07-19 01:03:59'),
(3, 1, 3, '2017-07-19 01:03:59', '2017-07-19 01:03:59'),
(4, 1, 4, '2017-07-19 01:03:59', '2017-07-19 01:03:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `miembro_lecciones`
--

CREATE TABLE `miembro_lecciones` (
  `id` int(11) NOT NULL,
  `miembro_id` int(11) DEFAULT NULL,
  `leccion_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8 NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ministerios`
--

CREATE TABLE `ministerios` (
  `id` int(11) NOT NULL,
  `mes` varchar(45) DEFAULT NULL,
  `dia` tinyint(4) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `descripcion` text,
  `actividad_id` int(11) DEFAULT NULL,
  `club_id` int(11) DEFAULT NULL,
  `iglesia_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ministerios`
--

INSERT INTO `ministerios` (`id`, `mes`, `dia`, `nombre`, `descripcion`, `actividad_id`, `club_id`, `iglesia_id`, `user_id`, `created_at`, `updated_at`) VALUES
(5, 'Mayo', 31, 'Visitar la casa del abuelo', 'jdfe dhfghewgrh ergewhreh', NULL, 1, NULL, 4, '2017-06-04 23:04:53', '2017-06-04 23:04:53'),
(6, 'Junio', 4, 'Visitar reten de menores', 'dhasdjsa djsahd djsahj', NULL, 1, NULL, 4, '2017-06-04 23:06:13', '2017-06-04 23:06:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulos`
--

CREATE TABLE `modulos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) DEFAULT NULL,
  `descripcion` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `modulos`
--

INSERT INTO `modulos` (`id`, `nombre`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 'Discipulado', 'Actividades Devocionales y Actividades Sociales', '2017-05-05 01:40:44', '2017-05-05 01:40:44'),
(2, 'Liderazgo', 'Actividades Devocionales y Actividades Sociales', '2017-05-05 01:40:44', '2017-05-05 01:40:44'),
(3, 'Evangelismo y Servicio', 'Actividades Misioneras y de Testificación', '2017-05-05 01:40:45', '2017-05-05 01:40:45'),
(4, 'Actividades Universitarias', '', '2017-05-05 01:40:46', '2017-05-05 01:40:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `id` int(11) NOT NULL,
  `n_referencia` varchar(100) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `banco` varchar(255) DEFAULT NULL,
  `monto` int(11) DEFAULT NULL,
  `concepto` varchar(255) DEFAULT NULL,
  `comprobante` varchar(255) DEFAULT NULL,
  `planilla` varchar(255) DEFAULT NULL,
  `cantidad_m` int(11) DEFAULT NULL,
  `cantidad_a` int(11) DEFAULT NULL,
  `cantidad_n` int(11) DEFAULT NULL,
  `cantidad_e` int(11) DEFAULT NULL,
  `directiva` int(11) DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `club_id` int(11) DEFAULT NULL,
  `actividad_id` int(11) DEFAULT NULL,
  `iglesia_id` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pagos`
--

INSERT INTO `pagos` (`id`, `n_referencia`, `fecha`, `banco`, `monto`, `concepto`, `comprobante`, `planilla`, `cantidad_m`, `cantidad_a`, `cantidad_n`, `cantidad_e`, `directiva`, `tipo`, `club_id`, `actividad_id`, `iglesia_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '77298332', '2017-05-18', 'banco de venezuela', 88, 'Inscripción de club/sj al campo', NULL, 'mision trasporte MERLY.docx', NULL, NULL, NULL, NULL, NULL, 'Inscripción de club/sj', 1, 21, NULL, 4, '2017-05-31 23:12:49', '2017-05-31 23:12:49'),
(2, 'jsdh888', '2017-06-13', 'banco provincial', 78778, 'Inscripción de club/sj al camporee/campestre', 'pago inscripcion congreso.PNG', 'devocional.docx', NULL, NULL, NULL, NULL, NULL, 'Inscripción de club/sj', 1, NULL, NULL, 4, '2017-06-02 06:05:50', '2017-06-02 06:05:50'),
(3, 'JHGS67898', '2017-06-01', 'Banco de venezuela', 10000, 'Inscripción de club/sj al campo', 'pago inscripcion congreso.PNG', 'imprimir.docx', NULL, NULL, NULL, NULL, NULL, 'Inscripción de club/sj', 1, NULL, NULL, 4, '2017-06-04 23:56:41', '2017-06-04 23:56:41'),
(4, 'HGHAHD77889', '2017-06-14', 'Banco provincial', 30000, NULL, 'planificacion.PNG', NULL, 30, 10, 5, 2, 4, 'Inscripción de acampantes', 1, NULL, NULL, 4, '2017-06-05 01:02:36', '2017-06-05 01:02:36'),
(5, 'HJHJ7878', '2017-06-06', 'Banco de venezuela', 20000, NULL, NULL, NULL, 67, 65, 4, 4, 3, 'Inscripción de acampantes', 1, NULL, NULL, 4, '2017-06-05 01:06:10', '2017-06-05 01:06:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `token` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pastores`
--

CREATE TABLE `pastores` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `cedula` varchar(45) DEFAULT NULL,
  `correo` varchar(150) DEFAULT NULL,
  `distrito_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pastores`
--

INSERT INTO `pastores` (`id`, `nombre`, `cedula`, `correo`, `distrito_id`, `created_at`, `updated_at`) VALUES
(1, 'Reinaldo Arcia', '25178998', 'reinaldo@gmail.com', 1, '2017-02-21 20:28:47', '2017-02-21 20:28:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `cedula` varchar(45) DEFAULT NULL,
  `telefono` varchar(100) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `estado_civil` varchar(45) DEFAULT NULL,
  `fecha_nac` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`id`, `nombre`, `cedula`, `telefono`, `correo`, `direccion`, `estado_civil`, `fecha_nac`, `created_at`, `updated_at`) VALUES
(1, 'ziu macayoº', '25262847', '3737878', 'sjdks@dklf.com', 'ksj ksjkf qwjkq', NULL, '2017-07-26', '2017-07-19 01:03:58', '2017-07-19 01:03:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `planificaciones`
--

CREATE TABLE `planificaciones` (
  `id` int(11) NOT NULL,
  `f_inicio` date DEFAULT NULL,
  `f_final` date DEFAULT NULL,
  `como` text,
  `que` text,
  `objetivo` text,
  `proposito` text,
  `club_id` int(11) DEFAULT NULL,
  `actividad_id` int(11) DEFAULT NULL,
  `persona_id` int(11) DEFAULT NULL,
  `iglesia_id` int(11) DEFAULT NULL,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `planificaciones`
--

INSERT INTO `planificaciones` (`id`, `f_inicio`, `f_final`, `como`, `que`, `objetivo`, `proposito`, `club_id`, `actividad_id`, `persona_id`, `iglesia_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '2017-05-19', '2017-05-16', 'iuisfie fireuier', 'ewiei efieuifiuei i i', 'iudiew rieuriuwei', 'iweiuirueir ruiei', NULL, NULL, 10, 1, 6, '2017-05-15 03:37:43', '2017-05-15 03:37:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plan_sostenimiento`
--

CREATE TABLE `plan_sostenimiento` (
  `id` int(11) NOT NULL,
  `mes` varchar(50) DEFAULT NULL,
  `general` text,
  `mes_ant` decimal(7,2) DEFAULT NULL,
  `inv` decimal(7,2) DEFAULT NULL,
  `inv_rec` decimal(7,2) DEFAULT NULL,
  `ganancia` decimal(7,2) DEFAULT NULL,
  `total_fondo` int(11) DEFAULT NULL,
  `club_id` int(11) DEFAULT NULL,
  `actividad_id` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `plan_sostenimiento`
--

INSERT INTO `plan_sostenimiento` (`id`, `mes`, `general`, `mes_ant`, `inv`, `inv_rec`, `ganancia`, `total_fondo`, `club_id`, `actividad_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'febrero', 'fkdfjd dkfkf dfkf', '89989.00', '9898.00', '89889.00', '999.00', 98989, 1, 20, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puntuaciones`
--

CREATE TABLE `puntuaciones` (
  `id` int(11) NOT NULL,
  `club_id` int(11) DEFAULT NULL,
  `circular_id` int(11) DEFAULT NULL,
  `actividad_id` int(11) DEFAULT NULL,
  `puntos` int(11) DEFAULT NULL,
  `mes` varchar(15) DEFAULT NULL,
  `iglesia_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `puntuaciones`
--

INSERT INTO `puntuaciones` (`id`, `club_id`, `circular_id`, `actividad_id`, `puntos`, `mes`, `iglesia_id`, `created_at`, `updated_at`) VALUES
(6, 1, NULL, 20, 80, NULL, NULL, '2017-06-22 20:20:55', '2017-06-22 20:20:55'),
(7, 1, NULL, 23, 75, NULL, NULL, '2017-06-22 20:20:55', '2017-06-22 20:20:55'),
(8, 2, NULL, 20, 80, NULL, NULL, '2017-06-22 20:23:29', '2017-06-22 20:23:29'),
(9, 2, NULL, 22, 75, NULL, NULL, '2017-06-22 20:23:29', '2017-06-22 20:23:29'),
(11, 1, 1, 22, 38, 'Enero', NULL, '2017-07-25 02:56:08', '2017-07-25 02:56:08'),
(12, 1, 1, 28, 50, 'Enero', NULL, '2017-07-25 02:57:51', '2017-07-25 02:57:51'),
(13, 2, 1, 23, 400, 'Enero', NULL, '2017-07-25 03:16:10', '2017-07-25 03:16:10'),
(14, 2, 1, 28, 50, 'Enero', NULL, '2017-07-25 03:16:30', '2017-07-25 03:16:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reglamentos`
--

CREATE TABLE `reglamentos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `introduccion` text,
  `reglamento` text,
  `categoria_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `reglamentos`
--

INSERT INTO `reglamentos` (`id`, `titulo`, `introduccion`, `reglamento`, `categoria_id`, `created_at`, `updated_at`) VALUES
(1, 'Reglamento para Directivos Club de Conquistadores/Guías Mayores', 'Este reglamento es un instrumento de compromiso, mediante el cual el club se sujeta a cumplir de manera voluntaria con las actividades estipulada por el Ministerio Juvenil del campo local. Este instrumento le garantiza una mejor optimización en las actividades del club. Siempre y cuando cumpla con las exigencias de este reglamento. No pretendemos que sea gravoso, antes bien que haya un compromiso genuino con el ministerio Juvenil.', '1. La obediencia es y será siempre la primera ley del cielo y del club.\n2. La puntualidad debe ser y es nuestra insignia. Los directivos deben estar 15 minutos antes de la hora pautada para la reunión.\n3. Los departamentales de Campos Conferencia General, División, Unión Campo Local, Zonas y Distritos son la máxima autoridad y merece respeto y consideración.\n4. No se permitirá bajo ninguna circunstancia en las reuniones oficiales el uso por parte de las damas, de mini faldas, pantalones muy cortos y ajustados al cuerpo (entre ellos licras). Y los caballeros pantalones cortos (short) o franelillas.\n5. Los uniformes serán según el modelo y bajo los lineamientos del reglamento del ministerio juvenil de la DIVISIÓN INTERAMERICANA. Los zapatos deben ser de color negro de vestir. Las insignias y botones reglamentarios son propiedad del ministerio juvenil. Al perder su feligresía el miembro deberá por moral regresar dichos botones e insignias. El uso del uniforme es de carácter obligatorio en toda reunión del club. El uniforme solo se usará en actividades propias del club, por lo tanto no puede ser utilizado para fines personales. Una vez que usted se retire del club debe regresar las insignias.\n6. Es Competencia de la directiva, que en reuniones del club y de la iglesia no se preparen comidas que contengan ningún tipo de carne. La violación de esta regla lo puede excluir de su responsabilidad.\n7. La directiva del club es la responsable del uso de banderas y banderines por parte de las unidades.\n8. Es de carácter obligatorio que existan las unidades dentro de la estructura del club. Estos también formaran un Grupo pequeño de salvación y Servicio. Por tal motivo deben integrar las unidades por sectores.\n9. No estará permitido un lenguaje homosexual, o juegos que induzcan a este tipo de prácticas. Recuerde esto es deshonra para Dios y oprobio para la Iglesia.\n10. Ningún miembro bajo disciplina podrá asistir a los campamentos promovidos por el club. En dicha disciplina no podrá portar el uniforme. Aunque no se le negará la asistencia a las reuniones del club.', 2, '2017-02-16 19:06:11', '2017-02-21 19:17:52'),
(3, 'dfjkgj', 'kjskajdk', 'jkjkjk', 1, '2017-03-14 22:06:29', '2017-03-14 22:06:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rols`
--

CREATE TABLE `rols` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `rols`
--

INSERT INTO `rols` (`id`, `nombre`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Administrador', 0, '2017-02-22 20:10:54', '2017-02-22 20:10:54'),
(2, 'Líder Juvenil', 0, '2017-02-22 20:10:55', '2017-02-22 20:10:55'),
(3, 'Líder de Zona', 0, '2017-02-22 20:10:55', '2017-02-22 20:10:55'),
(4, 'Secretario(a) de Club', 0, '2017-02-22 20:10:55', '2017-02-22 20:10:55'),
(5, 'Secretario(a)', 0, '2017-02-22 20:10:55', '2017-02-22 20:10:55'),
(6, 'Secretario(a) de Sociedad de Jóvenes', 0, '2017-02-22 20:10:55', '2017-02-22 20:10:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos`
--

CREATE TABLE `tipos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipos`
--

INSERT INTO `tipos` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Iglesia', '2017-02-21 20:23:16', '2017-02-21 20:23:16'),
(2, 'Grupo', '2017-02-21 20:23:16', '2017-02-21 20:23:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_a`
--

CREATE TABLE `tipos_a` (
  `id` int(11) NOT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `total_puntuacion` int(11) DEFAULT NULL,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipos_a`
--

INSERT INTO `tipos_a` (`id`, `tipo`, `nombre`, `total_puntuacion`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Fijas', '', 0, 5, '2017-02-17 01:05:26', '2017-02-17 01:05:26'),
(2, 'Generales', '', 0, 5, '2017-02-17 01:05:27', '2017-02-17 01:05:27'),
(3, 'Precamporee', '', 0, 5, '2017-02-17 01:05:28', '2017-02-17 01:05:28'),
(4, 'Bíblicos', '', 2150, 5, '2017-02-17 01:05:28', '2017-02-17 01:05:28'),
(5, 'Destrezas', '', 1250, 5, '2017-02-17 01:05:28', '2017-02-17 01:05:28'),
(6, 'Sociales', '', 500, 5, '2017-02-17 01:05:28', '2017-03-27 20:07:24'),
(7, 'Otros', 'Las olimpiadas del desierto', 2000, 5, '2017-02-17 01:05:29', '2017-06-01 03:52:23'),
(9, 'hey', NULL, NULL, 5, '2017-07-09 19:48:47', '2017-07-09 19:48:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `rol_id` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `nombre`, `email`, `password`, `status`, `rol_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'ziuling macayo', 'zmacayo@gmail.com', '$2y$10$0lG/sYur.dWL6ki6rlUnLuagzXEj4a053qpVCsYoeo2GWbQpAoRUC', 1, 1, 'zQ06yVdIowpiPB2gUe4drsmk0FAtQRMoaVEpWaklPAGmPzTzoWyzGA4agXiw', '2017-02-16 21:04:43', '2017-07-13 23:57:46'),
(2, 'ziu macayo', 'ziuling@hotmail.com', '$2y$10$OaJY38ItMXfEZyQp9j9/mOs0BI8iKmJ31eMCZqxLKdFz8XA9/xgM6', 1, 2, 'ndZMJXcE30aH97MTSNGlzFLxFxkLNBVCSRuGg6CBUYANp71FvGdD6xu2P7S7', '2017-02-16 21:04:44', '2017-07-14 23:32:16'),
(3, 'andreina lopez', 'andre@gmail.com', '$2y$10$B9eCqy4C7sTQ8wuoOIUsfeDhsrGKVHksL1w5d5LzndSSa2Pi08uZG', 1, 3, 'DVIcDBbBHj539N1tBYlvp48bVhAUk48BM0D05FfRc7WKH7OkbFTJz9HEisQ8', '2017-02-16 21:04:44', '2017-05-29 00:05:34'),
(4, 'jose castro', 'jose@hotmail.com', '$2y$10$7sjPYuKL4OUd4RIA8MKxced8Q6gPSj2A75ys94fndwT2GqaxD3MnC', 1, 4, 'lyp8P6f5nTEUbQBOkbbTjno4NRRkWAbxIqYzKDfeQ9qKN4Kav7lJQEKH8cwl', '2017-02-16 21:04:44', '2017-07-26 07:01:07'),
(5, 'pedro lopez', 'pedro@gmail.com', '$2y$10$W/TDu1fx458JAMdoQz3khOkNw.Q2H.hqQc3Cgrn8szaiP5A2cOmWu', 1, 5, 'Ojgt62UWPET7sHdX3I6vzbaPzJuEDRPLxC0Szpbjg6rwzsQvqSIa3vLrpySc', '2017-02-16 21:04:44', '2017-07-25 04:12:39'),
(6, 'luis macayo', 'luis@hotmail.com', '$2y$10$sMnlJR0.3OuvIKx.dUsJquSHR1S7GvYf502eTeyBnW7Vo.2eqiCh.', 1, 6, 'KBqvXhyRofEtnP2RJjYG6ms6Fg479fzWDugXqCndNf0vVimSqOD1E462oqoZ', '2017-02-16 21:04:45', '2017-05-23 00:14:38'),
(7, 'jose luis', 'jose@gmail.com', '$2y$10$QZW5RMouRZfB0cE3bKqWbu/phnOWp2HB8VAu9oGD0l6mU.OhhNVWq', 1, 4, NULL, '2017-04-29 00:52:34', '2017-06-20 21:09:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zonas`
--

CREATE TABLE `zonas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `zonas`
--

INSERT INTO `zonas` (`id`, `nombre`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 'Zona II', 3, '2017-03-17 00:37:15', '2017-03-17 00:37:15');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actividades`
--
ALTER TABLE `actividades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `circular_idx` (`circular_id`),
  ADD KEY `tipo_a_idx` (`tipo_a_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `act_modulo`
--
ALTER TABLE `act_modulo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `modulo_id` (`modulo_id`);

--
-- Indices de la tabla `cargos`
--
ALTER TABLE `cargos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `circulares`
--
ALTER TABLE `circulares`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tipo_de_club_idxd_idx` (`categoria_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `clubes`
--
ALTER TABLE `clubes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iglesia_idx` (`iglesia_id`),
  ADD KEY `categoria_idx` (`categoria_id`),
  ADD KEY `user_idx` (`user_id`);

--
-- Indices de la tabla `directivas`
--
ALTER TABLE `directivas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `persona_id` (`persona_id`),
  ADD KEY `grupo_id` (`grupo_id`),
  ADD KEY `cargo_id` (`cargo_id`),
  ADD KEY `iglesia_id` (`iglesia_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `club_id` (`club_id`);

--
-- Indices de la tabla `distritos`
--
ALTER TABLE `distritos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `zona_idx` (`zona_id`);

--
-- Indices de la tabla `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `club_idg` (`club_id`),
  ADD KEY `fk_grupos_Iglesias1_idx` (`iglesia_id`);

--
-- Indices de la tabla `identidad`
--
ALTER TABLE `identidad`
  ADD PRIMARY KEY (`id`),
  ADD KEY `club_id` (`club_id`);

--
-- Indices de la tabla `iglesias`
--
ALTER TABLE `iglesias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tipo_id` (`tipo_id`),
  ADD KEY `pastor_id` (`distrito_id`),
  ADD KEY `user_idx` (`user_id`);

--
-- Indices de la tabla `informes`
--
ALTER TABLE `informes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_informes_clubes1_idx` (`club_id`),
  ADD KEY `fk_informes_actividades1_idx` (`actividad_id`),
  ADD KEY `fk_informes_iglesias1_idx` (`iglesia_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `modulo_id` (`modulo_id`);

--
-- Indices de la tabla `informe_act`
--
ALTER TABLE `informe_act`
  ADD PRIMARY KEY (`id`),
  ADD KEY `informe_id` (`informe_id`),
  ADD KEY `actmod_id` (`actmod_id`);

--
-- Indices de la tabla `inscripciones`
--
ALTER TABLE `inscripciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_inscripciones_categorias1_idx` (`categoria_id`),
  ADD KEY `fk_inscripciones_clubes1_idx` (`club_id`),
  ADD KEY `fk_inscripciones_iglesias1_idx` (`iglesia_id`);

--
-- Indices de la tabla `investiduras`
--
ALTER TABLE `investiduras`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `lecciones`
--
ALTER TABLE `lecciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `miembros`
--
ALTER TABLE `miembros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `club_ide` (`club_id`),
  ADD KEY `grupo_idq` (`grupo_id`),
  ADD KEY `persona_idr` (`persona_id`),
  ADD KEY `fk_miembros_Iglesias1_idx` (`iglesia_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `miembro_investidura`
--
ALTER TABLE `miembro_investidura`
  ADD PRIMARY KEY (`id`),
  ADD KEY `miembro_idxa` (`miembro_id`),
  ADD KEY `h` (`investidura_id`);

--
-- Indices de la tabla `miembro_lecciones`
--
ALTER TABLE `miembro_lecciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `miembro_idxd` (`miembro_id`),
  ADD KEY `leccion_idxw` (`leccion_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ministerios`
--
ALTER TABLE `ministerios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ministerios_clubes1_idx` (`club_id`),
  ADD KEY `fk_ministerios_actividades1_idx` (`actividad_id`),
  ADD KEY `fk_ministerios_iglesias1_idx` (`iglesia_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `modulos`
--
ALTER TABLE `modulos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pago_club1_idx` (`club_id`),
  ADD KEY `fk_pago_actividad1_idx` (`actividad_id`),
  ADD KEY `fk_pagos_iglesias1_idx` (`iglesia_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `pastores`
--
ALTER TABLE `pastores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pastores_distritos1_idx` (`distrito_id`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cedula_UNIQUE` (`cedula`);

--
-- Indices de la tabla `planificaciones`
--
ALTER TABLE `planificaciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `club_planificacion_idx` (`club_id`),
  ADD KEY `planificacion_actividad_idx` (`actividad_id`),
  ADD KEY `fk_planificaciones_personas1_idx` (`persona_id`),
  ADD KEY `fk_planificaciones_iglesias1_idx` (`iglesia_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `plan_sostenimiento`
--
ALTER TABLE `plan_sostenimiento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `actividad_plan_idx` (`actividad_id`),
  ADD KEY `plan_club_idx` (`club_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `puntuaciones`
--
ALTER TABLE `puntuaciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `club_puntuacion_idx` (`club_id`),
  ADD KEY `actividad_puntuacion_idx` (`actividad_id`),
  ADD KEY `fk_puntuaciones_iglesias1_idx` (`iglesia_id`),
  ADD KEY `circular_id` (`circular_id`);

--
-- Indices de la tabla `reglamentos`
--
ALTER TABLE `reglamentos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_reglamentos_categorias1_idx` (`categoria_id`);

--
-- Indices de la tabla `rols`
--
ALTER TABLE `rols`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipos`
--
ALTER TABLE `tipos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipos_a`
--
ALTER TABLE `tipos_a`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `rol_foreign` (`rol_id`);

--
-- Indices de la tabla `zonas`
--
ALTER TABLE `zonas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lider` (`user_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actividades`
--
ALTER TABLE `actividades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT de la tabla `act_modulo`
--
ALTER TABLE `act_modulo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT de la tabla `cargos`
--
ALTER TABLE `cargos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `circulares`
--
ALTER TABLE `circulares`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `clubes`
--
ALTER TABLE `clubes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `directivas`
--
ALTER TABLE `directivas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `distritos`
--
ALTER TABLE `distritos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `grupos`
--
ALTER TABLE `grupos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `identidad`
--
ALTER TABLE `identidad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `iglesias`
--
ALTER TABLE `iglesias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `informes`
--
ALTER TABLE `informes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `informe_act`
--
ALTER TABLE `informe_act`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `inscripciones`
--
ALTER TABLE `inscripciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT de la tabla `investiduras`
--
ALTER TABLE `investiduras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `lecciones`
--
ALTER TABLE `lecciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `miembros`
--
ALTER TABLE `miembros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `miembro_investidura`
--
ALTER TABLE `miembro_investidura`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `miembro_lecciones`
--
ALTER TABLE `miembro_lecciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `ministerios`
--
ALTER TABLE `ministerios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `modulos`
--
ALTER TABLE `modulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `pastores`
--
ALTER TABLE `pastores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `planificaciones`
--
ALTER TABLE `planificaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `plan_sostenimiento`
--
ALTER TABLE `plan_sostenimiento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `puntuaciones`
--
ALTER TABLE `puntuaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `reglamentos`
--
ALTER TABLE `reglamentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `rols`
--
ALTER TABLE `rols`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `tipos`
--
ALTER TABLE `tipos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tipos_a`
--
ALTER TABLE `tipos_a`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `zonas`
--
ALTER TABLE `zonas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `actividades`
--
ALTER TABLE `actividades`
  ADD CONSTRAINT `circular_relacion` FOREIGN KEY (`circular_id`) REFERENCES `circulares` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tipos_a` FOREIGN KEY (`tipo_a_id`) REFERENCES `tipos_a` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `act_modulo`
--
ALTER TABLE `act_modulo`
  ADD CONSTRAINT `actmod` FOREIGN KEY (`modulo_id`) REFERENCES `modulos` (`id`);

--
-- Filtros para la tabla `circulares`
--
ALTER TABLE `circulares`
  ADD CONSTRAINT `categoria_idxd` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `usuario` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `clubes`
--
ALTER TABLE `clubes`
  ADD CONSTRAINT `categoria` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `iglesia` FOREIGN KEY (`iglesia_id`) REFERENCES `iglesias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `directivas`
--
ALTER TABLE `directivas`
  ADD CONSTRAINT `directiva_cargo` FOREIGN KEY (`cargo_id`) REFERENCES `cargos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `directiva_club` FOREIGN KEY (`club_id`) REFERENCES `clubes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `directiva_grupo` FOREIGN KEY (`grupo_id`) REFERENCES `grupos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `directiva_iglesia` FOREIGN KEY (`iglesia_id`) REFERENCES `iglesias` (`id`) ON DELETE NO ACTION ON UPDATE SET NULL,
  ADD CONSTRAINT `directiva_persona` FOREIGN KEY (`persona_id`) REFERENCES `personas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `directiva_usuario1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `distritos`
--
ALTER TABLE `distritos`
  ADD CONSTRAINT `zona` FOREIGN KEY (`zona_id`) REFERENCES `zonas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `grupos`
--
ALTER TABLE `grupos`
  ADD CONSTRAINT `club_key3` FOREIGN KEY (`club_id`) REFERENCES `clubes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_grupos_Iglesias1` FOREIGN KEY (`iglesia_id`) REFERENCES `iglesias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `identidad`
--
ALTER TABLE `identidad`
  ADD CONSTRAINT `club_idx` FOREIGN KEY (`club_id`) REFERENCES `clubes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `iglesias`
--
ALTER TABLE `iglesias`
  ADD CONSTRAINT `distrito_id` FOREIGN KEY (`distrito_id`) REFERENCES `distritos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tipo_de_iglesia` FOREIGN KEY (`tipo_id`) REFERENCES `tipos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `informes`
--
ALTER TABLE `informes`
  ADD CONSTRAINT `fk_informes_actividades1` FOREIGN KEY (`actividad_id`) REFERENCES `actividades` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_informes_clubes1` FOREIGN KEY (`club_id`) REFERENCES `clubes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_informes_iglesias1` FOREIGN KEY (`iglesia_id`) REFERENCES `iglesias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `informe_usuario` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `modulo_idx` FOREIGN KEY (`modulo_id`) REFERENCES `modulos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `informe_act`
--
ALTER TABLE `informe_act`
  ADD CONSTRAINT `actmod_idx` FOREIGN KEY (`actmod_id`) REFERENCES `act_modulo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `informe_idx` FOREIGN KEY (`informe_id`) REFERENCES `informes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `inscripciones`
--
ALTER TABLE `inscripciones`
  ADD CONSTRAINT `fk_inscripciones_categorias1` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_inscripciones_clubes1` FOREIGN KEY (`club_id`) REFERENCES `clubes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_inscripciones_iglesias1` FOREIGN KEY (`iglesia_id`) REFERENCES `iglesias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `miembros`
--
ALTER TABLE `miembros`
  ADD CONSTRAINT `club_key` FOREIGN KEY (`club_id`) REFERENCES `clubes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_miembros_Iglesias1` FOREIGN KEY (`iglesia_id`) REFERENCES `iglesias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `grupo_keyyg` FOREIGN KEY (`grupo_id`) REFERENCES `grupos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `persona_miembro` FOREIGN KEY (`persona_id`) REFERENCES `personas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `miembro_investidura`
--
ALTER TABLE `miembro_investidura`
  ADD CONSTRAINT `investidura` FOREIGN KEY (`investidura_id`) REFERENCES `investiduras` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `miembro` FOREIGN KEY (`miembro_id`) REFERENCES `miembros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `miembro_lecciones`
--
ALTER TABLE `miembro_lecciones`
  ADD CONSTRAINT `leccion` FOREIGN KEY (`leccion_id`) REFERENCES `lecciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `miembrotg` FOREIGN KEY (`miembro_id`) REFERENCES `miembros` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ministerios`
--
ALTER TABLE `ministerios`
  ADD CONSTRAINT `fk_ministerios_actividades1` FOREIGN KEY (`actividad_id`) REFERENCES `actividades` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ministerios_clubes1` FOREIGN KEY (`club_id`) REFERENCES `clubes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ministerios_iglesias1` FOREIGN KEY (`iglesia_id`) REFERENCES `iglesias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ministerios_users1` FOREIGN KEY (`user_id`) REFERENCES `tipos_a` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD CONSTRAINT `fk_pago_actividad1` FOREIGN KEY (`actividad_id`) REFERENCES `actividades` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pago_club1` FOREIGN KEY (`club_id`) REFERENCES `clubes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pagos_iglesias1` FOREIGN KEY (`iglesia_id`) REFERENCES `iglesias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pago_usuario` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pastores`
--
ALTER TABLE `pastores`
  ADD CONSTRAINT `fk_pastores_distritos1` FOREIGN KEY (`distrito_id`) REFERENCES `distritos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `planificaciones`
--
ALTER TABLE `planificaciones`
  ADD CONSTRAINT `club_planificacion` FOREIGN KEY (`club_id`) REFERENCES `clubes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_planificaciones_iglesias1` FOREIGN KEY (`iglesia_id`) REFERENCES `iglesias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_planificaciones_personas1` FOREIGN KEY (`persona_id`) REFERENCES `personas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `planificacion_actividad` FOREIGN KEY (`actividad_id`) REFERENCES `actividades` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `planificacion_usuario` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `plan_sostenimiento`
--
ALTER TABLE `plan_sostenimiento`
  ADD CONSTRAINT `actividad_plan` FOREIGN KEY (`actividad_id`) REFERENCES `actividades` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `plan_club` FOREIGN KEY (`club_id`) REFERENCES `clubes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `plan_usuario` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `puntuaciones`
--
ALTER TABLE `puntuaciones`
  ADD CONSTRAINT `actividad_puntuacion` FOREIGN KEY (`actividad_id`) REFERENCES `actividades` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `circular_idx` FOREIGN KEY (`circular_id`) REFERENCES `circulares` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `club_puntuacion` FOREIGN KEY (`club_id`) REFERENCES `clubes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_puntuaciones_iglesias1` FOREIGN KEY (`iglesia_id`) REFERENCES `iglesias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `reglamentos`
--
ALTER TABLE `reglamentos`
  ADD CONSTRAINT `fk_reglamentos_categorias1` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tipos_a`
--
ALTER TABLE `tipos_a`
  ADD CONSTRAINT `tipo_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `rol_foreign` FOREIGN KEY (`rol_id`) REFERENCES `rols` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `zonas`
--
ALTER TABLE `zonas`
  ADD CONSTRAINT `user_lider` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
