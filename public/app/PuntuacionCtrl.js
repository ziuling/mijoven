(function() {
  angular
    .module('MiJoven')
    .controller('PuntuacionCtrl', PuntuacionCtrl)
    .controller('CreatePuntuacionModalCtrl', CreatePuntuacionModalCtrl);


  function PuntuacionCtrl($scope, $uibModal, Puntuacion, Club, Iglesia, Circular, Puntuacion, Categoria, Actividad, Puntuacion, Detalle) {
    $scope.getPuntuaciones = getPuntuaciones;
    $scope.getDetalles = getDetalles;
    $scope.categorias = Categoria.query();
    $scope.circulares = Circular.query();
    $scope.actividades = Actividad.query();
    $scope.clubes = Club.query();
    $scope.iglesias = Iglesia.query();
   // $scope.puntuaciones = Puntuacion.query();
    //$scope.detalles = Detalle.query();
    $scope.option = 1;
    $scope.m = null;
    $scope.changeOption = changeOption;
    $scope.create = create;
    $scope.crear = crear;
    $scope.isLoading = false;
    $scope.puntuaciones = [];
    $scope.getPuntuaciones();
    $scope.getDetalles();

    $scope.meses = [ { name: 'Enero', numero: '01'},
    {name: 'Febrero', numero: '02' },
    {name: 'Marzo', numero: '03' },
    {name: 'Abril', numero: '04' },
    {name: 'Mayo', numero: '05'},
    {name: 'Junio', numero: '06'},
    {name: 'Julio', numero: '07'},
    {name: 'Agosto', numero: '08'},
    {name: 'Septiembre', numero: '09'},
    {name: 'Octubre', numero: '10'},
    {name: 'Noviembre', numero: '11'},
    {name: 'Diciembre', numero: '12'}]


    $scope.cambio = function () {
      $scope.m = $scope.puntuacion.mes;
      $scope.view = true;
      console.log($scope.m)
    };

    function changeOption(categoria) {
      $scope.option = categoria.id;
      console.log($scope.option);
      getPuntuaciones();
      getDetalles();
    }


    $scope.total = function(club){
        var club = club.id;
        var c = [];
        var p = 0;
        for(var i = 0; i < $scope.puntuaciones.length; i++){
          $scope.total[club] = 0;
          //for(var j=0; j<$scope.meses.length; j++){
            if($scope.puntuaciones[i].club_id === club){
              if($scope.puntuaciones[i].mes === 'Enero'){
                c[p] = $scope.puntuaciones[i];
                p = p + 1;
                tamano = c.length;
                var numero0 = Number(c[tamano-1].total || 0);
              }
              if($scope.puntuaciones[i].mes === 'Febrero'){
                c[p] = $scope.puntuaciones[i];
                p = p + 1;
                tamano = c.length;
                var numero1 = Number(c[tamano-1].total || 0);
              }
              if($scope.puntuaciones[i].mes === 'Marzo'){
                c[p] = $scope.puntuaciones[i];
                p = p + 1;
                tamano = c.length;
                var numero2 = Number(c[tamano-1].total || 0);
              }
              if($scope.puntuaciones[i].mes === 'Abril'){
                c[p] = $scope.puntuaciones[i];
                p = p + 1;
                tamano = c.length;
                var numero3 = Number(c[tamano-1].total || 0);
              }
              if($scope.puntuaciones[i].mes === 'Mayo'){
                c[p] = $scope.puntuaciones[i];                
                p = p + 1;
                tamano = c.length;                
                var numero4 = Number(c[tamano-1].total || 0);
              }
              if($scope.puntuaciones[i].mes === 'Junio'){
                c[p] = $scope.puntuaciones[i];
                p = p + 1;
                tamano = c.length;
                var numero5 = Number(c[tamano-1].total || 0);                
              }
              if($scope.puntuaciones[i].mes === 'Julio'){
                c[p] = $scope.puntuaciones[i];
                p = p + 1;
                tamano = c.length;
                var numero6 = Number(c[tamano-1].total || 0);
              }
              if($scope.puntuaciones[i].mes === 'Agosto'){
                c[p] = $scope.puntuaciones[i];
                p = p + 1;
                tamano = c.length;
                var numero7 = Number(c[tamano-1].total || 0);
              }
              if($scope.puntuaciones[i].mes === 'Septiembre'){
                c[p] = $scope.puntuaciones[i];
                p = p + 1;
                tamano = c.length;
                var numero8 = Number(c[tamano-1].total || 0);
              }
              if($scope.puntuaciones[i].mes === 'Octubre'){
                c[p] = $scope.puntuaciones[i];
                p = p + 1;
                tamano = c.length;
                var numero9 = Number(c[tamano-1].total || 0);
              }
              if($scope.puntuaciones[i].mes === 'Noviembre'){
                c[p] = $scope.puntuaciones[i];
                p = p + 1;
                tamano = c.length;
                var numero10 = Number(c[tamano-1].total || 0);
              }
              if($scope.puntuaciones[i].mes === 'Diciembre'){
                c[p] = $scope.puntuaciones[i];
                p = p + 1;
                tamano = c.length;
                var numero11 = Number(c[tamano-1].total || 0);
              }
            }

          //}
          var numero = 0;
          if(numero0 == undefined){
            numero0=0;
          }
          if(numero1 == undefined){
            numero1=0;
          }
          if(numero2 == undefined){
            numero2=0;
          }
          if(numero3 == undefined){
            numero3=0;
          }
          if(numero4 == undefined){
            numero4=0;
          }
          if(numero5 == undefined){
            numero5=0;
          }
          if(numero6 == undefined){
            numero6=0;
          }
          if(numero7 == undefined){
            numero7=0;
          }
          if(numero8 == undefined){
            numero8=0;
          }
          if(numero9 == undefined){
            numero9=0;
          }
          if(numero10 == undefined){
            numero10=0;
          }
          if(numero11 == undefined){
            numero11=0;
          }
          numero = numero0 + numero1 + numero2 + numero3 + numero4 + numero5 + numero6 + numero7 + numero8 + numero9 + numero10 + numero11;
          //console.log(numero);
          $scope.total[club] = numero;
        }
        
    }
    $scope.totali = function(iglesia){
        var iglesia = iglesia.id;
        var c = [];
        var p = 0;
        for(var i = 0; i < $scope.puntuaciones.length; i++){
          $scope.totali[iglesia] = 0;
          //for(var j=0; j<$scope.meses.length; j++){
            if($scope.puntuaciones[i].iglesia_id === iglesia){
              if($scope.puntuaciones[i].mes === 'Enero'){
                c[p] = $scope.puntuaciones[i];
                p = p + 1;
                tamano = c.length;
                var numero0 = Number(c[tamano-1].total || 0);
              }
              if($scope.puntuaciones[i].mes === 'Febrero'){
                c[p] = $scope.puntuaciones[i];
                p = p + 1;
                tamano = c.length;
                var numero1 = Number(c[tamano-1].total || 0);
              }
              if($scope.puntuaciones[i].mes === 'Marzo'){
                c[p] = $scope.puntuaciones[i];
                p = p + 1;
                tamano = c.length;
                var numero2 = Number(c[tamano-1].total || 0);
              }
              if($scope.puntuaciones[i].mes === 'Abril'){
                c[p] = $scope.puntuaciones[i];
                p = p + 1;
                tamano = c.length;
                var numero3 = Number(c[tamano-1].total || 0);
              }
              if($scope.puntuaciones[i].mes === 'Mayo'){
                c[p] = $scope.puntuaciones[i];                
                p = p + 1;
                tamano = c.length;                
                var numero4 = Number(c[tamano-1].total || 0);
              }
              if($scope.puntuaciones[i].mes === 'Junio'){
                c[p] = $scope.puntuaciones[i];
                p = p + 1;
                tamano = c.length;
                var numero5 = Number(c[tamano-1].total || 0);                
              }
              if($scope.puntuaciones[i].mes === 'Julio'){
                c[p] = $scope.puntuaciones[i];
                p = p + 1;
                tamano = c.length;
                var numero6 = Number(c[tamano-1].total || 0);
              }
              if($scope.puntuaciones[i].mes === 'Agosto'){
                c[p] = $scope.puntuaciones[i];
                p = p + 1;
                tamano = c.length;
                var numero7 = Number(c[tamano-1].total || 0);
              }
              if($scope.puntuaciones[i].mes === 'Septiembre'){
                c[p] = $scope.puntuaciones[i];
                p = p + 1;
                tamano = c.length;
                var numero8 = Number(c[tamano-1].total || 0);
              }
              if($scope.puntuaciones[i].mes === 'Octubre'){
                c[p] = $scope.puntuaciones[i];
                p = p + 1;
                tamano = c.length;
                var numero9 = Number(c[tamano-1].total || 0);
              }
              if($scope.puntuaciones[i].mes === 'Noviembre'){
                c[p] = $scope.puntuaciones[i];
                p = p + 1;
                tamano = c.length;
                var numero10 = Number(c[tamano-1].total || 0);
              }
              if($scope.puntuaciones[i].mes === 'Diciembre'){
                c[p] = $scope.puntuaciones[i];
                p = p + 1;
                tamano = c.length;
                var numero11 = Number(c[tamano-1].total || 0);
              }
            }
          //}
          var numero = 0;
          if(numero0 == undefined){
            numero0=0;
          }
          if(numero1 == undefined){
            numero1=0;
          }
          if(numero2 == undefined){
            numero2=0;
          }
          if(numero3 == undefined){
            numero3=0;
          }
          if(numero4 == undefined){
            numero4=0;
          }
          if(numero5 == undefined){
            numero5=0;
          }
          if(numero6 == undefined){
            numero6=0;
          }
          if(numero7 == undefined){
            numero7=0;
          }
          if(numero8 == undefined){
            numero8=0;
          }
          if(numero9 == undefined){
            numero9=0;
          }
          if(numero10 == undefined){
            numero10=0;
          }
          if(numero11 == undefined){
            numero11=0;
          }
          numero = numero0 + numero1 + numero2 + numero3 + numero4 + numero5 + numero6 + numero7 + numero8 + numero9 + numero10 + numero11;
          //console.log(numero);
          $scope.totali[iglesia] = numero;
        }
        
    }
   
    function create(puntuacion) {
      var modalInstance = $uibModal.open({
        templateUrl: 'PuntuacionModal.html',
        controller: 'CreatePuntuacionModalCtrl',
        size: 'md',
        resolve: {
           puntuacion : puntuacion,
           categoria_id : angular.copy($scope.option)
          }
      });

      modalInstance.result.then(function(puntuacion) {
        
        $scope.getPuntuaciones();
        swal(
          'Asignado!',
          'Ha sido asignado con éxito.',
          'success'
        );
        $scope.getDetalles();

      });
    }
    
    function crear(puntuacion) {

      var modalInstance = $uibModal.open({
          templateUrl: 'GeneralModal.html',
          controller: 'CreatePuntuacionModalCtrl',
          size: 'lg',
          resolve: {
            puntuacion: puntuacion,
           categoria_id : angular.copy($scope.option)
          }
      });

      modalInstance.result.then(function(puntuacion) {
        
        swal(
          'Asignado!',
          'Ha sido éxitoso',
          'success'
        );
        $scope.getPuntuaciones();
        $scope.getDetalles();
      });
    }
    
    function getPuntuaciones(){
      $scope.isLoading = true;
      $scope.puntuaciones = Puntuacion.query({id: $scope.option}, function(){
        $scope.puntuaciones.map(function(data){
          if(data.status === 1){
            data.stat = 'Activo';
            data.label = 'label-success';
          }else if(data.status === 0){
            data.stat = 'Inactivo';
            data.label = 'label-danger';
          }
          return data;
        })
      });
      $scope.isLoading=false;
    }

    function getDetalles(){
      $scope.isLoading = true;
      $scope.detalles = Detalle.query(function(){
        $scope.detalles.map(function(data){
          if(data.status === 1){
            data.stat = 'Activo';
            data.label = 'label-success';
          }else if(data.status === 0){
            data.stat = 'Inactivo';
            data.label = 'label-danger';
          }
          return data;
        })
      });
     
      $scope.isLoading=false;
    }
    
  }

  function CreatePuntuacionModalCtrl($scope, $uibModalInstance, Iglesia, Pago, User, Puntuacion, Detalle, Planificacion, Informe, Ministerio, Grupo,  Plan_sostenimiento, Puntuacion, categoria_id, Actividad, Club, Circular, Detalle, puntuacion) {
    //console.log(puntuacion.circular);
    $scope.puntuacion = new Puntuacion({circular_id:puntuacion.circular, categoria_id: categoria_id});
    $scope.puntuacion.mes = puntuacion.mes;
    $scope.users = User.query();
    $scope.clubes = Club.query();
    $scope.grupos = Grupo.query();
    $scope.pagos = Pago.query();
    $scope.iglesias = Iglesia.query();
    $scope.actividades = Actividad.query();
    $scope.puntuaciones = Puntuacion.query();
    $scope.detalles = Detalle.query();
    $scope.puntuacion.status = 0;
    $scope.isSend=false;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.puntuacion.totalm = 0 ;
    $scope.puntuacion.totalg = 0 ;
    $scope.puntuacion.total = 0 ;

    $scope.puntuacion.detalle = {
      actividad_id: [],
      puntos:[]
    }
    $scope.detalle = { 
      actividades : [], 
      puntos : []
    };

    $scope.check = function(value, checked) {
      
      var ida = $scope.detalle.actividades.indexOf(value);
      
      if (ida >= 0 && !checked) {
        $scope.detalle.actividades.splice(ida, 1);
      }
      if (ida < 0 && checked) {

        $scope.detalle.actividades.push(value.id);
        $scope.CurrentDate = new Date(); //fecha actual
        if($scope.CurrentDate){
          $scope.dia= $scope.CurrentDate.getDate();
          if($scope.dia <= 10 &&  $scope.dia >= 1){
            $scope.detalle.puntos.push(value.puntuacion);
          }else{
            $scope.detalle.puntos.push(0.5*(value.puntuacion));
          } 
        }
      }

      if(checked == false){
          $scope.detalle = { 
            actividades : [], 
            puntos : []
          };
          swal('Volver a seleccionar cuidadosamente');
          //alert('Volver a seleccionar cuidadosamente');
      }
     
      if(value.tipo_a_id === 1 || value.tipo_a_id === 3){
        var p = 0;
        var club = [];
        
        for(var i=0; i<$scope.puntuaciones.length; i++){
          $scope.puntuacion.totalm = 0;
          if($scope.puntuaciones[i].club_id === $scope.puntuacion.club_id && $scope.puntuaciones[i].mes === $scope.puntuacion.mes){
            club[p] = $scope.puntuaciones[i];
            p = p + 1;
            if(club.length == 1){
              var num = Number($scope.puntuaciones[i].totalm || 0);
              $scope.puntuacion.totalm = $scope.puntuacion.totalm + num;
              console.log($scope.puntuacion.totalm );
            }
            
            if(club.length > 1){
              var j =club.length-1;
              var cont = 0;
                while(cont < club.length){
                  var c= cont + j;
                  if(club[cont+c] == undefined){
                    var num = 0;
                    console.log($scope.puntuacion.totalm );
                  }else{
                    var num = Number(club[cont+c].totalm || 0);
                    $scope.puntuacion.totalm = $scope.puntuacion.totalm + num;
                  }
                  cont = cont + 1;
                }
            }
            
          }
        }

        if($scope.puntuaciones.length == 0){
            $scope.puntuacion.totalm = 0;
            for(var i = 0; i < $scope.detalle.puntos.length; i++){
              var numero = Number($scope.detalle.puntos[i] || 0);
              $scope.puntuacion.totalm = $scope.puntuacion.totalm + numero;
            }
        }else{
          for(var i = 0; i < $scope.detalle.puntos.length; i++){
            var numero = Number($scope.detalle.puntos[i] || 0);
            $scope.puntuacion.totalm = $scope.puntuacion.totalm + numero;
          }
        }
        console.log($scope.puntuacion.totalm );
      }else{
        for(var i=0; i<$scope.puntuaciones.length; i++){
          $scope.puntuacion.totalg = 0;
          if($scope.puntuaciones[i].club_id === $scope.puntuacion.club_id && $scope.puntuaciones[i].mes === $scope.puntuacion.mes){
            club[p] = $scope.puntuaciones[i];
            p = p + 1;
            if(club.length == 1){
              var num = Number($scope.puntuaciones[i].totalg || 0);
              $scope.puntuacion.totalg = $scope.puntuacion.totalg + num;
              console.log($scope.puntuacion.totalg );
            }
            
            if(club.length > 1){
              var j =club.length-1;
              var cont = 0;
                while(cont < club.length){
                  var c= cont + j;
                  if(club[cont+c] == undefined){
                    var num = 0;
                  }else{
                    var num = Number(club[cont+c].totalg || 0);
                    $scope.puntuacion.totalg = $scope.puntuacion.totalg + num;
                  }
                  cont = cont + 1;
                }
            }
            
          }
        }

        if($scope.puntuaciones.length == 0){
            $scope.puntuacion.totalg = 0;
            for(var i = 0; i < $scope.detalle.puntos.length; i++){
              var numero = Number($scope.detalle.puntos[i] || 0);
              $scope.puntuacion.totalg = $scope.puntuacion.totalg + numero;
            }
        }else{
          for(var i = 0; i < $scope.detalle.puntos.length; i++){
            var numero = Number($scope.detalle.puntos[i] || 0);
            $scope.puntuacion.totalg = $scope.puntuacion.totalg + numero;
          }
        }
      }  

      $scope.puntuacion.detalle.actividad_id = $scope.detalle.actividades;
      $scope.puntuacion.detalle.puntos = $scope.detalle.puntos;
      $scope.puntuacion.total = $scope.puntuacion.totalm + $scope.puntuacion.totalg;
    }
    
    function save(form) {
      $scope.puntuacion.detalle = JSON.stringify($scope.puntuacion.detalle);
      console.log($scope.puntuacion.detalle);
      
      if($scope.puntuacion.club_id){
        var cont=0;
        for( var i=0; i<$scope.puntuaciones.length; i++){
          for(var j=0; j<$scope.detalles.length; j++){
             if($scope.puntuaciones[i].id == $scope.detalles[j].puntuacion_id){
              var mes= $scope.puntuaciones[i].mes;
              var club= $scope.puntuaciones[i].club_id;
              if(mes == $scope.puntuacion.mes && club == $scope.puntuacion.club_id){
                for(var k=0; k<$scope.detalle.actividades.length; k++){
                  if($scope.detalle.actividades[k] == $scope.detalles[j].actividad_id){
                    cont = cont + 1;
                  }
                }
              }
              
             }
          }
        }
        if(cont != 0){
          swal('No se puede registrar la misma actividad en el mes');
          $scope.puntuacion.detalle = { 
            actividades : [], 
            puntos : []
          };
          console.log($scope.puntuacion.detalle);
        }else{
          if(form.$valid){
            console.log($scope.puntuacion.detalle);
            $scope.isSend=true;
            $scope.puntuacion.$save().then(function(puntuacion) {
              $uibModalInstance.close(puntuacion);
            });
          }
        }
      }else if($scope.puntuacion.iglesia_id){
        var cont=0;
        for( var i=0; i<$scope.puntuaciones.length; i++){
          for(var j=0; j<$scope.detalles.length; j++){
             if($scope.puntuaciones[i].id == $scope.detalles[j].puntuacion_id){
              var mes= $scope.puntuaciones[i].mes;
              var club= $scope.puntuaciones[i].iglesia_id;
              if(mes == $scope.puntuacion.mes && club == $scope.puntuacion.iglesia_id){
                for(var k=0; k<$scope.detalle.actividades.length; k++){
                  if($scope.detalle.actividades[k] == $scope.detalles[j].actividad_id){
                    cont = cont + 1;
                  }
                }
              }
              
             }
          }
        }
        if(cont != 0){
          swal('No se puede registrar la misma actividad en el mes');
          $scope.puntuacion.detalle = { 
            actividades : [], 
            puntos : []
          };
          console.log($scope.puntuacion.detalle);
        }else{
          if(form.$valid){
            $scope.isSend=true;
            $scope.puntuacion.$save().then(function(puntuacion) {
              $uibModalInstance.close(puntuacion);
            });
          }
        }
      }  
    }

    function cancel() {
      $scope.detalle = { 
        actividades : [], 
        puntos : []
      };
      $uibModalInstance.dismiss('cancel');
    }
  }

  
})();

