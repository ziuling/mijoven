/*(function() {
  angular
    .module('MiJoven')
    .controller('DetalleCtrl', DetalleCtrl)
    .controller('CreateDetalleModalCtrl', CreateDetalleModalCtrl)
    .controller('EditDetalleModalCtrl', EditDetalleModalCtrl);

  function DetalleCtrl($scope, $uibModal, Detalle, Circular, Categoria, Actividad, Puntuacion) {
    $scope.getDetalles = getDetalles;
    $scope.circulares = Circular.query();
    $scope.actividades = Actividad.query();
    $scope.categorias = Categoria.query();
    console.log($scope.actividades)
    $scope.option = 1;
    $scope.m = null;
    $scope.changeOption = changeOption;
    $scope.create = create;
    $scope.edit = edit;
    $scope.destroy = destroy;
    $scope.isLoading = false;
    $scope.detalles = [];

    $scope.meses = [ { name: 'Enero', numero: '01'},
    {name: 'Febrero', numero: '02' },
    {name: 'Marzo', numero: '03' },
    {name: 'Abril', numero: '04' },
    {name: 'Mayo', numero: '05'},
    {name: 'Junio', numero: '06'},
    {name: 'Julio', numero: '07'},
    {name: 'Agosto', numero: '08'},
    {name: 'Septiembre', numero: '09'},
    {name: 'Octubre', numero: '10'},
    {name: 'Noviembre', numero: '11'},
    {name: 'Diciembre', numero: '12'}]

    $scope.getDetalles();

    $scope.cambio = function () {
      $scope.m = $scope.puntuacion.mes;
      $scope.view = true;
      console.log($scope.m)
    };

    function changeOption(categoria) {
      $scope.option = categoria.id;
      console.log($scope.option);
      getDetalles();
    }
    
    function create() {
      var modalInstance = $uibModal.open({
        templateUrl: 'DetalleModal.html',
        controller: 'CreateDetalleModalCtrl',
        size: 'md',
        resolve: {
            circular_id : angular.copy($scope.c),
            puntuacion: puntuacion,
          }
      });

      modalInstance.result.then(function(detalle) {
        
        $scope.getDetalles();
        swal(
          'Creado!',
          'Ha sido creado con éxito.',
          'success'
        );
      });
    }

    function edit(detalle) {
      var modalInstance = $uibModal.open({
        templateUrl: 'DetalleModal.html',
        controller: 'EditDetalleModalCtrl',
        size: 'md',
        resolve: {
          detalle: detalle
        },
      });

      modalInstance.result.then(function(detalle) {
        swal(
          'Actualizado!',
          'Ha sido actualizado con éxito.',
          'success'
        );
        $scope.getDetalles();
      });
    }


    function destroy(detalle) {
      
      swal({
        title: 'Confirmar borrado',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borrar',
        showLoaderOnConfirm: true,
        preConfirm: function() {
          return new Promise(function(resolve, reject) {
            detalle.$delete().then(function() {
              resolve();
            }, function() {
              reject('Ha ocurrido un error');
            });
          });
        },
        allowOutsideClick: false,
      }).then(function() {
        swal(
          'Borrado!',
          'Ha sido borrado con éxito.',
          'success'
        );
        $scope.getDetalles();
      });
    }
    
    function getDetalles(){
      $scope.isLoading = true;
      $scope.detalles = Detalle.query( function(){
        $scope.detalles.map(function(data){
          if(data.status === 1){
            data.stat = 'Activo';
            data.label = 'label-success';
          }else if(data.status === 0){
            data.stat = 'Inactivo';
            data.label = 'label-danger';
          }
          return data;
        })
      });
      $scope.isLoading=false;
    }
    
  }

  function CreateDetalleModalCtrl($scope, $uibModalInstance, Detalle, Actividad, Club, Circular) {
    $scope.detalle = new Detalle();
    $scope.actividades = Actividad.query();
    $scope.clubes = Club.query();
    $scope.circulares = Circular.query();
    $scope.detalle.status = 0;
    $scope.isSend=false;
    $scope.save = save;
    $scope.cancel = cancel;

    function save(form) {
      if(form.$valid){
        $scope.isSend=true;
        $scope.detalle.$save().then(function(detalle) {
          $uibModalInstance.close(detalle);
        });
      }
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function EditDetalleModalCtrl($scope, $uibModalInstance, detalle) {
    $scope.detalle = detalle;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function save(form) {
      if(form.$valid)
      {
        $scope.isSend=true;
        $scope.detalle._method = 'PUT';
        $scope.detalle.$save().then(function(detalle) {
          $uibModalInstance.close(detalle);
        });
      }
    }
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();*/
