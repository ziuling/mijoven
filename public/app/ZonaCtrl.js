(function() {
  angular
    .module('MiJoven')
    .controller('ZonaCtrl', ZonaCtrl)
    .controller('CreateZonaModalCtrl', CreateZonaModalCtrl)
    .controller('EditZonaModalCtrl', EditZonaModalCtrl);
    

  function ZonaCtrl($scope, $uibModal, Zona) {
    $scope.getZonas= getZonas;
    $scope.create = create;
    $scope.edit = edit;
    $scope.destroy = destroy;
    $scope.isLoading = false;
    $scope.zonas = [];
  
    $scope.getZonas();
    
    function create() {
      var modalInstance = $uibModal.open({
        templateUrl: 'ZonaModal.html',
        controller: 'CreateZonaModalCtrl',
        size: 'md',
      });

      modalInstance.result.then(function(zona) {
        
        swal(
          'Creado!',
          'Ha sido creado con éxito.',
          'success'
        );
        $scope.getZonas();
      });
    }

    function edit(zona) {
      var modalInstance = $uibModal.open({
        templateUrl: 'ZonaModal.html',
        controller: 'EditZonaModalCtrl',
        size: 'md',
        resolve: {
          zona: zona
        },
      });

      modalInstance.result.then(function(zona) {
        swal(
          'Actualizado!',
          'Ha sido actualizado con éxito.',
          'success'
        );
        $scope.getZonas();
      });
    }

    function destroy(zona) {
      
      swal({
        title: 'Confirmar borrado',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borrar',
        showLoaderOnConfirm: true,
        preConfirm: function() {
          return new Promise(function(resolve, reject) {
            zona.$delete().then(function() {
              resolve();
            }, function() {
              reject('Ha ocurrido un error');
            });
          });
        },
        allowOutsideClick: false,
      }).then(function() {
        swal(
          'Borrado!',
          'Ha sido borrado con éxito.',
          'success'
        );
        $scope.getZonas();
      });
    }
    
    function getZonas(){
      $scope.isLoading = true;
      $scope.zonas = Zona.query(function(){
        $scope.zonas.map(function(data){
          if(data.status === 1){
            data.stat = 'Activo';
            data.label = 'label-success';
          }else if(data.status === 0){
            data.stat = 'Inactivo';
            data.label = 'label-danger';
          }
          return data;
        })
      });
      $scope.isLoading=false;
    }
    
  }

  function CreateZonaModalCtrl($scope, $uibModalInstance, Zona, User) {
    $scope.zona = new Zona();
    $scope.users = User.query();
    $scope.zona.status = 0;
    $scope.isSend=false;
    $scope.save = save;
    $scope.cancel = cancel;

    function save(form) {
      if(form.$valid){
        $scope.isSend=true;
        $scope.zona.$save().then(function(zona) {
          $uibModalInstance.close(zona);
        });
      }
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function EditZonaModalCtrl($scope, $uibModalInstance, zona, User) {
    $scope.users = User.query();
    $scope.zona = zona;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function save(form) {
      if(form.$valid)
      {
        $scope.isSend=true;
        $scope.zona._method = 'PUT';
        $scope.zona.$save().then(function(zona) {
          $uibModalInstance.close(zona);
        });
      }
    }
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();
