(function() {
  angular
    .module('MiJoven')
    .controller('ReglamentoCtrl', ReglamentoCtrl)
    .controller('CreateReglamentoModalCtrl', CreateReglamentoModalCtrl)
    .controller('EditReglamentoModalCtrl', EditReglamentoModalCtrl);
    

  function ReglamentoCtrl($scope, $uibModal, Reglamento) {
    $scope.getReglamentos= getReglamentos;
    $scope.create = create;
    $scope.edit = edit;
    $scope.destroy = destroy;
    $scope.isLoading = false;
    $scope.reglamentos = [];

  
    $scope.getReglamentos();
    
    function create() {
      var modalInstance = $uibModal.open({
        templateUrl: 'ReglamentoModal.html',
        controller: 'CreateReglamentoModalCtrl',
        size: 'lg',
      });

      modalInstance.result.then(function(reglamento) {
        
        swal(
          'Creado!',
          'Ha sido creado con éxito.',
          'success'
        );
        $scope.getReglamentos();
      });
    }

    function edit(reglamento) {
      var modalInstance = $uibModal.open({
        templateUrl: 'ReglamentoModal.html',
        controller: 'EditReglamentoModalCtrl',
        size: 'lg',
        resolve: {
          reglamento: reglamento
        },
      });

      modalInstance.result.then(function(reglamento) {
        swal(
          'Actualizado!',
          'Ha sido actualizado con éxito.',
          'success'
        );
        $scope.getReglamentos();
      });
    }

    function destroy(reglamento) {
      
      swal({
        title: 'Confirmar borrado',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borrar',
        showLoaderOnConfirm: true,
        preConfirm: function() {
          return new Promise(function(resolve, reject) {
            reglamento.$delete().then(function() {
              resolve();
            }, function() {
              reject('Ha ocurrido un error');
            });
          });
        },
        allowOutsideClick: false,
      }).then(function() {
        swal(
          'Borrado!',
          'Ha sido borrado con éxito.',
          'success'
        );
        $scope.getReglamentos();
      });
    }
    
    function getReglamentos(){
      $scope.isLoading = true;
      $scope.reglamentos = Reglamento.query(function(){
        $scope.reglamentos.map(function(data){
          if(data.status === 1){
            data.stat = 'Activo';
            data.label = 'label-success';
          }else if(data.status === 0){
            data.stat = 'Inactivo';
            data.label = 'label-danger';
          }
          return data;
        })
      });
      $scope.isLoading=false;
    }
    
  }

  function CreateReglamentoModalCtrl($scope, $uibModalInstance, Reglamento, Categoria) {
    $scope.categorias = Categoria.query();
    $scope.reglamento = new Reglamento();
    $scope.reglamento.status = 0;
    $scope.isSend=false;
    $scope.save = save;
    $scope.cancel = cancel;

    function save(form) {
      if(form.$valid){
        $scope.isSend=true;
        $scope.reglamento.$save().then(function(reglamento) {
          $uibModalInstance.close(reglamento);
        });
      }
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function EditReglamentoModalCtrl($scope, $uibModalInstance, reglamento, Categoria) {
    $scope.categorias = Categoria.query();
    $scope.reglamento = reglamento;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function save(form) {
      if(form.$valid)
      {
        $scope.isSend=true;
        $scope.reglamento._method = 'PUT';
        $scope.reglamento.$save().then(function(reglamento) {
          $uibModalInstance.close(reglamento);
        });
      }
    }
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();