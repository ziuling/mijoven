(function() {
  angular
    .module('MiJoven')
    .controller('InformeCtrl', InformeCtrl)
    .controller('CreateInformeModalCtrl', CreateInformeModalCtrl)
    .controller('DetalleInformeModalCtrl', DetalleInformeModalCtrl)
    .controller('HistorialInformeModalCtrl', HistorialInformeModalCtrl);
    
  function InformeCtrl($scope, $uibModal, Informe, Modulo) {
    $scope.getInformes= getInformes;
    $scope.modulos = Modulo.query();
    $scope.view = true;
    $scope.option = 1;
    $scope.create = create;
    $scope.detalle = detalle;
    $scope.historial = historial;
    $scope.destroy = destroy;
    $scope.isLoading = false;
    $scope.changeOption  = changeOption;

    $scope.getInformes();
    
    function create() {
      var modalInstance = $uibModal.open({
        templateUrl: 'InformeModal.html',
        controller: 'CreateInformeModalCtrl',
        size: 'md',
        resolve: {
        modulo_id : angular.copy($scope.option)
        }
      });

      modalInstance.result.then(function(informe) {
        
        $scope.getInformes();
        swal(
          'Creado!',
          'Ha sido creado con éxito.',
          'success'
        );
      });
    }

    function detalle(informe) {
        var modalInstance = $uibModal.open({
              templateUrl: 'detallesInformeModal.html',
              controller: 'DetalleInformeModalCtrl',
              size: 'lg',
              resolve: {
                informe: informe

              }
      });
    }
    function historial() {

        var modalInstance = $uibModal.open({
              templateUrl: 'historialInformeModal.html',
              controller: 'HistorialInformeModalCtrl',
              size: 'lg',
              
      });

    }
    function destroy(informe) {
      swal({
        title: 'Confirmar borrado',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borrar',
        showLoaderOnConfirm: true,
        preConfirm: function() {
          return new Promise(function(resolve, reject) {
            informe.$delete().then(function() {
              resolve();
            }, function() {
              reject('Ha ocurrido un error');
            });
          });
        },
        allowOutsideClick: false,
      }).then(function() {
        swal(
          'Borrado!',
          'Ha sido borrado con éxito.',
          'success'
        );
        $scope.getInformes();
      });
    }
    function getInformes(){
      $scope.isLoading = true;
      $scope.informes = Informe.query({id: $scope.option });
      $scope.isLoading=false;
    }
    function changeOption(modulo) {
      $scope.option = modulo.id;
      console.log($scope.option);
      getInformes();
    }
  }
  function CreateInformeModalCtrl($scope, $uibModalInstance, Informe, modulo_id, Act_modulo, Modulo) {
    $scope.informe = new Informe({modulo_id: modulo_id});
    $scope.modulos = Modulo.query();
    $scope.act_modulos = Act_modulo.query();
    $scope.semana = null;
    $scope.informe.status = 0;
    $scope.isSend=false;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.meses = [ { name: 'Enero', numero: '01'},
    {name: 'Febrero', numero: '02' },
    {name: 'Marzo', numero: '03' },
    {name: 'Abril', numero: '04' },
    {name: 'Mayo', numero: '05'},
    {name: 'Junio', numero: '06'},
    {name: 'Julio', numero: '07'},
    {name: 'Agosto', numero: '08'},
    {name: 'Septiembre', numero: '09'},
    {name: 'Octubre', numero: '10'},
    {name: 'Noviembre', numero: '11'},
    {name: 'Diciembre', numero: '12'}]

   /*$scope.semanas = function () {
      if($scope.semana == 4){
        $scope.cuatro = true;
        $scope.cinco = false;
      }
      else{
        $scope.cinco = true;
        $scope.cuatro = true;
      }
    };*/
  
   /*   
    $scope.agregar  = function(){
      $scope.asistencias.push($scope.asistencia);
      console.log($scope.asistencias);
      $scope.asistencia = { };
    }*/

    function save(form) {
      if(form.$valid){
        $scope.isSend=true;
        $scope.informe.$save().then(function(informe) {
          $uibModalInstance.close(informe);
        });
      }
    }
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function DetalleInformeModalCtrl($scope, $uibModalInstance, informe, Modulo, Act_modulo, Informe_act) {
    $scope.informe = informe;
    $scope.modulos = Modulo.query();
    $scope.actividades = Act_modulo.query();
    $scope.informes_act = Informe_act.query();
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function HistorialInformeModalCtrl($scope, $uibModalInstance, Informe) {

    $scope.informes = Informe.query();
    $scope.cancel = cancel;
    $scope.isSend=false;


    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();