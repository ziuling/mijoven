(function() {
  angular
    .module('MiJoven')
    .controller('PastorCtrl', PastorCtrl)
    .controller('CreatePastorModalCtrl', CreatePastorModalCtrl)
    .controller('EditPastorModalCtrl', EditPastorModalCtrl);
    

  function PastorCtrl($scope, $uibModal, Pastor) {
    $scope.getPastores= getPastores;
    $scope.create = create;
    $scope.edit = edit;
    $scope.destroy = destroy;
    $scope.isLoading = false;
    $scope.pastores = [];
  
    $scope.getPastores();
    
    function create() {
      var modalInstance = $uibModal.open({
        templateUrl: 'PastorModal.html',
        controller: 'CreatePastorModalCtrl',
        size: 'md',
      });

      modalInstance.result.then(function(pastor) {
        
        swal(
          'Creado!',
          'Ha sido creado con éxito.',
          'success'
        );
        $scope.getPastores();
        
      });
    }

    function edit(pastor) {
      var modalInstance = $uibModal.open({
        templateUrl: 'PastorModal.html',
        controller: 'EditPastorModalCtrl',
        size: 'md',
        resolve: {
          pastor: pastor
        },
      });

      modalInstance.result.then(function(pastor) {
        swal(
          'Actualizado!',
          'Ha sido actualizado con éxito.',
          'success'
        );
        $scope.getPastores();
      });
    }

    function destroy(pastor) {
      
      swal({
        title: 'Confirmar borrado',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borrar',
        showLoaderOnConfirm: true,
        preConfirm: function() {
          return new Promise(function(resolve, reject) {
            pastor.$delete().then(function() {
              resolve();
            }, function() {
              reject('Ha ocurrido un error');
            });
          });
        },
        allowOutsideClick: false,
      }).then(function() {
        swal(
          'Borrado!',
          'Ha sido borrado con éxito.',
          'success'
        );
        $scope.getPastores();
      });
    }
    
    function getPastores(){
      $scope.isLoading = true;
      $scope.pastores = Pastor.query(function(){
        $scope.pastores.map(function(data){
          if(data.status === 1){
            data.stat = 'Activo';
            data.label = 'label-success';
          }else if(data.status === 0){
            data.stat = 'Inactivo';
            data.label = 'label-danger';
          }
          return data;
        })
      });
      $scope.isLoading=false;
      
    }
    
  }

  function CreatePastorModalCtrl($scope, $uibModalInstance, Pastor, Distrito) {
    $scope.pastor = new Pastor();
    $scope.distritos = Distrito.query();
    $scope.pastor.status = 0;
    $scope.isSend=false;
    $scope.save = save;
    $scope.cancel = cancel;

    function save(form) {
      if(form.$valid){
        $scope.isSend=true;
        $scope.pastor.$save().then(function(pastor) {
          $uibModalInstance.close(pastor);
        });
      }
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function EditPastorModalCtrl($scope, $uibModalInstance, pastor, Distrito) {
    $scope.distritos = Distrito.query();
    $scope.pastor = pastor;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function save(form) {
      if(form.$valid)
      {
        $scope.isSend=true;
        $scope.pastor._method = 'PUT';
        $scope.pastor.$save().then(function(pastor) {
          $uibModalInstance.close(pastor);
        });
      }
    }
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();
