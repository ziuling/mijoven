(function() {
  angular
    .module('MiJoven')
    .controller('IglesiaCtrl', IglesiaCtrl)
    .controller('CreateIglesiaModalCtrl', CreateIglesiaModalCtrl)
    .controller('EditIglesiaModalCtrl', EditIglesiaModalCtrl);
    

  function IglesiaCtrl($scope, $uibModal, Iglesia) {
    $scope.getIglesias= getIglesias;
    $scope.create = create;
    $scope.edit = edit;
    $scope.destroy = destroy;
    $scope.isLoading = false;
    $scope.iglesias = [];
    
    $scope.getIglesias();
    
    function create() {
      var modalInstance = $uibModal.open({
        templateUrl: 'IglesiaModal.html',
        controller: 'CreateIglesiaModalCtrl',
        size: 'md',
      });

      modalInstance.result.then(function(iglesia) {
        
        swal(
          'Creado!',
          'Ha sido creado con éxito.',
          'success'
        );
        $scope.getIglesias();
      });
    }

    function edit(iglesia) {
      var modalInstance = $uibModal.open({
        templateUrl: 'IglesiaModal.html',
        controller: 'EditIglesiaModalCtrl',
        size: 'md',
        resolve: {
          iglesia: iglesia
        },
      });

      modalInstance.result.then(function(iglesia) {
        swal(
          'Actualizado!',
          'Ha sido actualizado con éxito.',
          'success'
        );
        $scope.getIglesias();
      });
    }

    function destroy(iglesia) {
      
      swal({
        title: 'Confirmar borrado',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borrar',
        showLoaderOnConfirm: true,
        preConfirm: function() {
          return new Promise(function(resolve, reject) {
            iglesia.$delete().then(function() {
              resolve();
            }, function() {
              reject('Ha ocurrido un error');
            });
          });
        },
        allowOutsideClick: false,
      }).then(function() {
        swal(
          'Borrado!',
          'Ha sido borrado con éxito.',
          'success'
        );
        $scope.getIglesias();
      });
    }
    
    function getIglesias(){
      $scope.isLoading = true;
      $scope.iglesias = Iglesia.query(function(){
        $scope.iglesias.map(function(data){
          if(data.status === 1){
            data.stat = 'Activo';
            data.label = 'label-success';
          }else if(data.status === 0){
            data.stat = 'Inactivo';
            data.label = 'label-danger';
          }
          return data;
        })
      });
      $scope.isLoading=false;
    }
  }

  function CreateIglesiaModalCtrl($scope, $uibModalInstance, Iglesia, Distrito) {
    $scope.iglesia = new Iglesia();
    $scope.distritos = Distrito.query();
    $scope.iglesia.status = 0;
    $scope.isSend=false;
    $scope.save = save;
    $scope.cancel = cancel;

    function save(form) {
      if(form.$valid){
        $scope.isSend=true;
        $scope.iglesia.$save().then(function(iglesia) {
          $uibModalInstance.close(iglesia);
        });
      }
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function EditIglesiaModalCtrl($scope, $uibModalInstance, iglesia, Distrito) {
    $scope.distritos = Distrito.query();
    $scope.iglesia = iglesia;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function save(form) {
      if(form.$valid)
      {
        $scope.isSend=true;
        $scope.iglesia._method = 'PUT';
        $scope.iglesia.$save().then(function(iglesia) {
          $uibModalInstance.close(iglesia);
        });
      }
    }
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();