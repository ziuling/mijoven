(function() {
  angular
    .module('MiJoven')
    .controller('MiembroCtrl', MiembroCtrl)
    .controller('CreateMiembroModalCtrl', CreateMiembroModalCtrl)
    .controller('EditMiembroModalCtrl', EditMiembroModalCtrl);

  function MiembroCtrl($scope, $uibModal, Miembro) {
    $scope.getMiembros = getMiembros;
    $scope.create = create;
    $scope.createGpss = createGpss;
    $scope.edit = edit;
    $scope.editGpss = editGpss;
    $scope.destroy = destroy;
    $scope.isLoading = false;
    $scope.miembros = [];

    $scope.getMiembros();
    
    function create() {
      var modalInstance = $uibModal.open({
        templateUrl: 'MiembroModal.html',
        controller: 'CreateMiembroModalCtrl',
        size: 'lg',
      });

      modalInstance.result.then(function(miembro) {
        
        $scope.getMiembros();
        swal(
          'Creado!',
          'Ha sido creado con éxito.',
          'success'
        );
      });
    }


    function createGpss() {
      var modalInstance = $uibModal.open({
        templateUrl: 'MiembroGpssModal.html',
        controller: 'CreateMiembroModalCtrl',
        size: 'lg',
      });

      modalInstance.result.then(function(miembro) {
        
        $scope.getMiembros();
        swal(
          'Creado!',
          'Ha sido creado con éxito.',
          'success'
        );
      });
    }

    function edit(miembro) {
      var modalInstance = $uibModal.open({
        templateUrl: 'MiembroModal.html',
        controller: 'EditMiembroModalCtrl',
        size: 'lg',
        resolve: {
          miembro: miembro
        },
      });

      modalInstance.result.then(function(miembro) {
        swal(
          'Actualizado!',
          'Ha sido actualizado con éxito.',
          'success'
        );
        $scope.getMiembros();
      });
    }

    function editGpss(miembro) {
      var modalInstance = $uibModal.open({
        templateUrl: 'MiembroGpssModal.html',
        controller: 'EditMiembroModalCtrl',
        size: 'lg',
        resolve: {
          miembro: miembro
        },
      });

      modalInstance.result.then(function(miembro) {
        swal(
          'Actualizado!',
          'Ha sido actualizado con éxito.',
          'success'
        );
        $scope.getMiembros();
      });
    }

    function destroy(miembro) {
      
      swal({
        title: 'Confirmar borrado',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borrar',
        showLoaderOnConfirm: true,
        preConfirm: function() {
          return new Promise(function(resolve, reject) {
            miembro.$delete().then(function() {
              resolve();
            }, function() {
              reject('Ha ocurrido un error');
            });
          });
        },
        allowOutsideClick: false,
      }).then(function() {
        swal(
          'Borrado!',
          'Ha sido borrado con éxito.',
          'success'
        );
        $scope.getMiembros();
      });
    }
    
    function getMiembros(){
      $scope.isLoading = true;
      $scope.miembros = Miembro.query(function(){
        $scope.miembros.map(function(data){
          if(data.status === 1){
            data.stat = 'Activo';
            data.label = 'label-success';
          }else if(data.status === 0){
            data.stat = 'Inactivo';
            data.label = 'label-danger';
          }
          return data;
        })
      });
      $scope.isLoading=false;
    }
    
  }

  function CreateMiembroModalCtrl($scope, $uibModalInstance, Miembro, Grupo, Leccion, Investidura) {
    $scope.miembro = new Miembro();
    $scope.grupos = Grupo.query();
    $scope.lecciones = Leccion.query();
    $scope.investiduras = Investidura.query();
    $scope.miembro.status = 0;
    $scope.isSend=false;
    $scope.save = save;
    $scope.cancel = cancel;

    function save(form) {
      console.log($scope.miembro)
      console.log($scope.miembro.miembro_investidura)
      
      $scope.miembro.persona = JSON.stringify($scope.miembro.persona);
      console.log($scope.miembro.persona);
      $scope.miembro.miembro_investidura = JSON.stringify($scope.miembro.miembro_investidura);
      console.log($scope.miembro.miembro_investidura);
      if(form.$valid){
        $scope.isSend=true;
        $scope.miembro.$save().then(function(miembro) {
          $uibModalInstance.close(miembro);
        });
      }
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function EditMiembroModalCtrl($scope, $uibModalInstance, miembro, Grupo, Leccion, Investidura) {
    $scope.grupos = Grupo.query();
    $scope.lecciones = Leccion.query();
    $scope.investiduras = Investidura.query();
    $scope.miembro = miembro;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function save(form) {
      if(form.$valid)
      {
        $scope.isSend=true;
        $scope.miembro._method = 'PUT';
        $scope.miembro.$save().then(function(miembro) {
          $uibModalInstance.close(miembro);
        });
      }
    }
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();
