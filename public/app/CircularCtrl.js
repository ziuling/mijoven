(function() {
  angular
    .module('MiJoven')
    .controller('CircularCtrl', CircularCtrl)
    .controller('CreateCircularModalCtrl', CreateCircularModalCtrl)
    .controller('DetalleModalCtrl', DetalleModalCtrl)
    .controller('EditCircularModalCtrl', EditCircularModalCtrl);
    

  
  function CircularCtrl($scope, $uibModal, Circular, Categoria) {
    $scope.getCirculares= getCirculares;
    $scope.categorias = Categoria.query();
    $scope.create = create;
    $scope.edit = edit;
    $scope.detalle = detalle;
    $scope.changeOption  = changeOption;
    $scope.option = 1;
    $scope.destroy = destroy;
    $scope.isLoading = false;
    $scope.circulares = [];
  
    $scope.getCirculares();

    
     
    $scope.enviar=function(circular) {
      $scope.circular = circular;
      $scope.circular.estado = '1';
      $scope.isSend=false;
        $scope.isSend=true;
        $scope.circular._method = 'PUT';
        $scope.circular.$save().then(function(circular) {
          swal(
          'Enviado!',
          'Ha sido enviado con éxito.',
          'success'
        );
        $scope.getCirculares();
        });
      
    }

    function create() {
      var modalInstance = $uibModal.open({
        templateUrl: 'CircularModal.html',
        controller: 'CreateCircularModalCtrl',
        size: 'lg',
        resolve: {
          categoria_id : angular.copy($scope.option)
        }
      });

      modalInstance.result.then(function(circular) {
        
        swal(
          'Creado!',
          'Ha sido creado con éxito.',
          'success'
        );
        $scope.getCirculares();
      });
    }

    function edit(circular) {
      var modalInstance = $uibModal.open({
        templateUrl: 'CircularModal.html',
        controller: 'EditCircularModalCtrl',
        size: 'lg',
        resolve: {
          circular: circular
        },
      });

      modalInstance.result.then(function(circular) {
        swal(
          'Actualizado!',
          'Ha sido actualizado con éxito.',
          'success'
        );
        $scope.getCirculares();
      });
    }

    function detalle(circular) {
      var modalInstance = $uibModal.open({
        templateUrl: 'detallesModal.html',
        controller: 'DetalleModalCtrl',
        size: 'lg',
        resolve: {
          circular: circular
        },
      });

      
    }

    function changeOption(categoria) {
      $scope.option = categoria.id;
      getCirculares();
    }

    function destroy(circular) {
      
      swal({
        title: 'Confirmar borrado',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borrar',
        showLoaderOnConfirm: true,
        preConfirm: function() {
          return new Promise(function(resolve, reject) {
            circular.$delete().then(function() {
              resolve();
            }, function() {
              reject('Ha ocurrido un error');
            });
          });
        },
        allowOutsideClick: false,
      }).then(function() {
        swal(
          'Borrado!',
          'Ha sido borrado con éxito.',
          'success'
        );
        $scope.getCirculares();
      });
    }
    
    function getCirculares(){
      $scope.isLoading = true;
      $scope.circulares = Circular.query( {id: $scope.option }, function(){
        $scope.circulares.map(function(data){
          if(data.status === 1){
            data.stat = 'Activo';
            data.label = 'label-success';
          }else if(data.status === 0){
            data.stat = 'Inactivo';
            data.label = 'label-danger';
          }
          return data;
        })
      });
      $scope.isLoading=false;
    }
    
  }


  function CreateCircularModalCtrl($scope, $uibModalInstance, Circular, categoria_id) {
    $scope.circular = new Circular({categoria_id: categoria_id});
    $scope.circular.estado = '0';
    $scope.circular.status = 0;
    $scope.isSend=false;
    $scope.save = save;
    $scope.cancel = cancel;

    function save(form) {
      if(form.$valid){
        $scope.isSend=true;
        $scope.circular.$save().then(function(circular) {
          $uibModalInstance.close(circular);
        });
      }
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function EditCircularModalCtrl($scope, $uibModalInstance, circular) {
    $scope.circular = circular
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function save(form) {
      if(form.$valid)
      {
        $scope.isSend=true;
        $scope.circular._method = 'PUT';
        $scope.circular.$save().then(function(circular) {
          $uibModalInstance.close(circular);
        });
      }
    }
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function DetalleModalCtrl($scope, $uibModalInstance, circular, Categoria) {
    $scope.categorias = Categoria.query();
    $scope.circular = circular
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();