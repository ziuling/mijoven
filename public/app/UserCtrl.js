(function() {
  angular
    .module('MiJoven')
    .controller('UserCtrl', UserCtrl)
    .factory('Rol', Rol)
    .controller('CreateUserModalCtrl', CreateUserModalCtrl)
    .controller('EditUserModalCtrl', EditUserModalCtrl);
   

  function UserCtrl($scope, $uibModal, User) {
    $scope.getUsers= getUsers;
    $scope.create = create;
    $scope.edit = edit;
    $scope.destroy = destroy;
    $scope.isLoading = false;

    $scope.getUsers();
    
    function create() {
      var modalInstance = $uibModal.open({
        templateUrl: 'UserModal.html',
        controller: 'CreateUserModalCtrl',
        size: 'md',
      });

      modalInstance.result.then(function(user) {
        
        $scope.getUsers();
        swal(
          'Creado!',
          'Ha sido creado con éxito.',
          'success'
        );
      });
    }

    function edit(user) {
      var modalInstance = $uibModal.open({
        templateUrl: 'UserModal.html',
        controller: 'EditUserModalCtrl',
        size: 'md',
        resolve: {
          user: user
        },
      });

      modalInstance.result.then(function(user) {
        swal(
          'Actualizado!',
          'Ha sido actualizado con éxito.',
          'success'
        );
        $scope.getUsers();
      });
    }

    function destroy(user) {
      
      swal({
        title: 'Confirmar borrado',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#20B2AA',
        cancelButtonColor: '#DC143C',
        confirmButtonText: 'Borrar',
        showLoaderOnConfirm: true,
        preConfirm: function() {
          return new Promise(function(resolve, reject) {
            user.$delete().then(function() {
              resolve();
            }, function() {
              reject('Ha ocurrido un error');
            });
          });
        },
        allowOutsideClick: false,
      }).then(function() {
        swal(
          'Borrado!',
          'Ha sido borrado con éxito.',
          'success'
        );
        $scope.getUsers();
      });
    }
    
    function getUsers(){
      $scope.isLoading = true;
      $scope.users = User.query(function(){
        $scope.users.map(function(data){
          if(data.status === 1){
            data.stat = 'Activo';
            data.label = 'label label-success';
          }else if(data.status === 0){
            data.stat = 'Inactivo';
            data.label = 'label label-danger';
          }
          return data;
        })
      });
      $scope.isLoading=false;
    }
    
  }

  function CreateUserModalCtrl($scope, $http, $uibModalInstance, User, Rol) {
    $scope.user = new User();
    $scope.roles = Rol.query();
    $scope.users = User.query();
    $scope.user.status = 0;
    $scope.isSend=false;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.estados = [ {
      'num': '1',
      'nombre': 'Activo'}, {'num': '0', 'nombre': 'Inactivo'} ];
    $scope.em = null;

   
    function save(form) {
        var root = 'http://localhost:8000/usuarios';

        $http.get(root)
          .then(function (response) {
            console.log(response.data);
           $scope.names = response.data.errors;
        });
        if(form.$valid){
          $scope.isSend=true;
          $scope.user.$save().then(function(user) {
            $uibModalInstance.close(user);
          });
        }

    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function EditUserModalCtrl($scope, $uibModalInstance, User, user, Rol) {
    $scope.roles = Rol.query();
    $scope.user = user;
    $scope.users = User.query();
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.isSend=false;
    $scope.estados = [ {
      'num': '1',
      'nombre': 'Activo'}, {'num': '0', 'nombre': 'Inactivo'} ];
    
    function save(form) {
      if(form.$valid){
        $scope.isSend=true;
        $scope.user._method = 'PUT';
        $scope.user.$save().then(function(user) {
          $uibModalInstance.close(user);
        });
      }  
    }
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function Rol($resource) {
    return $resource('/roles');
  }
})();
