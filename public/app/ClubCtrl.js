(function() {
  angular
    .module('MiJoven')
    .controller('ClubCtrl', ClubCtrl)
    .controller('CreateClubModalCtrl', CreateClubModalCtrl)
    .controller('EditClubModalCtrl', EditClubModalCtrl);
    

  function ClubCtrl($scope, $uibModal, Club) {
    $scope.getClubes= getClubes;
    $scope.create = create;
    $scope.edit = edit;
    $scope.destroy = destroy;
    $scope.isLoading = false;
    
    $scope.getClubes();
    
    function create() {
      var modalInstance = $uibModal.open({
        templateUrl: 'ClubModal.html',
        controller: 'CreateClubModalCtrl',
        size: 'md',
      });

      modalInstance.result.then(function(club) {
        
        swal(
          'Creado!',
          'Ha sido creado con éxito.',
          'success'
        );
        $scope.getClubes();

      });
    }

    function edit(club) {
      var modalInstance = $uibModal.open({
        templateUrl: 'ClubModal.html',
        controller: 'EditClubModalCtrl',
        size: 'md',
        resolve: {
          club: club
        },
      });

      modalInstance.result.then(function(club) {
        swal(
          'Actualizado!',
          'Ha sido actualizado con éxito.',
          'success'
        );
        $scope.getClubes();
      });
    }


    
    function destroy(club) {
      
      swal({
        title: 'Confirmar borrado',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#20B2AA',
        cancelButtonColor: '#DC143C',
        confirmButtonText: 'Borrar',
        showLoaderOnConfirm: true,
        preConfirm: function() {
          return new Promise(function(resolve, reject) {
            club.$delete().then(function() {
              resolve();
            }, function() {
              reject('Ha ocurrido un error');
            });
          });
        },
        allowOutsideClick: false,
      }).then(function() {
        swal(
          'Borrado!',
          'Ha sido borrado con éxito.',
          'success'
        );
        $scope.getClubes();
      });
    }
    
    function getClubes(){
      $scope.isLoading = true;
      $scope.clubes = Club.query(function(){
        $scope.clubes.map(function(data){
          if(data.status === 1){
            data.stat = 'Activo';
            data.label = 'label-success';
          }else if(data.status === 0){
            data.stat = 'Inactivo';
            data.label = 'label-danger';
          }
          return data;
        })
      });
      $scope.isLoading=false;
    }
    
  }

  function CreateClubModalCtrl($scope, $uibModalInstance, Club, Categoria, Iglesia, User) {
    $scope.club = new Club();
    $scope.categorias = Categoria.query();
    $scope.iglesias = Iglesia.query();
    $scope.users = User.query();
    $scope.club.status = 0;
    $scope.isSend=false;
    $scope.save = save;
    $scope.cancel = cancel;

    function save(form) {
      if(form.$valid){
        $scope.isSend=true;
        $scope.club.$save().then(function(club) {
          $uibModalInstance.close(club);
        });
      }
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function EditClubModalCtrl($scope, $uibModalInstance, club, Categoria, Iglesia, User) {
    $scope.categorias = Categoria.query();
    $scope.iglesias = Iglesia.query();
    $scope.users = User.query();
    $scope.club = club;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function save(form) {
      if(form.$valid)
      {
        $scope.isSend=true;
        $scope.club._method = 'PUT';
        $scope.club.$save().then(function(club) {
          $uibModalInstance.close(club);
        });
      }
    }
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();
