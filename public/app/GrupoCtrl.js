(function() {
  angular
    .module('MiJoven')
    .controller('GrupoCtrl', GrupoCtrl)
    .controller('CreateGrupoModalCtrl', CreateGrupoModalCtrl)
    .controller('EditGrupoModalCtrl', EditGrupoModalCtrl);
    

  function GrupoCtrl($scope, $uibModal, Grupo) {
    $scope.getGrupos = getGrupos;
    $scope.create = create;
    $scope.createGrupo = createGrupo;
    $scope.edit = edit;
    $scope.editname = editname;
    $scope.destroy = destroy;
    $scope.isLoading = false;
  
    $scope.getGrupos();
    
    function create() {
      var modalInstance = $uibModal.open({
        templateUrl: 'GrupoModal.html',
        controller: 'CreateGrupoModalCtrl',
        size: 'md',
      });

      modalInstance.result.then(function(grupo) {
        
        $scope.getGrupos();
        swal(
          'Creado!',
          'Ha sido creado con éxito.',
          'success'
        );
      });
    }

     function createGrupo() {
      var modalInstance = $uibModal.open({
        templateUrl: 'GruponameModal.html',
        controller: 'CreateGrupoModalCtrl',
        size: 'md',
      });

      modalInstance.result.then(function(grupo) {
        
        $scope.getGrupos();
        swal(
          'Creado!',
          'Ha sido creado con éxito.',
          'success'
        );
      });
    }


    function edit(grupo) {
      var modalInstance = $uibModal.open({
        templateUrl: 'GrupoModal.html',
        controller: 'EditGrupoModalCtrl',
        size: 'md',
        resolve: {
          grupo: grupo
        },
      });

      modalInstance.result.then(function(grupo) {
        swal(
          'Actualizado!',
          'Ha sido actualizado con éxito.',
          'success'
        );
        $scope.getGrupos();
      });
    }

    function editname(grupo) {
      var modalInstance = $uibModal.open({
        templateUrl: 'GruponameModal.html',
        controller: 'EditGrupoModalCtrl',
        size: 'md',
        resolve: {
          grupo: grupo
        },
      });

      modalInstance.result.then(function(grupo) {
        swal(
          'Actualizado!',
          'Ha sido actualizado con éxito.',
          'success'
        );
        $scope.getGrupos();
      });
    }

    function destroy(grupo) {
      
      swal({
        title: 'Confirmar borrado',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borrar',
        showLoaderOnConfirm: true,
        preConfirm: function() {
          return new Promise(function(resolve, reject) {
            grupo.$delete().then(function() {
              resolve();
            }, function() {
              reject('Ha ocurrido un error');
            });
          });
        },
        allowOutsideClick: false,
      }).then(function() {
        swal(
          'Borrado!',
          'Ha sido borrado con éxito.',
          'success'
        );
        $scope.getGrupos();
      });
    }
    
    function getGrupos(){
      $scope.isLoading = true;
      $scope.grupos = Grupo.query(function(){
        $scope.grupos.map(function(data){
          if(data.status === 1){
            data.stat = 'Activo';
            data.label = 'label-success';
          }else if(data.status === 0){
            data.stat = 'Inactivo';
            data.label = 'label-danger';
          }
          return data;
        })
      });
      $scope.isLoading=false;
    }
    
  }

  function CreateGrupoModalCtrl($scope, $uibModalInstance, Grupo) {
    $scope.grupo = new Grupo();
    $scope.grupo.status = 0;
    $scope.isSend=false;
    $scope.save = save;
    $scope.cancel = cancel;

    function save(form) {
      if(form.$valid){
        $scope.isSend=true;
        $scope.grupo.$save().then(function(grupo) {
          $uibModalInstance.close(grupo);
        });
      }
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function EditGrupoModalCtrl($scope, $uibModalInstance, grupo) {
    $scope.grupo = grupo;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function save(form) {
      if(form.$valid)
      {
        $scope.isSend=true;
        $scope.grupo._method = 'PUT';
        $scope.grupo.$save().then(function(grupo) {
          $uibModalInstance.close(grupo);
        });
      }
    }
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();