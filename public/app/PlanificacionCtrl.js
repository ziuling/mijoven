
(function() {
  angular
    .module('MiJoven')
    
    .controller('PlanificacionCtrl', function ($scope, $uibModal, Planificacion) {
    $scope.getPlanificaciones= getPlanificaciones;
    $scope.create = create;
    $scope.edit = edit;
    $scope.destroy = destroy;
    $scope.isLoading = false;
    
    $scope.getPlanificaciones();
    
    function create() {
      var modalInstance = $uibModal.open({
        templateUrl: 'PlanificacionModal.html',
        controller: 'CreatePlanificacionModalCtrl',
        size: 'lg',
      });

      modalInstance.result.then(function(planificacion) {
        
        swal(
          'Creado!',
          'Ha sido creado con éxito.',
          'success'
        );
        $scope.getPlanificaciones();
      });
    }

    function edit(planificacion) {
      var modalInstance = $uibModal.open({
        templateUrl: 'PlanificacionModal.html',
        controller: 'EditPlanificacionModalCtrl',
        size: 'lg',
        resolve: {
          planificacion: planificacion
        },
      });

      modalInstance.result.then(function(planificacion) {
        swal(
          'Actualizado!',
          'Ha sido actualizado con éxito.',
          'success'
        );
        $scope.getPlanificaciones();
      });
    }

    function destroy(planificacion) {
      
      swal({
        title: 'Confirmar borrado',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borrar',
        showLoaderOnConfirm: true,
        preConfirm: function() {
          return new Promise(function(resolve, reject) {
            planificacion.$delete().then(function() {
              resolve();
            }, function() {
              reject('Ha ocurrido un error');
            });
          });
        },
        allowOutsideClick: false,
      }).then(function() {
        swal(
          'Borrado!',
          'Ha sido borrado con éxito.',
          'success'
        );
        $scope.getPlanificaciones();
      });
    }
    
    function getPlanificaciones(){
      $scope.isLoading = true;
      $scope.planificaciones = Planificacion.query(function(){
        $scope.planificaciones.map(function(data){
          
          if(data.status === 1){
            data.stat = 'Activo';
            data.label = 'label-success';
          }else if(data.status === 0){
            data.stat = 'Inactivo';
            data.label = 'label-danger';
          }
          return data;
        })
      });
      $scope.isLoading=false;
    }
  })
  .filter('parseDate', function() {
      return function(input) {
        var result = input;
        var format = /^(\d{2})-(\d{2})-(\d{4})$/;
        var match = format.exec(input);
        if (input && match) {
          result = match[3] + '-' + match[2] + '-' + match[1];
        }
        return result;
      }
  })
  .controller('CreatePlanificacionModalCtrl', CreatePlanificacionModalCtrl)
  .controller('EditPlanificacionModalCtrl', EditPlanificacionModalCtrl);

  
  
  function CreatePlanificacionModalCtrl($scope, $uibModalInstance, Planificacion) {
    $scope.planificacion = new Planificacion();
    $scope.planificacion.status = 0;
    $scope.planificacion.estado = '0';
    $scope.isSend=false;
    $scope.save = save;
    $scope.cancel = cancel;

    function save(form) {
      if(form.$valid){
        $scope.isSend=true;
        $scope.planificacion.$save().then(function(planificacion) {
          $uibModalInstance.close(planificacion);
        });
      }
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function EditPlanificacionModalCtrl($scope, $uibModalInstance, planificacion) {
    $scope.planificacion = planificacion;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function save(form) {
      if(form.$valid)
      {
        $scope.isSend=true;
        $scope.planificacion._method = 'PUT';
        $scope.planificacion.$save().then(function(planificacion) {
          $uibModalInstance.close(planificacion);
        });
      }
    }
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();
