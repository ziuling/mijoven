(function() {
  angular
    .module('MiJoven')
    .controller('DirectivaCtrl', DirectivaCtrl)
    .controller('CreateDirectivaModalCtrl', CreateDirectivaModalCtrl)
    .controller('EditDirectivaModalCtrl', EditDirectivaModalCtrl);

  function DirectivaCtrl($scope, $uibModal, Directiva,Cargo) {
    $scope.getDirectivas = getDirectivas;
    $scope.cargos = Cargo.query();
    $scope.create = create;
    $scope.createGpss = createGpss;
    $scope.edit = edit;
    $scope.editGpss = editGpss;
    $scope.destroy = destroy;
    $scope.isLoading = false;
    $scope.directivas = [];

    $scope.getDirectivas();
    
    function create() {
      var modalInstance = $uibModal.open({
        templateUrl: 'DirectivaModal.html',
        controller: 'CreateDirectivaModalCtrl',
        size: 'md',
      });

      modalInstance.result.then(function(directiva) {
        
        $scope.getDirectivas();
        swal(
          'Creado!',
          'Ha sido creado con éxito.',
          'success'
        );
      });
    }

    function createGpss() {
      var modalInstance = $uibModal.open({
        templateUrl: 'DirectivaGpssModal.html',
        controller: 'CreateDirectivaModalCtrl',
        size: 'md',
      });

      modalInstance.result.then(function(directiva) {
        
        $scope.getDirectivas();
        swal(
          'Creado!',
          'Ha sido creado con éxito.',
          'success'
        );
      });
    }

    function edit(directiva) {
      var modalInstance = $uibModal.open({
        templateUrl: 'DirectivaModal.html',
        controller: 'EditDirectivaModalCtrl',
        size: 'md',
        resolve: {
          directiva: directiva
        },
      });

      modalInstance.result.then(function(directiva) {
        swal(
          'Actualizado!',
          'Ha sido actualizado con éxito.',
          'success'
        );
        $scope.getDirectivas();
      });
    }

    function editGpss(directiva) {
      var modalInstance = $uibModal.open({
        templateUrl: 'DirectivaGpssModal.html',
        controller: 'EditDirectivaModalCtrl',
        size: 'md',
        resolve: {
          directiva: directiva
        },
      });

      modalInstance.result.then(function(directiva) {
        swal(
          'Actualizado!',
          'Ha sido actualizado con éxito.',
          'success'
        );
        $scope.getDirectivas();
      });
    }

    function destroy(directiva) {
      
      swal({
        title: 'Confirmar borrado',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borrar',
        showLoaderOnConfirm: true,
        preConfirm: function() {
          return new Promise(function(resolve, reject) {
            directiva.$delete().then(function() {
              resolve();
            }, function() {
              reject('Ha ocurrido un error');
            });
          });
        },
        allowOutsideClick: false,
      }).then(function() {
        swal(
          'Borrado!',
          'Ha sido borrado con éxito.',
          'success'
        );
        $scope.getDirectivas();
      });
    }
    
    function getDirectivas(){
      $scope.isLoading = true;
      $scope.directivas = Directiva.query(function(){
        $scope.directivas.map(function(data){
          if(data.status === 1){
            data.stat = 'Activo';
            data.label = 'label-success';
          }else if(data.status === 0){
            data.stat = 'Inactivo';
            data.label = 'label-danger';
          }
          return data;
        })
      });
      $scope.isLoading=false;
    }
    
  }

  function CreateDirectivaModalCtrl($scope, $uibModalInstance, Directiva, Cargo, Grupo) {
    $scope.directiva = new Directiva();
    $scope.cargos = Cargo.query();
    $scope.grupos = Grupo.query();
    $scope.directiva.status = 0;
    $scope.isSend=false;
    $scope.save = save;
    $scope.cancel = cancel;

    function save(form) {
      $scope.directiva.persona = JSON.stringify($scope.directiva.persona);
      console.log($scope.directiva.persona);
      if(form.$valid){
        $scope.isSend=true;
        $scope.directiva.$save().then(function(directiva) {
          $uibModalInstance.close(directiva);
        });
      }
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function EditDirectivaModalCtrl($scope, $uibModalInstance, directiva, Cargo, Grupo) {
    $scope.cargos = Cargo.query();
    $scope.grupos = Grupo.query();
    $scope.directiva = directiva;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function save(form) {
      if(form.$valid)
      {
        $scope.isSend=true;
        $scope.directiva._method = 'PUT';
        $scope.directiva.$save().then(function(directiva) {
          $uibModalInstance.close(directiva);
        });
      }
    }
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();
