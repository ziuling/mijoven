(function() {
  angular
    .module('MiJoven')
    .controller('PagoCtrl', PagoCtrl)
    .controller('CreatePagoModalCtrl', CreatePagoModalCtrl)
    .controller('DetallePagoModalCtrl', DetallePagoModalCtrl)
    .controller('EditPagoModalCtrl', EditPagoModalCtrl);
    

  function PagoCtrl($scope, $uibModal, Pago) {
    $scope.getPagos= getPagos;
    $scope.create = create;
    $scope.edit = edit;
    $scope.detalle = detalle;
    $scope.destroy = destroy;
    
    $scope.isLoading = false;
    $scope.pagos = [];
  
    $scope.getPagos();

    
    
    function create() {
      var modalInstance = $uibModal.open({
        templateUrl: 'PagoModal.html',
        controller: 'CreatePagoModalCtrl',
        size: 'lg',
      });

      modalInstance.result.then(function(pago) {

        swal(
          'Registrado!',
          'Ha sido registrado con éxito.',
          'success'
        );
        
        $scope.getPagos();
        
      });
    }


    function edit(pago) {
      var modalInstance = $uibModal.open({
        templateUrl: 'PagoModal.html',
        controller: 'EditPagoModalCtrl',
        size: 'lg',
        resolve: {
          pago: pago
        },
      });

      modalInstance.result.then(function(pago) {
        swal(
          'Actualizado!',
          'Ha sido actualizado con éxito.',
          'success'
        );
        $scope.getPagos();
      });
    }

    function detalle(pago) {
      if(pago.tipo == 'Inscripción de club/sj')
      {
        var modalInstance = $uibModal.open({
              templateUrl: 'detallesPago1Modal.html',
              controller: 'DetallePagoModalCtrl',
              size: 'lg',
              resolve: {
                pago: pago
              },
            });
      }
      else if(pago.tipo == 'Inscripción de acampantes'){
        var modalInstance = $uibModal.open({
              templateUrl: 'detallesPago2Modal.html',
              controller: 'DetallePagoModalCtrl',
              size: 'lg',
              resolve: {
                pago: pago
              },
            });
      }
    }
 
    function destroy(pago) {
      
      swal({
        title: 'Confirmar borrado',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#20B2AA',
        cancelButtonColor: '#DC143C',
        confirmButtonText: 'Borrar',
        showLoaderOnConfirm: true,
        preConfirm: function() {
          return new Promise(function(resolve, reject) {
            pago.$delete().then(function() {
              resolve();
            }, function() {
              reject('Ha ocurrido un error');
            });
          });
        },
        allowOutsideClick: false,
      }).then(function() {
        swal(
          'Borrado!',
          'Ha sido borrado con éxito.',
          'success'
        );
        $scope.getPagos();
      });
    }
    
    function getPagos(){
      $scope.isLoading = true;
      $scope.pagos = Pago.query(function(){
        $scope.pagos.map(function(data){
          if(data.status === 1){
            data.stat = 'Activo';
            data.label = 'label-success';
          }else if(data.status === 0){
            data.stat = 'Inactivo';
            data.label = 'label-danger';
          }
          return data;
        })
      });
      $scope.isLoading=false;
    }
  }

  function CreatePagoModalCtrl($scope, $uibModalInstance, Pago) {
    $scope.pago = new Pago();
    $scope.pago.status = 0;
    $scope.isSend=false;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.FileMessage = null;

    $scope.setFile = function(element) {
      $scope.$apply(function($scope) {
        $scope.theFile = element.files[0];
        $scope.FileMessage = '';
        var filename = $scope.theFile.name;
        console.log(filename.length)
        var index = filename.lastIndexOf(".");
        var strsubstring = filename.substring(index, filename.length);
        if (strsubstring == '.pdf')
        {
          console.log('Correcto');
          $scope.FileMessage = null;

        }
        else {
          $scope.theFile = '';
          $scope.FileMessage = 'Solo archivos con extensión pdf';
        }
      });
    };

    $scope.sFil = function(element) {
      $scope.$apply(function($scope) {
        $scope.Fil = element.files[0];
        $scope.FilMessage = '';
        var filename = $scope.Fil.name;
        console.log(filename.length)
        var index = filename.lastIndexOf(".");
        var strsubstring = filename.substring(index, filename.length);
        if (strsubstring == '.pdf')
        {
          $scope.FilMessage = null;
          console.log('Correcto');
        }
        else {
          $scope.Fil = '';
          $scope.FilMessage = 'Solo archivos con extensión pdf';
        }
      });
    };

    function save(form) {
      if($scope.FileMessage == 'Solo archivos con extensión pdf' || $scope.FilMessage == 'Solo archivos con extensión pdf'){
        swal(
          'Oops...',
          'Solo se permite archivos con extensión pdf!',
          'error'
        )
      }else{
        if(form.$valid){

          $scope.isSend=true;
          swal({
              title: '¿Seguro en registrar el pago?',
              text: "Será enviado automaticamente",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#20B2AA',
              cancelButtonColor: '#DC143C',
              confirmButtonText: 'Si! seguro'
            }).then(function () {
              $scope.pago.$save().then(function(pago) {
                $uibModalInstance.close(pago);
              });
            });
        }
      }

      
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function DetallePagoModalCtrl($scope, $uibModalInstance, pago) {
    $scope.tipo = pago.tipo;
    console.log($scope.tipo)
    $scope.pago = pago
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function EditPagoModalCtrl($scope, $uibModalInstance, pago) {
    
    $scope.pago = pago;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function save(form) {
      if(form.$valid)
      {
        $scope.isSend=true;
        $scope.pago._method = 'PUT';
        $scope.pago.$save().then(function(pago) {
          $uibModalInstance.close(pago);
        });
      }
    }
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();