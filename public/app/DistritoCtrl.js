(function() {
  angular
    .module('MiJoven')
    .controller('DistritoCtrl', DistritoCtrl)
    .controller('CreateDistritoModalCtrl', CreateDistritoModalCtrl)
    .controller('EditDistritoModalCtrl', EditDistritoModalCtrl);
    

  function DistritoCtrl($scope, $uibModal, Distrito) {
    $scope.getDistritos= getDistritos;
    $scope.create = create;
    $scope.edit = edit;
    $scope.destroy = destroy;
    $scope.isLoading = false;
    $scope.distritos = [];
  
    $scope.getDistritos();
    
    function create() {
      var modalInstance = $uibModal.open({
        templateUrl: 'DistritoModal.html',
        controller: 'CreateDistritoModalCtrl',
        size: 'md',
      });

      modalInstance.result.then(function(distrito) {
        
        
        swal(
          'Creado!',
          'Ha sido creado con éxito.',
          'success'
        );
        $scope.getDistritos();
      });
    }

    function edit(distrito) {
      var modalInstance = $uibModal.open({
        templateUrl: 'DistritoModal.html',
        controller: 'EditDistritoModalCtrl',
        size: 'md',
        resolve: {
          distrito: angular.copy(distrito)
        },
      });

      modalInstance.result.then(function(distrito) {
        swal(
          'Actualizado!',
          'Ha sido actualizado con éxito.',
          'success'
        );
        $scope.getDistritos();
      });
    }

    function destroy(distrito) {
      
      swal({
        title: 'Confirmar borrado',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borrar',
        showLoaderOnConfirm: true,
        preConfirm: function() {
          return new Promise(function(resolve, reject) {
            distrito.$delete().then(function() {
              resolve();
            }, function() {
              reject('Ha ocurrido un error');
            });
          });
        },
        allowOutsideClick: false,
      }).then(function() {
        swal(
          'Borrado!',
          'Ha sido borrado con éxito.',
          'success'
        );
        $scope.getDistritos();
      });
    }
    
    function getDistritos(){
      $scope.isLoading = true;
      $scope.distritos = Distrito.query(function(){
        $scope.distritos.map(function(data){
          if(data.status === 1){
            data.stat = 'Activo';
            data.label = 'label-success';
          }else if(data.status === 0){
            data.stat = 'Inactivo';
            data.label = 'label-danger';
          }
          return data;
        })
      });
      $scope.isLoading=false;
      
    }
    
  }

  function CreateDistritoModalCtrl($scope, $uibModalInstance, Distrito, Zona) {
    $scope.distrito = new Distrito();
    $scope.zonas = Zona.query();
    $scope.distrito.status = 0;
    $scope.isSend=false;
    $scope.save = save;
    $scope.cancel = cancel;

    function save(form) {
      if(form.$valid){
        $scope.isSend=true;
        $scope.distrito.$save().then(function(distrito) {
          $uibModalInstance.close(distrito);
        });
      }
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function EditDistritoModalCtrl($scope, $uibModalInstance, distrito,Zona) {
    $scope.zonas = Zona.query();
    $scope.distrito = distrito;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function save(form) {
      if(form.$valid)
      {
        $scope.isSend=true;
        $scope.distrito._method = 'PUT';
        $scope.distrito.$save().then(function(distrito) {
          $uibModalInstance.close(distrito);
        });
      }
    }
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();
