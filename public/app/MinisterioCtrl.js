(function() {
  angular
    .module('MiJoven')
    .controller('MinisterioCtrl', MinisterioCtrl)
    .controller('CreateMinisterioModalCtrl', CreateMinisterioModalCtrl)
    .controller('DetalleMinisterioModalCtrl', DetalleMinisterioModalCtrl)
    .controller('EditMinisterioModalCtrl', EditMinisterioModalCtrl);
    

  function MinisterioCtrl($scope, $uibModal, Ministerio) {
    $scope.getMinisterios= getMinisterios;
    $scope.create = create;
    $scope.edit = edit;
    $scope.detalle = detalle;
    $scope.destroy = destroy;
    $scope.isLoading = false;
    $scope.ministerios = [];
  
    $scope.getMinisterios();
    
    function create() {
      var modalInstance = $uibModal.open({
        templateUrl: 'MinisterioModal.html',
        controller: 'CreateMinisterioModalCtrl',
        size: 'md',
      });

      modalInstance.result.then(function(ministerio) {
        
        swal(
          'Creado!',
          'Ha sido creado con éxito.',
          'success'
        );
        $scope.getMinisterios();
        
      });
    }

    function edit(ministerio) {
      var modalInstance = $uibModal.open({
        templateUrl: 'MinisterioModal.html',
        controller: 'EditMinisterioModalCtrl',
        size: 'md',
        resolve: {
          ministerio: ministerio
        },
      });

      modalInstance.result.then(function(ministerio) {
        swal(
          'Actualizado!',
          'Ha sido actualizado con éxito.',
          'success'
        );
        $scope.getMinisterios();
      });
    }

    function detalle(ministerio) {
        var modalInstance = $uibModal.open({
              templateUrl: 'detallesMinisterioModal.html',
              controller: 'DetalleMinisterioModalCtrl',
              size: 'lg',
              resolve: {
                ministerio: ministerio

              }
      });
    }

    function destroy(ministerio) {
      
      swal({
        title: 'Confirmar borrado',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borrar',
        showLoaderOnConfirm: true,
        preConfirm: function() {
          return new Promise(function(resolve, reject) {
            ministerio.$delete().then(function() {
              resolve();
            }, function() {
              reject('Ha ocurrido un error');
            });
          });
        },
        allowOutsideClick: false,
      }).then(function() {
        swal(
          'Borrado!',
          'Ha sido borrado con éxito.',
          'success'
        );
        $scope.getMinisterios();
      });
    }
    
    function getMinisterios(){
      $scope.isLoading = true;
      $scope.ministerios = Ministerio.query(function(){
        $scope.ministerios.map(function(data){
          if(data.status === 1){
            data.stat = 'Activo';
            data.label = 'label-success';
          }else if(data.status === 0){
            data.stat = 'Inactivo';
            data.label = 'label-danger';
          }
          return data;
        })
      });
      $scope.isLoading=false;
      
    }
    
  }

  function CreateMinisterioModalCtrl($scope, $uibModalInstance, Ministerio) {
    $scope.ministerio = new Ministerio();
    $scope.ministerio.status = 0;
    $scope.isSend=false;
    $scope.save = save;
    $scope.cancel = cancel;

    function save(form) {
      if(form.$valid){
        $scope.isSend=true;
        $scope.ministerio.$save().then(function(ministerio) {
          $uibModalInstance.close(ministerio);
        });
      }
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function EditMinisterioModalCtrl($scope, $uibModalInstance, ministerio) {
    $scope.ministerio = ministerio;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function save(form) {
      if(form.$valid)
      {
        $scope.isSend=true;
        $scope.ministerio._method = 'PUT';
        $scope.ministerio.$save().then(function(ministerio) {
          $uibModalInstance.close(ministerio);
        });
      }
    }
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

   function DetalleMinisterioModalCtrl($scope, $uibModalInstance, ministerio) {
    $scope.ministerio = ministerio;
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();
