(function() {
  angular
    .module('MiJoven')
    .controller('IdentidadCtrl', IdentidadCtrl)
    .controller('CreateIdentidadModalCtrl', CreateIdentidadModalCtrl)
    .controller('EditIdentidadModalCtrl', EditIdentidadModalCtrl);
    

  function IdentidadCtrl($scope, $uibModal, Identidad) {
    $scope.getIdentidades= getIdentidades;
    $scope.create = create;
    $scope.edit = edit;
    $scope.destroy = destroy;
    $scope.isLoading = false;
    $scope.identidades = [];
    
    $scope.getIdentidades();
    
    function create() {
      var modalInstance = $uibModal.open({
        templateUrl: 'IdentidadModal.html',
        controller: 'CreateIdentidadModalCtrl',
        size: 'md',
      });

      modalInstance.result.then(function(identidad) {
        
        swal(
          'Creado!',
          'Ha sido creado con éxito.',
          'success'
        );
        $scope.getIdentidades();
      });
    }

    function edit(identidad) {
      var modalInstance = $uibModal.open({
        templateUrl: 'IdentidadModal.html',
        controller: 'EditIdentidadModalCtrl',
        size: 'md',
        resolve: {
          identidad: identidad
        },
      });

      modalInstance.result.then(function(identidad) {
        swal(
          'Actualizado!',
          'Ha sido actualizado con éxito.',
          'success'
        );
        $scope.getIdentidades();
      });
    }

    function destroy(identidad) {
      
      swal({
        title: 'Confirmar borrado',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borrar',
        showLoaderOnConfirm: true,
        preConfirm: function() {
          return new Promise(function(resolve, reject) {
            identidad.$delete().then(function() {
              resolve();
            }, function() {
              reject('Ha ocurrido un error');
            });
          });
        },
        allowOutsideClick: false,
      }).then(function() {
        swal(
          'Borrado!',
          'Ha sido borrado con éxito.',
          'success'
        );
        $scope.getIdentidades();
      });
    }
    
    function getIdentidades(){
      $scope.isLoading = true;
      $scope.identidades = Identidad.query(function(){
        $scope.identidades.map(function(data){
          if(data.status === 1){
            data.stat = 'Activo';
            data.label = 'label-success';
          }else if(data.status === 0){
            data.stat = 'Inactivo';
            data.label = 'label-danger';
          }
          return data;
        })
      });
      $scope.isLoading=false;
    }
  }

  function CreateIdentidadModalCtrl($scope, $uibModalInstance, Identidad) {
    $scope.identidad = new Identidad();
    $scope.identidad.status = 0;
    $scope.isSend=false;
    $scope.save = save;
    $scope.cancel = cancel;

    function save(form) {
      console.log($scope.identidad);
      if(form.$valid){
        $scope.isSend=true;
        $scope.identidad.$save().then(function(identidad) {
          $uibModalInstance.close(identidad);
        });
      }
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function EditIdentidadModalCtrl($scope, $uibModalInstance, identidad) {
    $scope.identidad = identidad;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function save(form) {
      if(form.$valid)
      {
        $scope.isSend=true;
        $scope.identidad._method = 'PUT';
        $scope.identidad.$save().then(function(identidad) {
          $uibModalInstance.close(identidad);
        });
      }
    }
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();