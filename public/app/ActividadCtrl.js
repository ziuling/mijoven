(function() {
  angular
    .module('MiJoven')
    .controller('ActividadCtrl', ActividadCtrl)
    .controller('CreateActividadModalCtrl', CreateActividadModalCtrl)
    .controller('EditActividadModalCtrl', EditActividadModalCtrl);
    

  function ActividadCtrl($scope, $uibModal,  Actividad, Tipo_a, Circular) {
    $scope.getActividades= getActividades;
    $scope.tipos_a = Tipo_a.query();
    $scope.circulares = Circular.query();
    $scope.option = 1;
    $scope.circular = null;
    $scope.boton = true;
    $scope.cambio = function () {
      $scope.circular = $scope.actividad.circular_id;
      console.log($scope.circular)
    };
    $scope.create = create;
    $scope.edit = edit;
    $scope.destroy = destroy;
    $scope.isLoading = false;
    $scope.changeOption  = changeOption;
    $scope.actividades = [];

    $scope.getActividades();

    

    function create() {
      if ($scope.option == 2 || $scope.option == 3) {
        var modalInstance = $uibModal.open({
        templateUrl: 'ActividadModal.html',
        controller: 'CreateActividadModalCtrl',
        size: 'lg',
        resolve: {
        tipo_a_id : angular.copy($scope.option),
        circular_id : angular.copy($scope.circular)
        } 
      });
      } else {
        var modalInstance = $uibModal.open({
        templateUrl: 'FijaModal.html',
        controller: 'CreateActividadModalCtrl',
        size: 'md',
        resolve: {
        tipo_a_id : angular.copy($scope.option),
        circular_id : angular.copy($scope.circular)
        }
      });
        
      }

      modalInstance.result.then(function(actividad) {
        
        $scope.getActividades();
        swal(
          'Creado!',
          'Ha sido creado con éxito.',
          'success'
        );
      });
    }


    function edit(actividad) {
      if ($scope.option == 2 || $scope.option == 3) {
        var modalInstance = $uibModal.open({
          templateUrl: 'ActividadModal.html',
          controller: 'EditActividadModalCtrl',
          size: 'lg',
          resolve: {
            actividad: actividad
          },
        });
      }else {
        var modalInstance = $uibModal.open({
          templateUrl: 'FijaModal.html',
          controller: 'EditActividadModalCtrl',
          size: 'md',
          resolve: {
            actividad: actividad
          },
        });
      }

      modalInstance.result.then(function(actividad) {
        swal(
          'Actualizado!',
          'Ha sido actualizado con éxito.',
          'success'
        );
        $scope.getActividades();
      });
    }

    function destroy(actividad) {
      
      swal({
        title: 'Confirmar borrado',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borrar',
        showLoaderOnConfirm: true,
        preConfirm: function() {
          return new Promise(function(resolve, reject) {
            actividad.$delete().then(function() {
              resolve();
            }, function() {
              reject('Ha ocurrido un error');
            });
          });
        },
        allowOutsideClick: false,
      }).then(function() {
        swal(
          'Borrado!',
          'Ha sido borrado con éxito.',
          'success'
        );
        $scope.getActividades();
      });
    }
    
    function getActividades(){
      $scope.isLoading = true;
        $scope.actividades = Actividad.query({ id: $scope.option }, function(){
        $scope.actividades.map(function(data){
          if(data.status === 1){
            data.stat = 'Activo';
            data.label = 'label-success';
          }else if(data.status === 0){
            data.stat = 'Inactivo';
            data.label = 'label-danger';
          }
          return data;
        })
      });
      $scope.isLoading=false;
   
    }
    function changeOption(tipo_a) {
      $scope.option = tipo_a.id;
      console.log($scope.option)
      getActividades();
      $scope.boton = false;
    }
    

  }

  function CreateActividadModalCtrl($scope, $uibModalInstance, Actividad, tipo_a_id, circular_id) {
    $scope.actividad = new Actividad({ tipo_a_id: tipo_a_id, circular_id: circular_id });
    $scope.actividad.status = 0;
    $scope.isSend=false;
    $scope.save = save;
    $scope.cancel = cancel;
    

    function save(form) {
      if(form.$valid){
        $scope.isSend=true;
        $scope.actividad.$save().then(function(actividad) {
          $uibModalInstance.close(actividad);
        });
      }
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function EditActividadModalCtrl($scope, $uibModalInstance, actividad) {
    $scope.actividad = actividad;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function save(form) {
      if(form.$valid)
      {
        $scope.isSend=true;
        $scope.actividad._method = 'PUT';
        $scope.actividad.$save().then(function(actividad) {
          $uibModalInstance.close(actividad);
        });
      }
    }
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();