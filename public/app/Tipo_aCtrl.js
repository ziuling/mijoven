(function() {
  angular
    .module('MiJoven')
    .controller('Tipo_aCtrl', Tipo_aCtrl)
    .controller('CreateTipo_aModalCtrl', CreateTipo_aModalCtrl)
    .controller('EditTipo_aModalCtrl', EditTipo_aModalCtrl);
    

  function Tipo_aCtrl($scope, $uibModal, Tipo_a) {
    $scope.getTipos_a = getTipos_a;
    $scope.create = create;
    $scope.edit = edit;
    $scope.destroy = destroy;
    $scope.isLoading = false;
    $scope.tipos_a = [];
  
    $scope.getTipos_a();
    
    function create() {
      var modalInstance = $uibModal.open({
        templateUrl: 'Tipo_aModal.html',
        controller: 'CreateTipo_aModalCtrl',
        size: 'md',
      });

      modalInstance.result.then(function(tipo_a) {
        
        swal(
          'Creado!',
          'Ha sido creado con éxito.',
          'success'
        );
        $scope.getTipos_a();
      });
    }

    function edit(tipo_a) {
      var modalInstance = $uibModal.open({
        templateUrl: 'Tipo_aModal.html',
        controller: 'EditTipo_aModalCtrl',
        size: 'md',
        resolve: {
          tipo_a: tipo_a
        },
      });

      modalInstance.result.then(function(tipo_a) {
        swal(
          'Actualizado!',
          'Ha sido actualizado con éxito.',
          'success'
        );
        $scope.getTipos_a();
      });
    }

    function destroy(tipo_a) {
      
      swal({
        title: 'Confirmar borrado',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borrar',
        showLoaderOnConfirm: true,
        preConfirm: function() {
          return new Promise(function(resolve, reject) {
            tipo_a.$delete().then(function() {
              resolve();
            }, function() {
              reject('Ha ocurrido un error');
            });
          });
        },
        allowOutsideClick: false,
      }).then(function() {
        swal(
          'Borrado!',
          'Ha sido borrado con éxito.',
          'success'
        );
        $scope.getTipos_a();
      });
    }
    
    function getTipos_a(page){
      $scope.isLoading = true;
      $scope.tipos_a = Tipo_a.query({
          page: page,
          max: 4
        },function(success) {}, function(error) {});
      $scope.isLoading=false;
    }
    
  }

  function CreateTipo_aModalCtrl($scope, $uibModalInstance, Tipo_a) {
    $scope.tipo_a = new Tipo_a();
    $scope.tipo_a.status = 0;
    $scope.isSend=false;
    $scope.save = save;
    $scope.cancel = cancel;

    function save(form) {
      console.log(form)
      if(form.$valid){
        $scope.isSend=true;
        $scope.tipo_a.$save().then(function(tipo_a) {
          $uibModalInstance.close(tipo_a);
        });
      }
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function EditTipo_aModalCtrl($scope, $uibModalInstance, tipo_a) {
    $scope.tipo_a = tipo_a;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function save(form) {
      if(form.$valid)
      {
        $scope.isSend=true;
        $scope.tipo_a._method = 'PUT';
        $scope.tipo_a.$save().then(function(tipo_a) {
          $uibModalInstance.close(tipo_a);
        });
      }
    }
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();
