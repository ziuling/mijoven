(function() {
  angular
    .module('MiJoven')
    .controller('Plan_sostenimientoCtrl', Plan_sostenimientoCtrl)
    .controller('CreatePlan_sostenimientoModalCtrl', CreatePlan_sostenimientoModalCtrl)
    .controller('DetallePlan_sostenimientoModalCtrl', DetallePlan_sostenimientoModalCtrl)
    .controller('EditPlan_sostenimientoModalCtrl', EditPlan_sostenimientoModalCtrl);
    

  function Plan_sostenimientoCtrl($scope, $uibModal, Plan_sostenimiento, Actividad) {
    $scope.getPlan_sostenimientos= getPlan_sostenimientos;
    $scope.actividades = Actividad.query();
    $scope.create = create;
    $scope.edit = edit;
    $scope.detalle = detalle;
    $scope.destroy = destroy;
    $scope.isLoading = false;
    $scope.plan_sostenimientos = [];
  
    $scope.getPlan_sostenimientos();

    
    function create() {
      var modalInstance = $uibModal.open({
        templateUrl: 'Plan_sostenimientoModal.html',
        controller: 'CreatePlan_sostenimientoModalCtrl',
        size: 'lg',
      });

      modalInstance.result.then(function(plan_sostenimiento) {
        
        swal(
          'Creado!',
          'Ha sido creado con éxito.',
          'success'
        );
        $scope.getPlan_sostenimientos();
        
      });
    }

    function edit(plan_sostenimiento) {
      var modalInstance = $uibModal.open({
        templateUrl: 'Plan_sostenimientoModal.html',
        controller: 'EditPlan_sostenimientoModalCtrl',
        size: 'lg',
        resolve: {
          plan_sostenimiento: plan_sostenimiento
        },
      });

      modalInstance.result.then(function(plan_sostenimiento) {
        swal(
          'Actualizado!',
          'Ha sido actualizado con éxito.',
          'success'
        );
        $scope.getPlan_sostenimientos();
      });
    }

    function detalle(plan_sostenimiento) {
        var modalInstance = $uibModal.open({
              templateUrl: 'detallesPlanModal.html',
              controller: 'DetallePlan_sostenimientoModalCtrl',
              size: 'lg',
              resolve: {
                plan_sostenimiento: plan_sostenimiento

              }
      });
    }

    function destroy(plan_sostenimiento) {
      
      swal({
        title: 'Confirmar borrado',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borrar',
        showLoaderOnConfirm: true,
        preConfirm: function() {
          return new Promise(function(resolve, reject) {
            plan_sostenimiento.$delete().then(function() {
              resolve();
            }, function() {
              reject('Ha ocurrido un error');
            });
          });
        },
        allowOutsideClick: false,
      }).then(function() {
        swal(
          'Borrado!',
          'Ha sido borrado con éxito.',
          'success'
        );
        $scope.getPlan_sostenimientos();
      });
    }
    
    function getPlan_sostenimientos(){
      $scope.isLoading = true;
      $scope.plan_sostenimientos = Plan_sostenimiento.query(function(){
        $scope.plan_sostenimientos.map(function(data){
          if(data.status === 1){
            data.stat = 'Activo';
            data.label = 'label-success';
          }else if(data.status === 0){
            data.stat = 'Inactivo';
            data.label = 'label-danger';
          }
          return data;
        })
      });
      $scope.isLoading=false;
      
    }
    
  }

  function CreatePlan_sostenimientoModalCtrl($scope, $uibModalInstance, Plan_sostenimiento) {
    $scope.plan_sostenimiento = new Plan_sostenimiento();
    $scope.planes = Plan_sostenimiento.query();
    $scope.plan_sostenimiento.status = 0;
    $scope.isSend=false;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.datos = [];
 
   /* $scope.calcular = function(){
      var resultado = 0;
      var este_es = 0;
        for(var i=0; i<$scope.plan_sostenimientos; i++){
           resultado = $scope.plan_sostenimiento[i].mes_ant
        }
        console.log(resultado)
    }*/
    
    function save(form) {
      console.log($scope.plan_sostenimiento);
      if(form.$valid){
        $scope.isSend=true;
        $scope.plan_sostenimiento.$save().then(function(plan_sostenimiento) {
          $uibModalInstance.close(plan_sostenimiento);
        });
      }
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function EditPlan_sostenimientoModalCtrl($scope, $uibModalInstance, plan_sostenimiento) {
    $scope.plan_sostenimiento = plan_sostenimiento;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function save(form) {
      if(form.$valid)
      {
        $scope.isSend=true;
        $scope.plan_sostenimiento._method = 'PUT';
        $scope.plan_sostenimiento.$save().then(function(plan_sostenimiento) {
          $uibModalInstance.close(plan_sostenimiento);
        });
      }
    }
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function DetallePlan_sostenimientoModalCtrl($scope, $uibModalInstance, plan_sostenimiento, Plan_sostenimiento) {
    $scope.plan_sostenimiento = plan_sostenimiento;
    $scope.plan_sostenimientos = Plan_sostenimiento.query();
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();
