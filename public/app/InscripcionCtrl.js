
(function() {
  angular
    .module('MiJoven')
    
    .controller('InscripcionCtrl', InscripcionCtrl)
    .controller('CreateInscripcionModalCtrl', CreateInscripcionModalCtrl)
    .controller('EditInscripcionModalCtrl', EditInscripcionModalCtrl);
    

  function InscripcionCtrl($scope, $uibModal, Inscripcion, Categoria) {
    $scope.getInscripciones= getInscripciones;
    $scope.categorias = Categoria.query();
    $scope.view = true;
    $scope.option = 1;
    $scope.create = create;
    $scope.edit = edit;
    $scope.destroy = destroy;
    $scope.isLoading = false;
    $scope.changeOption  = changeOption;
    
    $scope.getInscripciones();
    
    function create() {
      var modalInstance = $uibModal.open({
        templateUrl: 'InscripcionModal.html',
        controller: 'CreateInscripcionModalCtrl',
        size: 'md',
        resolve: {
        categoria_id : angular.copy($scope.option)
        }
      });

      modalInstance.result.then(function(inscripcion) {
        
        $scope.getInscripciones();
        swal(
          'Creado!',
          'Ha sido creado con éxito.',
          'success'
        );
      });
    }

    function edit(inscripcion) {
      var modalInstance = $uibModal.open({
        templateUrl: 'InscripcionModal.html',
        controller: 'EditInscripcionModalCtrl',
        size: 'md',
        resolve: {
          inscripcion: inscripcion
        },
      });

      modalInstance.result.then(function(inscripcion) {
        swal(
          'Actualizado!',
          'Ha sido actualizado con éxito.',
          'success'
        );
        $scope.getInscripciones();
      });
    }

    function destroy(inscripcion) {
      
      swal({
        title: 'Confirmar borrado',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borrar',
        showLoaderOnConfirm: true,
        preConfirm: function() {
          return new Promise(function(resolve, reject) {
            inscripcion.$delete().then(function() {
              resolve();
            }, function() {
              reject('Ha ocurrido un error');
            });
          });
        },
        allowOutsideClick: false,
      }).then(function() {
        swal(
          'Borrado!',
          'Ha sido borrado con éxito.',
          'success'
        );
        $scope.getInscripciones();
      });
    }
    
    function getInscripciones(){
      $scope.isLoading = true;
      $scope.inscripciones = Inscripcion.query({id: $scope.option });
      $scope.isLoading=false;
    }

    function changeOption(categoria) {
      $scope.option = categoria.id;
      if ($scope.option == 4) {
        $scope.view = false;
      } else { $scope.view = true;}
      getInscripciones();
    }
    
  }
  function CreateInscripcionModalCtrl($scope, $uibModalInstance, Inscripcion, categoria_id) {
    $scope.inscripcion = new Inscripcion({categoria_id: categoria_id});
    $scope.inscripcion.status = 0;
    $scope.isSend=false;
    $scope.save = save;
    $scope.cancel = cancel;

    function save(form) {
      if(form.$valid){
        $scope.isSend=true;
        $scope.inscripcion.$save().then(function(inscripcion) {
          $uibModalInstance.close(inscripcion);
        });
      }
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
  function EditInscripcionModalCtrl($scope, $uibModalInstance, inscripcion) {
    
    $scope.inscripcion = inscripcion;
    $scope.save = save;
    $scope.cancel = cancel;
    $scope.isSend=false;
    
    function save(form) {
      if(form.$valid)
      {
        $scope.isSend=true;
        $scope.inscripcion._method = 'PUT';
        $scope.inscripcion.$save().then(function(inscripcion) {
          $uibModalInstance.close(inscripcion);
        });
      }
    }
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }


})();