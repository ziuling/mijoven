(function() {
  angular
    .module('MiJoven')
    .controller('ReporteCtrl', ReporteCtrl)
    .controller('PuntuacionModalCtrl', PuntuacionModalCtrl)
    .controller('AcampanteModalCtrl', AcampanteModalCtrl)
    .controller('DonanteModalCtrl', DonanteModalCtrl)
    .controller('InscripcionModalCtrl', InscripcionModalCtrl)
    .controller('VoluntarioModalCtrl', VoluntarioModalCtrl);

  function ReporteCtrl($scope, $uibModal, Circular, Grupo, Pago, Categoria, Puntuacion, Reglamento, Reporte, Zona, Club, Iglesia, Inscripcion) {
    $scope.circulares = Circular.query();
    $scope.grupos = Grupo.query();
    $scope.categorias = Categoria.query();
    $scope.reglamentos = Reglamento.query();
    $scope.pagos = Pago.query();
    $scope.zonas = Zona.query();
    $scope.clubes = Club.query();
    $scope.iglesias = Iglesia.query();
    $scope.inscripciones = Inscripcion.query();
    $scope.puntuaciones = Puntuacion.query();
    $scope.option = 1;

    $scope.isLoading = false;

    $scope.r = null;
    $scope.id = null;
    $scope.categoria = null;
    $scope.user = null;
    $scope.club = null; 
    $scope.iglesia = null; 
    $scope.entidad = null; 
    $scope.total = null; 
    $scope.categoria_id = null; 

    $scope.reportes = [{name:'Circulares'},
       {name: 'Reglamentos'}, {name: 'Pagos'}];
    $scope.reportes1 = [{name:'Circulares'},
     {name: 'Grupos pequeños Salvación y Servicio'},
      {name: 'Directiva y sus miembros'}];
    $scope.reportes2 = [{name:'Circulares'},
     {name: 'Grupos pequeños Salvación y Servicio'},
      {name: 'Directiva y sus miembros'},
       {name: 'Reglamentos'}];

    $scope.consultas = [{name:'Montos de inscripciones'},
     {name: 'Puntuación acumulada'},];
    $scope.consultas1 = [{name: 'Cantidad de acampantes'},
       {name: 'Voluntarios de Cerca de Ti'},
       {name: 'Donantes de sangre'},];
    $scope.consultas2 = [{name: 'Voluntarios de Cerca de Ti'},
      {name: 'Donantes de sangre'},];   

    $scope.view= false;
    $scope.view1= false;
    $scope.view2= false;
    $scope.view3= false;
    $scope.view4= false;
    $scope.vista= false;
    $scope.vista1= false;
    $scope.vista2= false;
    $scope.vista3= false;
    $scope.vista4= false;
    $scope.selecciones= false;
    $scope.selecciones1= false;

    $scope.cambio = function(){
      if($scope.reporte.name == 'Circulares'){
        $scope.view= true;
        $scope.view1= false;
        $scope.view2= false;
        $scope.view3= false;
      }else if($scope.reporte.name == 'Grupos pequeños Salvación y Servicio'){
        $scope.view= false;
        $scope.view1= true;
        $scope.view2= false;
        $scope.view3= false;
      }else if($scope.reporte.name == 'Directiva y sus miembros'){
        $scope.view= false;
        $scope.view1= false;
        $scope.view2= true;
        $scope.view3= false;
      }else if($scope.reporte.name == 'Reglamentos'){
        $scope.view= false;
        $scope.view1= false;
        $scope.view2= false;
        $scope.view3= true;
      }else if($scope.reporte.name == 'Pagos'){
        $scope.view= false;
        $scope.view1= false;
        $scope.view2= false;
        $scope.view3= false;
        $scope.view4= true;
      }
      else{
        $scope.view= false;
        $scope.view1= false;
        $scope.view2= false;
        $scope.view3= false;
        $scope.view4= false;
      }
      
    }

    $scope.change = function(){
      //console.log($scope.consultas.name)
      if($scope.consultas.name == 'Montos de inscripciones'){
        $scope.vista= true;
        $scope.vista1= false;
        $scope.vista2= false;
        $scope.vista3= false;
        $scope.vista4=false;
      }else if($scope.consultas.name == 'Puntuación acumulada'){
        $scope.vista= false;
        $scope.vista1= true;
        $scope.vista2= false;
        $scope.vista3= false;
        $scope.vista4=false;
      }else if($scope.consultas.name == 'Cantidad de acampantes'){
        $scope.vista= false;
        $scope.vista1= false;
        $scope.vista2= true;
        $scope.vista3= false;
        $scope.vista4=false;
      }else if($scope.consultas.name == 'Voluntarios de Cerca de Ti'){
        $scope.vista= false;
        $scope.vista1= false;
        $scope.vista2= false;
        $scope.vista3= true;
        $scope.vista4=false;
      }else if($scope.consultas.name == 'Donantes de sangre'){
        $scope.vista= false;
        $scope.vista1= false;
        $scope.vista2= false;
        $scope.vista3= false;
        $scope.vista4= true;
      }
      else{
        $scope.vista= false;
        $scope.vista1= false;
        $scope.vista2= false;
        $scope.vista3= false;
        $scope.vista4=false;
      }
      
    }
    $scope.changeOption = function(categoria) {
      $scope.option = categoria.id;
      if($scope.option ==  1 || $scope.option == 2 || $scope.option == 3){
          $scope.selecciones= true;
          $scope.selecciones1= false;
      }else if($scope.option == 4){
        $scope.selecciones= false;
        $scope.selecciones1= true;
      }
    }

    $scope.puntuacion = function(club) {
      console.log(club)
      var modalInstance = $uibModal.open({
        templateUrl: 'PuntuacionModal.html',
        controller: 'PuntuacionModalCtrl',
        size: 'md',
        resolve: {
        club : angular.copy($scope.club)
        }
      });
    }

    $scope.acampante = function() {
      var modalInstance = $uibModal.open({
        templateUrl: 'AcampanteModal.html',
        controller: 'AcampanteModalCtrl',
        size: 'md',
        resolve: {
        categoria_id : angular.copy($scope.option)
        }
        
      });
    }

    $scope.donante = function() {
      var modalInstance = $uibModal.open({
        templateUrl: 'DonanteModal.html',
        controller: 'DonanteModalCtrl',
        size: 'md',
        resolve: {
        categoria_id : angular.copy($scope.option)
        }
        
      });
    }

    $scope.inscripcion = function() {
      var modalInstance = $uibModal.open({
        templateUrl: 'InscripcionModal.html',
        controller: 'InscripcionModalCtrl',
        size: 'md',
        resolve: {
          categoria_id : angular.copy($scope.option)
        }
        
      });
    }

    $scope.voluntario = function() {
      var modalInstance = $uibModal.open({
        templateUrl: 'VoluntarioModal.html',
        controller: 'VoluntarioModalCtrl',
        size: 'md',
        resolve: {
        categoria_id : angular.copy($scope.option)
        }
        
      });
    }
  }  
  
  function PuntuacionModalCtrl($scope, $uibModalInstance, club, Puntuacion) {
    console.log(club)
    $scope.puntuaciones = Puntuacion.query();
    $scope.cancel = cancel

    $scope.total = function(club){
        var club = club;
        $scope.total[club] = 0;
        for(var i = 0; i < $scope.puntuaciones.length; i++){
          if($scope.puntuaciones[i].club_id === club){
            var numero = Number($scope.puntuaciones[i].total || 0);
            $scope.total[club] = $scope.total[club] + numero;
          }
        }
      
    }
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function AcampanteModalCtrl($scope, $uibModalInstance, Pago, categoria_id, Club, Iglesia) {
    console.log(categoria_id);
    $scope.option = categoria_id;
    $scope.pagos = Pago.query();
    $scope.clubes = Club.query();
    $scope.iglesias = Iglesia.query();
    $scope.cancel = cancel
    //$scope.total = function(){
        $scope.total = 0;
        console.log($scope.clubes.length);

        for(var i = 0; i < $scope.clubes.length; i++){
          if($scope.clubes[i].categoria_id == $scope.option){
            console.log($scope.clubes[i].categoria_id);
            for(var i = 0; i < $scope.pagos.length; i++){
              if($scope.pagos[i].club_id == $scope.clubes[i].id){
                console.log($scope.pagos[i].club_id);
                var numero = Number($scope.pagos[i].cantidad_n || 0);
                var numero1 = Number($scope.pagos[i].cantidad_m || 0);
                var numero2 = Number($scope.pagos[i].cantidad_a || 0);
                var numero3 = Number($scope.pagos[i].cantidad_e || 0);
                $scope.total = $scope.total + numero + numero1 + numero2 + numero3;
              }
            }
          }
        }
    //}

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
  
  function DonanteModalCtrl($scope, $uibModalInstance, Persona, Miembro, categoria_id) {
    console.log(categoria_id);
    $scope.option = categoria_id;
    $scope.personas = Persona.query();
    $scope.miembros = Miembro.query();
    $scope.cancel = cancel

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function InscripcionModalCtrl($scope, $uibModalInstance, Inscripcion, categoria_id) {
    $scope.option = categoria_id;
    $scope.inscripciones = Inscripcion.query();
    $scope.cancel = cancel

   
    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

  function VoluntarioModalCtrl($scope, $uibModalInstance, Miembro, Persona) {
    $scope.miembros = Miembro.query();
    $scope.personas = Persona.query();
    $scope.cancel = cancel

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
  

})();
