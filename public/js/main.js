$(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}


//according menu

$(document).ready(function()
{
    //Add Inactive Class To All Accordion Headers
    $('.accordion-header').toggleClass('inactive-header');
	
	//Set The Accordion Content Width
	var contentwidth = $('.accordion-header').width();
	$('.accordion-content').css({});
	
	//Open The First Accordion Section When Page Loads
	$('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
	$('.accordion-content').first().slideDown().toggleClass('open-content');
	
	// The Accordion Effect
	$('.accordion-header').click(function () {
		if($(this).is('.inactive-header')) {
			$('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
			$(this).toggleClass('active-header').toggleClass('inactive-header');
			$(this).next().slideToggle().toggleClass('open-content');
		}
		
		else {
			$(this).toggleClass('active-header').toggleClass('inactive-header');
			$(this).next().slideToggle().toggleClass('open-content');
		}
	});
	
	return false;
});

angular
	.module('MiJoven', ['ngResource', 'ui.bootstrap', 'ngSanitize', 'textAngular',  'file-model'])

	.config(function($provide){
		$provide.decorator('taOptions', ['taRegisterTool', '$delegate', function(taRegisterTool, taOptions){
			// $delegate is the taOptions we are decorating
			// register the tool with textAngular
			taRegisterTool('colourRed', {
				iconclass: "fa fa-square red",
				action: function(){
					this.$editor().wrapSelection('forecolor', 'red');
				}
			});
			// add the button to the default toolbar definition
			taOptions.toolbar[1].push('colourRed');
			return taOptions;
		}]);
	})

	
     .directive('dateFormat', function() {
      return {
        require: 'ngModel',
        link: function(scope, element, attr, ngModelCtrl) {
          //Angular 1.3 insert a formater that force to set model to date object, otherwise throw exception.
          //Reset default angular formatters/parsers
          ngModelCtrl.$formatters.length = 0;
          ngModelCtrl.$parsers.length = 0;
        }
      };
    })
   
	.directive('stringToNumber', function() {
	  return {
	    require: 'ngModel',
	    link: function(scope, element, attrs, ngModel) {
	      ngModel.$parsers.push(function(value) {
	        return '' + value;
	      });
	      ngModel.$formatters.push(function(value) {
	        return parseFloat(value);
	      });
	    }
	  };
	})

	.directive('validFile', function() {

	    var validFormats = ['pdf'];
	    return {
	        require: 'ngModel',
	        link: function (scope, elem, attrs, ctrl) {
	            ctrl.$validators.validFile = function() {
	                elem.on('change', function () {
	                   var value = elem.val(),
	                       ext = value.substring(value.lastIndexOf('.') + 1).toLowerCase();   
	                   return validFormats.indexOf(ext) !== -1;
	                });
	           };
	        }
	    };
	})


   .config(['$httpProvider', function($httpProvider) {
	  $httpProvider.defaults.transformRequest = function(data) {
	    if(undefined === data) return data;
	    var formData = new FormData();
	    angular.forEach(data, function(value, key) {
	      if(value instanceof FileList) {
	        if(value.length === 1)
	          formData.append(key, value[0]);
	        else {
	          angular.foreach(value, function(file, index) {
	            formData.append(key + '_' + index, file);
	          });
	        }
	      } else {
	        formData.append(key, value);
	      }
	    });
	    return formData;
	  };
	  $httpProvider.defaults.headers.post['Content-Type'] = undefined;
	}])
	
    .filter("filtroTipo", function() {
        return function(input) {
            var salida = [];
            angular.forEach(input, function(tipo_a) {
                if (tipo_a.total_puntuacion != 0) {
                salida.push(tipo_a)
                }
            })
            return salida;
        }
    })
    .filter("filtroA", function() {
        return function(input) {
            var salida = [];
            angular.forEach(input, function(actividad) {
                if (actividad.tipo_a_id === 1 && actividad.tipo_a_id === 3) {
                salida.push(actividad)
                }
            })
            return salida;
        }
    })
    .filter("filtroB", function() {
        return function(input) {
            var salida = [];
            angular.forEach(input, function(actividad) {
                if (actividad.mes === 'Enero') {
                salida.push(actividad)
                }
            })
            return salida;
        }
    })
    .filter("filtroC", function() {
        return function(input) {
            var salida = [];
            angular.forEach(input, function(actividad) {
                if (actividad.tipo_a_id === 1) {
                salida.push(actividad)
                }
            })
            return salida;
        }
    })

    .directive('checklistModel', function($parse, $compile) {
	  // contains
	  function contains(arr, item, comparator) {
	    if (angular.isArray(arr)) {
	      for (var i = arr.length; i--;) {
	        if (comparator(arr[i], item)) {
	          return true;
	        }
	      }
	    }
	    return false;
	  }

	  // add
	  function add(arr, item, comparator) {
	    arr = angular.isArray(arr) ? arr : [];
	      if(!contains(arr, item, comparator)) {
	          arr.push(item);
	      }
	    return arr;
	  }  

	  // remove
	  function remove(arr, item, comparator) {
	    if (angular.isArray(arr)) {
	      for (var i = arr.length; i--;) {
	        if (comparator(arr[i], item)) {
	          arr.splice(i, 1);
	          break;
	        }
	      }
	    }
	    return arr;
	  }

	  // http://stackoverflow.com/a/19228302/1458162
	  function postLinkFn(scope, elem, attrs) {
	     // exclude recursion, but still keep the model
	    var checklistModel = attrs.checklistModel;
	    attrs.$set("checklistModel", null);
	    // compile with `ng-model` pointing to `checked`
	    $compile(elem)(scope);
	    attrs.$set("checklistModel", checklistModel);

	    // getter / setter for original model
	    var getter = $parse(checklistModel);
	    var setter = getter.assign;
	    var checklistChange = $parse(attrs.checklistChange);
	    var checklistBeforeChange = $parse(attrs.checklistBeforeChange);

	    // value added to list
	    var value = attrs.checklistValue ? $parse(attrs.checklistValue)(scope.$parent) : attrs.value;


	    var comparator = angular.equals;

	    if (attrs.hasOwnProperty('checklistComparator')){
	      if (attrs.checklistComparator[0] == '.') {
	        var comparatorExpression = attrs.checklistComparator.substring(1);
	        comparator = function (a, b) {
	          return a[comparatorExpression] === b[comparatorExpression];
	        };
	        
	      } else {
	        comparator = $parse(attrs.checklistComparator)(scope.$parent);
	      }
	    }

	    // watch UI checked change
	    scope.$watch(attrs.ngModel, function(newValue, oldValue) {
	      if (newValue === oldValue) { 
	        return;
	      } 

	      if (checklistBeforeChange && (checklistBeforeChange(scope) === false)) {
	        scope[attrs.ngModel] = contains(getter(scope.$parent), value, comparator);
	        return;
	      }

	      setValueInChecklistModel(value, newValue);

	      if (checklistChange) {
	        checklistChange(scope);
	      }
	    });

	    function setValueInChecklistModel(value, checked) {
	      var current = getter(scope.$parent);
	      if (angular.isFunction(setter)) {
	        if (checked === true) {
	          setter(scope.$parent, add(current, value, comparator));
	        } else {
	          setter(scope.$parent, remove(current, value, comparator));
	        }
	      }
	      
	    }

	    // declare one function to be used for both $watch functions
	    function setChecked(newArr, oldArr) {
	      if (checklistBeforeChange && (checklistBeforeChange(scope) === false)) {
	        setValueInChecklistModel(value, scope[attrs.ngModel]);
	        return;
	      }
	      scope[attrs.ngModel] = contains(newArr, value, comparator);
	    }

	    // watch original model change
	    // use the faster $watchCollection method if it's available
	    if (angular.isFunction(scope.$parent.$watchCollection)) {
	        scope.$parent.$watchCollection(checklistModel, setChecked);
	    } else {
	        scope.$parent.$watch(checklistModel, setChecked, true);
	    }
	  }

	  return {
	    restrict: 'A',
	    priority: 1000,
	    terminal: true,
	    scope: true,
	    compile: function(tElement, tAttrs) {
	      if ((tElement[0].tagName !== 'INPUT' || tAttrs.type !== 'checkbox') && (tElement[0].tagName !== 'MD-CHECKBOX') && (!tAttrs.btnCheckbox)) {
	        throw 'checklist-model should be applied to `input[type="checkbox"]` or `md-checkbox`.';
	      }

	      if (!tAttrs.checklistValue && !tAttrs.value) {
	        throw 'You should provide `value` or `checklist-value`.';
	      }

	      // by default ngModel is 'checked', so we set it if not specified
	      if (!tAttrs.ngModel) {
	        // local scope var storing individual checkbox model
	        tAttrs.$set("ngModel", "checked");
	      }

	      return postLinkFn;
	    }
	  };
	})
   
    .factory('Directiva', Directiva)
    .factory('Miembro', Miembro)
    .factory('Leccion', Leccion)
	.factory('Investidura', Investidura)
	.factory('Miembro_leccion', Miembro_leccion)
	.factory('Miembro_investidura', Miembro_investidura)
	.factory('Zona', Zona)
	.factory('Distrito', Distrito)
	.factory('Pastor', Pastor)
	.factory('Iglesia', Iglesia)
	.factory('Club', Club)
	.factory('Grupo', Grupo)
	.factory('Inscripcion', Inscripcion)
	.factory('Identidad', Identidad)
	.factory('Reglamento', Reglamento)
	.factory('Actividad', Actividad)
	.factory('Circular', Circular)
	.factory('Categoria', Categoria)
	.factory('Cargo', Cargo)
	.factory('Persona', Persona)
	.factory('Planificacion', Planificacion)
	.factory('Plan_sostenimiento', Plan_sostenimiento)
	.factory('Ministerio', Ministerio)
	.factory('Puntuacion', Puntuacion)
	.factory('Detalle', Detalle)
	.factory('Pago', Pago)
	.factory('Informe', Informe)
	.factory('Modulo', Modulo)
	.factory('Act_modulo', Act_modulo)
	.factory('Informe_act', Informe_act)
	.factory('Reporte', Reporte)
	.factory('Tipo_a', Tipo_a) //tipo de actividades
	.factory('User', User);

    function Directiva($resource) {
	    return $resource('directivas/:id', { id: '@id' });
	}
	function Miembro($resource) {
	    return $resource('miembros/:id', { id: '@id' });
	}
	function Leccion($resource) {
		return $resource('/lecciones/:id',{ id: '@id' });
	}
	function Investidura($resource) {
		return $resource('/investiduras/:id',{ id: '@id' });
	}
	function Miembro_leccion($resource) {
		return $resource('/miembro_lecciones/:id',{ id: '@id' });
	}
	function Miembro_investidura($resource) {
		return $resource('/miembro_investiduras/:id',{ id: '@id' });
	}
	function Actividad($resource){
        return $resource('actividades/:id', { id: '@id' });
	}
	function Circular($resource){
        return $resource('/circulares/:id', { id: '@id' });
	}
	function Tipo_a($resource) {
		return $resource('/tipos/:id',{ id: '@id' });
	}
	function Zona($resource) {
	    return $resource('/administracion/zonas/:id', { id: '@id' });
	}
	function Distrito($resource) {
	    return $resource('/administracion/distritos/:id', { id: '@id' });
	}
	function Pastor($resource) {
	    return $resource('/administracion/pastores/:id', { id: '@id' });
	}
	function Iglesia($resource) {
	    return $resource('/administracion/iglesias/:id', { id: '@id' });
	}
	function Club($resource) {
	    return $resource('/administracion/clubes/:id', { id: '@id' });
	}
	function Inscripcion($resource) {
	   return $resource('/administracion/inscripciones/:id', { id: '@id' });
	}
	function Identidad($resource) {
	   return $resource('/identidades/:id', { id: '@id' });
	}
	function Reglamento($resource) {
	    return $resource('/administracion/reglamentos/:id', { id: '@id' });
	}
	function Grupo($resource) {
	    return $resource('/direcciong/grupos/:id', { id: '@id' });
	}
	function Reporte($resource) {
	    return $resource('/reportes/:id', { id: '@id' });
	}
	function User($resource) {
		return $resource('/usuarios/:id',{ id: '@id' });
	}
	function Categoria($resource) {
		return $resource('/categorias/:id',{ id: '@id' });
	}
	function Cargo($resource) {
		return $resource('/cargos/:id',{ id: '@id' }); //colocar la misma ruta en web.php
	}
	function Persona($resource) {
		return $resource('/personas/:id',{ id: '@id' });
	}
    function Planificacion($resource) {
		return $resource('/planificaciones/:id',{ id: '@id' });
	}
    function Puntuacion($resource) {
		return $resource('/puntuaciones/:id',{ id: '@id' });
	}
	function Detalle($resource) {
		return $resource('/detalles/:id',{ id: '@id' });
	}
	function Pago($resource) {
		return $resource('pago/:id', { id: '@id' });
	}
	function Informe($resource) {
		return $resource('/informes/:id',{ id: '@id' });
	}
	function Modulo($resource) {
		return $resource('/modulos/:id',{ id: '@id' });
	}
	function Act_modulo($resource) {
		return $resource('/act_modulos/:id',{ id: '@id' });
	}
	function Informe_act($resource) {
		return $resource('/informes_act/:id',{ id: '@id' });
	}
	function Plan_sostenimiento($resource) {
		return $resource('/planes/:id',{ id: '@id' });
	}
	function Ministerio($resource) {
		return $resource('ministerios/:id',{ id: '@id' });
	}
	
