<!-- Modal -->
<script type="text/ng-template" class="modal" id="detallesMinisterioModal.html">
    <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="detallesMinisterioModalLabel"></h4>
    </div>
      
        <div class="modal-body">
            <div class="row" >
	            <div class="panel panel-primary">
		            <div class="panel-heading">
		             	Actividad realizada el @{{ ministerio.dia }} 
		            </div>
		        </div>    
		        <div class="panel-body">
			   	   	   <table id="detalles" class="table table-striped table-bordered table-condensed table-responsive">
			   	   	       <thead>
				   	   	      	<th>Actividad</th>
				   	   	      	<th>Descripción</th>
			   	   	       </thead>
			   	   	       <tbody>
				   	   	   	    <tr>
				   	   	   	  	  	<td>@{{ ministerio.nombre }}</td>
				   	   	   	  	  	<td>@{{ ministerio.descripcion }}</td>
				   	   	   	  	</tr>
				   	   	   </tbody>	  	
			   	   	   </table>
		   	    </div>
	   	   </div> 

   	   </div>
             
	    <div class="modal-footer">
	        <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
        </div>

</script>