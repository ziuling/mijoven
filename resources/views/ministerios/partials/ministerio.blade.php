<!-- Modal -->
<script type="text/ng-template" id="MinisterioModal.html">
    <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="MinisterioModalLabel">Ministerio fuera de serie</h4>
    </div>
      <form class="form" name="form" ng-cloak novalidate>
        {{ csrf_field() }}
        <div class="modal-body">
          <div class="row">
            <div class="form-group col-xs-11">
                <label class="control-label pull-right">Campos requeridos(<span class="required ">*</span>)</label>
            </div>
	        <div class="form-group col-xs-6 col-xs-offset-1">
		        <span class="required">*</span><label class="control-label ">Mes:</label>             
                <select name="mes" class="form-control" ng-model="ministerio.mes" required>
                              <?php
                                  $mes=date("n"); 
                                  $rango=11; 
                                  for ($i=$mes-1;$i<=$mes+$rango;$i++){ 
                                     $meses=date('F', mktime(0, 0, 0, $i, 1, date("Y") ) );
                                     if ($meses=="January") $meses="Enero";
                                     if ($meses=="February") $meses="Febrero";
                                     if ($meses=="March") $meses="Marzo";
                                     if ($meses=="April") $meses="Abril";
                                     if ($meses=="May") $meses="Mayo";
                                     if ($meses=="June") $meses="Junio";
                                     if ($meses=="July") $meses="Julio";
                                     if ($meses=="August") $meses="Agosto";
                                     if ($meses=="September") $meses="Septiembre";
                                     if ($meses=="October") $meses="Octubre";
                                     if ($meses=="November") $meses="Noviembre";
                                     if ($meses=="December") $meses="Diciembre";
                                     echo "<option value='$meses'>$meses</option>"; 
                                  } 
                              ?> 
                </select>	
                <span class="messages" ng-show="form.$submitted || form.name.$touched">
                <span class="" ng-show="form.mes.$error.required">Mes requerido</span>
     		</div>
            <div class="form-group col-xs-3">
                <span class="required">*</span><label class="control-label ">Día:</label>
                <input type="number" class="form-control" name="dia" ng-model="ministerio.dia" max="31" required>
                <span class="messages" ng-show="form.$submitted || form.name.$touched">
                <span class="" ng-show="form.dia.$error.required">Día es requerido</span>
                <span class="" ng-show="form.dia.$error.max">El día maximo es 31</span>
            </div> 
    		<div class="form-group col-xs-10 col-xs-offset-1">
    			<span class="required">*</span><label class="control-label ">Actividad realizada:</label>
    			<textarea class="form-control" name="nombre" ng-model="ministerio.nombre" rows="1" required></textarea>
                <span class="messages" ng-show="form.$submitted || form.name.$touched">
                <span class="" ng-show="form.nombre.$error.required">Actividad es requerida</span>
    		</div> 
            <div class="form-group col-xs-10 col-xs-offset-1">
                <span class="required">*</span><label class="control-label">Descripción</label>
                <textarea class="form-control" name="descripcion" ng-model="ministerio.descripcion" rows="6" required></textarea>
                <span class="messages" ng-show="form.$submitted || form.name.$touched">
                <span class="" ng-show="form.descripcion.$error.required">Descripción de la actividad es requerida</span>
            </div>
    				    
		  </div>	
	    </div>
	    <div class="modal-footer">
	        <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
	        <button type="submit" class="btn btn-success" ng-click="save(form)">Guardar</button>
        </div>
    </form>

</script>