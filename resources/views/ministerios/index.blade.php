@extends('layouts.app')

@section('content')

		@if (session('status'))
		    <div class="alert alert-success">
		        {{ session('status') }}
		    </div>
		@endif

	    <section id="main-content">
            <section class="wrapper">
		  <div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-th-list"></i> Procesar ministerio fuera de serie</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="home">Inicio</a></li>
						<li><i class="fa fa-table"></i>Ministerio</li>
					</ol>
				</div>
		  </div>
              <!-- page start-->
                <div class="row">
                 
                  <div class="col-lg-12">
                      <section class="panel" ng-controller="MinisterioCtrl">
                          <header class="panel-heading">
                          </header>
                        <table class="table table-striped table-advance table-hover">
						    <thead>
								<th>Mes</th>
								<th>Día</th>
								<th>Actividad</th>
								<th>Acciones</th>
							</thead>
							<tbody>
								<tr ng-repeat="ministerio in ministerios">
									<td ng-bind="ministerio.mes"></td>
									<td ng-bind="ministerio.dia"></td>
									<td ng-bind="ministerio.nombre"></td>
									<td>
									    <div class="btn-group">
										    <button class="btn btn-eliminar" type="button" ng-click="destroy(ministerio)"><i class="icon_close_alt2"></i></button>
										    <button class="btn btn-info" type="button" ng-click="detalle(ministerio)">Detalles</button>
										    @include("ministerios.partials.show")
										</div>    
									    
									</td>
								</tr>
							    <tr ng-if="ministerios.length < 1"><td colspan="4"><h5 class="text-center">Sin registros</h5></td></tr>
							</tbody>
						</table>
						<button class="btn btn-enviar pull-right" type="button" ng-click="create()"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
						@include("ministerios.partials.ministerio")
				  </div>
				    
				</div>
		    </section>
	    </section>   
    </div>	    
@endsection
@section('script')
    <script type="text/javascript" src="{{asset('app/MinisterioCtrl.js')}}"></script>
    
@endsection