@extends('layouts.app')

@section('content')
    
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <section id="main-content">
            <section class="wrapper">
              <div class="row">
                  <div class="col-lg-12">
                      <h3 class="page-header"><i class="icon_document_alt"></i> Registrar Pagos</h3>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-home"></i><a href="home">Inicio</a></li>
                            <li><i class="icon_document_alt"></i>Pagos</li>
                        </ol>
                  </div>
              </div>
              <div class="row" ng-controller="PagoCtrl">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                          </header>

                            <table class="table table-striped table-advance table-hover">
                                <thead>
                                    <th>Referencia de Pago</th>
                                    <th>Fecha</th>
                                    <th>Tipo de Pago</th>
                                    <th>Concepto</th>
                                    <th>Acciones</th>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="pago in pagos">
                                        <td ng-bind="pago.n_referencia"></td>
                                        <td ng-bind="pago.fecha | date:'dd/MM/yyyy'"></td>
                                        <td ng-bind="pago.tipo"></td>
                                        <td ng-bind="pago.concepto || 'Sin Concepto'"></td>
                                        <td>
                                            <div class="btn-group">
                                                <button class="btn btn-eliminar" type= "button" ng-click="destroy(pago)"><i class="icon_close_alt2"></i></button>
                                                <button class="btn btn-info" type="button" ng-click="detalle(pago)">Detalles</button>
                                                @include("pagos.partials.detalles1")
                                                @include("pagos.partials.detalles2")
                                            </div>
                                            <!--<a href="pago/1/@{{ pago.id }}" target="_blank"><button class="btn btn-default" type="button"><span class="glyphicon glyphicon-download-alt"></span></button></a>-->
                                        </td>
                                    </tr>
                                    <tr ng-if="pagos.length < 1">
                                       <td colspan="4"><h5 class="text-center">Sin registros</h5></td>
                                    </tr>
                                </tbody>
                            </table>
                            
                            <button class="btn btn-enviar pull-right" type="button" ng-click="create()"><span class="glyphicon glyphicon-plus"></span> Nuevo pago</button>
                            @include("pagos.partials.pago")
                             

                      </section>
                  </div>
              </div>
            </section>
        </section>
    
 @endsection
 @section('script')
    <script type="text/javascript" src="{{asset('app/PagoCtrl.js')}}"></script>
    
@endsection