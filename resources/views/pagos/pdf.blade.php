<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    
	<style>
	    html{
			margin:0; 
			margin-top: 18mm;
			
			margin-bottom: 10mm;
		}
	    @font-face {
		  font-family: Junge;
		  src: url(Junge-Regular.ttf);
		}

		.clearfix:after {
		   content: "";
		   display: table;
		   clear: both;
		}

		a {
		  color: #001028;
		  text-decoration: none;
		}

		body {
		  font-family: Junge;
		  position: relative;
		  width: 21cm;  
		  height: 29.7cm; 
		  margin: 0 auto; 
		  color: #001028;
		  background: #FFFFFF; 
		  font-size: 14px; 
		}

		.arrow {
		  margin-bottom: 4px;
		}

		.arrow.back {
		  text-align: right;
		}

		.inner-arrow {
		  padding-right: 10px;
		  height: 30px;
		  display: inline-block;
		  background-color: rgb(233, 125, 49);
		  text-align: center;

		  line-height: 30px;
		  vertical-align: middle;
		}

		.arrow.back .inner-arrow {
		  background-color: rgb(233, 217, 49);
		  padding-right: 0;
		  padding-left: 10px;
		}

		.arrow:before,
		.arrow:after {
		  content:'';
		  display: inline-block;
		  width: 0; height: 0;
		  border: 15px solid transparent;
		  vertical-align: middle;
		}

		.arrow:before {
		  border-top-color: rgb(233, 125, 49);
		  border-bottom-color: rgb(233, 125, 49);
		  border-right-color: rgb(233, 125, 49);
		}

		.arrow.back:before {
		  border-top-color: transparent;
		  border-bottom-color: transparent;
		  border-right-color: rgb(233, 217, 49);
		  border-left-color: transparent;
		}

		.arrow:after {
		  border-left-color: rgb(233, 125, 49);
		}

		.arrow.back:after {
		  border-left-color: rgb(233, 217, 49);
		  border-top-color: rgb(233, 217, 49);
		  border-bottom-color: rgb(233, 217, 49);
		  border-right-color: transparent;
		}

		.arrow span { 
		  display: inline-block;
		  width: 80px; 
		  margin-right: 20px;
		  text-align: right; 
		}

		.arrow.back span { 
		  margin-right: 0;
		  margin-left: 20px;
		  text-align: left; 
		}

		h1 {
		  color: #5D6975;
		  font-family: Junge;
		  font-size: 2.4em;
		  line-height: 1.4em;
		  font-weight: normal;
		  text-align: center;
		  border-top: 1px solid #5D6975;
		  border-bottom: 1px solid #5D6975;
		  margin: 0 0 2em 0;
		}

		h1 small { 
		  font-size: 0.45em;
		  line-height: 1.5em;
		  float: left;
		} 

		h1 small:last-child { 
		  float: right;
		} 

		#project { 
		  float: left; 
		}

		#company { 
		  float: right; 
		}

		table {
		  width: 100%;
		  border-collapse: collapse;
		  border-spacing: 0;
		  margin-bottom: 30px;
		}

		table th,
		table td {
		  text-align: center;
		}

		table th {
		  padding: 5px 20px;
		  color: #5D6975;
		  border-bottom: 1px solid #C1CED9;
		  white-space: nowrap;        
		  font-weight: normal;
		}

		table .service,
		table .desc {
		  text-align: left;
		}

		table td {
		  padding: 20px;
		  text-align: right;
		}

		table td.service,
		table td.desc {
		  vertical-align: top;
		}

		table td.unit,
		table td.qty,
		table td.total {
		  font-size: 1.2em;
		}

		table td.sub {
		  border-top: 1px solid #C1CED9;
		}

		table td.grand {
		  border-top: 1px solid #5D6975;
		}

		table tr:nth-child(2n-1) td {
		  background: #EEEEEE;
		}

		table tr:last-child td {
		  background: #DDDDDD;
		}

		#details {
		  margin-bottom: 30px;
		}

		footer {
		  color: #5D6975;
		  width: 100%;
		  height: 30px;
		  position: absolute;
		  bottom: 0;
		  border-top: 1px solid #C1CED9;
		  padding: 8px 0;
		  text-align: center;
		}
		
	</style>
	<title>Pago</title>
</head>
<body>
   {{--  ENCABEZADO  --}}

    <main>
      <h1  class="clearfix">Misión venezolana NorOriental </h1>
      <table>
        <thead>
          <tr>
            <th class="service" colspan="2">
             <div id="details" class="clearfix">
		        
		        <div id="company">
		          <div class="arrow back"><div class="inner-arrow">PAGO REALIZADO CON ÉXITO <span></span></div></div>
		         
		        </div>
		      </div>
            </th>
          </tr>
          <tr>
            <th class="desc" colspan="2">DESCRIPCIÓN</th>
          </tr>
        </thead>
        <tbody>
          @if($data->tipo == 'Inscripción de club/sj')
          <tr>
            <td class="service">PAGO REALIZADO EN LA FECHA DE:</td>
            <td class="desc">{{ $data->fecha }}</td>
          </tr>
          <tr>
            <td class="service">TIPO DE PAGO:</td>
            <td class="desc">{{ $data->tipo }}</td>
            
          </tr>
          <tr>
            <td class="service">NUMERO DE REFERENCIA:</td>
            <td class="desc">{{ $data->n_referencia }}</td>
            
          </tr>
          
          <tr>
            <td class="service">EN EL BANCO:</td>
            <td class="desc">{{ $data->banco }}</td>
          </tr>
          <tr>
            <td class="service">MONTO:</td>
            <td class="desc">{{ $data->monto }}</td>
            
          </tr>
          <tr>
            <td class="service">CONCEPTO:</td>
            <td class="desc">{{ $data->concepto }}</td>
            
          </tr>
          @else
          <tr>
            <td class="service">PAGO REALIZADO EN LA FECHA DE:</td>
            <td class="desc">{{ $data->fecha }}</td>
          </tr>
          <tr>
            <td class="service">TIPO DE PAGO:</td>
            <td class="desc">{{ $data->tipo }}</td>
            
          </tr>
          <tr>
            <td class="service">NUMERO DE REFERENCIA:</td>
            <td class="desc">{{ $data->n_referencia }}</td>
            
          </tr>
          
          <tr>
            <td class="service">EN EL BANCO:</td>
            <td class="desc">{{ $data->banco }}</td>
          </tr>
          <tr>
            <td class="service">MONTO:</td>
            <td class="desc">{{ $data->monto }}</td>
            
          </tr>
          <tr>
            <td class="service">CANTIDAD TOTAL DE MIEMBROS:</td>
            <td class="desc">{{ $data->cantidad_m}}</td>
          </tr>
          <tr>
            <td class="service">CANTIDAD TOTAL DE ACOMPAÑANTES</td>
            <td class="desc">{{ $data->cantidad_a}}</td>
            
          </tr>
          <tr>
            <td class="service">CANTIDAD TOTAL DE NIÑOS:</td>
            <td class="desc">{{ $data->cantidad_n}}</td>
          </tr>
          <tr>
            <td class="service">CANTIDAD TOTAL DE ECÓNOMOS</td>
            <td class="desc">{{ $data->cantidad_e}}</td>
          </tr>
          <tr>
            <td class="service">CANTIDAD DE DIRECTIVA:</td>
            <td class="desc">{{ $data->directiva}}</td>
          </tr>
          <tr>
            <td class="service">CANTIDAD TOTAL DE ACAMPANTES</td>
            <td class="desc">{{ ($data->cantidad_m) + ($data->cantidad_a) + ($data->cantidad_n) + ($data->cantidad_e) + ($data->directiva) }}</td>
          </tr>
          @endif
        </tbody>
      </table>
      <!--<div id="details" class="clearfix">
        <div id="project">
          <div class="arrow"><div class="inner-arrow"><span>Banco</span></div></div>
          <div class="arrow"><div class="inner-arrow"><span>Monto</span></div></div>
          <div class="arrow"><div class="inner-arrow"><span>Referencia</span></div></div>
        </div>
        <div id="company">
          <div class="arrow back"><div class="inner-arrow">{{ $data->banco }} <span></span></div></div>
          <div class="arrow back"><div class="inner-arrow">{{ $data->monto }} <span></span></div></div>
          <div class="arrow back"><div class="inner-arrow">{{ $data->n_referencia }} <span></span></div></div>
        </div>
      </div>-->
     <!-- <div id="notices">
        <div>NOTICE:</div>
        <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
      </div>-->
    </main>

    <footer>
        @if($data->club->user->rol_id == 4)
           Club de {{$data->club->categoria->nombre}} {{ $data->club->nombre}}
        @elseif($data->club->user->rol_id == 6)
          Sociedad de Jóvenes de la iglesia {{ $data->iglesia->nombre }}
        @endif
    </footer>
</body>
</html>