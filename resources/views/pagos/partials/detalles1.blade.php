<!-- Modal -->
<script type="text/ng-template" class="modal" id="detallesPago1Modal.html">
    <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="detallesPago1ModalLabel"></h4>
    </div>
      
        <div class="modal-body">
           <div class="row" >
	            <div class="panel panel-primary">
		            <div class="panel-heading">
		             	Pago realizado el: @{{ pago.fecha | date }} 
		            </div>
		        </div>    
		        <div class="panel-body">
			   	   	   <table id="detalles" class="table table-striped table-bordered table-condensed table-responsive">
			   	   	       <thead>
				   	   	      	<th>Tipo de Pago</th>
				   	   	      	<th>Nº de referencia</th>
				   	   	      	<th>Banco</th>
			   	   	       </thead>
			   	   	       <tbody>
				   	   	   	    <tr>
				   	   	   	  	  	<td>@{{ pago.tipo }}</td>
				   	   	   	  	  	<td>@{{ pago.n_referencia }}</td>
				   	   	   	  	  	<td>@{{ pago.banco }}</td>
				   	   	   	  	</tr>
				   	   	   </tbody>	  	
			   	   	       <thead>
				   	   	      	<th>Concepto</th>
				   	   	      	<th>Planilla</th>
				   	   	      	<th>Comprobante</th>
			   	   	       </thead>  
			   	   	   	   <tbody>
	                            <tr>
				   	   	   	  	  	<td>@{{ pago.concepto }}</td>
				   	   	   	  	  	<td><a href='/download/@{{ pago.planilla }}'>Descargar</a></td>
				   	   	   	  	  	<td><a href='/download/@{{ pago.comprobante }}'>Descargar</a></td>
				   	   	   	  	</tr>
			   	   	   	   </tbody>
			   	   	   </table>
		   	    </div>
	   	   </div> 

   	   </div>
             
	    <div class="modal-footer">
	        <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
        </div>

</script>