<!-- Modal -->
<script type="text/ng-template" id="PagoModal.html">
    <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="PagoModalLabel">Pagos</h4>
    </div>
      <form class="form" name="form" enctype="multipart/form-data" ng-cloak novalidate>
        {{ csrf_field() }}
        <div class="modal-body">
            <div class="row">
                <div class="form-group col-xs-11">
                <label class="control-label pull-right">Campos requeridos(<span class="required ">*</span>)</label>
            </div>
            <div class="form-group  col-xs-10 col-xs-offset-1">
                <span class="required">*</span><label class="control-label">Tipo de pago:</label>
                <select class="form-control" name="tipo" id="tipo-pago" ng-model="pago.tipo" required>
                    <option>Inscripción de club/sj</option>
                    <option>Inscripción de acampantes</option>
                </select>
                <span class="messages" ng-show="form.$submitted || form.name.$touched">
                <span class="" ng-show="form.tipo.$error.required">Tipo requerido</span>
            </div> 
           
            <div class="form-group col-xs-10 col-xs-offset-1">
                <span class="required">*</span><label>Fecha de Pago:</label>
                <input type="date" name="fecha" ng-model="pago.fecha" class="form-control" date-format required/>
                <span class="messages" ng-show="form.$submitted || form.name.$touched">
                <span class="" ng-show="form.fecha.$error.required">Fecha requerido</span>     
            </div>
            <div class="form-group col-xs-10 col-xs-offset-1">
                <span class="required">*</span><label class="control-label">Nº de Referencia:</label>
                <input type="text" name="n_referencia" ng-model="pago.n_referencia" class="form-control" required/>
                <span class="messages" ng-show="form.$submitted || form.name.$touched">
                <span class="" ng-show="form.n_referencia.$error.required">Numero de referencia requerido</span>
            </div>
            <div class="form-group col-xs-10 col-xs-offset-1">
                <span class="required">*</span><label class="control-label">Banco:</label>
                <input type="text" name="banco" ng-model="pago.banco" class="form-control"  required/>
                <span class="messages" ng-show="form.$submitted || form.name.$touched">
                <span class="" ng-show="form.banco.$error.required">Banco requerido</span>
            </div> 
            <div class="form-group col-xs-10 col-xs-offset-1">
                <span class="required">*</span><label class="control-label">Monto:</label>
                <input type="text" name="monto" ng-model="pago.monto" class="form-control" placeholder="Bs." required/>
                <span class="messages" ng-show="form.$submitted || form.name.$touched">
                <span class="" ng-show="form.monto.$error.required">Monto requerido</span>
                       
            </div> 

            <div class="form-group col-xs-10 col-xs-offset-1" ng-if="pago.tipo == 'Inscripción de club/sj' ">
                <label class="control-label">Concepto:</label>
                <select class="form-control" name="concepto" ng-model="pago.concepto" ng-disabled="pago.tipo==='Inscripción de acampantes'">
                   <option>Inscripción de club/sj al campo</option>
                   <option>Inscripción de club/sj al camporee/campestre</option>
                </select>
            </div> 

            <div class="form-group col-xs-10 col-xs-offset-1" ng-if="pago.tipo == 'Inscripción de club/sj' ">
                <span class="required">*</span><label class="control-label" for="planilla">Subir Planilla</label>
                <input type="file" name="planilla" file-model="pago.planilla" ng-model="pago.planilla" id="planilla" ng-disabled="pago.tipo!='Inscripción de club/sj'"  onchange="angular.element(this).scope().setFile(this)" required>
                <span style="color: red;">@{{FileMessage}}</span>
                <span class="messages" ng-show="form.$submitted || form.name.$touched">
                <span class="" ng-show="form.planilla.$error.required">Planilla requerida</span>
            </div> 
            <div ng-if="pago.tipo == 'Inscripción de acampantes' ">
                <div class="form-group col-xs-10 col-xs-offset-1">
                    <label>Cantidad de Miembros:</label>
                    <input type="number" string-to-number name="cantidad_m" id="cantidad_m" ng-model="pago.cantidad_m" class="form-control" ng-disabled="pago.tipo!='Inscripción de acampantes'">     
                </div>
                <div class="form-group col-xs-10 col-xs-offset-1">
                    <label>Cantidad de Acompañantes:</label>
                    <input type="number" string-to-number name="cantidad_a" id="cantidad_a" ng-model="pago.cantidad_a" class="form-control" ng-disabled="pago.tipo!='Inscripción de acampantes'" >
                    <span class="messages" ng-show="form.$submitted || form.name.$touched">
                    <span class="" ng-show="form.cantidad_a.$error.number">Ingrese 0</span>     
                </div>
                <div class="form-group col-xs-10 col-xs-offset-1">
                    <label>Cantidad de Niños:</label>
                    <input type="number" string-to-number name="cantidad_n" id="cantidad_n" ng-model="pago.cantidad_n" class="form-control" ng-disabled="pago.tipo!='Inscripción de acampantes'">     
                </div> 
                <div class="form-group col-xs-10 col-xs-offset-1">
                    <label>Cantidad de Ecónomos:</label>
                    <input type="number" string-to-number name="cantidad_e" id="cantidad_e" ng-model="pago.cantidad_e" class="form-control"  ng-disabled="pago.tipo!='Inscripción de acampantes'">     
                </div> 
                <div class="form-group col-xs-10 col-xs-offset-1">
                    <label>Directiva:</label>
                    <input type="number" string-to-number  name="directiva" id="directiva" ng-model="pago.directiva" class="form-control" ng-disabled="pago.tipo!='Inscripción de acampantes'">     
                </div> 
                
            </div>
            <div class="form-group col-xs-10 col-xs-offset-1">
                <span class="required">*</span><label class="control-label" for="comprobante">Subir Comprobante</label>
                <input type="file" name="comprobante" file-model="pago.comprobante" ng-model="pago.comprobante" id="comprobante"  onchange="angular.element(this).scope().sFil(this)"  required>
               <span style="color:red">@{{FilMessage}}</span> 
                <span class="messages" ng-show="form.$submitted || form.name.$touched">
                <span class="" ng-show="form.comprobante.$error.required">Comprobante requerido</span>
               
            </div>
        </div>    
        <div class="modal-footer">
            <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
            <button type="submit" class="btn btn-success" ng-click="save(form)">Guardar</button>
        </div>
    </form>

</script>
