<!-- Modal -->
<script type="text/ng-template" class="modal" id="detallesPago2Modal.html">
    <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="detallesPago2ModalLabel"></h4>
    </div>
      
        <div class="modal-body">
           <div class="row">
             <div class="panel panel-primary">
	            <div class="panel-heading">
	             	 Pago realizado el: @{{ pago.fecha | date }} 
	            </div>
		        <div class="panel-body">
			   	   	   <table id="detalles" class="table table-striped table-bordered table-condensed table-responsive">
			   	   	       <thead>
				   	   	      	<th>Tipo de Pago</th>
				   	   	      	<th>Nº de referencia</th>
				   	   	      	<th>Banco</th>
				   	   	      	<th>Comprobante</th>
			   	   	       </thead>
			   	   	       <tbody>
				   	   	   	    <tr>
				   	   	   	  	  	<td>@{{ pago.tipo }}</td>
				   	   	   	  	  	<td>@{{ pago.n_referencia }}</td>
				   	   	   	  	  	<td>@{{ pago.banco }}</td>
				   	   	   	  	  	<td><a href='/download/@{{ pago.comprobante }}'>Descargar</a></td>
				   	   	   	  	</tr>
				   	   	   </tbody>	  	
			   	   	       <thead>
				   	   	      	<th>Cantidad de miembros</th>
				   	   	      	<th>Cantidad de niños</th>
				   	   	      	<th>Cantidad de acompañantes</th>
				   	   	      	<th>Cantidad de ecónomos</th>
			   	   	       </thead>  
			   	   	   	   <tbody>
	                            <tr>
				   	   	   	  	  	<td>@{{ pago.cantidad_m }}</td>
				   	   	   	  	  	<td>@{{ pago.cantidad_n }}</td>
				   	   	   	  	  	<td>@{{ pago.cantidad_a }}</td>
				   	   	   	  	  	<td>@{{ pago.cantidad_e }}</td>
				   	   	   	  	</tr>
			   	   	   	   </tbody>
			   	   	   	   <thead>
				   	   	      	<th>Directiva</th>
				   	   	      	<th>Total de acampantes</th>
			   	   	       </thead>  
			   	   	   	   <tbody>
	                            <tr>
				   	   	   	  	  	<td>@{{ pago.directiva }}</td>
				   	   	   	  	  	<td>@{{ (pago.cantidad_m) + (pago.cantidad_a) + (pago.cantidad_n) + (pago.cantidad_e) + (pago.directiva) }} </td>
				   	   	   	  	</tr>
			   	   	   	   </tbody>
			   	   	   </table>
		   	    </div>
		   	 </div>
		   	</div>   
   	   </div>
             
	    <div class="modal-footer">
	        <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
        </div>

</script>