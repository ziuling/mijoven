@extends('layouts.app')

@section('content')

    <div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3">
       <div class="titulo" style="margin-bottom: 50px;">     
		    <h3 class="text-center">Crear Identidad</h3>
		</div>  

       @if (session('status'))
		    <div class="alert alert-success">
		        {{ session('status') }}
		    </div>
		@endif

		{!!Form::open(array('url'=>'club', 'method'=>'POST', 'autocomplete'=>'off', 'files'=>'true'))!!}
        {{Form::token()}}
            <div class="row">
		        <div class="form-group col-xs-12 col-sm-8 col-sm-offset-2">
		          <label for="logo" class="control-label">Subir logo del Club</label>
		          <input type="file" id="logo" name="logo" ng-model="club.logo">
		        </div>
		        <div class="form-group col-xs-12 col-sm-8 col-sm-offset-2">
		          <label class="control-label" ="">Dirección:</label>
		          <input type="text" class="form-control" ng-model="club.direccion" name="direccion" placeholder="Inserte dirección de reunión">
		        </div>
		        <div class="form-group col-xs-12 col-sm-8 col-sm-offset-2">
		          <button class="btn btn-primary pull-right" type="submit">Guardar</button>
		        </div>
		    </div>

		{!!Form::close()!!}
	</div>	

@endsection		