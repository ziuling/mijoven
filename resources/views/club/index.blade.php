@extends('layouts.app')

@section('content')

    <div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3">
        
        <div class="titulo" style="margin-bottom: 50px;">     
		    <h3 class="text-center">Gestionar Identidad</h3>
		</div>  
		
		@if (session('status'))
		    <div class="alert alert-success">
		        {{ session('status') }}
		    </div>
		@endif

	    <section class="main" style="">

	        <div class="table-responsive">
			    <table class="table table-hover ">
				    <thead>
					    <th>Dirección de reunión</th>
					    <th>Logo</th>
					    <th>Acciones</th>
				    </thead>
				    <tbody>
						@forelse($clubes as $club)
						    <tr>
								<td>{{$club->direccion}}</td>
							    <td>
							    <img src="{{asset('img/logos/'.$club->logo)}}" alt="{{$club->nombre}}" height="100px" width="100px" style="border-radius: 50%">
								</td>
								<td>
									<a href="{{URL::action('ClubesCtrl@edit', $club->id)}}"><button class="btn btn-primary" type="button"><span class="glyphicon glyphicon-edit"></span></button></a>
								   <a href="" data-target="#modal-delete-{{$club->id}}" data-toggle="modal"><button class="btn btn-danger" type="button"><span class="glyphicon glyphicon-trash"></span></button></a>
								</td>
							</tr>
							@include('club.partial.modal')
							@empty
							<tr><td colspan="4"><h5 class="text-center">Sin registro</h5></td></tr>
						@endforelse
					</tbody>
				</table>
	            <a href="club/create"><button class="btn btn-success pull-right" type="button"><span class="glyphicon glyphicon-plus"></span> Crear</button></a>
			</div>
	
		</section>

    </div>

@endsection
@section('script')
	<script type="text/javascript" src="{{asset('app/ClubCtrl.js')}}"></script>
@endsection