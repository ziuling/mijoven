@extends('layouts.app')

@section('content')

	    @if (session('status'))
	      <div class="alert alert-success">
	        {{ session('status') }}
	      </div>
	    @endif
        
        <section id="main-content">
	        <section class="wrapper">
			    <div class="row">
					<div class="col-lg-12">
						<h3 class="page-header"><i class="icon_document_alt"></i> Gestionar Actividades</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a href="home">Inicio</a></li>
							<li><i class="icon_document_alt"></i>Actividades</li>
						</ol>
					</div>
				</div>
				<div class="row" ng-controller="ActividadCtrl">
				   
				    <div class="col-lg-12">
			            <div class="form-group" style="margin-top:40px;">
			                <select class="form-control" name="circular" required ng-model="actividad.circular_id" ng-options="circular.id as circular.titulo for circular in circulares" ng-change="cambio()"  value="@{{actividad.circular_id}}">
					            <option value="">--Seleccione Una Circular--</option>
			                </select>
			            </div>
				        <div class="extension" >
				            <a href="{{ asset('/circulares') }}" class="pull-right" > Gestionar Circular</a>
				        </div>
			        </div>
					<div class="col-lg-12">
					    <div class="form-group text-center">
						    <div class="btn-group" style="margin-top:40px;">
							        <button class="btn " ng-class="{'btn-grupo':tipo_a.id === option, 'btn-default':tipo_a.id != option}" ng-repeat="tipo_a in tipos_a" name="option" ng-click="changeOption(tipo_a)" >@{{tipo_a.tipo }} </button>
			                        <a href="{{ asset('/tipos') }}" class="pull-right" style="margin-top: 12px;"> Tipos de actividades</a>
					        </div>
						</div>
				    </div>
					<div class="col-lg-12">
						<section class="panel">
	                          <header class="panel-heading">
	                          </header>
	                            <table  class="table table-striped table-advance table-hover">
								    <thead>
									    <th>Actividad</th>
									    <th>Puntuación</th>
										<th>Acciones</th>
									</thead>

									<tbody>
										<tr ng-repeat="actividad in actividades | filter:{circular_id: actividad.circular_id}">
												<td>@{{actividad.nombre}}</td>
												<td>@{{actividad.puntuacion || 'Sin puntos'}}</td>
												<td>
													<div class="btn-group">
														<button class="btn btn-primary" type="button" ng-click="edit(actividad)"><span class="glyphicon glyphicon-edit"></span></button>
														<button class="btn btn-eliminar" type="button" ng-click="destroy(actividad)"><i class="icon_close_alt2"></i></button>
													</div>
												</td>
									    </tr>
										<!--<tr  ng-if="actividades.length <1 "><td colspan="4"><h5 class="text-center">Sin registros</h5></td></tr>-->
							  	    </tbody>
								</table>

								<button  class="btn btn-enviar pull-right" type="button" ng-click="create()" ng-disabled="boton"><span class="glyphicon glyphicon-plus"></span> Nueva actividad</button>
								@include('actividades.partials.actividad')
								@include('actividades.partials.fija')
		                           
								</div>
							</div>
						</div>
				    </div>  
			    </div>
			</section>
		</section>
				
@endsection
@section('script')	
	<script type="text/javascript" src="{{asset('app/ActividadCtrl.js')}}"></script>

@endsection