<!-- Modal -->
<script type="text/ng-template" class="modal" id="ActividadModal.html">
    <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="ActividadModalLabel">Actividades</h4>
    </div>
    <form class="form" name="form" ng-cloak novalidate>
        {{ csrf_field() }}

        <div class="modal-body">

            <div class="row">
                <div class="form-group col-xs-11">
                    <label class="control-label pull-right">Campos requeridos(<span class="required ">*</span>)</label>
                </div>
                <input type="hidden" name="circular.id" ng-model="actividad.circular_id" value="@{{actividad.circular_id}}">
			    <div class="form-group col-xs-8 col-xs-offset-1">
                    <label class="control-label">Seleccione el mes:</label>
                    <select class="form-control" name="mes" ng-model="actividad.mes" requerid>
                          <?php
                              $mes=date("n"); 
                              $rango=11; 
                              for ($i=$mes;$i<=$mes+$rango;$i++){ 
                                 $meses=date('F', mktime(0, 0, 0, $i, 1, date("Y") ) );
                                 if ($meses=="January") $meses="Enero";
                                 if ($meses=="February") $meses="Febrero";
                                 if ($meses=="March") $meses="Marzo";
                                 if ($meses=="April") $meses="Abril";
                                 if ($meses=="May") $meses="Mayo";
                                 if ($meses=="June") $meses="Junio";
                                 if ($meses=="July") $meses="Julio";
                                 if ($meses=="August") $meses="Agosto";
                                 if ($meses=="September") $meses="Septiembre";
                                 if ($meses=="October") $meses="Octubre";
                                 if ($meses=="November") $meses="Noviembre";
                                 if ($meses=="December") $meses="Diciembre";
                                 echo "<option value='$meses'>$meses</option>"; 
                              } 
                          ?> 
    			    </select>
                    
			    </div>
                <div class="form-group col-xs-5 col-xs-offset-1">
				    <span class="required">*</span><label>Fecha inicio</label>
		            <input type="date" class="form-control" name="f_inicio" ng-model="actividad.f_inicio"  date-format required>
                    <span class="messages" ng-show="form.$submitted || form.name.$touched">
                    <span class="" ng-show="form.f_inicio.$error.required">Fecha requerida</span>
                    
		        </div>
		        <div class="form-group col-xs-5">
				    <span class="required">*</span><label>Fecha final</label>
		            <input type="date" class="form-control" name="f_final" ng-model="actividad.f_final" date-format required>
                    <span class="messages" ng-show="form.$submitted || form.name.$touched">
                    <span class="" ng-show="form.f_final.$error.required">Fecha requerida</span>
		        </div>
				<div class="form-group col-xs-10 col-xs-offset-1">
				    <span class="required">*</span><label>Actividad</label>
		            <input type="text" class="form-control" name="nombre" ng-model="actividad.nombre"  title="título" required>
                    <span class="messages" ng-show="form.$submitted || form.name.$touched">
                    <span class="" ng-show="form.nombre.$error.required">Nombre de actividad requerido</span>
		        </div>
				<div class="form-group col-xs-10 col-xs-offset-1">
				    <span class="required">*</span><label>Puntuación</label>
		            <input type="number" string-to-number  class="form-control" name="puntuacion" ng-model="actividad.puntuacion"  title="Valor" required>
                    <span class="messages" ng-show="form.$submitted || form.name.$touched">
                    <span class="" ng-show="form.puntuacion.$error.required">Puntuación requerida</span>
		        </div>
	
			    <div class="form-group col-xs-10 col-xs-offset-1">
                    <span class="required">*</span><label class="control-label">Descripción:</label>
                     <!-- <textarea class="form-control" name="descripcion" ng-model="actividad.descripcion" rows="3"></textarea>-->
                    <div text-angular="text-angular" name="descripcion" ng-model="actividad.descripcion"  ta-disabled='disabled' required></div>
                    <span class="messages" ng-show="form.$submitted || form.name.$touched">
                    <span class="" ng-show="form.descripcion.$error.required">Descripción de la actividad requerida</span>
		        </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
          <button type="submit" class="btn btn-success" ng-click="save(form)">Guardar</button>
        </div>  
  </form>

</script>