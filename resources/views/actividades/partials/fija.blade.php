<!-- Modal -->
<script type="text/ng-template" class="modal" id="FijaModal.html">
    <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="FijaModalLabel">Actividades fijas</h4>
    </div>
    <form class="form" name="form" ng-cloak novalidate>
        {{ csrf_field() }}

        <div class="modal-body">

            <div class="row">
                <div class="form-group col-xs-11">
                    <label class="control-label pull-right">Campos requeridos(<span class="required ">*</span>)</label>
                </div>
				<div class="form-group col-sm-7 col-sm-offset-1 col-xs-12">
				    <span class="required">*</span><label>Actividad</label>
		            <input type="text" class="form-control" name="nombre" ng-model="actividad.nombre"  title="título" required>
                    <span class="messages" ng-show="form.$submitted || form.name.$touched">
                    <span class="" ng-show="form.nombre.$error.required">Nombre de actividad requerido</span>
		        </div>
				<div class="form-group col-sm-3 col-xs-12">
				    <span class="required">*</span><label>Puntuación</label>
		            <input type="number" string-to-number  class="form-control" name="puntuacion" ng-model="actividad.puntuacion"  title="Valor" required>
                    <span class="messages" ng-show="form.$submitted || form.name.$touched">
                    <span class="" ng-show="form.puntuacion.$error.required">Puntuación requerida</span>
		        </div>
	
			    <div class="form-group col-sm-10 col-sm-offset-1 col-xs-12">
					<span class="required">*</span><label class="control-label">Descripción:</label>
                    <div text-angular="text-angular" name="descripcion" ng-model="actividad.descripcion"  ta-disabled='disabled' required></div>
                    <span class="messages" ng-show="form.$submitted || form.name.$touched">
                    <span class="" ng-show="form.descripcion.$error.required">Descripción de la actividad requerida</span>
		        </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
          <button type="submit" class="btn btn-success" ng-click="save(form)">Guardar</button>
        </div>  
  </form>

</script>