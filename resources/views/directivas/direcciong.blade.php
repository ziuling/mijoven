@extends('layouts.app')

@section('content')
 
		
	@if (session('status'))
		<div class="alert alert-success">
		    {{ session('status') }}
		</div>
	@endif

	<section id="main-content">
        <section class="wrapper">
		    <div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-th-list"></i>Gestionar Identidad</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="home">Inicio</a></li>
						<li><i class="fa fa-table"></i><a href="directivas">Directiva</a></li>
						<li><i class="fa fa-table"></i>Identidad grupo</li>
					</ol>
				</div>
			</div>
              <!-- page start-->
                <div class="row">
                 
                    <div class="col-lg-12">
                        <section class="panel" ng-controller="GrupoCtrl">
                          <header class="panel-heading">
                          </header>
                            <table class="table table-striped table-advance table-hover">
							    <thead>
								    <th>Nombre de Grupo pequeño Salvación y Servicio</th>
								    <th>Dirección de Reunión</th>
								    <th>Acciones</th>
							    </thead>
							    <tbody>
								    <tr ng-repeat="grupo in grupos">
									    <td ng-bind="grupo.nombre"></td>
										<td ng-bind="grupo.direccion"></td>
										<td>
											<div class="btn-group">
												<button class="btn btn-primary" type="button" ng-click="editname(grupo)"><pan class="glyphicon glyphicon-edit"></span></button>
											    <button class="btn btn-eliminar" type="button" ng-click="destroy(grupo)"><i class="icon_close_alt2"></i></button>
										    </div>
										</td>
									</tr>
									<tr ng-if="grupos.length < 1"><td colspan="6"><h5 class="text-center">Sin registros</h5></td></tr>
								</tbody>
						    </table>
						    <button class="btn btn-enviar pull-right" type="button" ng-click="createGrupo()"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
				            @include('directivas.partials.direccionG')
					
		                </section>

                    </div>
                </div>
        </section>
    </section>
@endsection
@section('script')
	<script type="text/javascript" src="{{asset('app/GrupoCtrl.js')}}"></script>
@endsection