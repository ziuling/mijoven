@extends('layouts.app')

@section('content')
   
	@if (session('status'))
	    <div class="alert alert-success">
			{{ session('status') }}
	    </div>
    @endif

    <section id="main-content">
        <section class="wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-th-list"></i>Gestionar Directiva</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="home">Inicio</a></li>
						<li><i class="fa fa-table"></i>Directiva</li>
					</ol>
				</div>
			</div>
              <!-- page start-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-header">
						<!-- Nav tabs it-->
						<ul class="nav nav-tabs" role="tablist">
						    <li role="presentation" class="active">
						        <a href="#Directivas" aria-controls="Directivas" role="tab" data-toggle="tab">Directiva</a>
						    </li>
						    <li role="presentation">
						        <a href="#DirectivaGpss" aria-controls="DirectivaGpss" role="tab" data-toggle="tab">Directiva de Grupos pequeños Salvación y Servicio (Gpss)</a>
						    </li>
						</ul>
				    </div>
				    <div class="tab-content">
					    <div role="tabpanel" class="tab-pane active" id="Directivas" ng-controller="DirectivaCtrl">
							<table class="table table-striped table-advance table-hover">
								<thead>
									<th>Nombre</th>
									<th>Cargo</th>
									<th>Email</th>
									<th>Telefono</th>
									<th>Dirección</th>
									<th>Acciones</th>
								</thead>
								<tbody>
									<tr ng-repeat="directiva in directivas" ng-if="directiva.t == 'o'">
										<td ng-bind="directiva.persona.nombre"></td>
										<td ng-bind="directiva.cargo.nombre"></td>
										<td ng-bind="directiva.persona.correo"></td>
										<td ng-bind="directiva.persona.telefono"></td>
										<td ng-bind="directiva.persona.direccion"></td>
										<td>
											<div class="btn-group">
												<button class="btn btn-primary" type="button" ng-click="edit(directiva)"><span class="glyphicon glyphicon-edit"></span></button>
												<button class="btn btn-eliminar" type="button" ng-click="destroy(directiva)"><i class="icon_close_alt2"></i></button>
										    </div>		
										</td>
									</tr>
									<tr ng-if="directivas.length <1"><td colspan="6"><h5 class="text-center">Sin registros</h5></td></tr>
								</tbody>
							</table>
							
							<button class="btn btn-enviar pull-right" type="button" ng-click="create()"><span class="glyphicon glyphicon-plus"></span> Nuevo miembro de directiva</button>
							@include('directivas.partials.directiva')
			                @rol("Secretario(a) de Club")
							<a href="identidades" class="pull-right" style="margin-top: 12px; margin-right: 5px;"> Gestionar Identidad</a>
			                @endrol
							
					    </div>

					    <div role="tabpanel" class="tab-pane" id="DirectivaGpss" ng-controller="DirectivaCtrl">

							<table class="table table-striped table-advance table-hover">
								<thead>
									<th>Nombre</th>
									<th>Cargo</th>
									<th>Grupo</th>
									<th>Acciones</th>
								</thead>
								<tbody>
								    <tr ng-repeat="directiva in directivas" ng-if="directiva.t == 'g' ">
										<td ng-bind="directiva.persona.nombre"></td>
										<td ng-bind="directiva.cargo.nombre"></td>
										<td ng-bind="directiva.grupo.nombre"></td>
										<td>
											<div class="btn-group">
												<button class="btn btn-primary" type="button" ng-click="editGpss(directiva)"><span class="glyphicon glyphicon-edit"></span></button>
												<button class="btn btn-eliminar" type="button" ng-click="destroy(directiva)"><i class="icon_close_alt2"></i></button>
											</div>	
										</td>
		                                
									</tr>	
									<tr ng-if="directivas.length <1"><td colspan="6"><h5 class="text-center">Sin registros</h5></td></tr>
								</tbody>
							</table>
			                
							<button class="btn btn-enviar pull-right" type="button" ng-click="createGpss()"><span class="glyphicon glyphicon-plus"></span> Nuevo miembro de directiva</button>
							<a href="direcciong" class="pull-right" style="margin-top: 12px; margin-right: 5px;"> Gestionar Identidad</a>
							@include('directivas.partials.directivaGpss')
					    </div> 
			        </div>
			    </div>  
            </div>
        </section>
    </section>

@endsection
@section('script')
	<script type="text/javascript" src="{{asset('app/DirectivaCtrl.js')}}"></script>
@endsection