<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="pdf.css" media="all" />

	<style type="text/css">
	    html{
			margin-left: -100px; 
		}
		.e{
			margin-left: 10px;
			margin-right: -100px;
		}
		.text-center{
			text-align: center;
	        display: block;
	        width: 100%;
		}
		.text-right{
			text-align: right;
	        display: block;
	        width: 100%;
		}
	    @font-face {
		  font-family: Junge;
		  src: url(Junge-Regular.ttf);
		}

		.clearfix:after {
		  content: "";
		  display: table;
		  clear: both;
		}

		a {
		  color: #001028;
		  text-decoration: none;
		}

		body {
		  font-family: Junge;
		  position: relative;
		  width: 21cm;  
		  height: 29.7cm; 
		  margin: 0 auto; 
		  color: #001028;
		  background: #FFFFFF; 
		  font-size: 14px; 
		}

		.arrow {
		  margin-bottom: 4px;
		}

		.arrow.back {
		  text-align: right;
		}

		.inner-arrow {
		  padding-right: 10px;
		  height: 30px;
		  display: inline-block;
		  background-color: rgb(233, 125, 49);
		  text-align: center;

		  line-height: 30px;
		  vertical-align: middle;
		}

		.arrow.back .inner-arrow {
		  background-color: rgb(233, 217, 49);
		  padding-right: 0;
		  padding-left: 10px;
		}

		.arrow:before,
		.arrow:after {
		  content:'';
		  display: inline-block;
		  width: 0; height: 0;
		  border: 15px solid transparent;
		  vertical-align: middle;
		}

		.arrow:before {
		  border-top-color: rgb(233, 125, 49);
		  border-bottom-color: rgb(233, 125, 49);
		  border-right-color: rgb(233, 125, 49);
		}

		.arrow.back:before {
		  border-top-color: transparent;
		  border-bottom-color: transparent;
		  border-right-color: rgb(233, 217, 49);
		  border-left-color: transparent;
		}

		.arrow:after {
		  border-left-color: rgb(233, 125, 49);
		}

		.arrow.back:after {
		  border-left-color: rgb(233, 217, 49);
		  border-top-color: rgb(233, 217, 49);
		  border-bottom-color: rgb(233, 217, 49);
		  border-right-color: transparent;
		}

		.arrow span { 
		  display: inline-block;
		  width: 80px; 
		  margin-right: 20px;
		  text-align: right; 
		}

		.arrow.back span { 
		  margin-right: 0;
		  margin-left: 20px;
		  text-align: left; 
		}

		h1 {
		  color: #5D6975;
		  font-family: Junge;
		  font-size: 2.4em;
		  line-height: 1.4em;
		  font-weight: normal;
		  text-align: center;
		  border-top: 1px solid #5D6975;
		  border-bottom: 1px solid #5D6975;
		  margin: 0 0 2em 0;
		}

		h1 small { 
		  font-size: 0.45em;
		  line-height: 1.5em;
		  float: left;
		} 

		h1 small:last-child { 
		  float: right;
		} 

		#project { 
		  float: left; 
		}

		#company { 
		  float: right; 
		}

		table {
		  width: 100%;
		  border-collapse: collapse;
		  border-spacing: 0;
		  margin-bottom: 30px;
		}

		table th,
		table td {
		  text-align: center;
		}

		table th {
		  padding: 5px 20px;
		  color: #5D6975;
		  border-bottom: 1px solid #C1CED9;
		  white-space: nowrap;        
		  font-weight: normal;
		}

		table .service,
		table .desc {
		  text-align: left;
		}

		table td {
		  padding: 10px;
		  text-align: right;
		}

		table td.service,
		table td.desc {
		  vertical-align: top;
		}

		table td.unit,
		table td.qty,
		table td.total {
		  font-size: 1.2em;
		}

		table td.sub {
		  border-top: 1px solid #C1CED9;
		}

		table td.grand {
		  border-top: 1px solid #5D6975;
		}

		table tr:nth-child(2n-1) td {
		  background: #EEEEEE;
		}

		table tr:last-child td {
		  background: #DDDDDD;
		}

		#details {
		  margin-bottom: 30px;
		}

		footer {
		  color: #5D6975;
		  width: 100%;
		  height: 30px;
		  position: absolute;
		  bottom: 0;
		  border-top: 1px solid #C1CED9;
		  padding: 8px 0;
		  text-align: center;
		}
	</style>
	<title>Grupo pequeño Salvación y Servicio</title>
</head>
<body>
    <main>

   {{--  ENCABEZADO  --}}
    <div class="e" style="width:100%;display: block; padding-bottom: 60px;">
		<img style="margin-left: 360px; margin-top: 20px;" src="/xampp/htdocs/mijoven/public/img/mvnor.jpg" width="150" height="130">
	{{-- <img style="margin-left: 200px;" src="/xampp/htdocs/mijoven/public/img/mojoven.png" width="100" height="90"> --}}
		<span class="text-center" style="font-size: 20px;">Misión Venezolana Nor-Oriental</span>
		<strong class="text-center"  style="font-size: 18px;">Planilla por grupo pequeño salvación y servicio</strong>
    </div>   
    <div class="row e" style=" padding: 5px; ">
    	<strong style="color:; font-size: 16px; ">Nombre de la Iglesia:</strong><span style=" color:	#20B2AA;  margin-left: 5px; font-size: 16px; ">{{ $grupo->club->iglesia->nombre }}</span>

    	<strong style="color:; font-size: 16px; text-align: right; width: 100%; display: block;">Anciano Responsable:
        @if($anciano == null)
    	<span>Sin definir</span>
    	@else
    	<span style=" color:	#20B2AA;  font-size: 16px; margin-left: 5px;" >
    	 {{ $anciano->persona->nombre}} <span>
    	@endif 
    	</strong>
    </div>
    <div class="row e" style=" padding: 5px; ">
    	<strong style="color:; font-size: 16px; text-align: left; width: 100%; display: block;">Líder Responsable:
    	@if($lider == null)
    	<span>Sin definir</span>
    	@else
    	<span style=" color:	#20B2AA;  font-size: 16px; margin-left: 5px;" >
    	 {{ $lider->persona->nombre}} <span>
    	@endif 
    	</strong>
    	<strong style="color:; font-size: 16px; text-align: right; width: 100%; display: block;">Líder Asociado Responsable:
    	@if($anciano == null)
    	<span>Sin definir</span>
    	@else
    	<span style=" color:	#20B2AA;  font-size: 16px; margin-left: 5px;" >
    	 {{ $lidera->persona->nombre}} <span>
    	@endif 
    	</strong>
    </div>
    <div class="row e" style=" padding: 5px; ">
    	<strong style="color:; font-size: 16px; ">Nombre de Grupo pequeño:</strong><span style=" color:	#20B2AA;  font-size: 16px; margin-left: 5px;" >{{$grupo->nombre }}</span>
    	
    </div>
    <div class="row " style="margin-top: 30px;">
		<table >
		    <thead >
		        <tr  style="background-color:#778899; ">
		           <th class="desc" ><span style="color:black; font-size: 12px;">N</span></th>
				    <th class="desc" ><span style="color:black; font-size: 12px;">Nombres</span></th>
				    <th class="desc" ><span style="color:black; font-size: 12px;">Fecha de nacimiento</span></th>
				    <th class="desc" ><span style="color:black; font-size: 12px;">Edad</span></th>
				    <th class="desc" ><span style="color:black;   font-size: 12px;">Investiduras Alcanzadas</span></th>
				    <th class="desc" ><span style="color:black;   font-size: 12px;">Telefono</span></th>
				    <th class="desc" ><span style="color:black;   font-size: 12px;">Dirección</span></th>
				</tr>
		    </thead>
			<tbody>
			    @if($cantidad === 0)
                    <div class="info">
    	    	       <h3 class="text-center" style="margin-top: 50px;">SIN REGISTROS</h3>
    	            </div>
			    @else
		        @foreach($mie as $key => $m)
                <tr  style="background-color:; ">
		           <th class="desc" ><span style="color:black; font-size: 10px;"> {{ $key + 1 }}</span></th>
				    <th class="desc" ><span style="color:black; font-size: 10px;">{{ $m->persona->nombre }}</span></th>
				    <th class="desc" ><span style="color:black; font-size: 10px;"> {{ $m->curso }} </span></th>
				    <th class="desc" ><span style="color:black;   font-size: 10px;">
				    @foreach($lecciones as $l)
				    @if($l->miembro_id == $m->id)
				    <span>-</span> {{ $l->leccion_id }} <span>-</span>
				    @endif
				    @endforeach
				     </span></th>
				    <th class="desc" ><span style="color:black;   font-size: 10px;"> {{$m->persona->telefono }} </span></th>
				    <th class="desc" ><span style="color:black;   font-size: 10px;"> {{ $m->responsable }} </span></th>
				    <th class="desc" ><span style="color:black;   font-size: 10px;"> {{ $m->categoria }} </span></th>
				    <th class="desc" ><span style="color:black;   font-size: 10px;"> {{ $m->necesidad }} </span></th>
				    <th class="desc" ><span style="color:black;   font-size: 10px;"> {{ $m->observacion }} </span></th>
		    	   </th>
				</tr>
				@endforeach    
                @endif
			</tbody>
		</table>

    </div>
      
    </main>
    <footer class="e" >
      @if($rol == 4)
           Club de {{$grupo->club->categoria->nombre}} {{ $grupo->club->nombre}}
        @elseif($rol == 6)
          Sociedad de Jóvenes de la iglesia {{$grupo->iglesia->nombre}} 
        @endif
    </footer>
 
</body>
</html>

   
