<!-- Modal -->
<script type="text/ng-template" id="GruponameModal.html">
  <div class="modal-header">
    <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="GruponameModalLabel">Grupo</h4>
  </div>
  <form class="form" name="form" ng-cloak novalidate>
    {{ csrf_field() }}
    <div class="modal-body">
      <div class="row">
        <div class="form-group col-xs-11">
          <label class="control-label pull-right">Campo requerido(<span class="required ">*</span>)</label>
        </div>
        <div  class="form-group col-xs-8 col-xs-offset-1">
          <span class="required">*</span><label title="">Nombre de Gpss:</label>
          <input type="text" class="form-control" ng-model="grupo.nombre" name="nombre" required>
          <span class="messages" ng-show="form.$sub || mittedform.name.$touched">
          <span class="" ng-show="form.nombre.$error.required">Nombre requerido</span>
        </div>
        <div class="form-group col-xs-8 col-xs-offset-1">
          <span class="required">*</span><label title="">Dirección de reunión:</label>
          <input type="text" class="form-control" ng-model="grupo.direccion" name="direccion" minlength="10"  required>
          <span class="messages" ng-show="form.$sub || mittedform.name.$touched">
          <span class="" ng-show="form.direccion.$error.required">Dirección requerido</span>
          <span class="" ng-show="form.direccion.$error.minlength">Mínimo 10 carácteres</span>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
      <button type="submit" class="btn btn-success" ng-click="save(form)">Guardar</button>
    </div>
  </form>
</script>