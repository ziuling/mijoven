<!-- Modal -->
<script type="text/ng-template" id="DirectivaGpssModal.html">
      <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="DirectivaModalLabel">Miembro de directiva</h4>
      </div>
      <form class="form" name="form" ng-cloak novalidate>
        {{ csrf_field() }}
        <div class="modal-body">
          <div class="row">
            <div class="form-group col-xs-11">
              <label class="control-label pull-right">Campos requeridos(<span class="required ">*</span>)</label>
            </div>
            <div class="col-xs-8 col-xs-offset-1">
              <div class="form-group">
                <span class="required">*</span><label class="control-label">Grupo pequeño:</label>
                  <select class="form-control" name="grupo" ng-model="directiva.grupo_id" ng-options="grupo.id as grupo.nombre for grupo in grupos" required></select>
                  <span class="messages" ng-show="form.$submitted || form.name.$touched">
                  <span class="" ng-show="form.grupo.$error.required">Grupo requerido</span>
              </div>
            </div>
            <div class="form-group col-xs-4 col-xs-offset-1">
              <span class="required">*</span><label title="">Cédula:</label>
              <input type="text" class="form-control" ng-model="directiva.persona.cedula" name="cedula" title="Inserte cédula" required>
              <span class="messages" ng-show="form.$submitted || form.name.$touched">
              <span class="" ng-show="form.cedula.$error.required">Cédula requerida</span>
            </div>
            <div class="form-group col-xs-6">
              <span class="required">*</span><label title="">Nombre:</label>
              <input type="text" class="form-control" ng-model="directiva.persona.nombre" name="nombre" title="Inserte Nombre y Apellido" required>
              <span class="messages" ng-show="form.$submitted || form.name.$touched">
              <span class="" ng-show="form.nombre.$error.required">Nombre requerido</span>
            </div>
            <div class="form-group col-xs-4 col-xs-offset-1">
              <label title="">Telefono:</label>
              <input type="text" class="form-control" ng-model="directiva.persona.telefono" name="telefono" >
            </div>
            <div class="form-group col-xs-6">
              <span class="required">*</span><label title="">Correo Electrónico:</label>
              <input type="email" class="form-control" ng-model="directiva.persona.correo" name="correo" required>
              <span class="messages" ng-show="form.$submitted || form.name.$touched">
              <span class="" ng-show="form.correo.$error.required">Correo electrónico requerido</span>
              <span class="" ng-show="form.correo.$error.email">no tiene formato email</span>
            </div>
            <div class="form-group col-xs-10 col-xs-offset-1">
              <label title="">Dirección:</label>
              <input type="text" class="form-control" ng-model="directiva.persona.direccion" name="direccion">
            </div> 
            <div class="form-group col-xs-10 col-xs-offset-1">
                <span class="required">*</span><label class="control-label">Cargo:</label>
                <select class="form-control" name="cargo" ng-model="directiva.cargo_id" ng-options="cargo.id as cargo.nombre for cargo in cargos" required></select>
              </select>
              <span class="messages" ng-show="form.$submitted || form.name.$touched">
              <span class="" ng-show="form.cargo.$error.required">Cargo requerido</span>
            </div>  
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
          <button type="submit" class="btn btn-success" ng-click="save(form)">Guardar</button>
        </div>
      </form>
</script>