@extends('layouts.app')

@section('content')

  @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif

  <section id="main-content" ng-controller="MiembroCtrl">
    <section class="wrapper">
      <div class="row">
        <div class="col-lg-12">
          <h3 class="page-header"><i class="fa fa-th-list"></i>Gestionar Miembros</h3>
          <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="home">Inicio</a></li>
            <li><i class="fa fa-table"></i>Miembros</li>
          </ol>
        </div>
      </div>
      <!-- page start-->
      <div class="row">   
        <div class="col-lg-12">
          <div class="page-header">
          <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active">
                <a href="#Miembro" aria-controls="Miembro" role="tab" data-toggle="tab">Miembros</a>
              </li>
              <li role="presentation">
                <a href="#MiembroGpss" aria-controls="MiembroGpss" role="tab" data-toggle="tab">Miembros de Grupo pequeños Salvación y Servicio (Gpss)</a>
              </li>
            </ul>
          </div>

          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="Miembro" >

                <table class="table table-striped table-advance table-hover">
                    <thead>
                      <th>Nombre</th>
                      <th>Correo</th>
                      <th>Direccion</th>
                      <th>Acciones</th>
                    </thead>

                    <tbody>
                      <tr ng-repeat="miembro in miembros" ng-if="miembro.t == 'o'">
                        <td ng-bind="miembro.persona.nombre"></td>
                        <td ng-bind="miembro.persona.correo"></td>
                        <td ng-bind="miembro.persona.direccion"></td>
                        <td>
                          <div class="btn-group">
                            <button class="btn btn-primary" type="button" ng-click="edit(miembro)"><span class="glyphicon glyphicon-edit"></span></button>
                            <button class="btn btn-eliminar" type="button" ng-click="destroy(miembro)"><i class="icon_close_alt2"></i></button>
                          </div>
                        </td>
                      </tr>
                      <tr ng-if="miembros.length <1"><td colspan="4"><h5 class="text-center">Sin registros</h5></td></tr>
                    </tbody>
                </table>
                <button class="btn btn-enviar pull-right" type="button" ng-click="create()"><span class="glyphicon glyphicon-plus"></span> Nuevo miembro</button>
                @include("miembros.partials.miembro")
            </div>
            <div role="tabpanel" class="tab-pane " id="MiembroGpss" >
              
              <table class="table table-striped table-advance table-hover">
                <thead>
                  <th>Nombre</th>
                  <th>Grupo pequeño</th>
                  <th>Curso</th>
                  <th>Acciones</th>
                </thead>
                <tbody>
                  <tr ng-repeat="miembro in miembros" ng-if="miembro.t == 'g'">
                    <td ng-bind="miembro.persona.nombre"></td>
                    <td ng-bind="miembro.grupo.nombre "></td>
                    <td ng-bind="miembro.curso "></td>
                    <td>
                      <div class="btn-group">
                        <button class="btn btn-primary" type="button" ng-click="editGpss(miembro)"><span class="glyphicon glyphicon-edit"></span></button>
                        <button class="btn btn-eliminar" type="button" ng-click="destroy(miembro)"><i class="icon_close_alt2"></i></button>
                      </div>
                    </td>
                  </tr>
                  <tr ng-if="miembros.length <1"><td colspan="4"><h5 class="text-center">Sin registros</h5></td></tr>
                </tbody>
              </table>
              <button class="btn btn-enviar pull-right" type="button" ng-click="createGpss()"><span class="glyphicon glyphicon-plus"></span> Nuevo miembro</button>
              @include("miembros.partials.miembroGpss")

            </div>
          </div>
        </div>
      </div>
    </section>
  </section>

@endsection
@section('script')
  <script type="text/javascript" src="{{asset('app/MiembroCtrl.js')}}"></script>
@endsection