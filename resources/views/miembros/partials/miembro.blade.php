<!-- Modal -->
<script  class="modal" type="text/ng-template" id="MiembroModal.html">
  <div class="modal-header">
    <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="MiembroModalLabel">Miembro</h4>
  </div>
  <form class="form" name="form" ng-cloak novalidate>
    {{ csrf_field() }}
    <div class="modal-body">
        <div class="row">
          <div class="form-group col-xs-11">
            <label class="control-label pull-right">Campos requeridos(<span class="required ">*</span>)</label>
          </div>
          <div class="form-group col-xs-4 col-xs-offset-1">
            <span class="required">*</span><label title="">Cédula:</label>
            <input type="text" class="form-control" ng-model="miembro.persona.cedula" name="persona.cedula" required>
            <span class="messages" ng-show="form.$submitted || form.name.$touched">
            <span class="" ng-show="form.cedula.$error.required">Cédula requerida</span> 
          </div>
          <div class="form-group col-xs-6">
            <span class="required">*</span><label title="">Nombre completo:</label>
            <input type="text" class="form-control" ng-model="miembro.persona.nombre" name="persona.nombre" required>
            <span class="messages" ng-show="form.$submitted || form.name.$touched">
            <span class="" ng-show="form.nombre.$error.required">Nombre requerido</span> 
          </div>
          <div class="form-group col-xs-4 col-xs-offset-1">
            <label title="">Telefono:</label>
            <input type="text" class="form-control" ng-model="miembro.persona.telefono" name="persona.telefono">
          </div> 
          <div class="form-group col-xs-6">
            <span class="required">*</span><label title="">Correo Electrónico:</label>
            <input type="email" class="form-control" ng-model="miembro.persona.correo" name="persona.correo" required>
            <span class="messages" ng-show="form.$submitted || form.name.$touched">
            <span class="" ng-show="form.correo.$error.required">Correo electrónico requerido</span>
            <span class="" ng-show="form.correo.$error.email">No tiene formato email</span>
          </div>
          <div class="form-group col-xs-8 col-xs-offset-1">
            <label title="">Dirección:</label>
            <input type="text" class="form-control" ng-model="miembro.persona.direccion" name="persona.direccion">
          </div>
          <div class="form-group col-xs-7 col-xs-offset-1">
            <label class="control-label">Fecha de nacimiento:</label>
            <input type="date" class="form-control" ng-model="miembro.persona.fecha_nac" name="persona.fecha_nac"  date-format/>
          </div>
          <div class="form-group col-xs-3">
            <label class="control-label">Edad:</label>
           
          </div>
          <div class="form-group col-xs-10 col-xs-offset-1">
            <label class="control-label">Tipo de sangre:
            <input type="text" ng-model="miembro.tipo_sangre" name="tipo_sangre" class="form-control" title="Tipo de sangre"></label>
          </div>
          <div class="form-group col-xs-10 col-xs-offset-1">
            <span class="required">*</span><label class="control-label">¿Dona sangre? </label>
            <label class="radio-inline" style="margin-left:8px;">
            <input type="radio" name="donante" ng-model="miembro.donante"  value="1">Si</label>
            <label class="radio-inline" style="margin-left:8px;">
            <input type="radio" name="donante" ng-model="miembro.donante" required value="0">No</label>
            <span class="messages" ng-show="form.$submitted || form.name.$touched">
            <span class="" ng-show="form.donante.$error.required">Campo requerido</span> 
          </div> 
          <div class="form-group col-xs-10 col-xs-offset-1">
            <span class="required">*</span><label class="control-label">¿Voluntario de Cerca De Ti?</label>
            <label class="radio-inline" style="margin-left:8px;">
            <input type="radio"  name="voluntario" ng-model="miembro.voluntario"  id="voluntario" value="1">Si</label>
            <label class="radio-inline" style="margin-left:8px;">
            <input type="radio" name="voluntario" ng-model="miembro.voluntario" required id="voluntario" value="0">No</label>
            <span class="messages" ng-show="form.$submitted || form.name.$touched">
            <span class="" ng-show="form.voluntario.$error.required">Campo requerido</span> 
          </div>
          <div class="form-group col-xs-10 col-xs-offset-1">
            <label class="control-label">Investiduras alcanzadas:</label>
            <select multiple class="form-control" name="investidura_id" ng-model="miembro.miembro_investidura.investidura_id" ng-options="investidura.id as investidura.nombre for investidura in investiduras"></select> 
          </div>  
          <div class="col-xs-8 col-xs-offset-1">
            <label class="control-label">Ministerios juveniles:</label>
            <label class="radio-inline" style="margin-bottom:8px;">
            <input type="radio"  name="ministerio" ng-model="miembro.ministerio" value="Coro">Coro</label>
            <label class="radio-inline" style="margin-bottom:8px;">
            <input type="radio"  name="ministerio" ng-model="miembro.ministerio" value="Orquesta">Orquesta</label>
            <label class="radio-inline" style="margin-bottom:8px;">
            <input type="radio"  name="ministerio" ng-model="miembro.ministerio" value="Banda">Banda</label>
            <label class="radio-inline" style="margin-bottom:8px;">
            <input type="radio"  name="ministerio" ng-model="miembro.ministerio" value="Otro">Otro</label>
          </div>
        </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
      <button type="submit" class="btn btn-success" ng-click="save(form)">Guardar</button>
    </div>
  </form>
</script>