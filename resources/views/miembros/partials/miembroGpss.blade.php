<!-- Modal -->
<script class="modal" type="text/ng-template" id="MiembroGpssModal.html">
      <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="MiembroGpssModalLabel">Miembro de Grupo pequeño salvación y servicio</h4>
      </div>
      <form class="form" name="form" ng-cloak novalidate>
        {{ csrf_field() }}
        <div class="modal-body">
          <div class="row" style="padding-bottom: 20px">
            <div class="form-group col-xs-11">
              <label class="control-label pull-right">Campos requeridos(<span class="required ">*</span>)</label>
            </div>
            <div class="col-xs-10 col-xs-offset-1">
              <div class="form-group">
                <span class="required">*</span><label class="control-label ">Grupo pequeño:</label>     
                <select class="form-control" name="grupo" ng-model="miembro.grupo_id" ng-options="grupo.id as grupo.nombre for grupo in grupos" required></select>  
                <span class="messages" ng-show="form.$sub || mittedform.name.$touched">
                <span class="" ng-show="form.grupo.$error.required">Grupo requerido</span>               
              </div>
            </div>       
              <div class="form-group col-xs-6 col-xs-offset-1">
			      	  <span class="required">*</span><label class="control-label">Curso:</label>
                <input type="text" name="curso" ng-model="miembro.curso" class="form-control" required>
                <span class="messages" ng-show="form.$sub || mittedform.name.$touched">
                <span class="" ng-show="form.curso.$error.required">Curso requerido</span>
              </div>
              <div class="form-group col-xs-4">
			      	  <span class="required">*</span><label class="control-label">Cédula:</label>
                <input type="text" name="cedula" ng-model="miembro.persona.cedula" class="form-control" required>
                <span class="messages" ng-show="form.$sub || mittedform.name.$touched">
                <span class="" ng-show="form.cedula.$error.required">Cédula requerida</span>
              </div>
              <div class="form-group col-xs-6  col-xs-offset-1 ">
			      	  <span class="required">*</span><label class="control-label">Nombre completo:</label>
                <input type="text" name="nombre" ng-model="miembro.persona.nombre" class="form-control" required>
                <span class="messages" ng-show="form.$sub || mittedform.name.$touched">
                <span class="" ng-show="form.nombre.$error.required">Nombre requerido</span>
              </div>
              <div class="form-group col-xs-4">
			      	  <label class="control-label">Telefono:</label>
                <input type="text" name="telefono" ng-model="miembro.persona.telefono" class="form-control">
              </div>
			        <div class="form-group col-xs-5 col-xs-offset-1">
				        <span class="required">*</span><label class="control-label">Correo Electrónico:</label>
                <input type="email" name="correo" ng-model="miembro.persona.correo" class="form-control" required>
                <span class="messages" ng-show="form.$submitted || form.name.$touched">
                <span class="" ng-show="form.correo.$error.required">Correo electrónico requerido
                <span class="" ng-show="form.correo.$error.email">No tiene formato email</span>
              </div>
              <div class="form-group col-xs-5">
				        <label class="control-label">Dirección</label>
                <input type="text" name="direccion" ng-model="miembro.persona.direccion" class="form-control">
              </div>
			        <div class="form-group col-xs-10 col-xs-offset-1">
				        <label class="control-label">Necesidad:</label>
                <input type="text" name="necesidad" ng-model="miembro.necesidad" class="form-control">
              </div>
			              
              <div class="form-group col-xs-6 col-xs-offset-1">
			      	  <label class="control-label">Fecha de Nacimiento:</label>
                <input type="date" name="fecha_nac" ng-model="miembro.persona.fecha_nac" class="form-control" date-format/>
              </div>
              <div class="form-group col-xs-4">
				        <label>Edad:</label> 
                <input type="hidden" name="" placeholder="Edad->">
              </div>
              <div class="form-group col-xs-10 col-xs-offset-1">
                <label class="control-label">Tipo de sangre:
                <input type="text" ng-model="miembro.tipo_sangre" name="tipo_sangre" class="form-control" title="Tipo de sangre"></label>
              </div>
              <div class="form-group col-xs-10 col-xs-offset-1">
			          <label class="control-label">Observación:</label>
                <label class="radio-inline">
                <input type="radio"  name="observación" ng-model="miembro.observacion"  value="A">A</label>
                <label class="radio-inline">
                <input type="radio" name="observacion" ng-model="miembro.observacion"  value="B">B</label>
                <label class="radio-inline">
                <input type="radio" name="observacion" ng-model="miembro.observacion"  value="C">C</label>
              </div>   
              <div class="form-group col-xs-10 col-xs-offset-1">
                <span class="required">*</span><label class="control-label" >Categoría:</label>
                <label class="radio-inline" title="Niño">
                <input type="radio"  name="categoria" ng-model="miembro.categoria" value="Niño" title="Niño">N</label>
                <label class="radio-inline" title="Joven">
                <input type="radio" name="categoria" ng-model="miembro.categoria" value="Joven" title="Joven">J</label>
                <label class="radio-inline" title="Adulto">
                <input type="radio" name="categoria" ng-model="miembro.categoria" value="Adulto" title="Adulto">A</label>
                <label class="radio-inline" title="Ex Adventista">
                <input type="radio" name="categoria" ng-model="miembro.categoria" value="Ex Adventista" title="Ex Adventista" required="">E.A.</label>
                <span class="messages" ng-show="form.$submitted || form.name.$touched">
                <span class="" ng-show="form.categoria.$error.required">Categoría requerida</span>
              </div>
			        <div class="form-group col-xs-10 col-xs-offset-1">
                  <span class="required">*</span><label class="control-label">¿Voluntario de Cerca De Ti?</label>
                  <label class="radio-inline">
                  <input type="radio"  name="voluntario" ng-model="miembro.voluntario" value="1">Si</label>
                  <label class="radio-inline">
                  <input type="radio" name="voluntario" ng-model="miembro.voluntario" value="0" required>No</label> 
			            <span class="messages" ng-show="form.$submitted || form.name.$touched">
                  <span class="" ng-show="form.voluntario.$error.required">Campo requerido</span>
              </div>
              <div class="form-group col-xs-10 col-xs-offset-1">
                  <span class="required">*</span><label class="control-label" >¿Donante de sangre? </label>
                  <label class="radio-inline">
                  <input type="radio" name="donante" ng-model="miembro.donante" value="1">Si</label>
			            <label class="radio-inline">
                  <input type="radio" name="donante" ng-model="miembro.donante" value="0" required>No</label>  
                  <span class="messages" ng-show="form.$submitted || form.name.$touched">
                  <span class="" ng-show="form.donante.$error.required">Campo requerido</span>       
			        </div>

			        <div class="form-group col-xs-10 col-xs-offset-1">
                 <span class="required">*</span><label class="control-label">Lecciones Impartidas:</label>
                <select multiple class="form-control" name="leccion_id" ng-model="miembro.miembro_leccion.leccion_id" ng-options="leccion.id as leccion.numero for leccion in lecciones" required></select> 
                <span class="messages" ng-show="form.$submitted || form.name.$touched">
                <span class="" ng-show="form.leccion_id.$error.required">Lección requerida</span>
              </div>
			    </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
          <button type="submit" class="btn btn-success" ng-click="save(form)">Guardar</button>
        </div>
      </form>
</script>  