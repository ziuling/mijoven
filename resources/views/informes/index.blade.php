@extends('layouts.app')

@section('content')
	
    @if (session('status'))
	      <div class="alert alert-success">
	        {{ session('status') }}
	      </div>
	    @endif
        
        <section id="main-content">
	        <section class="wrapper">
			    <div class="row">
					<div class="col-lg-12">
						<h3 class="page-header"><i class="icon_document_alt"></i> Procesar Informe J.A</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a href="home">Inicio</a></li>
							<li><i class="icon_document_alt"></i>Informes</li>
						</ol>
					</div>
				</div>
				
				<div class="row" ng-controller="InformeCtrl">
				    <div class="col-lg-12">
				        <div class="btn-group" style="margin-top:40px;">
							<button class="btn " ng-class="{'btn-grupo':modulo.id === option, 'btn-default':modulo.id != option}" ng-repeat="modulo in modulos" name="option" ng-click="changeOption(modulo)" ng-bind="modulo.nombre" ></button>
					    </div>
				    </div>
				    <div class="col-lg-12">
						<section class="panel">
	                          <header class="panel-heading">
	                              
	                          </header>
	                        
	                            <table class="table table-striped table-advance table-hover">
									<thead>
										<th>Mes</th>
										<th>Acciones</th>
								    </thead>

								    <tbody>
	                                     <tr ng-repeat="informe in informes">
												<td ng-bind="informe.mes"><td>
												<td>
													<div class="btn-group">
														<button class="btn btn-eliminar" type="button" ng-click="destroy(informe)"><i class="icon_close_alt2"></i></button>
														<button class="btn btn-info" type="button" ng-click="detalle(informe)">Detalles</button>
														@include("informes.partials.detalle")
													</div>

												</td>
											</tr>
											<tr ng-if="informes.length < 1"><td colspan="2"><h5 class="text-center">Sin registros</h5></td></tr>   
									</tbody>

							   </table>
								<a href="historial"><button class="btn btn-historial pull-right" id="historial" ng-click="" style="margin-left:5px"> Historial</button></a>
								<button  class="btn btn-enviar pull-right" type="button" ng-click="create()" ><span class="glyphicon glyphicon-plus"></span> Nuevo informe</button>
								@include('informes.partials.informe')
						</section>
								
					</div>
				</div>
			</section>
        </section>
   
@endsection
@section('script')
	<script type="text/javascript" src="{{asset('app/InformeCtrl.js')}}"></script>
@endsection