<!-- Modal -->
<script type="text/ng-template" class="modal" id="detallesInformeModal.html">
    <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="detallesInformeModalLabel"></h4>
    </div>
      
        <div class="modal-body">
           <div class="row" >
	            <div class="panel panel-primary">
		            <div class="panel-heading">
		             	<strong ng-if="informe.modulo_id == 1 ">Actividades de Discipulado en el mes de:</strong>
		             	<strong ng-if="informe.modulo_id == 2 ">Actividades de Líderazgo en el mes de:</strong>
		             	<strong ng-if="informe.modulo_id == 3 ">Actividades de Evangelismo y Servicio en el mes de:</strong> 
		             	<strong ng-if="informe.modulo_id == 4 ">Actividades Universitarias en el mes de:</strong>   <span ng-bind="informe.mes"></span>
		            </div>
		        </div>    
		        <div class="panel-body">
		            <table id="detalles" class="table table-striped table-bordered table-condensed table-responsive">
			            <thead>
					   	   	<th>ACTIVIDADES REALIZADAS EN EL PLAN DE TRABAJO</th>
					   	   	<th>DESCRIPCIÓN</th>
				   	   	</thead>
				   	   	<tbody>
					   	   	<tr >
					   	   	   	<td >
					   	   	   	<span ng-repeat="inf in informes_act | filter: {informe_id: informe.id }" style="margin-right: 10px;">--@{{ inf.actividad.nombre }}--</span></td>
					   	   	   	<td>@{{ informe.descripcion }}</td>
					   	    </tr>
					   	</tbody>	
				   	</table>
			   	   	
		   	    </div>
	   	   </div> 

   	   </div>
             
	    <div class="modal-footer">
	        <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
        </div>

</script>