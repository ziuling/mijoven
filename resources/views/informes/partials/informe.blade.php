<!-- Modal -->
<script type="text/ng-template" class="modal" id="InformeModal.html">
    <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="InformeModalLabel">Informe de actividades</h4>
    </div>
       <form class="form" name="form" ng-cloak novalidate>
        {{ csrf_field() }}
	        <div class="modal-body">
	            <div class="row">
	                <div class="form-group col-xs-11">
			            <label class="control-label pull-right">Campos requeridos(<span class="required ">*</span>)</label>
			        </div>
			        <input type="hidden" name="modulo_id" ng-model="informe.modulo_id" value="@{{informe.modulo_id}}">
					<div class="form-group col-xs-8 col-xs-offset-2" ng-repeat="modulo in modulos | filter: { id: informe.modulo_id }">
				        <h3 class="text-center">@{{modulo.nombre}}</h3> 
				    </div>
				    <div class="row"> 
					    <div class="form-group col-xs-5 col-xs-offset-1">
			                <span class="required">*</span><label class="control-label">Mes:</label>
			                <select class="form-control"  name="mes" ng-model="informe.mes" required>
		                        <option value=""></option>
		                        <option ng-repeat="mes in meses" value="@{{ mes.name }}">@{{ mes.name }}</option>
		                    </select>
			                <!--<select class="form-control" name="mes" ng-model="informe.mes" requerid>
			                          <?php
			                              $mes=date("n"); 
			                              $rango=12; 
			                              for ($i=$mes;$i<=$mes+$rango;$i++){ 
			                                 $meses=date('F', mktime(0, 0, 0, $i, 1, date("Y") ) );
			                                 if ($meses=="January") $meses="Enero";
			                                 if ($meses=="February") $meses="Febrero";
			                                 if ($meses=="March") $meses="Marzo";
			                                 if ($meses=="April") $meses="Abril";
			                                 if ($meses=="May") $meses="Mayo";
			                                 if ($meses=="June") $meses="Junio";
			                                 if ($meses=="July") $meses="Julio";
			                                 if ($meses=="August") $meses="Agosto";
			                                 if ($meses=="September") $meses="Septiembre";
			                                 if ($meses=="October") $meses="Octubre";
			                                 if ($meses=="November") $meses="Noviembre";
			                                 if ($meses=="December") $meses="Diciembre";
			                                 echo "<option value='$meses'>$meses</option>"; 
			                              } 
			                          ?> 
			    			</select>-->
			    			<span class="messages" ng-show="form.$submitted || form.name.$touched">
                            <span class="" ng-show="form.mes.$error.required">Mes requerido</span>
			    	    </div>
					</div>
				    <div class="row">  
			            <div class="form-group col-xs-10 col-xs-offset-1" >  
							<span class="required">*</span><label class="control-label ">Seleccione las actividades realizadas a continuación:</label>
							<select multiple class="form-control" name="actmod_id" ng-model="informe.informe_act.actmod_id"  ng-options="act_modulo.id as act_modulo.nombre for act_modulo in act_modulos | filter: { modulo_id: informe.modulo_id } " ng-change="cambio()" required>
				            </select>
				            <span class="messages" ng-show="form.$submitted || form.name.$touched">
                            <span class="" ng-show="form.actmod_id.$error.required">Actividad requerida</span>
		                </div>
	                </div>
	                <div class="row">
		                <div class="form-group col-xs-10 col-xs-offset-1" >
		                    <span class="required">*</span><label class="control-label">Describa brevemente cada actividad:</label>
		                	<textarea class="form-control" name="descripcion" ng-model="informe.descripcion" required></textarea>
		                	<span class="messages" ng-show="form.$submitted || form.name.$touched">
                            <span class="" ng-show="form.descripcion.$error.required">Descripción requerido</span>
		                </div>	
	                </div>
	                
	            </div>
	        </div>     
	        <div class="modal-footer">
	            <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
                <button type="submit" class="btn btn-success" ng-click="save(form)">Guardar</button>
	        </div>
	   </form>
	</script>      



		

