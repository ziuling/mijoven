@extends('layouts.app')

@section('content')
	
    @if (session('status'))
	      <div class="alert alert-success">
	        {{ session('status') }}
	      </div>
	    @endif
        
        <section id="main-content">
	        <section class="wrapper">
			    <div class="row">
					<div class="col-lg-12">
						<h3 class="page-header"><i class="icon_document_alt"></i> Historial</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a href="home">Inicio</a></li>
							<li><i class="icon_document_alt"></i><a href="informes">Informes</a></li>
							<li><i class="icon_document_alt"></i>Historial</li>
						</ol>
					</div>
				</div>
				<div class="row" ng-controller="InformeCtrl">
				    
				    <div class="col-xs-8">
						<section class="panel">
	                          <header class="panel-heading">
	                             <strong>Meses registrados</strong>
	                          </header>
	                        
	                            <table class="table table-striped table-advance table-hover">
									

								    <tbody>
	                                     <tr ng-repeat="informe in informes">
											<td><a href="informe/1/@{{ informe.club_id }}/@{{ informe.mes }}" target="_blank" ng-bind=" informe.mes" ></a></td>
										</tr>
										<tr ng-if="informes.length < 1"><td colspan="2"><h5 class="text-center">Sin registros</h5></td></tr>   
									</tbody>

							   </table>
								
						</section>
								
					</div>
				</div>
			</section>
        </section>
   
@endsection
@section('script')
	<script type="text/javascript" src="{{asset('app/InformeCtrl.js')}}"></script>
@endsection