<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<style>
	main{
		margin-left:30px; 
		margin-right:30px; 
	}
	.page-break {
	    page-break-after: always;
	}
	.text-center{
		text-align: center;
	    display: block;
	    width: 100%;
	}
	.text-right{
		text-align: right;
	    display: block;
	    width: 100%;
	}
	footer {
		color: #5D6975;
		width: 100%;
		height: 30px;
		position: absolute;
		bottom: 0;
		border-top: 1px solid #C1CED9;
		padding: 8px 0;
		text-align: center;
	}
		.info, .correcto, .ojo, .error, .validation {
		    border: 1px solid;
		    margin: 10px 0px;
		    padding:15px 10px 15px 50px;
		    background-repeat: no-repeat;
		    background-position: 10px center;
		    font-family:Arial, Helvetica, sans-serif;
		    font-size:13px;
		    text-align:left;
		    width:auto;
		}
		.info {
		    color: #00529B;
		    background-color: #BDE5F8;
		    background-image: url('imagenes/info.jpg');
		}
	</style>
	<title>INFORME</title>
</head>
<body>

    {{--  ENCABEZADO  --}}
    <div class="text-center" style="width:100%;display: block">
		
		<img style="" src="/xampp/htdocs/mijoven/public/img/mvnor.jpg" width="150" height="130">
		
		<span class="text-center" style="font-size: 20px;">Misión Venezolana NorOriental</span>
		<strong class="text-center">PLANILLA DE INFORME DE JÓVENES ADVENTISTAS</strong>
    </div> 
    

    <main style="margin-top: 25px;">
        @if($inf == 0)
    	    <div class="info">
    	    	<h3 class="text-center" style="margin-top: 50px;">SIN REGISTROS</h3>
    	    </div>
    	@endif

        @foreach($data as $da)
    		<div class="row" style="">
            @foreach($modulos as $md)
		        @if($md->id === $da->modulo_id)
			        <strong class="text-center" style="font-size: 18px; padding-top: 25px;">{{$md->nombre}} :</strong>
			        <h4>Actividades cumplidas:</h4>
		            @foreach($actividades as $act)
		                @if($act->informe_id === $da->id)
		                    @foreach($actividad as $a)
			                    @if($a->id === $act->actmod_id)
							        <div class="row">
							        <strong>-</strong> <span>{{ $a->nombre}}</span>
							        </div>
						    	@endif
					    	@endforeach
				    	@endif
		    	    @endforeach
		    	@endif
	    	@endforeach
	    	</div>
	    	<div class="row" style="padding-top: 35px;">
	    	<strong >Descripción:</strong>
	    	<p>{{ $da->descripcion}}</p>
            </div>
    	@endforeach
    </main>
    <footer>
    	
        @if($rol == 4)
           Club de {{$obj->categoria->nombre}} {{ $obj->nombre}}
        @elseif($rol == 6)
          Sociedad de Jóvenes de la iglesia {{ $obj->nombre }}
        @endif
    </footer>
</body>
</html>