@extends('layouts.app')

@section('content')

		@if (session('status'))
		    <div class="alert alert-success">
		        {{ session('status') }}
		    </div>
		@endif

		<section id="main-content">
          <section class="wrapper">
		  <div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-table"></i> Tipos de actividades</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="home">Inicio</a></li>
						<li><i class="fa fa-table"></i><a href="actividades">Actividades</a></li>
						<li><i class="fa fa-th-list"></i>Tipos de actividades</li>
					</ol>
				</div>
			</div>
              <!-- page start-->
              <div class="row">
                 
                  <div class="col-lg-12">
                      <section class="panel" ng-controller="Tipo_aCtrl">
                          <header class="panel-heading">
                              
                          </header>
                          @rol("Secretario(a)")
                          <table class="table table-striped table-advance table-hover">
                           <thead>
                              
                                <th >Tipo</th>
								<th >Nombre</th>
								<th >Puntuación</th>
                                <th><i class="icon_cogs"></i> Acciones</th>
                           </thead>
                           <tbody>
                                <tr ng-repeat="tipo_a in tipos_a | filtroTipo">
	                                <td ng-bind="tipo_a.tipo"></td>
	                                <td ng-bind="tipo_a.nombre || 'Sin nombre asignado'"></td>
	                                <td ng-bind="tipo_a.total_puntuacion || 'Sin puntuación asignada'"></td>
						            <td>
                                    <div class="btn-group">
						                <button class="btn btn-primary" type="button" ng-click="edit(tipo_a)"><span class="glyphicon glyphicon-edit"></span></button>
										<button class="btn btn-eliminar" ng-disabled=" tipo_a.tipo== 'Fijas' || tipo_a.tipo == 'Generales' || tipo_a.tipo == 'Precamporee'" type="button" ng-click="destroy(tipo_a)"><i class="icon_close_alt2"></i></button>
									</div>	
						            </td>
					           </tr>
					        <tr ng-if="tipos_a.length <1"><td colspan="4"><h5 class="text-center">Sin registros</h5></td></tr>
					        </tbody>
                                    
                        </table>
                        <button class="btn btn-enviar pull-right" type="button" ng-click="create()" style="margin-left: 2px;"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
                        @include('tipos.partial.tipo')

                        @endrol
                        @rol("Líder de Zona")
                        <table class="table table-striped table-advance table-hover">
                           <thead>
                                <th >Tipo</th>
                                <th><i class="icon_cogs"></i> Acciones</th>
                           </thead>
                           <tbody>
                                <tr ng-repeat="tipo_a in tipos_a">
	                                <td>@{{tipo_a.tipo}}</td>
						            <td>
	                                    <div class="btn-group">
							            <button class="btn btn-primary" type="button" ng-click="edit(tipo_a)"><span class="glyphicon glyphicon-edit"></span></button>
										<button class="btn btn-eliminar" type="button" ng-click="destroy(tipo_a)"><span class="glyphicon glyphicon-trash"></span></button>
									    </div>	
						            </td>
					            </tr>
					            <tr ng-if="tipos_a.length <1"><td colspan="4"><h5 class="text-center">Sin registros</h5></td></tr>
					        </tbody>
                                    
                        </table>
                        <button class="btn btn-enviar pull-right" type="button" ng-click="create()" style="margin-left: 2px;"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
                        @include('tipos.partial.t')

                        {!! $tipos->links() !!}

                        @endrol
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>

@endsection
@section('script')
	<script type="text/javascript" src="{{asset('app/Tipo_aCtrl.js')}}"></script>
@endsection