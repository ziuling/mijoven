<!-- Modal -->

<script type="text/ng-template" class="modal" id="Tipo_aModal.html">
    <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="Tipo_aModalLabel">Tipos de Actividades</h4>
    </div>
      <form class="form" name="form" ng-cloak novalidate>
        {{ csrf_field() }}
        <div class="modal-body">
           <div class="row">
                <div class="form-group col-xs-11">
                	<label class="control-label pull-right">Campos requeridos(<span class="required ">*</span>)</label>
                </div>
 	            <div class="form-group col-xs-10 col-xs-offset-1">
		            <span class="required">*</span><label class="control-label ">Tipo:</label>        
		            <input type="text" name="tipo" ng-model="tipo_a.tipo" class="form-control" required>
                	<span class="messages" ng-show="form.$submitted || form.name.$touched">
                    <span class="" ng-show="form.tipo.$error.required">Tipo requerido</span>		
		        </div>
				<div class="form-group col-xs-10 col-xs-offset-1">
					<label class="control-label">Nombre:</label>
		            <input type="text" name="nombre" ng-model="tipo_a.nombre" class="form-control" >
				</div>
				<div class="form-group col-xs-10 col-xs-offset-1">
					<label class="control-label ">Total de puntuación:</label>
		            <input type="text" name="total_puntuacion" ng-model="tipo_a.total_puntuacion" class="form-control">
				</div>
	       </div>
	     
	    <div class="modal-footer">
	        <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
	        <button type="submit" class="btn btn-success" ng-click="save(form)">Guardar</button>
        </div>
    </form>

</script>
