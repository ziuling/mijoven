@extends('layouts.app')

@section('content')

		@if (session('status'))
		    <div class="alert alert-success">
		        {{ session('status') }}
		    </div>
		@endif

	<section id="main-content">
        <section class="wrapper">
		  <div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-th-list"></i> Gestionar Sistema</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="home">Inicio</a></li>
						<li><i class="fa fa-th-list"></i>Sistema</li>
					</ol>
				</div>
			</div>
              <!-- page start-->
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                          <header class="panel-heading">
                              
                          </header>

                          <div class="page-header">
						      <!-- Nav tabs -->
							  <ul class="nav nav-tabs" role="tablist">
							    <li role="presentation" class="active">
							       <a href="#Zonas" aria-controls="Zonas" role="tab" data-toggle="tab">Zonas</a>
							    </li>
							    <li role="presentation">
							       <a href="#Distritos" aria-controls="Distritos" role="tab" data-toggle="tab">Distritos</a>
							    </li>
							    <li role="presentation">
							       <a href="#Pastores" aria-controls="Pastores" role="tab" data-toggle="tab">Pastores</a>
							    </li>
							    <li role="presentation">
							       <a href="#Iglesias" aria-controls="Iglesias" role="tab" data-toggle="tab">Iglesias</a>
							    </li>
							    <li role="presentation">
							       <a href="#Clubes" aria-controls="Clubes" role="tab" data-toggle="tab">Clubes</a>
							    </li>
							    <li role="presentation">
							        <a href="#Inscripciones" aria-controls="Inscripciones" role="tab" data-toggle="tab">Inscripciones</a>
							    </li>
							    <li role="presentation">
							        <a href="#Reglamentos" aria-controls="Reglamentos" role="tab" data-toggle="tab">Reglamentos</a>
							    </li>
							  </ul>
							</div>

                          <div class="tab-content">
					        <div role="tabpanel" class="tab-pane active" id="Zonas" ng-controller="ZonaCtrl">
							   <table class="table table-striped table-advance table-hover">
								   <thead>
									   <th>Zonas del campo Mvnor</th>
									   <th>Líder Zona</th>
									   <th>Acciones</th>
								   </thead>
								   <tbody>
										<tr ng-repeat="zona in zonas">
											<td ng-bind="zona.nombre"></td>
											<td ng-bind="zona.lider.nombre || 'Sin Lider Asignado'"></td>
											<td>
											    <div class="btn-group">
													<button class="btn btn-primary" type="button" ng-click="edit(zona)"><span class="glyphicon glyphicon-edit"></span></button>
													<button class="btn btn-eliminar" type="button" ng-click="destroy(zona)"><i class="icon_close_alt2"></i></button>
                                                </div>
											</td>
										</tr>
										<tr ng-if="zonas.length < 1"><td colspan="4"><h5 class="text-center">Sin registros</h5></td></tr>
								   </tbody>
							   </table>
								<button  class="btn btn-enviar pull-right" type="button" ng-click="create()"><span class="glyphicon glyphicon-plus"></span> Nueva Zona</button>
								@include('informaciones.partials.zona')
					        </div>

						    <div role="tabpanel" class="tab-pane" id="Distritos" ng-controller="DistritoCtrl">
						    	<table class="table table-striped table-advance table-hover">
									<thead>
										<th>Distritos</th>
										<th>Zona</th>
										<th>Acciones</th>
									</thead>
									<tbody>
			                            <tr ng-repeat="distrito in distritos">
			                               <td ng-bind="distrito.nombre"></td>
			                               <td ng-bind="distrito.zona.nombre"></td>
										   <td>
											    <div class="btn-group">
													<button class="btn btn-primary" type="button" ng-click="edit(distrito)"><span class="glyphicon glyphicon-edit"></span></button>
													<button class="btn btn-eliminar" type="button" ng-click="destroy(distrito)"><i class="icon_close_alt2"></i></button>
												</div>

										    </td>
										</tr>
										<tr ng-if="distritos.length < 1"><td colspan="4"><h5 class="text-center">Sin registros</h5></td></tr>
									</tbody>
								</table>
								<button class="btn btn-enviar pull-right" type="button" ng-click="create()"><span class="glyphicon glyphicon-plus"></span> Nuevo Distrito</button>
								@include('informaciones.partials.distrito')
						    </div>
				    
						    <div role="tabpanel" class="tab-pane" id="Pastores" ng-controller="PastorCtrl">.
						    	<table class="table table-striped table-advance table-hover">
									<thead>
										<th>Cédula</th>
										<th>Pastores distritales</th>
										<th>Correo</th>
										<th>Distrito</th>
										<th>Acciones</th>
									</thead>
									<tbody>
									    <tr ng-repeat="pastor in pastores">
									    	<td ng-bind="pastor.cedula"></td>
									    	<td ng-bind="pastor.nombre"></td>
									    	<td ng-bind="pastor.correo"></td>
									    	<td ng-bind="pastor.distrito.nombre"></td>
											<td>
											    <div class="btn-group">

													<button class="btn btn-primary" type="button" ng-click="edit(pastor)"><span class="glyphicon glyphicon-edit"></span></button>
													<button class="btn btn-eliminar" type="button" ng-click="destroy(pastor)"><i class="icon_close_alt2"></i></button>
												</div>
											</td>
										</tr>
										<tr ng-if="pastores.length < 1"><td colspan="4"><h5 class="text-center">Sin registros</h5></td></tr>
									</tbody>
								</table>
								<button class="btn btn-enviar pull-right" type="button" ng-click="create()"><span class="glyphicon glyphicon-plus"></span> Nuevo Pastor</button>
								@include('informaciones.partials.pastor')
						    </div>

						    <div role="tabpanel" class="tab-pane" id="Iglesias" ng-controller="IglesiaCtrl">
						    	<table class="table table-striped table-advance table-hover">
									<thead>
										<th>Iglesias</th>
										<th>Tipo</th>
										<th>Direccion</th>
										<th>Distrito</th>
										<th>Secretario/a de sj</th>
										<th>Acciones</th>
									</thead>
									<tbody>
										<tr ng-repeat="iglesia in iglesias">
											<td ng-bind="iglesia.nombre"></td>
											<td ng-bind="iglesia.tipo.nombre"></td>
											<td ng-bind="iglesia.direccion"></td>
											<td ng-bind="iglesia.distrito.nombre"></td>
											<td ng-bind="iglesia.usuario.nombre || 'Sin secretario asignado'"></td>
											<td>
											    <div class="btn-group">

													<button class="btn btn-primary" type="button" ng-click="edit(iglesia)"><span class="glyphicon glyphicon-edit"></span></button>
													<button class="btn btn-eliminar" type="button" ng-click="destroy(iglesia)"><i class="icon_close_alt2"></i></button>
												</div>
											</td>
										</tr>
										<tr ng-if="iglesias.length < 1"><td colspan="6"><h5 class="text-center">Sin registros</h5></td></tr>
									</tbody>
								</table>
								<button class="btn btn-enviar pull-right" type="button" ng-click="create()"><span class="glyphicon glyphicon-plus"></span> Nueva Iglesia</button>
								@include('informaciones.partials.iglesia')
						    </div>

						    <div role="tabpanel" class="tab-pane" id="Clubes" ng-controller="ClubCtrl">
						    	<table class="table table-striped table-advance table-hover">
									<thead>
										<th>Tipo de club</th>
										<th>Nombre del club</th>
										<th>Iglesia</th>
										<th>Telefono</th>
										<th>Secretario/a</th>
										<th>Acciones</th>
									</thead>
									<tbody>
										<tr ng-repeat="club in clubes">
											<td ng-bind="club.categoria.nombre"></td>
											<td ng-bind="club.nombre"></td>
											<td ng-bind="club.iglesia.nombre"></td>
											<td ng-bind="club.telefono || 'Sin asignar'"></td>
											<td ng-bind="club.user.nombre || 'Sin secretario asignado'"></td>
											<td>
											    <div class="btn-group">

													<button class="btn btn-primary" type="button" ng-click="edit(club)"><span class="glyphicon glyphicon-edit"></span></button>
													<button class="btn btn-eliminar" type="button" ng-click="destroy(club)"><i class="icon_close_alt2"></i></button>
												</div>
											</td>
										</tr>
										<tr ng-if="clubes.length < 1"><td colspan="6"><h5 class="text-center">Sin registros</h5></td></tr>
									</tbody>
								</table>
								<button class="btn btn-enviar pull-right" type="button" ng-click="create()"><span class="glyphicon glyphicon-plus"></span> Nuevo Club</button>
								@include('informaciones.partials.club')
						    </div>

						    <div role="tabpanel" class="tab-pane" id="Inscripciones" ng-controller="InscripcionCtrl">
						        <div class="row" style="padding-bottom: 20px">
				                   <div class="col-xs-10 col-xs-offset-0 col-sm-6 col-sm-offset-3">
				                       <div class="form-group text-center">
					                        <div class="btn-group">
											    <button class="btn " ng-class="{'btn-grupo':categoria.id === option, 'btn-default':categoria.id != option}" ng-repeat="categoria in categorias" name="option" ng-click="changeOption(categoria)" >@{{categoria.nombre }} </button>
											</div>
					                    </div>
					               </div>
					            </div>
					            <div ng-show="view">
							    	<table class="table table-striped table-advance table-hover">
										<thead>
											<th>Fecha Tope</th>
											<th>Club (Camporee y Misión)</th>
											<th>Miembros</th>
											<th>Acompañantes</th>
											<th>Niños (6-9)</th>
											<th>Seguro</th>
											<th>Acciones</th>
										</thead>
										<tbody>
											<tr ng-repeat="inscripcion in inscripciones">
												<td ng-bind="inscripcion.fecha"></td>	
												<td ng-bind="inscripcion.monto"></td>
												<td ng-bind="inscripcion.monto_miembro"></td>
												<td ng-bind="inscripcion.monto_acompanante"></td>
												<td ng-bind="inscripcion.monto_nino"></td>
												<td ng-bind="inscripcion.seguro"></td>
												
												<td>
											        <div class="btn-group">
														<button class="btn btn-primary" type="button" ng-click="edit(inscripcion)"><span class="glyphicon glyphicon-edit"></span></button>
														<button class="btn btn-eliminar" type="button" ng-click="destroy(inscripcion)"><i class="icon_close_alt2"></i></button>
													</div>
												</td>
											</tr>
												<tr ng-if="inscripciones.length < 1 "><td colspan="6"><h5 class="text-center">Sin registros</h5></td></tr>
										</tbody>
									</table>
									<!--<h5 class="">El seguro tiene un monto de: @{{inscripciones[0].seguro}} Bsf</h5> -->
									<button class="btn btn-enviar pull-right" type="button" ng-click="create()"><span class="glyphicon glyphicon-plus"></span> Agregar Inscripción</button>
									@include('informaciones.partials.inscripcion')
									
								</div>
								<div ng-hide="view">
									<table class="table table-striped table-advance table-hover">
										<thead>
											<th>Fecha Tope</th>
											<th>Sociedad de jóvenes (Campestre y Misión)</th>
											<th>Miembros</th>
											<th>Acompañantes</th>
											<th>Niños (6-9)</th>
											<th>Seguro</th>
											<th>Acciones</th>
										</thead>
										<tbody>
											<tr ng-repeat="inscripcion in inscripciones">
												<td ng-bind="inscripcion.fecha | date:'dd/MM/yyyy' "></td>
												<td ng-bind="inscripcion.monto"></td>
												<td ng-bind="inscripcion.monto_miembro"></td>
												<td ng-bind="inscripcion.monto_acompanante"></td>
												<td ng-bind="inscripcion.monto_nino"></td>
												<td ng-bind="inscripcion.seguro"></td>
												
												<td>
											        <div class="btn-group">

														<button class="btn btn-primary" type="button" ng-click="edit(inscripcion)"><span class="glyphicon glyphicon-edit"></span></button>
														<button class="btn btn-eliminar" type="button" ng-click="destroy(inscripcion)"><i class="icon_close_alt2"></i></button>
													</div>
												</td>
											</tr>
												<tr ng-if="inscripciones.length < 1 "><td colspan="6"><h5 class="text-center">Sin registros</h5></td></tr>
										</tbody>
									</table>
									<!--<h5 class="">El seguro tiene un monto de: @{{inscripciones[0].seguro}} Bsf</h5>-->
									<button class="btn btn-success pull-right" type="button" ng-click="create()"><span class="glyphicon glyphicon-plus"></span> Agregar Inscripción</button>
						        </div>
						    </div>

						    <div role="tabpanel" class="tab-pane" id="Reglamentos" ng-controller="ReglamentoCtrl">
								<table class="table table-striped table-advance table-hover">
									<thead>
										<th>Título</th>
										<th >Acciones</th>
									</thead>
									<tbody>
										<tr ng-repeat="reglamento in reglamentos">
											<td ng-bind="reglamento.titulo"></td>
											<td>
											    <div class="btn-group">
													<button title="editar" class="btn btn-primary" type="button" ng-click="edit(reglamento)"><span class="glyphicon glyphicon-edit"></span></button>
													<button title="eliminar" class="btn btn-eliminar" type="button" ng-click="destroy(reglamento)"><i class="icon_close_alt2"></i></button>
													<button title="ver" class="btn btn-default" type="button" ng-click="ver(reglamento)"><span class="glyphicon glyphicon-download-alt"></span></button>
												</div>
											</td>
										</tr>
										<tr ng-if="reglamentos.length < 1"><td colspan="3"><h5 class="text-center">Sin registros</h5></td></tr>
									</tbody>
								</table>
								<button class="btn btn-enviar pull-right" type="button" ng-click="create()"><span class="glyphicon glyphicon-plus"></span> Nuevo reglamento</button>
								@include('informaciones.partials.reglamento')
						    </div>
				        </div>
				   </section>
               </div>
		    </div>
		</section>
    </section>


@endsection
@section('script')
	<script type="text/javascript" src="{{asset('app/ZonaCtrl.js')}}"></script>
	<script type="text/javascript" src="{{asset('app/DistritoCtrl.js')}}"></script>
	<script type="text/javascript" src="{{asset('app/PastorCtrl.js')}}"></script>
	<script type="text/javascript" src="{{asset('app/IglesiaCtrl.js')}}"></script>
	<script type="text/javascript" src="{{asset('app/ClubCtrl.js')}}"></script>
	<script type="text/javascript" src="{{asset('app/InscripcionCtrl.js')}}"></script>
	<script type="text/javascript" src="{{asset('app/ReglamentoCtrl.js')}}"></script>
@endsection