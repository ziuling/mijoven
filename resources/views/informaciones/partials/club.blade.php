<!-- Modal -->
<script type="text/ng-template" class="modal" id="ClubModal.html">
      <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="ClubModalLabel">Registro de Clubes</h4>
      </div>
      <form class="form" name="form" ng-cloak novalidate>
        {{ csrf_field() }}
        <div class="modal-body">
          <div class="row">
            <div class="form-group col-xs-11">
              <label class="control-label pull-right">Campos requeridos(<span class="required ">*</span>)</label>
            </div> 
            <div class="form-group col-xs-10 col-xs-offset-1">
              <span class="required">*</span><label class="control-label ">Tipo de Club:</label>            
                <select class="form-control" ng-options="categoria.id as categoria.nombre for categoria in categorias | filter: { tipo: 1}" name="categoria_id" ng-model="club.categoria_id" required>
                </select>
                <span class="messages" ng-show="form.$submitted || form.name.$touched">
                <span ng-show="form.categoria_id.$error.required">Tipo de club requerido</span>
            </div>
           
            <div class="form-group col-xs-10 col-xs-offset-1">
              <span class="required">*</span><label class="control-label ">Iglesia:</label>            
                <select class="form-control" name="iglesia" required ng-model="club.iglesia_id" ng-options="iglesia.id as iglesia.nombre for iglesia in iglesias"></select>
                <span class="messages" ng-show="form.$submitted || form.name.$touched">
                <span ng-show="form.iglesia.$error.required">Iglesia requerido</span>
            </div>
          
            <div class="form-group col-xs-10 col-xs-offset-1">
              <span class="required">*</span><label class="control-label">Nombre del club:</label>
              <input type="text" class="form-control" ng-model="club.nombre" name="nombre"  minlength="8" required />
              <span class="messages" ng-show="form.$submitted || form.name.$touched">
              <span ng-show="form.nombre.$error.required">Nombre requerido</span>
              <span ng-show="form.nombre.$error.minlength">Mínimo 8 carácteres</span>
            </div>
       
            <div class="form-group col-xs-10 col-xs-offset-1">
              <label class="control-label">Teléfono:</label>
              <input type="text" class="form-control" ng-model="club.telefono" name="telefono"  maxlength="11" minlength="11" >
              <span class="messages" ng-show="form.$submitted || form.name.$touched">
              <span class="" ng-show="form.telefono.$error.minlength">Mínimo 11 carácteres numericos, sin simbolos ni espacio</span>
            </div>
            <div class="form-group col-xs-10 col-xs-offset-1">
              <span class="required">*</span><label class="control-label ">Secretario(a):</label>            
              <select class="form-control" name="user_id" ng-model="club.user_id" value="club.user_id" ng-options="user.id as user.nombre for user in users | filter: {rol_id: 4}" required >
              </select>
              <span class="messages" ng-show="form.$submitted || form.name.$touched">
              <span ng-show="form.user_id.$error.required">Secretario requerido</span>
            </div> 
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
          <button type="submit" class="btn btn-success" ng-click="save(form)">Guardar</button>
        </div>
      </form>

</script>