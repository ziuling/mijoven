 <!-- Modal -->
<script type="text/ng-template" class="modal" id="IglesiaModal.html">
      <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="IglesiaModalLabel">Registro de Iglesias o Grupos</h4>
      </div>

      <form class="form" name="form" ng-cloak novalidate>
        {{ csrf_field() }}

       <div class="modal-body">
         <div class="row">
            <div class="form-group col-xs-12 col-sm-8 col-sm-offset-2">

	  	        <label class="control-label">Tipo:</label>
              <label class="radio-inline">
                <input type="radio" name="tipo" id="church-radio" ng-model="iglesia.tipo_id" value="1">Iglesia
              </label>
              <label class="radio-inline">
                <input type="radio" name="tipo" id="group-radio" ng-model="iglesia.tipo_id" value="2">Grupo
              </label>
		        </div>
		     
            <div class="form-group col-xs-12 col-sm-8 col-sm-offset-2">
              <label class="control-label">Iglesia:</label>
              <input type="text" class="form-control" ng-model="iglesia.nombre" name="nombre" placeholder="" required />
            </div>
            
            <div class="form-group col-xs-12 col-sm-8 col-sm-offset-2">
			       <label class="control-label">Dirección:</label>
			        <input type="text" class="form-control" ng-model="iglesia.direccion" name="direccion" placeholder="" required />
	          </div>
	          <div class="form-group col-xs-12 col-sm-8 col-sm-offset-2">
                  <label class="control-label ">Distrito:</label>             
                  <select class="form-control" name="distrito" required ng-model="iglesia.distrito_id" ng-options="distrito.id as distrito.nombre for distrito in distritos"></select>
            </div>
            <div class="form-group col-xs-12 col-sm-8 col-sm-offset-2">
              <label class="control-label ">Secretario/a de sociedad de jóvenes:</label>
              <select class="form-control" name="user" ng-options="user.id as user.nombre for user in users | filter: { rol_id: 6 }" ng-model="iglesia.user_id">
              </select>
            </div>  
         </div>
			 </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
          <button type="submit" class="btn btn-success" ng-click="save(form)">Guardar</button>
        </div>
      </form>

</script>



	  