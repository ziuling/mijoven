<!-- Modal -->
<script type="text/ng-template" class="modal" id="DistritoModal.html">
  <div class="modal-header">
    <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="DistritoModalLabel">Distrito</h4>
  </div>
  <form class="form" name="form" ng-cloak novalidate>
    {{ csrf_field() }}
    <div class="modal-body">
      <div class="row">
        <div class="form-group col-xs-12 col-sm-8 col-sm-offset-2">
          <label class="control-label">Distrito:</label>
          <input type="text" class="form-control" ng-model="distrito.nombre" name="nombre" placeholder="" required />
        </div>
        <div class="form-group col-xs-12 col-sm-8 col-sm-offset-2">
          <label class="control-label">Zona:</label>
          <select class="form-control" name="zona" ng-model="distrito.zona_id" ng-options="zona.id as zona.nombre for zona in zonas"></select>
        </div>
      </div> 
    </div>  
    <div class="modal-footer">
      <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
      <button type="submit" class="btn btn-success" ng-click="save(form)">Guardar</button>
    </div>
  </form>

</script>