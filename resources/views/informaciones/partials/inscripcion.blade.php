<!-- Modal -->
<script type="text/ng-template" class="modal" id="InscripcionModal.html">
      <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="InscripcionModalLabel">Monto de Inscripciones</h4>
      </div>

      <form class="form" name="form" ng-cloak novalidate>
        {{ csrf_field() }}

       <div class="modal-body">

            <div class="row">
              <div class="form-group col-xs-7 col-sm-5 col-sm-offset-1">
                <label class="control-label">Fecha tope:</label>
                <input type="date" class="form-control" name="fecha" ng-model="inscripcion.fecha"  date-format/>
              </div>
              <div class="form-group col-xs-5 col-sm-5">
                <label class="control-label">Monto por club/sj:</label>
                <input type="text" name="monto" ng-model="inscripcion.monto" class="form-control" placeholder="Bs.">
              </div> 
              <div class="form-group col-xs-6 col-sm-5 col-sm-offset-1">
                <label>Cuota por miembro:</label>
                <input type="text" name="monto_miembro" ng-model="inscripcion.monto_miembro" class="form-control" placeholder="Bs.">
              </div>
              <div class="form-group col-xs-6 col-sm-5">
                <label>Cuota por acompañante:</label>
                <input type="text" name="monto_acompanante" ng-model="inscripcion.monto_acompanante" class="form-control" placeholder="Bs.">
              </div>
              <div class="form-group col-xs-6 col-sm-5 col-sm-offset-1">
                <label>Cuota por niño:</label>
                <input type="text" name="monto_nino" ng-model="inscripcion.monto_nino" class="form-control" placeholder="Bs.">
              </div>
              <div class="form-group col-xs-6 col-sm-5">
                <label >Monto Seguro:</label>
                    <input type="text"  name="seguro"  ng-model="inscripcion.seguro" class="form-control" placeholder="Bs."> 

              </div> 
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
          <button type="submit" class="btn btn-success" ng-click="save(form)">Guardar</button>
        </div>
      </form>
    </div>
  </div>
</div>
</script>

   
 