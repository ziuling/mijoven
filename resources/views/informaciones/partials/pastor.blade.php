<!-- Modal -->
<script type="text/ng-template" class="modal" id="PastorModal.html">
  <div class="modal-header">
    <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="PastorModalLabel">Registro de Pastores Distritales</h4>
  </div>

  <form class="form" name="form" ng-cloak novalidate>
        {{ csrf_field() }}

    <div class="modal-body">
      <div class="row">
        <div class="form-group col-xs-12 col-sm-8 col-sm-offset-2">
	  	    <label class="control-label">Cédula:</label>
				  <input type="text" name="cedula" class="form-control" ng-model="pastor.cedula" name="cedula" placeholder="" required />
	  	  </div>
		
        <div class="form-group col-xs-12 col-sm-8 col-sm-offset-2">
          <label class="control-label">Pastor:</label>
          <input type="text" class="form-control" ng-model="pastor.nombre" name="nombre" placeholder="" required />
        </div>

        <div class="form-group col-xs-12 col-sm-8 col-sm-offset-2">
			    <label class="control-label">Correo Electrónico:</label>
			    <input type="email" class="form-control" ng-model="pastor.correo" name="correo" placeholder="" required />
			  </div>

	      <div class="form-group col-xs-12 col-sm-8 col-sm-offset-2">
			    <label class="control-label ">Distrito:</label>			       
				  <select class="form-control" name="distrito" required ng-model="pastor.distrito_id" ng-options="distrito.id as distrito.nombre for distrito in distritos"></select>
			 </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
      <button type="submit" class="btn btn-success" ng-click="save(form)">Guardar</button>
    </div>
  </form>

</script>
