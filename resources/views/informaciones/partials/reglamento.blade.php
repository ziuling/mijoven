<!-- Modal -->
<script type="text/ng-template" id="ReglamentoModal.html">
      <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="ReglamentoModalLabel">Reglamento general</h4>
      </div>

      <form class="form" name="form" ng-cloak novalidate>
        {{ csrf_field() }}

       <div class="modal-body">

         <div class="row">
            <div class="form-group col-xs-4 col-sm-3 col-sm-offset-1">
              <label class="control-label ">Categoría:</label>             
                <select class="form-control" name="categoria" required ng-model="reglamento.categoria_id" ng-options="categoria.id as categoria.nombre for categoria in categorias"></select>
            </div>

            <div class="form-group col-xs-8 col-sm-7">
              <label class="control-label">Título:</label>
              <textarea class="form-control" rows="1" ng-model="reglamento.titulo" name="titulo"></textarea>
            </div>
          
            <div class="form-group col-xs-12 col-sm-10 col-sm-offset-1">
              <label class="control-label">Introducción:</label>
              <textarea class="form-control" rows="4" ng-model="reglamento.introduccion" name="introduccion"></textarea>
            </div>
          
             <div class="form-group col-xs-12 col-sm-10 col-sm-offset-1">
                <label class="control-label">Reglamento:</label>
                <textarea class="form-control" rows="5" ng-model="reglamento.reglamento" name="reglamento"></textarea>
            </div>
          </div>


        <div class="modal-footer">
          <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
          <button type="submit" class="btn btn-success" ng-click="save(form)">Guardar</button>
        </div>
      </form>
    </div>
  </div>
</div>
</script>
