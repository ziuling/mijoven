<!-- Modal -->
<script type="text/ng-template" class="modal" id="ZonaModal.html">
      <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="ZonaModalLabel">Zona</h4>
      </div>
      <form class="form" name="form" ng-cloak novalidate>
        {{ csrf_field() }}
        <div class="modal-body">
          <div class="row">
            <div class="form-group{{ $errors->has('nombre') ? ' has-error': '' }} col-xs-12 col-sm-8 col-sm-offset-2">
              <label class="control-label" for="nombre">Nombre</label>
              <input type="text" class="form-control" ng-model="zona.nombre" name="nombre" placeholder="Nombre de Zona">
              @if($errors->has('nombre'))
                <span class="help-block">
                  {{ $errors->first('nombre') }}
                </span>
              @endif
            </div>
            <div class="form-group col-xs-12 col-sm-8 col-sm-offset-2">
              <label class="control-label ">Líder:</label>            
              <select class="form-control" name="lider" ng-options="user.id as user.nombre for user in users | filter: {rol_id: 3}" ng-model="zona.user_id" >
              </select>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
          <button type="submit" class="btn btn-success" ng-click="save(form)">Guardar</button>
        </div>

      </form>
  
</script>