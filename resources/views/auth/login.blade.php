@extends('layouts.inicio')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">


                <div class="" style="margin-top: -12px;">
                    <form class="login-form" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="login-wrap">
                            <p class="login-img"><img src="{{ asset('/img/mojoven2.png') }}" width="120" height="110"></p>
                            <div class="input-group{{ $errors->has('email') ? ' has-error' : '' }}">
                              <span class="input-group-addon"><i class="icon_profile"></i></span>
                              <input type="email" name="email" class="form-control" placeholder="Email/Correo Electrónico" value="{{ old('email') }}" autofocus>
                                
                               <!-- @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif-->

                            </div>
                            <div class="input-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                                <input type="password" name="password" class="form-control" placeholder="Password/Contraseña">

                              <!--  @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif-->

                            </div>
                                
                         
                            <button class="btn btn-primary btn-lg btn-block" type="submit">Iniciar</button>
                            <a class="btn btn-info btn-lg btn-block" style="color:white" href="{{ url('/password/reset') }}">Recuperar contraseña</a>

                        </div>
                       

                       <!-- <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Correo Electrónico</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group footer">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Iniciar
                                </button>
                                <a class="btn btn-link" href="{{ url('/password/reset') }}" style="color:black">
                                    Recuperar Contraseña
                                </a>
                            </div>
                        </div>-->
                    </form>
                    
                      <strong class="text-center" style="margin-top: 20px;">
                            @if ($errors->has('email') || $errors->has('password'))
                                <div class="alert alert-danger">
                                <span class="help-block"><strong  style="">{{ $errors->first('email') }}  {{ $errors->first('password') }} </strong></span>

                                <span class="help-block"><strong  style=""></strong></span>                               
                                </div>
                            @endif
                        </strong>
                        
                </div>
               
            </div>
        </div>
    </div>
</div>
@endsection
