@extends('layouts.app')

@section('content')
 
		
		@if (session('status'))
		    <div class="alert alert-success">
		        {{ session('status') }}
		    </div>
		@endif

		<section id="main-content">
            <section class="wrapper">
		  <div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-th-list"></i> Gestionar planificación</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="home">Inicio</a></li>
						<li><i class="fa fa-th-list"></i>Planificación</li>
					</ol>
				</div>
			</div>
              <!-- page start-->
                <div class="row">
                 
                  <div class="col-lg-12">
                      <section class="panel" ng-controller="PlanificacionCtrl">
                          <header class="panel-heading">
                          
                          </header>
                        <table class="table table-striped table-advance table-hover">
					    <thead>
							<th>Fecha Inicio</th>
							<th>Fecha Final</th>
							<th>¿Qué se hará?</th>
							<th>Acciones</th>
						</thead>
						<tbody>
							<tr ng-repeat="planificacion in planificaciones">
								<td ng-bind=" planificacion.f_inicio | date:'dd/MM/yyyy' "></td>
								<td ng-bind=" planificacion.f_final | date:'dd/MM/yyyy'"></td>
								<td ng-bind=" planificacion.que"></td>
								<td>
								    <div class="btn-group">
									    <button class="btn btn-primary" type="button" ng-click="edit(planificacion)"><span class="glyphicon glyphicon-edit"></span></button>
										<button class="btn btn-eliminar" type="button" ng-click="destroy(planificacion)"><i class="icon_close_alt2"></i></button>
									</div>
								</td>
							</tr>
							<tr ng-if="planificaciones.length <1"><td colspan="4"><h5 class="text-center">Sin registros</h5></td></tr>
						</tbody>
					</table>
					
					<a href="planificacion/1/{{$rol}}/{{$id}}" target="_blank" class="pull-right"><button  class="btn btn-default" type="button"><span class="glyphicon glyphicon-download-alt"></span></button></a>
					<button class="btn btn-enviar pull-right" style="margin-left: -15px; " type="button" ng-click="create()"><span class="glyphicon glyphicon-plus"></span> Nueva actividad</button>
					@include('planificaciones.partial.planificacion')

					
			    </div>
		      </div>
			</div>	
	   </section>
    </div>
@endsection
@section('script')
	<script type="text/javascript" src="{{asset('app/PlanificacionCtrl.js')}}"></script>
@endsection