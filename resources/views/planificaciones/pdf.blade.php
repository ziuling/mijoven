<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="pdf.css" media="all" />

	<style type="text/css">
	    html{
			margin-left: -100px; 
		}
		.e{
			margin-left: 120px;
		}
		.text-center{
			text-align: center;
	        display: block;
	        width: 100%;
		}
		.text-right{
			text-align: right;
	        display: block;
	        width: 100%;
		}
	    @font-face {
		  font-family: Junge;
		  src: url(Junge-Regular.ttf);
		}

		.clearfix:after {
		  content: "";
		  display: table;
		  clear: both;
		}

		a {
		  color: #001028;
		  text-decoration: none;
		}

		body {
		  font-family: Junge;
		  position: relative;
		  width: 21cm;  
		  height: 29.7cm; 
		  margin: 0 auto; 
		  color: #001028;
		  background: #FFFFFF; 
		  font-size: 14px; 
		}

		.arrow {
		  margin-bottom: 4px;
		}

		.arrow.back {
		  text-align: right;
		}

		.inner-arrow {
		  padding-right: 10px;
		  height: 30px;
		  display: inline-block;
		  background-color: rgb(233, 125, 49);
		  text-align: center;

		  line-height: 30px;
		  vertical-align: middle;
		}

		.arrow.back .inner-arrow {
		  background-color: rgb(233, 217, 49);
		  padding-right: 0;
		  padding-left: 10px;
		}

		.arrow:before,
		.arrow:after {
		  content:'';
		  display: inline-block;
		  width: 0; height: 0;
		  border: 15px solid transparent;
		  vertical-align: middle;
		}

		.arrow:before {
		  border-top-color: rgb(233, 125, 49);
		  border-bottom-color: rgb(233, 125, 49);
		  border-right-color: rgb(233, 125, 49);
		}

		.arrow.back:before {
		  border-top-color: transparent;
		  border-bottom-color: transparent;
		  border-right-color: rgb(233, 217, 49);
		  border-left-color: transparent;
		}

		.arrow:after {
		  border-left-color: rgb(233, 125, 49);
		}

		.arrow.back:after {
		  border-left-color: rgb(233, 217, 49);
		  border-top-color: rgb(233, 217, 49);
		  border-bottom-color: rgb(233, 217, 49);
		  border-right-color: transparent;
		}

		.arrow span { 
		  display: inline-block;
		  width: 80px; 
		  margin-right: 20px;
		  text-align: right; 
		}

		.arrow.back span { 
		  margin-right: 0;
		  margin-left: 20px;
		  text-align: left; 
		}

		h1 {
		  color: #5D6975;
		  font-family: Junge;
		  font-size: 2.4em;
		  line-height: 1.4em;
		  font-weight: normal;
		  text-align: center;
		  border-top: 1px solid #5D6975;
		  border-bottom: 1px solid #5D6975;
		  margin: 0 0 2em 0;
		}

		h1 small { 
		  font-size: 0.45em;
		  line-height: 1.5em;
		  float: left;
		} 

		h1 small:last-child { 
		  float: right;
		} 

		#project { 
		  float: left; 
		}

		#company { 
		  float: right; 
		}

		table {
		  width: 100%;
		  border-collapse: collapse;
		  border-spacing: 0;
		  margin-bottom: 30px;
		}

		table th,
		table td {
		  text-align: center;
		}

		table th {
		  padding: 5px 20px;
		  color: #5D6975;
		  border-bottom: 1px solid #C1CED9;
		  white-space: nowrap;        
		  font-weight: normal;
		}

		table .service,
		table .desc {
		  text-align: left;
		}

		table td {
		  padding: 10px;
		  text-align: right;
		}

		table td.service,
		table td.desc {
		  vertical-align: top;
		}

		table td.unit,
		table td.qty,
		table td.total {
		  font-size: 1.2em;
		}

		table td.sub {
		  border-top: 1px solid #C1CED9;
		}

		table td.grand {
		  border-top: 1px solid #5D6975;
		}

		table tr:nth-child(2n-1) td {
		  background: #EEEEEE;
		}

		table tr:last-child td {
		  background: #DDDDDD;
		}

		#details {
		  margin-bottom: 30px;
		}

		footer {
		  color: #5D6975;
		  width: 100%;
		  height: 30px;
		  position: absolute;
		  bottom: 0;
		  border-top: 1px solid #C1CED9;
		  padding: 8px 0;
		  text-align: center;
		}
	</style>
	<title>Planificación anual</title>
</head>
<body>
    <main>

   {{--  ENCABEZADO  --}}
    <div class="e" style="width:100%;display: block">
		<img style="margin-left: -60px;" src="/xampp/htdocs/mijoven/public/img/mojoven.png" width="120" height="100">
		<img style="margin-left: 200px; margin-top: 20px;" src="/xampp/htdocs/mijoven/public/img/mvnor.jpg" width="150" height="130">
		<img class="logo" style="float: right;  text-transform: uppercase; margin-right: -60px; margin-top: 20px; border-radius: 50px; " src="/xampp/htdocs/mijoven/public/img/logos/{{$logo}}" width="120" height="100">
		<span class="text-center" style="font-size: 20px;">Misión Venezolana Nor-Oriental</span>
		<strong class="text-center"  style="font-size: 15px;">Plan de trabajo {{ $año_actual }}-{{ $año_siguiente }}</strong>
		
    </div>   
    <div class="">

		<table style="padding-top: 30px;">
		    <thead >
		        <tr style="color:black; font-size: 15px;" >
					<th class="desc"><span style="" >¿Qué se hará?</span></th>
					<th class="desc"><span style="">¿Cómo se hará?</span></th>
					<th class="desc"><span style="">¿Quién lo hará?</span></th>
					<th class="desc"></th>
			    	<th class="desc"><span style="">Fecha Inicio</span></th>
					<th class="desc"><span style="">Fecha Final</span></th>
					<th class="desc"><span style="">¿Objetivos?</span></th>
					<th class="desc"><span style="">¿Propósito misionero?</span></th>
				</tr>
		    </thead>
		    @foreach($meses as $mes)
		    <thead ><tr><th colspan="8" style="background-color:  #778899; " ><span style="color:black; font-size: 18px;">{{ $mes }}</span></th></tr></thead>
			<tbody>
                @foreach($data as $da)
			     @if($da->mes == $mes)
			     <tr>
                   <!-- ********* que se hará *******-->
			       @if($da->que == null)
                      <td >Por definir</td>
			       @endif
			       <td >{{ $da->que }}</td>
			       <!-- **** como se hará ****-->
				   @if($da->como == null)
                      <td>Por definir</td>
			       @endif
				   <td >{{ $da->como }}</td>
			    <!-- ****** quien lo hará ***** -->
			       @if($da->responsable == null)
                      <td >Por definir</td>
			       @endif
				   <td >{{ $da->responsable }}</td>
				   <td></td>
                     <!-- ****** fecha inicio ******-->

				   @if($da->f_inicio == null)
                      <td>Por definir</td>
			       @endif
				   <td >{{ $da->f_inicio }}</td>
			    <!-- ****** fecha final ****** -->
				   @if($da->f_final == null)
                      <td>Por definir</td>
			       @endif
				   <td>{{ $da->f_final }}</td>
			    <!-- *******  objetivo ********* -->
				   @if($da->objetivo == null)
                      <td>Por definir</td>
			       @endif
				   <td>{{ $da->objetivo }}</td>
			    <!-- **** propósito *** -->
				   @if($da->proposito == null)
                      <td>Por definir</td>
			       @endif
				   <td>{{ $da->proposito }}</td>
			    </tr>
			    
			    @endif

			    @endforeach
		       
			</tbody>
			@endforeach
		</table>

    </div>
      
    </main>
    <footer class="e" >
      @if($rol == 4)
        Club de {{$objeto->categoria->nombre}} {{ $objeto->nombre}}
      @else
        Sociedad de Jóvenes de la iglesia {{ $objeto->nombre }}
      @endif
    </footer>
 
</body>
</html>

   
