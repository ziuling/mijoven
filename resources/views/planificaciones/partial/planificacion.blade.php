<!-- Modal -->
<script  class="modal" type="text/ng-template" id="PlanificacionModal.html">
      <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="PlanificacionModalLabel">Planificación</h4>
      </div>

      <form class="form" name="form" ng-cloak novalidate>
        {{ csrf_field() }}

        <div class="modal-body">
          <div class="row">
            <div class="form-group col-xs-11">
	            <label class="control-label pull-right">Campos requeridos(<span class="required ">*</span>)</label>
	        </div>
          <input type="hidden" name="estado" ng-model="planificacion.estado" value="@{{planificacion.estado}}">
          <div class="form-group col-xs-8 col-xs-offset-1">
                <span class="required">*</span><label class="control-label ">Mes:</label>  
                    <select name="mes" class="form-control" ng-model="planificacion.mes" required>
                                  <?php
                                      $mes=date("n"); 
                                      $rango=11; 
                                      for ($i=$mes-1;$i<=$mes+$rango;$i++){ 
                                         $meses=date('F', mktime(0, 0, 0, $i, 1, date("Y") ) );
                                         if ($meses=="January") $meses="Enero";
                                         if ($meses=="February") $meses="Febrero";
                                         if ($meses=="March") $meses="Marzo";
                                         if ($meses=="April") $meses="Abril";
                                         if ($meses=="May") $meses="Mayo";
                                         if ($meses=="June") $meses="Junio";
                                         if ($meses=="July") $meses="Julio";
                                         if ($meses=="August") $meses="Agosto";
                                         if ($meses=="September") $meses="Septiembre";
                                         if ($meses=="October") $meses="Octubre";
                                         if ($meses=="November") $meses="Noviembre";
                                         if ($meses=="December") $meses="Diciembre";
                                         echo "<option value='$meses'>$meses</option>"; 
                                      } 
                                  ?> 
                    </select> 
                    <span class="messages" ng-show="form.$submitted || form.name.$touched">
                    <span class="" ng-show="form.mes.$error.required">Mes requerido</span>
          </div> 
            
    			<div class="form-group col-xs-4 col-sm-offset-1 col-xs-offset-1">
    				<span class="required">*</span><label class="control-label">Fecha inicio:</label>
    				<input class="form-control" type="date" name="f_inicio" ng-model="planificacion.f_inicio" min="01/01/yyyy" max="31/12/yyyy"  date-format/ placeholder="<?php echo date("Y-m-d");?>" required>
    				<span class="messages" ng-show="form.$submitted || form.name.$touched">
                    <span class="" ng-show="form.f_inicio.$error.required">Fecha de inicio es requerido</span>
    			</div>	
          <div class="form-group col-xs-4 col-xs-offset-1" style="margin-left: -25px;">	
    				<label class="control-label">Fecha final:</label>
    				<input class="form-control" type="date" name="f_final" ng-model="planificacion.f_final" min="01/01/yyyy" max="31/12/yyyy" date-format value="<?php echo date("Y-m-d");?>">
    			</div>	
          <div class=" col-xs-8 col-xs-offset-1">
    				<span class="required">*</span><label class="control-label">Responsable:</label>
            <input type="text" name="responsable" ng-model="planificacion.responsable" class="form-control" required>
            <span class="messages" ng-show="form.$submitted || form.name.$touched">
            <span class="" ng-show="form.responsable.$error.required">Campo requerido</span>
    			</div>	
         	
    		  
          <div class="form-group col-xs-5 col-sm-offset-1 col-xs-offset-1">
              <span class="required">*</span><label class="control-label">¿Qué se hará?</label>
              <textarea class="form-control" name="que" ng-model="planificacion.que" rows="1" required></textarea>
              <span class="messages" ng-show="form.$submitted || form.name.$touched">
              <span class="" ng-show="form.que.$error.required">El 'qué' es requerido</span>
          </div>	
    			<div class="form-group col-xs-5 ">
              <span class="required">*</span><label>¿Cómo se hará?</label>
    			    <textarea class="form-control" name="como" ng-model="planificacion.como" rows="1" required></textarea>
    			    <span class="messages" ng-show="form.$submitted || form.name.$touched">
              <span class="" ng-show="form.como.$error.required">El 'cómo' es requerido</span>
    			</div>
    			<div class="form-group col-xs-10  col-xs-offset-1">
    			    <span class="required">*</span><label>Objetivos</label>
    			   <textarea class="form-control" name="objetivo" ng-model="planificacion.objetivo" rows="2" required></textarea>
    			   <span class="messages" ng-show="form.$submitted || form.name.$touched">
                   <span class="" ng-show="form.objetivo.$error.required">Objetivo requerido</span>
    			</div>
    			<div class="form-group col-xs-10 col-xs-offset-1">
    			   <span class="required">*</span><label>Propósito Misionero</label>			 
    			   <textarea class="form-control" name="proposito" ng-model="planificacion.proposito" rows="2" required></textarea>
    			   <span class="messages" ng-show="form.$submitted || form.name.$touched">
                   <span class="" ng-show="form.proposito.$error.required">Propósito requerido</span>
    			</div>
  	    </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
          <button type="submit" class="btn btn-primary" ng-click="save(form)">Guardar</button>
        </div>
      </form>
  
</script>
