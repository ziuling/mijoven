<!DOCTYPE html>
<html lang="es"  ng-app="MiJoven">
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge">-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" type="text/css" href="{{ asset('css/sweetalert2.min.css') }}">
    <!-- Styles -->
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <!-- Bootstrap CSS -->    
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <!--TextAngular -->
    <link href="{{asset('css/textAngular.css')}}" rel="stylesheet">
    
    <link href="{{asset('css/bootstrap-theme.css')}}" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="{{asset('css/elegant-icons-style.css')}}" rel="stylesheet" />
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet"/>
    <link href="{{asset('css/style-responsive.css')}}" rel="stylesheet" />
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    <style type="text/css">
      .messages {
        color: #FA787E;
      }
      
      form.ng-submitted input.ng-invalid{
        border-color: #FA787E;
      }
      
      form input.ng-invalid.ng-touched {
        border-color: #FA787E;
      }
      footer {
        color: #5D6975;
        width: 100%;
        position: absolute;
        bottom: 0;
        /*border-top: 1px solid #C1CED9;*/
        padding: 8px 0;
        text-align: center;
      }
        /*
          form input.ng-valid.ng-touched {
            border-color: #78FA89;
          }
        */  
    </style>
</head>
<body>
 

    <!--header start-->
    <header class="header dark-bg" >
          <div class="toggle-nav" style="">
                <div class="icon-reorder tooltips" data-original-title="Menú de navegación" data-placement="bottom"><i class="icon_menu"  style="margin-top: 10px;"></i></div>
          </div>
            <!--logo start-->
            <a href="home" class="logo" style="margin-top: 15px;">Mijoven <span class="lite">7D</span></a>
            <!--logo end-->
            <div class="top-nav notification-row">                
                <!-- notificatoin dropdown start-->
                <ul class="nav pull-right top-menu">
                  <li class="dropdown">
                      <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        @rol("Secretario(a) de Club")
                          @if(Auth::user()->club->identidad)
                            <span class="profile-ava">
                            <img alt="" height="40" width="40" src="img/logos/{{ Auth::user()->club->identidad->logo }}">
                            </span>
                          @else
                            <span class="profile-ava">
                                <img alt="" height="40" width="40" src="img/mojoven.png">
                            </span>
                          @endif
                        @endrol
                        @rol("Secretario(a) de Sociedad de Jóvenes", "Líder Juvenil", "Líder de Zona", "Secretario(a)")
                          <span class="profile-ava">
                            <img alt="" height="40" width="40" src="img/mojoven.png">
                          </span>
                        @endrol  
                        @if(Auth::user()->rol->nombre == 'Secretario(a) de Club' )
                          <span class="username">{{ Auth::user()->club->categoria->nombre }}, {{ Auth::user()->club->nombre }}</span>
                          <b class="caret"></b>
                        @elseif(Auth::user()->rol->nombre == 'Secretario(a) de Sociedad de Jóvenes')
                          <span class="username">Iglesia {{ Auth::user()->Iglesia->nombre }}</span>
                          <b class="caret"></b>
                        @else
                          <span class="username">Bienvenido {{ Auth::user()->rol->nombre }}, {{ Auth::user()->nombre }}</span>
                          <b class="caret"></b>
                        @endif 
                      </a>
                      <ul class="dropdown-menu extended logout">
                        <div class="log-arrow-up"></div>
                          <li class="eborder-top">
                            <a href="home"><i class="icon_profile"></i>Mijoven7D</a>
                          </li>
                          <li>
                            <a  href="{{ url('/logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();"><span class="glyphicon glyphicon-log-out" style="margin-right: 5px"></span>Salir</a>
                          </li>
                          <li>
                            <a href="documentation.html"><i class="icon_key_alt"></i> Documentación</a>
                          </li>
                            
                      </ul>
                    </li>

                </ul>
                <!-- notificatoin dropdown end-->
            </div>
    </header>      
    <!--header end-->

      <!--sidebar start-->
        <aside>
          <div id="sidebar"  class="nav-collapse ">
                  <!-- sidebar menu start-->
                  <ul class="sidebar-menu">  
                      @rol("Secretario(a) de Club","Secretario(a) de Sociedad de Jóvenes")
                               <li><a href="{{ url('/directivas') }}">Directivas</a></li>
                               <li><a href="{{ url('/miembros') }}">Miembros</a></li>
                               <li><a href="{{ url('/planificaciones') }}">Planificación</a></li>
                              
                               <li><a href="{{ url('/informes') }}">Informe J.A.</a></li>
                            @rol("Secretario(a) de Club")
                                <li><a href="{{ url('/planes') }}">Plan de sostenimiento</a></li>
                            @endrol
                               <li><a href="{{ url('/ministerios') }}">Ministerio fuera de serie</a></li>
                               <li><a href="{{ url('/pago') }}">Pagos</a></li>     
                            @endrol    
                            @rol("Líder de Zona")  
                                <li class="sub-menu">
                                  <a href="javascript:;" class="">
                                      <span>Actividades</span>
                                      <span class="menu-arrow arrow_carrot-right"></span>
                                  </a>
                                  <ul class="sub">
                                      <li><a class="" href="{{ url('/circulares') }}">Circulares</a></li>                          
                                      <li><a class="" href="{{ url('/tipos') }}">Tipos de Actividades</a></li>
                                      <li><a class="" href="{{ url('/actividades') }}">Actividades</a></li>
                                  </ul>
                                </li>  
                            @endrol    
                            @rol("Secretario(a)")
                                <li class="sub-menu">
                                  <a href="javascript:;" class="">
                                      
                                      <span>Actividades</span>
                                      <span class="menu-arrow arrow_carrot-right"></span>
                                  </a>
                                  <ul class="sub">
                                      <li><a class="" href="{{ url('/circulares') }}">Circulares</a></li>                          
                                      <li><a class="" href="{{ url('/tipos') }}">Tipos de Actividades</a></li>
                                      <li><a class="" href="{{ url('/actividades') }}">Actividades</a></li>
                                  </ul>
                                </li>
                                <li><a href="{{ url('/puntuaciones') }}">Puntuaciones</a></li>     
                                
                            @endrol    
                            @rol("Líder Juvenil") 
                                <li><a href="{{ url('/puntuaciones') }}">Puntuaciones</a></li> 
                                <li><a href="{{ url('/usuarios') }}">Usuarios</a></li>
                                <li><a href="{{ url('/administracion') }}">Sistema</a></li>
                                <li><a href="consultas">Reportes y Consultas</a></li>        
                            @endrol    
                            @rol("Secretario(a) de Club","Secretario(a) de Sociedad de Jóvenes", "Líder de Zona")
                                <li><a href="consultas">Reportes y Consultas</a></li>        
                            @endrol
                            @rol("Administrador")
                                <li><a href="{{ url('/usuarios') }}">Gestionar Usuarios</a></li>
                                <li><a href="{{ url('/Respaldo') }}">Gestionar Respaldo</a></li>
                            @endrol              
                            <li>
                              <a  href="{{ url('/logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();"><span class="glyphicon glyphicon-log-out" style="margin-right: 5px"></span>Salir</a>
                              <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                            </li> 
                  </ul>
                 
          </div>
        </aside>
        

  @yield('content')

    <!-- Scripts -->
   
    <script src="{{asset('/js/jquery.min.js')}}"></script>
    <script src="{{asset('/js/bootstrap.min.js ') }}"></script>
    <script src="{{asset('/js/sweetalert2.min.js')}}"></script>
    <script src="{{asset('/js/angular.min.js')}}"></script>
    <script src="{{asset('/js/angular-resource.min.js')}}"></script>
    <script src="{{asset('/js/angular-sanitize.min.js')}}"></script>
    <script src="{{asset('/js/ui-bootstrap-tpls.min.js')}}"></script>
    <script src="{{asset('/js/main.js')}}"></script>
    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('/js/bootstrap.min.js ') }}"></script>

    <script src="js/bootstrap.min.js"></script>
    
    <!-- TextAngular -->
    <script src="{{ asset ('/js/textAngular-rangy.min.js') }}"></script>
    <script src="{{ asset ('/js/textAngular-sanitize.min.js') }}"></script>
    <script src="{{ asset ('/js/textAngular.min.js') }}"></script>
    

    <script src="{{ asset ('/js/angular-file-model.js') }}"></script>

    <!-- nicescroll -->
    <script src="{{asset('js/jquery.scrollTo.min.js')}}"></script>
    <script src="{{asset('js/jquery.nicescroll.js')}}" type="text/javascript"></script>
    <!--custome script for all page-->
    <script src="{{asset('js/scripts.js')}}"></script>


   @yield('script')

</body>
</html>
