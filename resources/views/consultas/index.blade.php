@extends('layouts.app')

@section('content')

  @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif

  <section id="main-content" >
    <div class="col-lg-12">
      <section class="wrapper">
        <div class="row">
          <div class="">
            <h3 class="page-header"><i class="fa fa-table"></i> Generar Reportes y Consultas</h3>
              <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="home">Inicio</a></li>
                <li><i class="fa fa-th-table"></i>Reportes y Consultas</li>
              </ol>
          </div>
        </div>
        <div class="row" ng-controller="ReporteCtrl">
          <section class="panel">
            <header class="panel-heading">
            </header>
            <div class="page-header">
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"  class="active">
                  <a href="#consultas" aria-controls="consultas" role="tab" data-toggle="tab">Consultas</a>
                </li>
                <li role="presentation">
                  <a href="#reportes" aria-controls="reportes" role="tab" data-toggle="tab">Reportes</a>
                </li>
                    
                   
              </ul>
            </div>
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" ng-view="consultas" id="consultas">
                
                <div class="col-xs-6">
                  @rol("Secretario(a) de Club", "Secretario(a) de Sociedad de Jóvenes")
                    <select class="form-control " ng-model="consultas.name"  ng-change="change()">
                       <option value="">Seleccione...</option>
                       <option ng-repeat="consulta in consultas " value="@{{consulta.name}}" ng-bind="consulta.name"></option>
                     </select>
                  @endrol  
                  @rol("Líder Juvenil")
                    <div class="row col-xs-6">
                      <div class="form-group ">
                        <div class="btn-group">
                          <button class="btn " ng-class="{'btn-grupo':categoria.id === option, 'btn-default':categoria.id != option}" ng-repeat="categoria in categorias" name="option" ng-click="changeOption(categoria)" ng-bind="categoria.nombre"></button>
                        </div>
                      </div>
                    </div> 
                    <select class="form-control " ng-model="consultas.name"  ng-change="change()">
                       <option value="">Seleccione...</option>
                       <option ng-repeat="consulta in consultas1" value="@{{consulta.name}}" ng-bind="consulta.name"></option>
                    </select>
                  @endrol  
                   @rol("Líder de Zona")
                   <select class="form-control " ng-model="consultas.name"  ng-change="change()">
                       <option value="">Seleccione...</option>
                       <option ng-repeat="consulta in consultas2" value="@{{consulta.name}}" ng-bind="consulta.name"></option>
                     </select>
                  @endrol  
                </div>

                <section class="col-xs-12" style="padding-top: 20px;">
                  <div ng-show="vista"> <!-- //////////////////////////// INSCRIPCIONES ///////////////////////////// -->
                    <button class="btn btn-enviar" type="button" ng-click="inscripcion()">Presione aquí</button>
                    @include('consultas.partials.inscripcion')
                      
                  </div>
                  <div ng-show="vista1" > <!-- ///////////////////////////// PUNTUACION //////////////////7-->
                    <button class="btn btn-enviar" type="button" ng-click="puntuacion(club)">Presione aquí</button>
                    @include('consultas.partials.puntuacion')    
                  </div>

                  <div ng-show="vista2"> <!-- //////////////////////// ACAMPANTES /////////////////////////////-->

                    <button class="btn btn-enviar" type="button" ng-click="acampante()">Presione aquí</button>
                    @include('consultas.partials.acampante')
                  </div>
                  <div ng-show="vista3"> <!--////////////////////// VOLUNTARIOS //////////////////////////////-->

                      <button class="btn btn-enviar" type="button" ng-click="voluntario()">Presione aquí</button>
                      @include('consultas.partials.voluntario')
                    
                  </div>
                  <div ng-show="vista4"> <!--/////////////////////////////////////// DONANTES /////////////-->
                      
                      <button class="btn btn-enviar" type="button" ng-click="donante()">Presione aquí</button>
                      @include('consultas.partials.donante')
                    
                  </div>
                </section>
              </div>
              <div role="tabpanel" class="tab-pane " ng-view="reportes" id="reportes" >
                <div class="col-xs-6">
                  @rol("Secretario(a) de Club", "Secretario(a) de Sociedad de Jóvenes")
                   <select class="form-control " ng-model="reporte.name"  ng-change="cambio()">
                       <option value="">Seleccione...</option>
                       <option ng-repeat="reporte in reportes " value="@{{reporte.name}}" ng-bind="reporte.name"></option>
                     </select>
                  @endrol  
                  @rol("Líder Juvenil")
                   <select class="form-control " ng-model="reporte.name"  ng-change="cambio()">
                       <option value="">Seleccione...</option>
                       <option ng-repeat="reporte in reportes1" value="@{{reporte.name}}" ng-bind="reporte.name"></option>
                     </select>
                  @endrol  
                   @rol("Líder de Zona")
                   <select class="form-control " ng-model="reporte.name"  ng-change="cambio()">
                       <option value="">Seleccione...</option>
                       <option ng-repeat="reporte in reportes2" value="@{{reporte.name}}" ng-bind="reporte.name"></option>
                     </select>
                  @endrol  
                </div> 
                <section class="col-xs-12" style="padding-top: 20px;">
                  <div ng-show="view"> <!-- //////////////////////////////////// CIRCULARES /////////////////////// -->
                    @rol("Secretario(a) de Club", "Secretario(a) de Sociedad de Jóvenes")
                        <strong style="margin-top: 20px; font-size: 25px;">Circulares del Líder Juvenil: <strong style="margin-top: 20px; font-size: 25px;"></strong></strong>
                        <input type="hidden" ng-model="categoria" value="@{{categoria}}" ng-init="categoria = {{ $categoria }}" >
                        <input type="hidden" ng-model="id" value="@{{id}}" ng-init="id = {{ $id }}" >
                        <div class="row" style="margin-top: 20px; margin-left: 15px;" ng-repeat="circular in circulares | filter: { estado : 1, categoria_id: categoria, user_id: id}">
                           <a  href="circular/1/@{{ circular.id }}"  target="_blank" class="glyphicon glyphicon-download-alt"><span  style="margin-left: 10px;"  ng-bind="circular.titulo" ></span></a>
                        </div>
                        <strong style="margin-top: 20px; font-size: 25px;">Circulares del Líder de Zona: <strong style="margin-top: 20px; font-size: 25px;"></strong></strong>
                        <input type="hidden" ng-model="user" value="@{{user}}" ng-init="user = {{ $user }}" >
                        <div class="row" style="margin-top: 20px; margin-left: 15px;" ng-repeat="circular in circulares | filter: { estado : 1, categoria_id: categoria, user_id: user}">
                           <a  href="circular/1/@{{ circular.id }}"  target="_blank" class="glyphicon glyphicon-download-alt"><span  style="margin-left: 10px;"  ng-bind="circular.titulo" ></span></a>
                        </div>
                    @endrol
                    @rol("Líder Juvenil")
                    <div ng-repeat="zona in zonas" >
                      <strong style="margin-top: 20px; font-size: 25px;">Circulares del Líder de la <strong style="margin-top: 20px; font-size: 25px;" ng-bind=" zona.nombre"></strong></strong>
                        <div class="row" style="margin-top: 20px; margin-left: 15px;" ng-repeat="circular in circulares | filter: { estado : 1, user_id: zona.user_id}">
                           <a  href="circular/1/@{{ circular.id }}"  target="_blank" class="glyphicon glyphicon-download-alt" ><span  style="margin-left: 10px;"  ng-bind="circular.titulo" ></span></a>
                        </div>
                    </div>
                    @endrol  
                    @rol("Líder de Zona")
                      <h3 style="padding-top: 20px;">Circulares del Líder Juvenil</h3>
                      <strong>Aventureros</strong>
                      <ul>
                        <li ng-repeat="circular in circulares | filter: { estado : 1, categoria_id: 1, user_id: zona.user_id}"><a  href="circular/1/@{{ circular.id }}"  target="_blank"><button class="btn btn-default" type="button" title="Ver/Imprimir" ng-bind="circular.titulo"></button></a></li>
                      </ul>
                      <strong>Conquistadores</strong>
                      <ul>
                        <li ng-repeat="circular in circulares | filter: { estado : 1, categoria_id: 2, user_id: zona.user_id}"><a  href="circular/1/@{{ circular.id }}"  target="_blank"><button class="btn btn-default" type="button" title="Ver/Imprimir" ng-bind="circular.titulo"></button></a></li>
                      </ul>
                      <strong>Guías mayores</strong>
                      <ul>
                        <li ng-repeat="circular in circulares | filter: { estado : 1, categoria_id: 3, user_id: zona.user_id}"><a  href="circular/1/@{{ circular.id }}"  target="_blank"><button class="btn btn-default" type="button" title="Ver/Imprimir" ng-bind="circular.titulo"></button></a></li>
                      </ul>
                      <strong>Jóvenes</strong>
                      <ul>
                        <li ng-repeat="circular in circulares | filter: { estado : 1, categoria_id: 4, user_id: zona.user_id}"><a  href="circular/1/@{{ circular.id }}"  target="_blank"><button class="btn btn-default" type="button" title="Ver/Imprimir" ng-bind="circular.titulo"></button></a></li>
                      </ul>
                    @endrol  
                  </div>
                  <div ng-show="view1"> <!-- ////////////////////////////////// GRUPOS /////////////////////////////-->
                    <div class="row col-xs-6">
                      <div class="form-group ">
                        <div class="btn-group">
                          <button class="btn " ng-class="{'btn-grupo':categoria.id === option, 'btn-default':categoria.id != option}" ng-repeat="categoria in categorias" name="option" ng-click="changeOption(categoria)" ng-bind="categoria.nombre"></button>
                        </div>
                      </div>
                      
                    </div> 
                    <section ng-show="selecciones">
                      <div class="row col-xs-8" style="padding-top: 10px;">
                        <select class="form-control col-xs-6" ng-model="club">
                          <option value="">Seleccione club...</option>
                          <option ng-repeat="club in clubes | filter: { categoria_id: option}" ng-bind="club.nombre" value="@{{club.id}}"></option>
                        </select>
                      </div> 
                      <div class="row col-xs-8" style="padding-top: 10px;">
                          <div class="row" style="margin-top: 20px; margin-left: 15px;" ng-repeat="grupo in grupos | filter:{club_id : club}">
                            <a  href="grupo/1/@{{ grupo.id }}" target="_blank" ng-if="club != null" style="margin-left: 15px;" class="glyphicon glyphicon-download-alt"><span  style="margin-left: 10px;" ng-bind="grupo.nombre" ></span></a>
                          </div>
                      </div>
                    </section>  
                    <section ng-show="selecciones1">
                        <div class="row col-xs-8" style="">
                          <select class="form-control" ng-model="zona">
                           <option value="">Seleccione zona...</option>
                           <option ng-repeat="zona in zonas" ng-bind="zona.nombre" value="@{{zona.id}}"></option>
                          </select>
                        </div> 
                        <div class="row col-xs-8" style="padding-top: 10px;">
                          <select class="form-control" ng-model="iglesia">
                           <option value="">Seleccione iglesia..</option>
                           <option ng-repeat="iglesia in iglesias" ng-bind="iglesia.nombre" value="@{{iglesia.id}}"></option>
                          </select>
                        </div> 
                        <div class="row col-xs-8" style="padding-top: 10px;">
                          <div class="row" style="margin-top: 20px; margin-left: 15px;" ng-repeat="grupo in grupos | filter:{iglesia_id : iglesia}" >
                            <a  href="grupo/1/@{{ grupo.id }}" target="_blank"  ng-if="iglesia != null"  class="glyphicon glyphicon-download-alt"><span  style="margin-left: 10px;"  ng-bind="grupo.nombre" ></span></a>
                          </div>
                        </div>
                    </section>
                  </div>

                  <div ng-show="view2"> <!-- ////////////////////////// DIRECTIVAS /////////////////////////////-->
                    <div class="row col-xs-6">
                      <div class="form-group ">
                        <div class="btn-group">
                          <button class="btn " ng-class="{'btn-grupo':categoria.id === option, 'btn-default':categoria.id != option}" ng-repeat="categoria in categorias" name="option" ng-click="changeOption(categoria)" ng-bind="categoria.nombre"></button>
                        </div>
                      </div>
                    </div> 
                    <section ng-show="selecciones">
                      <div class="row col-xs-8" style="padding-top: 10px;">
                        <select class="form-control col-xs-6" ng-model="club">
                          <option value="">Seleccione club...</option>
                          <option ng-repeat="club in clubes | filter: { categoria_id: option}" ng-bind="club.nombre" value="@{{club.id}}"></option>
                        </select>
                      </div> 
                      <div class="row col-xs-8" style="padding-top: 10px;">
                          <div class="row" style="margin-top: 20px; margin-left: 15px;" >
                            <a  href="directiva/1/@{{ club }}" ng-if="club != null" style="margin-left: 15px;" target="_blank" class="glyphicon glyphicon-download-alt"><span style="margin-left: 10px;">Click para Ver/Descargar</span></a>
                          </div>
                      </div>
                    </section>  
                    <section ng-show="selecciones1">
                      <div class="row col-xs-8" style="">
                          <select class="form-control" ng-model="zona">
                           <option value="">Seleccione zona...</option>
                           <option ng-repeat="zona in zonas" ng-bind="zona.nombre" value="@{{zona.id}}"></option>
                          </select>
                      </div> 
                      <div class="row col-xs-8" style="padding-top: 10px;">
                          <select class="form-control" ng-model="iglesia">
                           <option value="">Seleccione iglesia..</option>
                           <option ng-repeat="iglesia in iglesias" ng-bind="iglesia.nombre" value="@{{iglesia.id}}"></option>
                          </select>
                      </div> 
                      <div class="row col-xs-8" style="padding-top: 10px;">
                          <div class="row" style="margin-top: 20px; margin-left: 15px;" >
                            <a  href="directiva/1/@{{ iglesia }}" ng-if="iglesia != null" style="margin-left: 15px;" target="_blank" class="glyphicon glyphicon-download-alt"><span style="margin-left: 10px;">Click para Ver/Descargar</span></a>
                          </div>
                      </div>
                    </section>
                  </div>
                  <div ng-show="view3"> <!-- REGLAMENTOS -->
                    @rol("Secretario(a) de Club", "Secretario(a) de Sociedad de Jóvenes")
                      <input type="hidden" ng-model="categoria" value="@{{categoria}}" ng-init="categoria = {{ $categoria }}" >
                      <ul>
                        <li ng-repeat="reglamento in reglamentos | filter: { categoria_id: categoria}"><a  href="reglamento/1/@{{ reglamento.id }}"  target="_blank"><button class="btn btn-default" type="button" title="Ver/Imprimir" ng-bind="reglamento.nombre"></button></a></li>
                      </ul>
                    @endrol
                  </div>
                  <div ng-show="view4"> <!-- PAGOS -->
                    @rol("Secretario(a) de Club", "Secretario(a) de Sociedad de Jóvenes")
                      <input type="hidden" ng-model="entidad" value="@{{entidad}}" ng-init="entidad = {{ $entidad }}" >

                      <table id="detalles" class="table table-striped table-bordered table-condensed table-responsive" >
                        <thead style="background-color:  #20B2AA;">
                            <th>Reporte</th>
                            <th>Comprobante</th>
                            <th>Planilla</th>
                        </thead>
                        <tbody>
                            <tr ng-repeat="pago in pagos | filter: {club_id: entidad }">
                                <td><a href="pago/1/@{{ pago.id }}" target="_black" style=" font-weight: bold; font-size: 15px;" >Pago de @{{pago.tipo}}</a></td>
                                <td><a href="/download/@{{ pago.comprobante }}"  style=" font-size: 15px; font-weight: bold;">Descargar</a></td>
                                <td><a  href="/download/@{{ pago.planilla }}"  style="color: #CD853F; font-size: 15px; font-weight: bold;" ng-if="pago.tipo=='Inscripción de club/sj'">Descargar</a>
                                <span ng-if="pago.tipo=='Inscripción de acampantes'">No amerita</span> </td>
                            </tr>
                            <tr ng-repeat="pago in pagos | filter: {iglesia_id: entidad }">
                                <td><a href="pago/1/@{{ pago.id }}" target="_black" style="font-weight: bold; font-size: 15px;" >Pago de @{{pago.tipo}}</a></td>
                                <td><a href="/download/@{{ pago.comprobante }}"  style=" font-size: 15px; font-weight: bold;">Descargar</a></td>
                                <td><a  href="/download/@{{ pago.planilla }}"  style="color: #CD853F; font-size: 15px; font-weight: bold;" ng-if="pago.tipo=='Inscripción de club/sj'">Descargar</a>
                                <span ng-if="pago.tipo=='Inscripción de acampantes'">No amerita</span> </td>
                            </tr>
                       </tbody>     
                         
                     </table>
                    @endrol
                  </div>
                </section>
              </div>    
              

          </section>  
        </div>  
      </section>  
    </div>
  </section>
  
@endsection
@section('script')
  <script type="text/javascript" src="{{asset('app/ReporteCtrl.js')}}"></script>
  <script type="text/javascript" src="{{asset('app/PuntuacionCtrl.js')}}"></script>

@endsection