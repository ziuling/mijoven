<!-- Modal -->
<script type="text/ng-template" class="modal" id="VoluntarioModal.html">
    <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="VoluntarioModalLabel"></h4>
    </div>
      
        <div class="modal-body">
           <div class="row" >
                
                <div ng-repeat="categoria in categorias | filter:{tipo:1}">
                      <h3 style="padding-top: 20px;">Categoría: @{{ categoria.nombre}}</h3>
                      <div ng-repeat="club in clubes | filter: {categoria_id: categoria.id}">
                        <div ng-repeat="miembro in miembros | filter: { club_id: club.id, voluntario:1}">
                          <ul>
                            <li ng-bind="miembro.persona.nombre"></li>
                            <li ng-bind="miembro.persona.edad"></li> 
                            <li ng-bind="miembro.persona.correo"></li> 
                            <li ng-bind="miembro.persona.telefono"></li> 
                          </ul>
                         </div>
                      </div>
                      <div ng-repeat="club in clubes | filter: {categoria_id: categoria.id}">
                        <div ng-repeat="grupo in grupos | filter: {club_id: club.id}">
                          <div ng-repeat="miembro in miembros | filter: { grupo_id: grupo.id, voluntario:1}">
                            <ul>
                              <li ng-bind="miembro.persona.nombre"></li>
                              <li ng-bind="miembro.persona.edad"></li> 
                              <li ng-bind="miembro.persona.correo"></li> 
                              <li ng-bind="miembro.persona.telefono"></li> 
                            </ul>
                           </div>
                        </div>
                      </div>
                </div>    
		        
	   	   </div> 

   	   </div>
             
	    <div class="modal-footer">
	        <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
        </div>

</script>