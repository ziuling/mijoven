<!-- Modal -->
<script type="text/ng-template" class="modal" id="InscripcionModal.html">
    <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="InscripcionModalLabel"></h4>
    </div>
      
        <div class="modal-body">
           <div class="row" >
                
                <div ng-repeat="inscripcion in inscripciones | filter: { categoria_id: option}">
                    <strong>Montos válidos antes de la fecha: <span ng-bind="inscripcion.fecha"></span></strong>
                        <ul style="margin-top: ">
                          <li ><strong>Inscripción de club:</strong> <span ng-bind="inscripcion.monto"></span> Bs.</li>
                          <li ><strong>Inscripción por miembro: </strong><span ng-bind="inscripcion.monto_miembro"></span> Bs.</li>
                          <li ><strong>Inscripción por acompañante:</strong> <span ng-bind="inscripcion.monto_acompanante"></span> Bs.</li>
                          <li ><strong>Inscripción por niño: </strong><span ng-bind="inscripcion.monto_nino"></span> Bs.</li>
                          <li ><strong>Seguro:</strong> <span ng-bind="inscripcion.seguro"></span></li>
                        </ul>
                </div>    
		        
	   	   </div> 

   	   </div>
             
	    <div class="modal-footer">
	        <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
        </div>

</script>