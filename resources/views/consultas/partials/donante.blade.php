<!-- Modal -->
<script type="text/ng-template" class="modal" id="DonanteModal.html">
    <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="DonanteModalLabel"></h4>
    </div>
        <div class="modal-body">
           <div class="row" >
              @{{option}}
                      <div ng-repeat="club in clubes | filter: {categoria_id: option}">
                        <div ng-repeat="miembro in miembros | filter: { club_id: club.id, donante:1}">
                          <ul>
                            <li ng-bind="miembro.persona.nombre"></li>
                            <li ng-bind="miembro.persona.edad"></li> 
                            <li ng-bind="miembro.persona.correo"></li> 
                            <li ng-bind="miembro.persona.telefono"></li> 
                          </ul>
                         </div>
                      </div>
                      <div ng-repeat="club in clubes | filter: {categoria_id: option}">
                        <div ng-repeat="grupo in grupos | filter: {club_id: club.id}">
                          <div ng-repeat="miembro in miembros | filter: { grupo_id: grupo.id, donante:1}">
                            <ul>
                              <li ng-bind="miembro.persona.nombre"></li>
                              <li ng-bind="miembro.persona.edad"></li> 
                              <li ng-bind="miembro.persona.correo"></li> 
                              <li ng-bind="miembro.persona.telefono"></li> 
                            </ul>
                           </div>
                        </div>
                      </div>

   	   </div>
             
	    <div class="modal-footer">
	        <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
        </div>

</script>