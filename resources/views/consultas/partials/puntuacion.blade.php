<!-- Modal -->
<script type="text/ng-template" class="modal" id="PuntuacionModal.html">
    <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="PuntuacionModalLabel"></h4>
    </div>
      
        <div class="modal-body">
           <div class="row" >
                <input type="hidden" ng-model="club" value="@{{club}}" ng-init="club = {{ $club }}" >
	            <div class="panel panel-primary text-center">
		            <input type="hidden" ng-init="total(club)" ng-repeat="puntuacion in puntuaciones">
                    <strong style= "color:#A38A10; font-size: 20px;">Tu puntuación acumulada: <span style="font-size: 20px;"  ng-bind="total[club]"></span></strong>
		        </div>    
		        
	   	   </div> 

   	   </div>
             
	    <div class="modal-footer">
	        <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
        </div>

</script>