@extends('layouts.app')

@section('content')

    <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2">
        
        <div class="titulo">     
		    <h3 class="text-center">Restaurar Sistema</h3>
		</div>  
		
		@if (session('status'))
		    <div class="alert alert-success">
		        {{ session('status') }}
		    </div>
		@endif

		<section class="main">
        <div class="page-header"></div>
		    <div class="">
			  <!-- Tab panes -->
			  <div class="tab-content">
			    <div role="tabpanel" class="tab-pane active" id="tipos" ng-controller="">
					<table class="table table-hover table-responsive">
					    <thead>
							<th >Fecha</th>
							<th >Acciones</th>
						</thead>
						<tbody>
                            <tr ng-repeat="">
                                <td></td>
					            <td>
					                <button class="btn btn-primary" type="button" ng-click="Restaurar()"><span class="glyphicon glyphicon-"></span></button>
					            </td>
					        </tr>
					        <tr ng-if=""><td colspan="4"><h5 class="text-center">Sin registros</h5></td></tr>
						</tbody>
					</table>
			    </div>
		      </div>
			</div>	
	   </section>
    </div>
@endsection
@section('script')
@endsection