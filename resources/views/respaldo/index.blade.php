@extends('layouts.app')

@section('content')

    <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2">
        
        <div class="titulo">     
		    <h3 class="text-center">Gestionar Respaldo</h3>
		</div>  
		
		@if (session('status'))
		    <div class="alert alert-success">
		        {{ session('status') }}
		    </div>
		@endif

		<section class="main" style="margin-top: 70px;">

		   <div class="">
		        <div class="col-xs-6 col-xs-offset-3" >
		            <table >
		                <tr>
			                <td style="">    
	                           <a href="#respaldar" style=" font-size: 25px; margin: 20px;" >
	                               Hacer Respaldo
	                           </a>
			                </td>
			                <td style="">    
			   	               <a href="restaurar" style="margin: 20px; font-size: 25px">
			   	                   Restaurar Sistema
			   	               </a>
			                </td>
		                </tr>
                    </table>
                </div>
		   </div>
            
	   </section>
    </div>


@endsection