@extends('layouts.app')

@section('content')

  @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif

  <section id="main-content" >
    <div class="col-lg-12">
      <section class="wrapper">
        <div class="row">
          <div class="">
            <h3 class="page-header"><i class="fa fa-table"></i> Gestionar Puntuaciones</h3>
              <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="home">Inicio</a></li>
                <li><i class="fa fa-th-table"></i>Puntuaciones</li>
              </ol>
          </div>
        </div>
        <div class="row" ng-controller="PuntuacionCtrl">
          <section class="panel">
            <header class="panel-heading">
            </header>
            <div class="page-header">
              <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                       <a href="#mensuales" aria-controls="mensuales" role="tab" data-toggle="tab">Eventos mensuales</a>
                    </li>
                    <li role="presentation">
                       <a href="#generales" aria-controls="generales" role="tab" data-toggle="tab">Eventos Generales</a>
                    </li>
                    <li role="presentation">
                       <a href="#puntuacion" aria-controls="puntuacion" role="tab" data-toggle="tab">Puntuación general</a>
                    </li>
              </ul>
            </div>
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" ng-view="mensuales" id="mensuales" >
                <div class="row"> 
                  <div class="col-xs-6">
                    <div class="btn-group " style="margin-bottom: 5px">
                      <button class="btn " ng-class="{'btn-grupo':categoria.id === option, 'btn-default':categoria.id != option}" ng-repeat="categoria in categorias" name="option" ng-click="changeOption(categoria)" ng-bind="categoria.nombre"></button>
                    </div>
                  </div>
                  <div class="col-xs-4"> 
                    <div class="form-group" style="margin-bottom: 5px">
                      <select class="form-control" name="circular" ng-model="puntuacion.circular" required ng-options="circular.id as circular.titulo for circular in circulares | filter: {categoria_id: option} " ng-change="change()">
                        <option value="">Seleccione...</option>
                      </select>
                    </div>
                  </div>        
                  <div class="col-xs-2">      
                    <div class="form-group">
                      <select class="form-control"  name="mes" ng-model="puntuacion.mes" ng-change="cambio()" >
                        <option ng-repeat="mes in meses" value="@{{ mes.name }}" ng-bind="mes.name"></option>
                      </select>
                    </div>  
                  </div>     
                </div> 
                  <section>
                    <table ng-show="view" ng-if="option != 4" class="table table-striped table-advance table-hover" >
                      <thead>
                        
                      </thead>
                      <thead>
                        <th>Nº</th>
                        <th>Nombre de Club</th>
                        <th ng-repeat="actividad in actividades | filter:{ tipo_a_id: 1, circular_id: puntuacion.circular}" ng-bind="actividad.nombre"></th>
                        <th ng-repeat="actividad in actividades | filter:{circular_id: puntuacion.circular, tipo_a_id: 3, mes:puntuacion.mes}" ng-bind="actividad.nombre "></th>
                        <th>Total</th>
                      </thead>
                      <tbody>
                        <tr ng-repeat="club in clubes | filter: {categoria_id: option}" >
                          <td ng-bind="$index + 1"></td>
                          <td ng-bind="club.nombre"></td>
                          <td ng-repeat="actividad in actividades | filter:{circular_id: puntuacion.circular, tipo_a_id: 1}"><strong ng-repeat="puntuacion in puntuaciones | filter: {mes:puntuacion.mes, club_id: club.id}">
                          <span ng-repeat="detalle in detalles | filter:{ puntuacion_id: puntuacion.id, actividad_id:actividad.id}" ng-bind="detalle.puntos"></span></strong></td>
                          <td ng-repeat="actividad in actividades | filter:{circular_id: puntuacion.circular, tipo_a_id: 3, mes: puntuacion.mes}">
                          <strong ng-repeat="puntuacion in puntuaciones | filter: {mes:puntuacion.mes, club_id: club.id}"><span ng-repeat="detalle in detalles | filter:{puntuacion_id: puntuacion.id, actividad_id:actividad.id}" ng-bind="detalle.puntos"></span></strong></td>
                          <td ng-repeat="puntuacion in puntuaciones | filter: {circular_id: puntuacion.circular, mes:puntuacion.mes, club_id: club.id}" ng-bind="puntuacion.totalm" ng-if=" $last" ></td>
                          <!--<input type="hidden" ng-init="tom(club, puntuacion.mes)" ng-repeat="puntuacion in puntuaciones">-->
                        </tr>
                      </tbody>
                    </table>
                    <table ng-show="view" ng-if="option == 4" class="table table-striped table-advance table-hover" >
                      <thead>
                      </thead>
                      <thead>
                        <th>Nº</th>
                        <th>Nombre de Iglesia</th>
                        <th ng-repeat="actividad in actividades | filter:{ tipo_a_id: 1, circular_id: puntuacion.circular}" ng-bind="actividad.nombre"></th>
                        <th ng-repeat="actividad in actividades | filter:{circular_id: puntuacion.circular, tipo_a_id: 3, mes:puntuacion.mes}" ng-bind="actividad.nombre "></th>
                        <th>Total</th>
                      </thead>
                      <tbody>
                        <tr ng-repeat="iglesia in iglesias" >
                          <td ng-bind="$index + 1"></td>
                          <td ng-bind="iglesia.nombre"></td>
                          <td ng-repeat="actividad in actividades | filter:{circular_id: puntuacion.circular, tipo_a_id: 1}"><strong ng-repeat="puntuacion in puntuaciones | filter: {mes:puntuacion.mes, iglesia_id: iglesia.id}">
                          <span ng-repeat="detalle in detalles | filter:{ puntuacion_id: puntuacion.id, actividad_id:actividad.id}" ng-bind="detalle.puntos"></span></strong></td>
                          <td ng-repeat="actividad in actividades | filter:{circular_id: puntuacion.circular, tipo_a_id: 3, mes: puntuacion.mes}">
                          <strong ng-repeat="puntuacion in puntuaciones | filter: {mes:puntuacion.mes, iglesia_id: iglesia.id}"><span ng-repeat="detalle in detalles | filter:{puntuacion_id: puntuacion.id, actividad_id:actividad.id}" ng-bind="detalle.puntos"></span></strong></td>
                          <td ng-repeat="puntuacion in puntuaciones | filter: {circular_id: puntuacion.circular, mes:puntuacion.mes, iglesia_id: iglesia.id}" ng-bind="puntuacion.totalm" ng-if="puntuacion.totalm!=0" ></td>
                          </span>
                          
                        </tr>
                      </tbody>
                    </table>
                    <button class="btn btn-enviar pull-right" type="button" ng-click="create(puntuacion)" ng-disabled="puntuacion.circular==null || puntuacion.mes == null">Consulta</button>
                    @include('puntuaciones.partials.puntuacion')

                  </section> 
              </div>    
              <div role="tabpanel" class="tab-pane" ng-view="generales" id="generales">
                <div class="row"> 
                  <div class="col-xs-5">
                    <div class="btn-group " style="margin-bottom: 5px">
                      <button class="btn " ng-class="{'btn-grupo':categoria.id === option, 'btn-default':categoria.id != option}" ng-repeat="categoria in categorias" name="option" ng-click="changeOption(categoria)" ng-bind="categoria.nombre" ></button>
                    </div>
                  </div>
                  <div class="col-xs-5"> 
                    <div class="form-group" style="margin-bottom: 5px">
                      <select class="form-control" name="circular" ng-model="puntuacion.circular" required ng-options="circular.id as circular.titulo for circular in circulares | filter: {categoria_id: option} " ng-change="change()">
                        <option value="">Seleccione...</option>
                      </select>
                    </div>
                  </div>             
                </div> 
                <section>
                    <table ng-if="option != 4" class="table table-striped table-advance table-hover" >
                      <thead>
                        
                      </thead>
                      <thead>
                        <th>Nº</th>
                        <th>Nombre de Club</th>
                        <th ng-repeat="actividad in actividades | filter:{ tipo_a_id: 2, circular_id: puntuacion.circular}" ng-bind="actividad.nombre"></th>
                        <th>Total</th>
                      </thead>
                      <tbody>
                        <tr ng-repeat="club in clubes | filter: {categoria_id: option}" >
                          <td ng-bind="$index + 1"></td>
                          <td ng-bind="club.nombre"></td>
                          <td ng-repeat="actividad in actividades | filter:{circular_id: puntuacion.circular, tipo_a_id: 2}"><strong ng-repeat="puntuacion in puntuaciones | filter: {club_id: club.id}">
                          <span ng-repeat="detalle in detalles | filter:{ puntuacion_id: puntuacion.id, actividad_id:actividad.id}" ng-bind="detalle.puntos"></span></strong></td>
                          <td ng-repeat="puntuacion in puntuaciones | filter: { club_id: club.id}" ng-bind="puntuacion.totalg" ng-if="puntuacion.totalg !=0"></td>
                        </tr>
                      </tbody>
                    </table>
                    <table ng-if="option == 4" class="table table-striped table-advance table-hover" >
                      <thead>
                      </thead>
                      <thead>
                        <th>Nº</th>
                        <th>Nombre de Iglesia</th>
                        <th ng-repeat="actividad in actividades | filter:{ tipo_a_id: 2, circular_id: puntuacion.circular}" ng-bind="actividad.nombre"></th>
                        <th></th>
                        <th>Total</th>
                      </thead>
                      <tbody>
                        <tr ng-repeat="iglesia in iglesias" >
                          <td ng-bind="$index + 1"></td>
                          <td ng-bind="iglesia.nombre"></td>
                          <td ng-repeat="actividad in actividades | filter:{circular_id: puntuacion.circular, tipo_a_id: 2}"><strong ng-repeat="puntuacion in puntuaciones | filter: {iglesia_id: iglesia.id}">
                          <span ng-repeat="detalle in detalles | filter:{ puntuacion_id: puntuacion.id, actividad_id:actividad.id}" ng-bind="detalle.puntos"></span></strong></td>
                          <td ng-repeat="puntuacion in puntuaciones | filter: { iglesia_id: iglesia.id}" ng-bind="puntuacion.totalg" ng-if="puntuacion.totalg !=0"></td>
                        </tr>
                      </tbody>
                    </table>
                    <button class="btn btn-enviar pull-right" type="button" ng-click="crear(puntuacion)" ng-disabled="puntuacion.circular==null">Consulta</button>
                    @include('puntuaciones.partials.general')
                </section> 
              </div>
              <div role="tabpanel" class="tab-pane" id="puntuacion" >
              <div class="row"> 
                  <div class="col-xs-5">
                    <div class="btn-group " style="margin-bottom: 5px">
                      <button class="btn " ng-class="{'btn-grupo':categoria.id === option, 'btn-default':categoria.id != option}" ng-repeat="categoria in categorias" name="option" ng-click="changeOption(categoria)" ng-bind="categoria.nombre"></button>
                    </div>
                  </div>             
                </div> 
                <section>
                    <table ng-if="option != 4" class="table table-striped table-advance table-hover" >
                      <thead>
                        
                      </thead>
                      <thead>
                        <th>Nº</th>
                        <th>Nombre de Club</th>
                        <th>Enero</th>
                        <th>Febrero</th>
                        <th>Marzo</th>
                        <th>Abril</th>
                        <th>Mayo</th>
                        <th>Junio</th>
                        <th>Julio</th>
                        <th>Agosto</th>
                        <th>Septiembre</th>
                        <th>Octubre</th>
                        <th>Noviembre</th>
                        <th>Diciembre</th>
                        <th>Generales</th>
                        <th>Total</th>
                      </thead>
                      <tbody>
                        <tr ng-repeat="club in clubes | filter: {categoria_id: option}" >
                          <td ng-bind="$index + 1"></td>
                          <td ng-bind="club.nombre"></td>
                          <td  ng-repeat="mes in meses" >
                          <span  ng-repeat="puntuacion in puntuaciones | filter:{club_id: club.id, mes: mes.name}" ng-if="$last">@{{puntuacion.totalm}}</span>
                          </td>
                          <td ng-repeat="puntuacion in puntuaciones | filter:{club_id: club.id, totalm: null}" ng-bind="puntuacion.totalg" >
                          </td>
                          <td ng-bind="total[club.id]"></td>
                          <input type="hidden" ng-init="total(club)" ng-repeat="puntuacion in puntuaciones" name="">
                        </tr>
                      </tbody>
                    </table>
                    <table ng-if="option == 4" class="table table-striped table-advance table-hover" >
                      <thead>
                      </thead>
                      <thead>
                        <th>Nº</th>
                        <th>Nombre de Iglesia</th>
                        <th>Enero</th>
                        <th>Febrero</th>
                        <th>Marzo</th>
                        <th>Abril</th>
                        <th>Mayo</th>
                        <th>Junio</th>
                        <th>Julio</th>
                        <th>Agosto</th>
                        <th>Septiembre</th>
                        <th>Octubre</th>
                        <th>Noviembre</th>
                        <th>Diciembre</th>
                        <th>Generales</th>
                        <th>Total</th>
                      </thead>
                      <tbody>
                        <tr ng-repeat="iglesia in iglesias " >
                          <td ng-bind="$index + 1"></td>
                          <td ng-bind="iglesia.nombre"></td>
                          <td  ng-repeat="mes in meses" >
                          <span  ng-repeat="puntuacion in puntuaciones | filter:{iglesia_id: iglesia.id, mes: mes.name}" ng-if="$last" >@{{puntuacion.totalm}}</span>
                          </td>
                          <td ng-repeat="puntuacion in puntuaciones | filter:{iglesia_id: iglesia.id, totalm: null}" ng-bind="puntuacion.totalg" >
                          </td>
                          <td  ng-bind="totali[iglesia.id]"></td>
                          <input type="hidden" ng-init="totali(iglesia)" ng-repeat="puntuacion in puntuaciones">
                        </tr>
                      </tbody>
                    </table>
                </section>    
              </div> 
            </div>
          </section>  
        </div>  
      </section>  
    </div>
  </section>
  
@endsection
@section('script')
  <script type="text/javascript" src="{{asset('app/PuntuacionCtrl.js')}}"></script>

@endsection