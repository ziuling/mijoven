<!-- Modal -->
<script type="text/ng-template" id="GeneralModal.html">
      <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="GeneralModalLabel">Consultar Actividades</h4>
      </div>

      <form class="form" name="form" ng-cloak novalidate>
        {{ csrf_field() }}
        <div class="modal-body">
          <div class="row">
            <input type="hidden" name="mes" value="@{{puntuacion.mes}}">
            <input type="hidden" name="circular_id" value="@{{puntuacion.circular_id}}">
              
                <div class="col-xs-10 col-xs-offset-1 form-group" ng-if="puntuacion.categoria_id!='4'">
                  <select class="form-control" name="club_id" ng-model="puntuacion.club_id" ng-options="club.id as club.nombre for club in clubes | filter: { categoria_id: puntuacion.categoria_id}">
                  <option value="" >Seleccione...</option>
                  </select> 
                </div>
                <div class="col-xs-10 col-xs-offset-1 form-group" ng-if="puntuacion.categoria_id=='4'">
                  <select class="form-control" name="iglesia_id" ng-model="puntuacion.iglesia_id" ng-options="iglesia.id as iglesia.nombre for iglesia in iglesias ">
                    <option value="" >Seleccione...</option>
                  </select> 
                </div>
                

                <div class="col-xs-10 col-xs-offset-1 form-group"  ng-if="puntuacion.club_id != null" >
                  <div class=" form-group" style="background-color:  #20B2AA; ">
                    <a href="planificacion/1/@{{puntuacion.club_id}}" target="_black" style="color: #FFF5EE; font-weight: bold;">Planificación anual</a>
                  </div>
                  <div class=" form-group"  ng-repeat="pago in pagos | filter: {club_id: puntuacion.club_id }" style="background-color:  #20B2AA; border-radius: 5px;">
                    <a href="pago/1/@{{ pago.id }}" target="_black" style="color:  #FFF5EE; font-weight: bold;" >Pago de @{{pago.tipo}}</a>
                    <span>/</span>
                    <a href="/download/@{{ pago.comprobante }}"  style="color:   #FFF5EE; font-weight: bold;">Comprobante</a>
                    <span ng-if="pago.tipo=='Inscripción de club/sj'">/</span>
                    <a  href="/download/@{{ pago.planilla }}"  style="color: #CD853F; font-weight: bold;" ng-if="pago.tipo=='Inscripción de club/sj'">Planilla</a>
                  </div>
                  
                  
                </div>
                
                <div class="col-xs-10 col-xs-offset-1 form-group" ng-if="puntuacion.iglesia_id != null" style=" padding: 40px;">
                  
                  <div class="  form-group" style="background-color:  #20B2AA; border-radius: 5px;">
                    <a href="planificacion/2/@{{puntuacion.iglesia_id}}" target="_black" style="color:  #FFF5EE; font-weight: bold;">Planificación anual</a>
                  </div>
                  <div class="  form-group" ng-repeat="pago in pagos | filter: {iglesia_id: puntuacion.iglesia_id }" style="background-color:  #20B2AA; border-radius: 5px;">
                    <a  href="pago/1/@{{ pago.id }}" target="_black" style="color:  #FFF5EE; font-weight: bold;" >Pago de @{{pago.tipo}}</a>
                    <span>/</span>
                    <a  href="/download/@{{ pago.comprobante }}"  style="color:   #FFF5EE; font-weight: bold;">Comprobante</a>
                    <span ng-if="pago.tipo=='Inscripción de club/sj'">/</span>
                    <a  href="/download/@{{ pago.planilla }}"  style="color: #CD853F; font-weight: bold;" ng-if="pago.tipo=='Inscripción de club/sj'">Planilla</a>
                  </div>

                </div>

                
                <div class="form-group col-xs-10 col-xs-offset-1" style="background-color: #E0FFFF; padding: 40px;">
                  <strong style="font-weight: bold; font-size: 20px;">Confirme para la asignación de las puntuaciones respectivas:</strong>
                  <div class="row" style="padding-top: 15px;">
                    <label ng-repeat="actividad in actividades | filter: { tipo_a_id: 2, circular_id: puntuacion.circular}" style="margin-left:10px; ">
                    <input type="checkbox" checklist-model="detalle.actividades" checklist-value="actividad.id" ng-change="check(actividad, checked)" style="margin-right:10px; "> @{{actividad.nombre}}
                    </label>
                  </div>
                </div>
              </div>
              <input type="hidden"  name="totalg" ng-model="puntuacion.totalg" value="@{{ puntuacion.totalg }}" >
              <input type="hidden"  name="total" ng-model="puntuacion.total" value="@{{ puntuacion.totalm + puntuacion.totalg }}" >
        </div>    
        <div class="modal-footer">
          <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
          <button type="submit" class="btn btn-success" ng-click="save(form)" ng-disabled="puntuacion.club_id==null" ng-if="option != 4">Asignar</button>
          <button type="submit" class="btn btn-success" ng-click="save(form)" ng-disabled="puntuacion.iglesia==null" ng-if="option == 4">Asignar</button>
        </div>                    

      </form>
  
</script>
