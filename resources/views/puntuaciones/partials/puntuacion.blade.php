<!-- Modal -->
<script type="text/ng-template" id="PuntuacionModal.html">
      <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="PuntuacionModalLabel">Consultar Actividades<span class="glyphicon glyphicon-list-alt" style="margin-left: 5px;"></span></h4>
      </div>

      <form class="form" name="form" ng-cloak novalidate>
        {{ csrf_field() }}
        <div class="modal-body">
          <div class="row">

            <input type="hidden" name="mes" value="@{{ puntuacion.mes }}">
            <input type="hidden" name="circular_id" value="@{{puntuacion.circular_id}}">
             
                <div class="" > 
                  <div class="col-xs-10 col-xs-offset-1 form-group">
                    <select class="form-control" name="club_id" ng-model="puntuacion.club_id" ng-options="club.id as club.nombre for club in clubes | filter: { categoria_id: puntuacion.categoria_id} "  ng-if="puntuacion.categoria_id!='4'">
                    <option value="" >Seleccione club</option>
                    </select> 
                    <select class="form-control" name="iglesia_id" ng-model="puntuacion.iglesia_id" ng-options="iglesia.id as iglesia.nombre for iglesia in iglesias" ng-if="puntuacion.categoria_id=='4'" >
                    <option value="" >Seleccione iglesia</option>
                    </select> 
                  </div>
                   <div class="col-xs-10 col-xs-offset-1 form-group"  ng-if="puntuacion.club_id != null"  >
                      <div class=" form-group" style="background-color:  #20B2AA; ">
                        <a href="plan/@{{ puntuacion.club_id }}/@{{ puntuacion.mes }}" target="_black" style="color:  #FFF5EE; font-weight: bold;">Plan de sostenimiento</span></a>
                      </div>
                      <div class="form-group" style="background-color:  #20B2AA; " >
                       <a href="informe/1/@{{ puntuacion.club_id }}/@{{puntuacion.mes}}" target="_black" style="margin-left: 5px; color:   #FFF5EE; font-weight: bold;" >Informe mensual <span style="padding-left: 5px;">J.A</span></a>
                      </div>
                      <div class="form-group" style="background-color:  #20B2AA; ">
                        <a href="ministerio/1/@{{ puntuacion.club_id}}/@{{puntuacion.mes}}" target="_black" style="margin-left: 5px; color:   #FFF5EE; font-weight: bold;">Ministerio fuera <span style="padding-left: 5px;">de serie</span></a>
                      </div>
                   </div>
                
                    <div class="col-xs-10 col-xs-offset-1 form-group" ng-if="puntuacion.iglesia_id != null" >
                      <div class="form-group" style="background-color:  #20B2AA; ">
                        <a href="informe/2/@{{ puntuacion.iglesia_id }}/@{{puntuacion.mes}}" target="_black" style="margin-left: 5px; color:  #FFF5EE; font-weight: bold;" >Informe mensual <span style="padding-left: 5px;">J.A</span> </a>
                      </div>
                      <div class="form-group" style="background-color:  #20B2AA; ">
                       <a href="ministerio/2/@{{ puntuacion.iglesia_id}}/@{{puntuacion.mes}}" target="_black" style="margin-left: 5px; color:  #FFF5EE; font-weight: bold;">Ministerio fuera <span style="padding-left: 5px;">de serie</span></a>
                      </div>
                    </div>
              
                  <div class="col-xs-10 col-xs-offset-1 form-group"  >
                    <h4>Grupos pequeños Salvación y Servicio:</h4>
                  </div>
                    <div class="col-xs-2 col-xs-offset-1 form-group"  ng-repeat="grupo in grupos | filter:{club_id:puntuacion.club_id}" ng-if="puntuacion.club_id != null" style="background-color: #FFEFD5; ">
                      <a href="grupo/1/@{{ grupo.id }}" style="color: #CD853F; font-weight: bold;" target="_black">@{{grupo.nombre}}</a>
                    </div>
                    <div class="col-xs-2 col-xs-offset-1 form-group"  ng-repeat="grupo in grupos | filter:{iglesia_id:puntuacion.iglesia_id}" ng-if="puntuacion.iglesia_id != null" style="background-color: #FFEFD5;">
                      <a href="grupo/2/@{{ grupo.id }}" style="color: #CD853F; font-weight: bold;" target="_black">@{{grupo.nombre}}</a>

                  </div>
                </div>
                
                <div class="col-xs-10 col-xs-offset-1 form-group" style="background-color: #E0FFFF; padding: 40px;">
                  <strong style="font-weight: bold; font-size: 20px; ">Confirme para la asignación de las puntuaciones respectivas:</strong>
                  <div class="row" style="padding-top: 15px;">
                    <label ng-repeat="actividad in actividades | filter: { tipo_a_id: 1, circular_id: puntuacion.circular_id}" style="margin-left:10px; color: #008080; font-weight: bold;">
                    <input type="checkbox" checklist-model="detalle.actividades" checklist-value="actividad.id" ng-change="check(actividad, checked)" style="margin-right:10px; "> @{{actividad.nombre}}
                    </label>
                  </div>
                  <div class="row">
                    <label ng-repeat="actividad in actividades | filter: { tipo_a_id: 3, circular_id: puntuacion.circular_id, mes:puntuacion.mes}" style="margin-left:10px; color: #008080; font-weight: bold;">
                    <input type="checkbox" checklist-model="detalle.actividades" checklist-value="actividad.id" ng-change="check(actividad, checked)" style="margin-right:10px; " > @{{actividad.nombre}}
                    </label>
                  </div>
                </div> 
              <input type="hidden"  name="totalm" ng-model="puntuacion.totalm" value="@{{ puntuacion.totalm }}" >
              <input type="hidden"  name="total" ng-model="puntuacion.total" value="@{{ puntuacion.totalm + puntuacion.totalg }}" >
             
          </div>
        </div>    
        <div class="modal-footer">
          <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
          <button type="submit" class="btn btn-success" ng-click="save(form)" ng-disabled="puntuacion.club_id==null" ng-if="option != 4">Asignar</button>
          <button type="submit" class="btn btn-success" ng-click="save(form)" ng-disabled="puntuacion.iglesia==null" ng-if="option == 4">Asignar</button>
        </div>                    

      </form>
  
</script>
