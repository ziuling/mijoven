<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15"/>
    
	<style>
	
		.text-center{
			text-align: center;
	        display: block;
	        width: 100%;
		}
		.text-right{
			text-align: right;
	        display: block;
	        width: 100%;
		}
		footer {
		  color: #5D6975;
		  width: 100%;
		  height: 30px;
		  position: absolute;
		  bottom: 0;
		  border-top: 1px solid #C1CED9;
		  padding: 8px 0;
		  text-align: center;
		}
		
	</style>
	<title>Circular</title>
</head>
<body>
   {{--  ENCABEZADO  --}}
   @if($data->categoria_id == 1)
    <div style="width:100%;display: block">
		<strong class="text-center" style="padding-top: 60px;"><img src="/xampp/htdocs/mijoven/public/img/aventureros.png" width="120" height="110" style="float:left;" />MISION VENEZOLANA NOR-ORIENTAL<img src="/xampp/htdocs/mijoven/public/img/Jovenesadventista.png" width="120" height="110" style="float:right;" class="imgs" /></strong>
		<strong class="text-center">MINISTERIOS JUVENILES</strong>
		<strong class="text-center" style="padding-top: 10px; font-size: 18px;">{{ $data->asunto }}</strong>
		<strong class="text-center" style="padding-top: 5px; font-size: 20px;">"{{ $data->titulo }}"</strong>
		<strong class="text-center" style="padding-top: 5px; text-decoration: underline; font-size: 18px;">{{ $data->sub_titulo }}</strong>
        
    </div> 
    <div class="row" style="padding-top: 80px">
        <p style="text-align: justify; ">{!! $data->introduccion !!}</p>
        
	    <h4>Eventos Precamporee</h4>
	    @foreach($actividades as $actividad)
	     <ul>
	     	<li>{{ $actividad->nombre }} : <p>{{ $actividad->descripcion }}</p></li>
	     </ul>
	    @endforeach
        <p style="text-align: justify;">{!! $data->observacion !!}</p>
        <p style="text-align: justify;">{!! $data->recomendacion !!}</p>

    </div>
    @elseif($data->categoria_id == 2)
    <div style="width:100%;display: block">
		<span class="text-right">{{ $date }}</span>
		<span class="text-center"><img src="/xampp/htdocs/mijoven/public/img/mvnor.jpg" width="120" height="110"></span>
		<span class="text-center">Misión Venezolana Nor-Oriental</span>
		<strong class="text-center"> {{ $data->titulo }} </strong>
    </div>  

    <div >
   
    </div>
    @elseif($data->categoria_id == 3)  
    @elseif($data->categoria_id == 4)  
    @endif
    <footer>
       <div class="row">
           <strong>Ministerio Juveniles-Misión Venezolana Nororiental</strong>
       </div>
       <div class="row">
           @if($rol == 3)
              <span>Líder de la {{$data->user->zona->nombre}} :<strong>  {{ $data->user->nombre}}</strong></span>
           @else
              <span>Líder Juvenil:<strong>  {{ $lider }}</strong></span>
           @endif
       </div>

    </footer>
</body>

</html>