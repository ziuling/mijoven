@extends('layouts.app')

@section('content')

		@if (session('status'))
		    <div class="alert alert-success">
		        {{ session('status') }}
		    </div>
		@endif

		<section id="main-content">
            <section class="wrapper">
		  <div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-th-list"></i> Gestionar Circular</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="home">Inicio</a></li>
						<li><i class="icon_document_alt"></i><a href="actividades">Actividades</a></li>
						<li><i class="fa fa-th-list"></i>Circulares</li>
					</ol>
				</div>
			</div>
              <!-- page start-->
                <div class="row">
                 
                  <div class="col-lg-12">
                      <section class="panel" ng-controller="CircularCtrl">
                          <div class="btn-group " style="margin-bottom: 5px">
		                      <button class="btn " ng-class="{'btn-grupo':categoria.id === option, 'btn-default':categoria.id != option}" ng-repeat="categoria in categorias" name="option" ng-click="changeOption(categoria)" ng-bind="categoria.nombre" ></button>
		                    </div>
                          <header class="panel-heading">
		                    
                          </header>
                        <table class="table table-striped table-advance table-hover">
						    <thead>
								<th >Título</th>
								<th >Acciones</th>
							</thead>
							<tbody>
	                            <tr ng-repeat="circular in circulares">
	                                <td ng-bind="circular.titulo"></td>
						                <td>
						                    <div class="btn-group">
							                    <button class="btn btn-info" type="button" ng-click="edit(circular)" title="Editar"><span class="glyphicon glyphicon-edit"></span></button>
											    <button class="btn btn-eliminar" type="button" ng-click="destroy(circular)" title="Eliminar"><i class="icon_close_alt2"></i></button>
										     	<!--<button class="btn btn-info" type="button" ng-click="detalle(circular)">Detalles</button-->										     	<button class="btn btn-primary" type="button" ng-click="enviar(circular)" title="Enviar"><span class="glyphicon glyphicon-share-alt"></span></button>
		                                        @include('circulares.partial.detalles')
									     	</div>
											<a href="circular/1/@{{ circular.id }}"  target="_blank"><button class="btn btn-default" type="button" title="Ver/Imprimir"><span class="glyphicon glyphicon-download-alt"></span></button></a>
										     	
						                </td>
						        </tr>
						        <tr ng-if="circulares.length <1"><td colspan="4"><h5 class="text-center">Sin registros</h5></td></tr>
							</tbody>
						</table>
	                    <button class="btn btn-enviar pull-right" type="button" ng-click="create()" style="margin-left: 2px;"><span class="glyphicon glyphicon-plus"></span> Nueva Circular</button>
	                    @include('circulares.partial.circular')
	                    </section>
				  </div>
	            </div>
	   </section>
	   </section>
@endsection
@section('script')
	<script type="text/javascript" src="{{asset('app/CircularCtrl.js')}}"></script>
@endsection