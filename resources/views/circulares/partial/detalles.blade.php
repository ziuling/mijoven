<!-- Modal -->
<script type="text/ng-template" class="modal" id="detallesModal.html">
    <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="detallesModalLabel"> CIRCULAR: @{{circular.titulo}} </h4>
    </div>
      
        <div class="modal-body">
           <div class="row">
             <div class="col-xs-12 col-sm-10 col-sm-offset-1">
	            <div class="form-group col-xs-8">
		            <label class="control-label ">Categoría:</label>  @{{circular.categoria_id}}		
		        </div>
				<div class="form-group col-xs-12">
						<label class="control-label">Sub-título:</label> @{{circular.sub_titulo || 'Sin sub-titulo' }}
				</div>
				<div class="form-group col-xs-12">
					    <label class="control-label ">Asunto:</label> @{{circular.asunto || 'Sin asunto' }}
				</div>
				<div class="form-group col-xs-8">
					    <label class="control-label ">Fecha:</label> @{{circular.fecha}}

				</div>
				<div class="form-group col-xs-12">
					    <label class="control-label ">Destinarios:</label> @{{ circular.destinario || 'Sin destinario' }}
				</div>
				<div class="form-group col-xs-12">
					<label class="control-label" >Introducción:</label> 
					<span ng-bind-html="circular.introduccion"></span>
				</div>
				<div class="form-group col-xs-12">
					    <label class="control-label">Recomendaciones:</label> @{{circular.recomendacion || 'Sin recomendaciones' }}
				</div>
				<div class="form-group col-xs-12">
					    <label class="control-label ">Reconocimientos:</label> @{{circular.reconocimiento || 'Sin reconocimientos' }}
				</div>
				<div class="form-group col-xs-12">
					    <label class="control-label ">Observación:</label> @{{ circular.observacion || 'Sin observación'}}
				</div>
		    </div>	
	    </div>
	    <div class="modal-footer">
	        <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
        </div>

</script>