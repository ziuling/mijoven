<!-- Modal -->
<script type="text/ng-template" class="modal" id="CircularModal.html">
    <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="CircularModalLabel">Circular</h4>
    </div>
      <form class="form" name="form" ng-cloak novalidate>
        {{ csrf_field() }}
        <div class="modal-body">
       
           <div class="row">
                <div class="form-group col-xs-11">
                	<label class="control-label pull-right">Campos requeridos(<span class="required ">*</span>)</label>
                </div>
		        <input type="hidden" name="estado" ng-model="circular.estado" value="@{{circular.estado}}" class="form-control">

				<div class="form-group col-xs-10 col-xs-offset-1">
					<span class="required">*</span><label class="control-label">Título:</label>
		            <input type="text" name="titulo" ng-model="circular.titulo" class="form-control" required>
		            <span class="messages" ng-show="form.$submitted || form.name.$touched">
                    <span class="" ng-show="form.titulo.$error.required">Título requerido</span>
				</div>
				<div class="form-group col-xs-10 col-xs-offset-1">
					<label class="control-label">Sub-título:</label>
		            <input type="text" name="sub_titulo" ng-model="circular.sub_titulo" class="form-control">
				</div>
				<div class="form-group col-xs-10 col-xs-offset-1">
					<label class="control-label ">Asunto:</label>
		            <input type="text" name="asunto" ng-model="circular.asunto" class="form-control" >
				</div>
				<div class="form-group col-xs-10 col-xs-offset-1">
					<span class="required">*</span><label class="control-label ">Fecha:</label>
		            <input type="text" title="fecha de evento" name="fecha" ng-model="circular.fecha" class="form-control" placeholder="desde-hasta" required>
		            <span class="messages" ng-show="form.$submitted || form.name.$touched">
                    <span class="" ng-show="form.fecha.$error.required">Fecha requerida</span>
				</div>
				<div class="form-group col-xs-10 col-xs-offset-1">
					<label class="control-label ">Destinarios:</label>
					<textarea class="form-control" name="destinario" ng-model="circular.destinario" rows="1"></textarea>
				</div>
				<div class="form-group col-xs-10 col-xs-offset-1">
					<label class="control-label">Introducción:</label>
					<text-angular ta-toolbar="[['h1','h2','h3', 'h4', 'h5', 'quote'], ['bold','italics', 'underline', 'strikeThrough', 'colourRed'], ['ul', 'ol'], ['undo', 'redo'], ['justifyLeft','justifyCenter', 'justifyRight', 'justifyFull'], ['insertImage', 'insertLink'],['wordcount', 'charcount']]" name="introduccion" ng-model="circular.introduccion"></text-angular>
 					<!--<div text-angular="text-angular" name="introduccion" ng-model="circular.introduccion"  ta-disabled='disabled' required></div>
					<div text-angular="text-angular" name="introduccion" ng-model="circular.introduccion" class="ta-root">
						
						<input type="hidden" style="display: none;" name="introduccion" ng-model="circular.introduccion"   value="">
					</div>-->
										
							<!--<textarea class="form-control" name="introduccion" ng-model="circular.introduccion" rows="3"></textarea>-->
				</div>
				<div class="form-group col-xs-10 col-xs-offset-1">
					<label class="control-label">Recomendaciones:</label>
					
					<text-angular ta-toolbar="[['h1','h2','h3', 'h4', 'h5', 'quote'], ['bold','italics', 'underline', 'strikeThrough'], ['ul', 'ol'], ['undo', 'redo'], ['justifyLeft','justifyCenter', 'justifyRight', 'justifyFull'], ['insertImage', 'insertLink'],['wordcount', 'charcount']]" name="recomendacion" ng-model="circular.recomendacion"></text-angular>

				</div>
				<div class="form-group col-xs-10 col-xs-offset-1">
					<label class="control-label">Reconocimientos:</label>
					
					<text-angular ta-toolbar="[['h1','h2','h3', 'h4', 'h5', 'quote'], ['bold','italics', 'underline', 'strikeThrough'], ['ul', 'ol'], ['undo', 'redo'], ['justifyLeft','justifyCenter', 'justifyRight', 'justifyFull'], ['insertImage', 'insertLink'],['wordcount', 'charcount']]" name="reconocimiento" ng-model="circular.reconocimiento"></text-angular>

				</div>
				<div class="form-group col-xs-10 col-xs-offset-1">
					<label class="control-label ">Observación:</label>
					<text-angular ta-toolbar="[['h1','h2','h3', 'h4', 'h5', 'quote'], ['bold','italics', 'underline', 'strikeThrough'], ['ul', 'ol'], ['undo', 'redo'], ['justifyLeft','justifyCenter', 'justifyRight', 'justifyFull'], ['insertImage', 'insertLink'],['wordcount', 'charcount']]" name="observacion" ng-model="circular.observacion"></text-angular>
				</div>
	    </div>
	    <div class="modal-footer">
	      <div class="btn-group">
	        <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
	        <button type="submit" class="btn btn-success" ng-click="save(form)">Guardar</button>
	      </div>  
        </div>
    </form>

</script>