@extends('layouts.app')

@section('content')

        @if (session('status'))
		    <div class="alert alert-success">
		        {{ session('status') }}
		    </div>
	    @endif

	    <section id="main-content">
            <section class="wrapper">
		  <div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="icon_desktop"></i>Gestionar Usuarios</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="home">Inicio</a></li>
						<li><i class="icon_desktop"></i>Usuarios</li>
					</ol>	
				</div>
			</div>
              <!-- page start-->
                <div class="row">
                 
                  <div class="col-lg-12">
                      <section class="panel" ng-controller="UserCtrl">
                          <header class="panel-heading">
                          </header>
                        <table class="table table-striped table-advance table-hover">
					    <thead>
							<th>Nombre</th>
							<th>Correo Electrónico</th>
							<th>Rol</th>
							<th>Estatus</th>
							<th>Acciones</th>
						</thead>
						<tbody>
							  <tr ng-repeat="user in users">
								<td ng-bind="user.nombre""></td>
								<td ng-bind="user.email"></td>
								<td ng-bind="user.rol.nombre"></td>
                              	<td ng-bind="user.stat"><span class="@{{ user.label }}"></span></td>
								<td>
								    <div class="btn-group">
										<button class="btn btn-primary" type="button" ng-click="edit(user)"><span class="glyphicon glyphicon-edit"></span></button>
										<button class="btn btn-eliminar" type="button" ng-click="destroy(user)"><i class="icon_close_alt2"></i></button>
									</div>

								</td>
							  </tr>
						      <tr ng-if="users.length<1"><td colspan="5"><h5 class="text-center">Sin registros</h5></td></tr>
						</tbody>
					</table>
					<button class="btn btn-enviar pull-right" type="button" ng-click="create()"><span class="glyphicon glyphicon-plus"></span> Nuevo Usuario</button>
					@include('usuarios.partials.user')
			    </div>
		      </div>
			</section>
       </section>

@endsection
@section('script')
	<script type="text/javascript" src="{{asset('app/UserCtrl.js')}}"></script>
	

@endsection