<!-- Modal -->

<script class="modal" type="text/ng-template" id="UserModal.html">
    <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="UserModalLabel">Usuario</h4>
    </div>
    
    <form class="form" name="form" ng-cloak novalidate>

        <div class="modal-body">
     
           {{ csrf_field() }}
            <div class="row">  
                <div class="form-group col-xs-11">
                    <label class="control-label pull-right">Campos requeridos(<span class="required ">*</span>)</label>
                </div> 
                
                
                <div class="form-group col-xs-10 col-xs-offset-1">
                    <span class="required">*</span><label title="">Nombre completo:</label>
                    <input type="text" class="form-control" ng-model="user.nombre" name="nombre" maxlength="50" required>
                    <span class="messages" ng-show="form.$submitted || form.name.$touched">
                    <span class="" ng-show="form.nombre.$error.required">Nombre requerido</span>
                </div>
                <div class="form-group col-xs-10 col-xs-offset-1">
                    <span class="required">*</span><label title="">Correo Electrónico (Email):</label>
                    <input type="email" class="form-control" ng-model="user.email" name="email" ng-change="repetido()" required>
                    <span class="messages" ng-show="form.$submitted || form.name.$touched">
                    <span ng-show="form.email.$error.required">email requerido</span>
                    <span ng-show="form.email.$error.email">No tiene formato email</span>
                </div>
                <div class="form-group col-xs-10 col-xs-offset-1">
                    <span class="required">*</span><label title="">Contraseña (Password):</label>
                    <input type="password" class="form-control" ng-model="user.password" name="password"  maxlength="10" minlength="6" required>
                    <span class="messages" ng-show="form.$submitted || form.name.$touched">
                    <span class="" ng-show="form.password.$error.required">contraseña requerida</span>
                    <span class="" ng-show="form.password.$error.minlength">Mínimo 6 carácteres</span>
                </div>              
                <div class="form-group col-xs-10 col-xs-offset-1">
                    <span class="required">*</span><label title="">Rol:</label>
                    <select class="form-control" name="rol" ng-model="user.rol_id" ng-options="rol.id as rol.nombre for rol in roles" required>
                    </select>
                    <span class="messages" ng-show="form.$submitted || form.name.$touched">
                    <span ng-show="form.rol.$error.required">Rol requerido</span>
                </div>
                <div class="form-group col-xs-10 col-xs-offset-1">
                    <span class="required">*</span><label title="">Estado:</label>
                    <select class="form-control" name="status" ng-model="user.status" ng-options="estado.num as estado.nombre for estado in estados" required>
                    </select>
                    <span class="messages" ng-show="form.$submitted || form.name.$touched">
                    <span  ng-show="form.status.$error.required">Estado requerido</span>
                </div>
            </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
          <button type="submit" class="btn btn-enviar" ng-disabled="repete" ng-click="save(form)" >Guardar</button>
        </div>
    </form>

</script>
