<!-- Modal -->
<script type="text/ng-template" id="Plan_sostenimientoModal.html">
    <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="PlanModalLabel">Plan de sostenimiento mensual</h4>
    </div>
      <form class="form" name="form" ng-cloak novalidate>
        {{ csrf_field() }}
        <div class="modal-body">
           <div class="row">
                <div class="form-group col-xs-11">
		            <label class="control-label pull-right">Campos requeridos(<span class="required ">*</span>)</label>
		        </div>
	            <div class="form-group col-xs-6 col-xs-offset-1">
		            <span class="required">*</span><label class="control-label ">Ingrese mes:</label>             
                	<select name="mes" class="form-control" ng-model="plan_sostenimiento.mes" required>
					  <?php
					      $mes=date("n"); 
					      $rango=11; 
					      for ($i=$mes;$i<=$mes+$rango;$i++){ 
					         $meses=date('F', mktime(0, 0, 0, $i, 1, date("Y") ) );
					         if ($meses=="January") $meses="Enero";
							 if ($meses=="February") $meses="Febrero";
							 if ($meses=="March") $meses="Marzo";
							 if ($meses=="April") $meses="Abril";
							 if ($meses=="May") $meses="Mayo";
							 if ($meses=="June") $meses="Junio";
							 if ($meses=="July") $meses="Julio";
							 if ($meses=="August") $meses="Agosto";
							 if ($meses=="September") $meses="Septiembre";
							 if ($meses=="October") $meses="Octubre";
							 if ($meses=="November") $meses="Noviembre";
							 if ($meses=="December") $meses="Diciembre";
					         echo "<option value='$meses'>$meses</option>"; 
					      } 
					  ?> 
					</select>
					<span class="messages" ng-show="form.$submitted || form.name.$touched">
                    <span class="" ng-show="form.mes.$error.required">Mes requerido</span>		
		        </div>
		       
				<div class="form-group col-xs-10 col-xs-offset-1">
				   <span class="required">*</span><label>Plan general de fondos:</label>
				   <textarea class="form-control" name="general" ng-model="plan_sostenimiento.general" rows="3" required></textarea>
				   <span class="messages" ng-show="form.$submitted || form.name.$touched">
                   <span class="" ng-show="form.general.$error.required">Descripción del plan requerido</span>
				</div>
				<input type="hidden" ng-model="club" value="{{$club}}">
				<div class="form-group col-xs-5 col-xs-offset-1">
					<label>Cantidad de fondos del club en el mes anterior
					<span ng-repeat="plan in planes" ng-if="$last">@{{plan.total_fondo }}</span>
					<span ng-if="planes.length == 0">0</span></label>
				</div>
				<div class="form-group col-xs-5 ">
					<span class="required">*</span><label>Cantidad de dinero invertido durante el mes</label>
					<input type="number" string-to-number class="form-control" ng-model="plan_sostenimiento.inv" name="inv" required>
					<span class="messages" ng-show="form.$submitted || form.name.$touched">
                    <span class="" ng-show="form.inv.$error.required">Inversión requerido</span>
				</div>
				<div class="form-group col-xs-5 col-xs-offset-1">
					<span class="required">*</span><label>Cantidad de inversión recuperada</label>
					<input type="number" string-to-number class="form-control" ng-model="plan_sostenimiento.inv_rec" name="inv_rec" required>
					<span class="messages" ng-show="form.$submitted || form.name.$touched">
                    <span class="" ng-show="form.inv_rec.$error.required">Inversión recuperada requerido</span>
				</div>
				<div class="form-group col-xs-5">
					<span class="required">*</span><label>Ganancias Obtenidas</label>
					<input type="number" string-to-number class="form-control" ng-model="plan_sostenimiento.ganancia" name="ganancia" required>
					<span class="messages" ng-show="form.$submitted || form.name.$touched">
                    <span class="" ng-show="form.ganancia.$error.required">Ganancia requerida</span>
				</div>
				
				<div class="form-group col-xs-6 col-xs-offset-1">
					<label >Total de fondos actuales:
					<span ng-repeat="plan in planes" ng-if="$last">@{{ (plan.total_fondo || 0) + plan_sostenimiento.ganancia}}</span>
					<span ng-if="planes.length == 0">@{{ plan_sostenimiento.ganancia }} </span>
					<input ng-repeat="plan in planes" ng-if="$last" type="hidden" name="total_fondo" ng-model="plan_sostenimiento.total_fondo" value="@{{ plan_sostenimiento.total_fondo }}" ng-init="@{{ (plan.total_fondo || 0)  + plan_sostenimiento.ganancia }}"></label>
					<input type="hidden" name="total_fondo" ng-model="plan_sostenimiento.total_fondo" value="7000" ng-init="" ng-if="planes.length == 0" >
				</div>
		   </div>	
	    </div>
	    <div class="modal-footer">
	        <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
	        <button type="submit" class="btn btn-success" ng-click="save(form)">Guardar</button>
        </div>
    </form>

</script>