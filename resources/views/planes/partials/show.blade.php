<!-- Modal -->
<script type="text/ng-template" class="modal" id="detallesPlanModal.html">
    <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="detallesPlanModalLabel"></h4>
    </div>
      
        <div class="modal-body">
           <div class="row" >
	            <div class="panel panel-primary">
		            <div class="panel-heading">
		             	<strong>Plan de sostenimiento del mes:</strong> <span ng-bind="">@{{ plan_sostenimiento.mes }}</span>
		            </div>
		        </div>    
		        <div class="panel-body">
			   	   <table id="detalles" class="table table-striped table-bordered table-condensed table-responsive">
			   	   	       <thead>
				   	   	      	<th>Plan general</th>
				   	   	      	<th>Fondos del mes anterior</th>
				   	   	      	<th>Inversión</th>
			   	   	       </thead>
			   	   	       <tbody>
				   	   	   	    <tr>
				   	   	   	  	  	<td>@{{ plan_sostenimiento.general }}</td>
				   	   	   	  	  	<td ng-repeat="plan in plan_sostenimientos" ng-if="$last">@{{ plan.total_fondo || 0 }}</td>
				   	   	   	  	  	<td>@{{ plan_sostenimiento.inv }}</td>
				   	   	   	  	</tr>
				   	   	   </tbody>	  	
			   	   	       <thead>
				   	   	      	<th>Inversión recuperada</th>
				   	   	      	<th>Ganancia</th>
				   	   	      	<th>Total de fondos actuales</th>
			   	   	       </thead>  
			   	   	   	   <tbody>
	                            <tr>
				   	   	   	  	  	<td>@{{ plan_sostenimiento.inv_rec }}</td>
				   	   	   	  	  	<td>@{{ plan_sostenimiento.ganancia }}</td>
				   	   	   	  	  	<td>@{{ plan_sostenimiento.total_fondo }}</td>
				   	   	   	  	</tr>
			   	   	   	   </tbody>
			   	   	   </table>
		   	    </div>
	   	   </div> 

   	   </div>
             
	    <div class="modal-footer">
	        <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
        </div>

</script>