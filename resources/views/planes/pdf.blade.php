<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<style>
		main{
			margin-left:30px; 
			margin-right:30px; 
		}
		.page-break {
		    page-break-after: always;
		}
		.text-center{
			text-align: center;
		    display: block;
		    width: 100%;
		}
		.text-right{
			text-align: right;
		    display: block;
		    width: 100%;
		}
		footer {
			color: #5D6975;
			width: 100%;
			height: 30px;
			position: absolute;
			bottom: 0;
			border-top: 1px solid #C1CED9;
			padding: 8px 0;
			text-align: center;
		}
		.info, .correcto, .ojo, .error, .validation {
		    border: 1px solid;
		    margin: 10px 0px;
		    padding:15px 10px 15px 50px;
		    background-repeat: no-repeat;
		    background-position: 10px center;
		    font-family:Arial, Helvetica, sans-serif;
		    font-size:13px;
		    text-align:left;
		    width:auto;
		}
		.info {
		    color: #00529B;
		    background-color: #BDE5F8;
		    /*background-image: url('imagenes/info.jpg');*/
		}
	</style>
	<title>PLAN DE SOSTENIMIENTO</title>
</head>
<body>

    {{--  ENCABEZADO  --}}
    <div class="text-center" style="width:100%;display: block; margin-top: 50px;">
		
		<img style="" src="/xampp/htdocs/mijoven/public/img/mvnor.jpg" width="150" height="130">
		<span class="text-center" style="font-size: 20px;">Misión Venezolana NorOriental</span>
		<strong class="text-center">PLAN DE SOSTENIMIENTO</strong>
    </div> 
    
    <main style="margin-top: 50px;">
        @if($plan == 0)
    	    <div class="info">
    	    	<h3 class="text-center" style="margin-top: 50px;">SIN REGISTROS</h3>
    	    </div>
    	@endif

        @foreach($data as $da)
            <div class="row" style="">
		    	<strong style=" font-size: 18px; padding-top: 25px;">Plan general:</strong>
		    	<p style="background-color:  #778899;">{{ $da->general}} </p>
            </div>
    		<div class="row" >
			    <strong class="" style="font-size: 18px;" >Cantidad de fondos del mes anterior:</strong>
			    <p style="font-size: 18px; background-color:#D3D3D3;">{{ $da->mes_ant }} Bs.</p>
	    	</div>
	    	<div class="row" style="">
			    <strong class="" style="font-size: 18px;" >Inversión realizada en el mes:</strong>
			    <p style="font-size: 18px; background-color: #D3D3D3;">{{ $da->inv }} Bs.</p>
	    	</div>
	    	<div class="row" style="">
			    <strong class="" style="font-size: 18px;">Inversión recuperada:</strong>
			    <p style="font-size: 18px; background-color: #D3D3D3;">{{ $da->inv_rec }} Bs.</p>
	    	</div>
	    	<div class="row" style="">
			    <strong class="" style="font-size: 18px;">Ganancia en el mes:</strong>
			    <p style="font-size: 18px; background-color: #D3D3D3;">{{ $da->ganancia }} Bs.</p>
	    	</div>
	    	<div class="row" style="">
			    <strong class="" style="font-size: 18px;">Total de fondo:</strong>
			    <p style="font-size: 18px; background-color:#D3D3D3;">{{ $da->total_fondo }} Bs.</p>
	    	</div>
    	@endforeach
    </main>
    <footer>
            Club de {{$p->club->categoria->nombre}} {{ $p->club->nombre}}
    </footer>
</body>
</html>