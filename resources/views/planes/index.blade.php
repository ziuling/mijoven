@extends('layouts.app')

@section('content')
	
		@if (session('status'))
		    <div class="alert alert-success">
		        {{ session('status') }}
		    </div>
		@endif

	    <section id="main-content">
            <section class="wrapper">
		  <div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-th-list"></i>Procesar plan de sostenimiento</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="home">Inicio</a></li>
						<li><i class="fa fa-table"></i>Plan de sostenimiento</li>
					</ol>
				</div>
			</div>
              <!-- page start-->
                <div class="row">
                 
                  <div class="col-lg-12">
                      <section class="panel" ng-controller="Plan_sostenimientoCtrl">
                          <header class="panel-heading">
                          </header>
                        <table class="table table-striped table-advance table-hover">
						    <thead>
								<th>Mes</th>
								<th>Ganancias</th>
								<th>Total de fondo actual</th>
								<th>Acciones</th>
							</thead>
							<tbody>
								<tr ng-repeat="plan_sostenimiento in plan_sostenimientos">
									<td ng-bind="plan_sostenimiento.mes"></td>
									<td ng-bind="plan_sostenimiento.ganancia"></td>
									<td ng-bind="plan_sostenimiento.total_fondo"></td>
									<td>
									    <div class="btn-group">
											<button class="btn btn-eliminar" type="button" ng-click="destroy(plan_sostenimiento)"><i class="icon_close_alt2"></i></button>
	                                        <button class="btn btn-info" type="button" ng-click="detalle(plan_sostenimiento)">Detalles</button>
										    @include("planes.partials.show")
                                        </div>
									</td>
								</tr>
							    <tr ng-if="plan_sostenimientos.length < 1"><td colspan="4"><h5 class="text-center">Sin registros</h5></td></tr>
							</tbody>
						</table>
						<button class="btn btn-enviar pull-right" type="button" ng-click="create()"><span class="glyphicon glyphicon-plus"></span> Nuevo plan</button>
						@include("planes.partials.plan")
				    </div>
				    
				</div>
		    </section>
	    </section>   
    </div>	    
@endsection
@section('script')
    <script type="text/javascript" src="{{asset('app/Plan_sostenimientoCtrl.js')}}"></script>
@endsection