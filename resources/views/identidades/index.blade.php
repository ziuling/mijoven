@extends('layouts.app')

@section('content')
 
		
	@if (session('status'))
		<div class="alert alert-success">
		    {{ session('status') }}
		</div>
	@endif

	<section id="main-content">
        <section class="wrapper">
		    <div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-th-list"></i>Gestionar Identidad</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="home">Inicio</a></li>
						<li><i class="fa fa-table"></i><a href="directivas">Directiva</a></li>
						<li><i class="fa fa-table"></i>Identidad Club</li>
					</ol>
				</div>
			</div>
              <!-- page start-->
                <div class="row">
                 
                    <div class="col-lg-12">
                        <section class="panel" ng-controller="IdentidadCtrl">
                          <header class="panel-heading">
                          </header>
                            <table class="table table-striped table-advance table-hover">
							    <thead>
								    <th>Dirección de Reunión</th>
								    <th>Logo</th>
								    <th>Acciones</th>
							    </thead>
							    <tbody>
								    <tr ng-repeat="identidad in identidades">
										<td ng-bind="identidad.direccion"></td>
									    <td><img src="img/logos/@{{identidad.logo}}" height="100px" width="100px" style="border-radius: 50%"></td>
										<td>
											<div class="btn-group">
												<button class="btn btn-primary" type="button" ng-click="edit(identidad)"><pan class="glyphicon glyphicon-edit"></span></button>
											    <button class="btn btn-eliminar" type="button" ng-click="destroy(identidad)"><i class="icon_close_alt2"></i></button>
										    </div>
										</td>
									</tr>
									<tr ng-if="identidades.length < 1"><td colspan="6"><h5 class="text-center">Sin registros</h5></td></tr>
								</tbody>
						    </table>
						    <button class="btn btn-enviar pull-right" type="button" ng-click="create()" ng-disabled="identidades.length > 0"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
				            @include('identidades.partials.modal')
					
		                </section>

                    </div>
                </div>
        </section>
    </section>
@endsection
@section('script')
	<script type="text/javascript" src="{{asset('app/IdentidadCtrl.js')}}"></script>
@endsection