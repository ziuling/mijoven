<!-- Modal -->
<script type="text/ng-template" id="IdentidadModal.html">
  <div class="modal-header">
    <button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="IdentidadModalLabel"></h4>
  </div>
  <form class="form" name="form" enctype="multipart/form-data"  ng-cloak novalidate>
    {{ csrf_field() }}
    <div class="modal-body">
      <div class="row">
        <div class="form-group col-xs-11">
          <label class="control-label pull-right">Campo requerido(<span class="required ">*</span>)</label>
        </div>
        <div  class="form-group col-xs-8 col-xs-offset-1">
          <span class="required">*</span><label title="">Dirección de reunión:</label>
          <input type="text" class="form-control" ng-model="identidad.direccion" name="direccion" minlength="10" required>
          <span class="messages" ng-show="form.$sub || mittedform.name.$touched">
          <span class="" ng-show="form.direccion.$error.required">Dirección requerido</span>
          <span class="" ng-show="form.direccion.$error.minlength">Mínimo 10 carácteres</span>
        </div>
        <div class="form-group col-xs-8 col-xs-offset-1">
          <span class="required">*</span><label title="" for="logo">Logo:</label>
          <input type="file" name="logo" file-model="identidad.logo"  id="logo" required/>
          <label>@{{identidad.logo}}</label>
          <span class="messages" ng-show="form.$sub || mittedform.name.$touched">
          <span class="" ng-show="form.logo.$error.required">Logo requerido</span>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
      <button type="submit" class="btn btn-success" ng-click="save(form)">Guardar</button>
    </div>
  </form>
</script>