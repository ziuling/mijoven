<?php

use Illuminate\Database\Seeder;
use App\Tipo;

class TiposSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $t1 = Tipo::create(['nombre'=>'Iglesia']);
        $t2 = Tipo::create(['nombre'=>'Grupo']);

    }
}
