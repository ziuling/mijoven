<?php

use Illuminate\Database\Seeder;
use App\Zona;
use App\Distrito;
use App\Pastor;
use App\Iglesia;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $z1 = Zona::create([
            'nombre'=>'Zona I',
            'user_id'=>'3'
            ]);
        $z2 = Zona::create([
            'nombre'=>'Zona II',
            ]);
        $z3 = Zona::create([
            'nombre'=>'Zona III',
            ]);

        $d1 = Distrito::create([
        	'nombre' => 'Barcelona I',
        	'zona_id' => $z1->id,
        	]);
        $d2 = Distrito::create([
        	'nombre' => 'Barcelona II',
        	'zona_id' => $z2->id,
        	]);
        $d3 = Distrito::create([
        	'nombre' => 'Puerto la Cruz I',
        	'zona_id' => $z3->id,
        	]);

        $p1 = Pastor::create([
            'nombre'=>'Reinaldo Arcia',
            'correo'=>'reinaldo@gmail.com',
            'cedula'=>'25178998',
            'distrito_id'=> $d1->id,
            ]);
        $p2 = Pastor::create([
            'nombre'=>'German Garcia',
            'correo'=>'german@gmail.com',
            'cedula'=>'25869098',
            'distrito_id'=> $d2->id,
            ]);
        $p3 = Pastor::create([
            'nombre'=>'Luis Suarez',
            'correo'=>'luis@gmail.com',
            'cedula'=>'26178998',
            'distrito_id'=> $d3->id,
            ]);

        $i1 = Iglesia::create([
            'nombre'=>'central de Barcelona',
            'direccion'=>'Av. Contry Club Calle maturin',
            'tipo_id'=> '1',
            'distrito_id'=>$d1->id,
            'user_id'=>'6'
            ]);
        $i2 = Iglesia::create([
            'nombre'=>'Riveras del Neveri',
            'direccion'=>'Polvito',
            'tipo_id'=> '2',
            'distrito_id' => $d1->id,
            ]);
     
    }
}
