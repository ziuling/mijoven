<?php

use Illuminate\Database\Seeder;
use App\Rol;

class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $r1 = Rol::create(['nombre'=>'Administrador']);
        $r2 = Rol::create(['nombre'=>'Líder Juvenil']);
        $r3 = Rol::create(['nombre'=>'Líder de Zona']);
        $r4 = Rol::create(['nombre'=>'Secretario(a) de Club']);
        $r5 = Rol::create(['nombre'=>'Secretario(a)']);
        $r6 = Rol::create(['nombre'=>'Secretario(a) de Sociedad de Jóvenes']);
    }
}
