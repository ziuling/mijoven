<?php

use Illuminate\Database\Seeder;
use App\Grupo;

class GruposSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $g1 = Grupo::create([
            'nombre'=>'Jóvenes Instructores',
            'direccion'=>'calle maturin',
            'club_id'=>'1']);
        $g2 = Grupo::create([
            'nombre'=>'Mujeres triunfadoras',
            'direccion'=>'calle maturin',
            'club_id'=>'1']);    
    }
}
