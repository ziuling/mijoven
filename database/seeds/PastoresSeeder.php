<?php

use Illuminate\Database\Seeder;

class PastoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Pastor::create(['nombre'=>'Reinaldo Arcia','correo'=>'reinaldo@gmail.com', 'cedula'=>'25178998']);
        Pastor::create(['nombre'=>'German Garcia','correo'=>'german@gmail.com', 'cedula'=>'25869098']);
        Pastor::create(['nombre'=>'Luis Suarez','correo'=>'luis@gmail.com', 'cedula'=>'26178998']);
    }
}
