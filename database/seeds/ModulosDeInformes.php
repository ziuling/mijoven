<?php

use Illuminate\Database\Seeder;
use App\Modulo;
use App\Act_modulo;
use App\Participante;
use App\Asistencia;

class ModulosDeInformes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (Modulo::all() as $modulo) {
            $modulo->delete();
        }
        
    	$m1 = Modulo::create([
            'nombre'=>'Discipulado',
    		'descripcion'=>'Actividades Devocionales y Actividades Sociales']);

    	$Act1 = [
    		'Miembros participando en la sociedad JA',
    		'Miembros asistentes al club',
    		'Miembros participando en año bíblico',
    		'Grupo de oración intercesora',
    		'Grupos GPSS activos trabajando',
    		'Semanas de oración JA',
    		'Retiros espirituales',
    		'Campamentos Juveniles',
    		'Paseos y Caminatas',
    		'Ferias JA GAAM y Deportivas',
    		'Fogatas y/o aniversarios'
    		];

    	foreach ($Act1 as $act) {
    		Act_modulo::create([
                'nombre'=>$act,
                'modulo_id'=>$m1->id]);
    	}


    	$m2 = Modulo::create([
            'nombre'=>'Liderazgo',
    		'descripcion'=>'Actividades Devocionales y Actividades Sociales']);

    	$Act2 = [
    		'Lectura del Libro "Servicio Cristiano"//PATRIARCAS Y PROFETAS',
    		'Clases y especialidad J.A realizadas',
    		'Ceremonias de investidura',
    		'Feria de Conquistadores y aventureros',
    		'Reunión y capacitación de liderazgo',
    		];

    	foreach ($Act2 as $act) {
    		Act_modulo::create([
                'nombre'=>$act,
                'modulo_id'=>$m2->id]);
    	}

    	$m3 = Modulo::create([
            'nombre'=>'Evangelismo y Servicio',
    		'descripcion'=>'Actividades Misioneras y de Testificación']);

    	$Act3 = [
    		'Miembros activos en Mision trabaJA',
    		'Predicadores Juveniles "GANA CON JESÚS"',
    		'Cantidad de hermanos bautizados en el mes',
    		'estudios Bíblicos',
    		'cantatas de testificación',
    		'Evangelismo por internet (páginas web) RETO',
    		'Contactos misioneros',
    		'Recolección',
    		'Campañas Evangelísticas // CAMPAÑAS MINISTERIALES',
    		'Marchas de Salud',
    		'Miembros en bandas marciales',
    		'Miembros en la orquesta adventista',
    		'Servicio a la comunidad (último domingo de cada mes)',
    		'5 Días para dejar de fumar',
    		'Impacto JA "Cerca de Ti"',
    		'"Flores para ti Mamá"',
    		'"Operación balsamo"',
    		];

    	foreach ($Act3 as $act) {
    		Act_modulo::create([
                'nombre'=>$act,
                'modulo_id'=>$m3->id]);
    	}

    	$m4 = Modulo::create([
            'nombre'=>'Actividades Universitarias',
    		'descripcion'=>'']);

    	$Act4 = [
    		'Reuniones con S.U.V.A.',
    		'Estudios Bíblicos con Universitarios',
    		'Actividades especiales',
    		];

    	foreach ($Act4 as $act) {
    		Act_modulo::create([
                'nombre'=>$act,
                'modulo_id'=>$m4->id]);
    	}

    }
}
