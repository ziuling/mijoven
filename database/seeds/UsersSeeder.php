<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $u1 = User::create([
        	'nombre'=>'ziuling macayo',
        	'email'=>'zmacayo@gmail.com',
        	'rol_id'=>'1',
            'password'=> bcrypt('secret'),
            'status' => '1'
            ]);

        $u2 = User::create([
        	'nombre'=>'ziu macayo',
        	'email'=>'ziuling@hotmail.com',
        	'rol_id'=>'2',
            'password'=>bcrypt('secret'),
            'status' => '1'
            ]);

        $u3 = User::create([
            'nombre'=>'andreina lopez',
            'email'=>'andre@gmail.com',
            'rol_id'=>'3',
            'password'=> bcrypt('secret'),
            'status' => '1'

            ]);

        $u4 = User::create([
            'nombre'=>'jose castro',
            'email'=>'jose@hotmail.com',
            'rol_id'=>'4',
            'password'=>bcrypt('secret'),
            'status' => '1'

            ]);

        $u5 = User::create([
            'nombre'=>'pedro lopez',
            'email'=>'pedro@gmail.com',
            'rol_id'=>'5',
            'password'=> bcrypt('secret'),
            'status' => '1'

            ]);

        $u6 = User::create([
            'nombre'=>'luis macayo',
            'email'=>'luis@hotmail.com',
            'rol_id'=>'6',
            'password'=>bcrypt('secret'),
            'status' => '1'
            
            ]);
    }
}
