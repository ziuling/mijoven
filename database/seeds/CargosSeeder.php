<?php

use Illuminate\Database\Seeder;
use App\Cargo;

class CargosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $ca1= Cargo::create([
        	'nombre'=>'director(a)']);
        $ca2= Cargo::create([
        	'nombre'=>'sub-director(a)']);
        $ca3= Cargo::create([
        	'nombre'=>'tesorero(a)']);
        $ca4= Cargo::create([
            'nombre'=>'Consejero(a)']);
        $ca5= Cargo::create([
            'nombre'=>'Anciano responsable']);
        $ca6= Cargo::create([
            'nombre'=>'Lider responsable']);
        $ca7= Cargo::create([
            'nombre'=>'Lider asociado responsable']);
    }
}
