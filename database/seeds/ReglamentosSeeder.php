<?php

use Illuminate\Database\Seeder;
use App\Reglamento;

class ReglamentosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $r = Reglamento::create([
            'titulo'=>'Reglamento para Directivos Club de Conquistadores/Guías Mayores',
            'introduccion'=>'Este reglamento es un instrumento de compromiso, mediante el cual el club se sujeta a cumplir de manera voluntaria con las actividades estipulada por el Ministerio Juvenil del campo local. Este instrumento le garantiza una mejor optimización en las actividades del club. Siempre y cuando cumpla con las exigencias de este reglamento. No pretendemos que sea gravoso, antes bien que haya un compromiso genuino con el ministerio Juvenil.',
            'reglamento'=>'1. La obediencia es y será siempre la primera ley del cielo y del club.
						2. La puntualidad debe ser y es nuestra insignia. Los directivos deben estar 15 minutos antes de la hora pautada para la reunión.
						3. Los departamentales de Campos Conferencia General, División, Unión Campo Local, Zonas y Distritos son la máxima autoridad y merece respeto y consideración.
						4. No se permitirá bajo ninguna circunstancia en las reuniones oficiales el uso por parte de las damas, de mini faldas, pantalones muy cortos y ajustados al cuerpo (entre ellos licras). Y los caballeros pantalones cortos (short) o franelillas.
						5. Los uniformes serán según el modelo y bajo los lineamientos del reglamento del ministerio juvenil de la DIVISÓN INTERAMERICANA. Los zapatos deben ser de color negro de vestir. Las insignias y botones reglamentarios son propiedad del ministerio juvenil. Al perder su feliguresia el miembro deberá por moral regresar dichos botones e insignias. El uso del uniforme es de carácter obligatorio en toda reunión del club. El uniforme solo se usará en actividades propias del club, por lo tanto no puede ser utilizado para fines personales. Una vez que usted se retire del club debe regresar las insignias.
						6. Es Competencia de la directiva, que en reuniones del club y de la iglesia no se preparen comidas que contengan ningún tipo de carne. La violación de esta regla lo puede excluir de su responsabilidad.
						7. La directiva del club es la responsable del uso de banderas y banderines por parte de las unidades.
						8. Es de carácter obligatorio que existan las unidades dentro de la estructura del club. Estos también formaran un Grupo pequeño de salvación y Servicio. Por tal motivo deben integrar las unidades por sectores.
						9. No estará permitido un lenguaje homosexual, o juegos que induzcan a este tipo de prácticas. Recuerde esto es deshonra para Dios y oprobio para la Iglesia.
						10. Ningún miembro bajo disciplina podrá asistir a los campamentos promovidos por el club. En dicha disciplina no podrá portar el uniforme. Aunque no se le negará la asistencia a las reuniones del club.',
            'categoria_id'=> '2']);
    }
}
