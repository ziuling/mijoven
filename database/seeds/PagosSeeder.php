<?php

use Illuminate\Database\Seeder;
use App\Pago;

class PagosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $p1 = Pago::create([
            'fecha'=>'2017/03/12',
            'n_referencia'=>'458948549kvfjvkf',
            'banco'=>'venezuela',
            'monto'=>'2000',
            'comprobante'=>'',
            'concepto'=>'Inscripcion de club/sj al campo',
            'planilla'=>'',
            'cantidad_m'=>'20',
            'cantidad_n'=>'2',
            'cantidad_a'=>'33',
            'cantidad_e'=>'2',
            'directiva'=>'3',
            'acampantes'=>'60',
            'tipo'=>'pago de club/sj',
            'total'=>'3000',
            'club_id'=>'1',
            'actividad_id'=>''
            ]);

    }
}
