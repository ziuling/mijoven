<?php

use Illuminate\Database\Seeder;
use App\Inscripcion;

class InscripcionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i1 = Inscripcion::create([
            'fecha'=>'2017/03/12',
            'monto'=>'2000',
            'monto_miembro'=>'1500',
            'monto_acompanante'=>'1000',
            'monto_nino'=>'800',
            'seguro'=>'2000',
            'categoria_id'=> '1']);

        $i2 = Inscripcion::create([
            'fecha'=>'2017/03/12',
            'monto'=>'2300',
            'monto_miembro'=>'1800',
            'monto_acompanante'=>'1100',
            'monto_nino'=>'900',
            'seguro'=>'2500',
            'categoria_id'=> '2']);

        $i3 = Inscripcion::create([
            'fecha'=>'2017/05/12',
            'monto'=>'2500',
            'monto_miembro'=>'2000',
            'monto_acompanante'=>'1300',
            'monto_nino'=>'1000',
            'seguro'=>'2500',
            'categoria_id'=> '2']);

        $i4 = Inscripcion::create([
            'fecha'=>'2017/03/15',
            'monto'=>'3000',
            'monto_miembro'=>'2500',
            'monto_acompanante'=>'2000',
            'monto_nino'=>'2000',
            'seguro'=>'3000',
            'categoria_id'=> '3']);

		$i5 = Inscripcion::create([
		    'fecha'=>'2017/03/20',
		    'monto'=>'3200',
		    'monto_miembro'=>'2800',
		    'monto_acompanante'=>'2500',
		    'monto_nino'=>'2000',
		    'seguro'=>'4000',
		    'categoria_id'=> '4']);

    }   
    
}
