<?php

use Illuminate\Database\Seeder;
use App\Persona;

class PersonasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $p1 = Persona::create([
        	'nombre'=>'Carolina Herrera',
        	'cedula'=>'26731203',
        	'telefono'=>'8212917',
        	'correo'=>'carolina@hotmail.com',
        	'direccion'=>'las casitas',
        	'estado_civil'=>'Soltero',
        	'fecha_nac'=>'1994-01-12']);

        $p2 = Persona::create([
        	'nombre'=>'Ziuling Macayo',
        	'cedula'=>'25262847',
        	'telefono'=>'4149991241',
        	'correo'=>'ziuling@hotmail.com',
        	'direccion'=>'las casitas',
        	'estado_civil'=>'Soltero',
        	'fecha_nac'=>'1993-12-26']);
    }
}
