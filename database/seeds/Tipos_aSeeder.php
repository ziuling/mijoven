<?php

use Illuminate\Database\Seeder;
use App\Tipo_a;

class Tipos_aSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $t1= Tipo_a::create([
        	'tipo'=>'Fijas',
        	'nombre'=>'',
        	'total_puntuacion'=>'0'
        	]);
        $t2= Tipo_a::create([
        	'tipo'=>'Mensuales',
        	'nombre'=>'',
        	'total_puntuacion'=>'0'
        	]);
        $t3= Tipo_a::create([
        	'tipo'=>'Generales',
        	'nombre'=>'',
        	'total_puntuacion'=>'0'
        	]);
        $t4= Tipo_a::create([
        	'tipo'=>'Bíblicos',
        	'nombre'=>'',
        	'total_puntuacion'=>'2150'
        	]);
        $t5= Tipo_a::create([
        	'tipo'=>'Destrezas',
        	'nombre'=>'',
        	'total_puntuacion'=>'1250'
        	]);
        $t6= Tipo_a::create([
        	'tipo'=>'Sociales',
        	'nombre'=>'',
        	'total_puntuacion'=>'400'
        	]);
        $t7= Tipo_a::create([
        	'tipo'=>'Otros',
        	'nombre'=>'LAS OLIMPIADAS DEL DESIERTO',
        	'total_puntuacion'=>'1310'
        	]);
    }
}
