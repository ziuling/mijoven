<?php

use Illuminate\Database\Seeder;
use App\Planificacion;

class PlanificacionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $p1 = Planificacion::create([
            'f_inicio'=>'2017/03/02',
            'f_final'=>'2017/03/02',
            'como'=> '',
            'cuando'=> '',
            'que'=>'',
            'objetivos'=>'',
            'propositos'=>'',
            'club_id'=>'',
            'actividad_id'=>'',
            'responsable'=>'',
            ]);

       $p2 = Planificacion::create([
            'fecha'=>'2017/03/05',
            'como'=> '',
            'cuando'=> '',
            'que'=>'',
            'objetivos'=>'',
            'propositos'=>'',
            'club_id'=>'',
            'actividad_id'=>'',
            'responsable'=>'',
            ]);

        $p1 = Planificacion::create([
            'fecha'=>'2017/04/02',
            'como'=> '',
            'cuando'=> '',
            'que'=>'',
            'objetivos'=>'',
            'propositos'=>'',
            'club_id'=>'',
            'actividad_id'=>'',
            'responsable'=>'',
            ]);

       $p2 = Planificacion::create([
            'fecha'=>'2017/06/05',
            'como'=> '',
            'cuando'=> '',
            'que'=>'',
            'objetivos'=>'',
            'propositos'=>'',
            'club_id'=>'',
            'actividad_id'=>'',
            'responsable'=>'',
            ]);
    }
}
