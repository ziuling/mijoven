<?php

use Illuminate\Database\Seeder;
use App\Miembro;


class MiembrosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $m = Miembro::create([
        	'club_id' => '1',
        	'persona_id' => '2',
            'ministerio'=>'orquesta',
            'tipo_sangre'=>'o+',
            'donante'=>'1',
            'voluntario'=>'1'
            ]);
    }
}
