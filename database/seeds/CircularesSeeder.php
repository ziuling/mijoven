<?php

use Illuminate\Database\Seeder;
use App\Circular;

class CircularesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $c1 = Circular::create([
        	'titulo'=>'3era Circular para los clubes de Conquistadores y Guías Mayores', 
            'sub_titulo'=>'', 
            'destinario'=>'Pastores, Coordinadores juveniles zonales, Directores de Clubes, MIVENOR.',
            'asunto'=>'Camporee Conquistadores 2017 – Eventos Precamporee',
            'fecha'=>'09-15 de abril 2017.',
            'recomendacion'=>'',
            'introduccion'=>'Apreciados amigos:
				Bendiciones del Cielo! Esperamos que el Señor les esté ayudando en vuestras vidas.
				Esta tercera circular será un resumen de lo ya explicado anteriormente en las circulares 1 y 2, y aclarar algunas dudas que tenemos relacionados con el Camporee. El objetivo de esto es ofrecer información veraz y actualizada, para que ustedes continúen a trabajar o inicien su trabajo.
				NOTA: antes de comenzar, les informamos oficialmente que a partir de este momento, estamos dividiendo la actividad de Conquiguías en 2, es decir, club de Conquistadores y Guías Mayores por separado (Como era antes). Esto quiere decir que las cuotas, los eventos, la puntuación y todo lo demás será llevado Conquistadores aparte de Guías Mayores.
				a. Cada club de Conquistadores tendrá al menos dos unidades, una de chicas y una de chicos. Asimismo, los miembros de Conquistadores se dividirán por edades, a saber: Junior (10-12 años) y Senior (13-15 años). Para el camporee del 2018, cada club de Conquis tendrá al menos 4 unidades: dos de Junio (chicas y chicos) y 2 de Senior (chicas y chicos). Cada unidad constará de entre 6-9 miembros, y a partir de 10 tendrán que hacer una nueva unidad.
				b. Cada club de Guías Mayores tendrá al menos dos unidades: una de damas y caballeros, de entre 4 y 8 miembros cada una. A partir de 9, formará una nueva unidad',
            'acontecimiento'=>'',
            'reconocimiento'=>'',
            'observacion'=>'',
            'categoria_id'=>'2'
        	]);

        $c2 = Circular::create([
        	'titulo'=>'“Los Elegidos de Dios: De la Esclavitud a la Libertad”', 
            'sub_titulo'=>'CIRCULAR 4', 
            'destinario'=>'',
            'asunto'=>'',
            'fecha'=>'',
            'recomendacion'=>'',
            'introduccion'=>'Mis amados muchachos:
				Reciban todo nuestro cariño, y sinceros deseos porque vuestra vida pueda ser impactada por la obra del Espíritu Santo.
				A continuación les estamos enviando la circular oficial de nuestro Primer Camporee de Conquistadores y Guías Mayores, a celebrarse desde el Lunes 10 y hasta el domingo 16 del 2017. Por favor tomen nota de cada uno de ellos, y prepárense para dar lo mejor de lo mejor!
				Lugar: Guaranache, edo. Sucre
				Capellán: Pr. Miguel Bervis (Asociación Venezolana Centrooccidental
				Seguro: Bs. 4.000,00
				Querer es poder. Coloquemos nuestro esfuerzo en las Manos de Dios, y Él nos dará la Victoria. Hay tanta gente que estará feliz de asistir a este camporee, de manera especial chicos no adventistas. Pero es necesario trabajar desde YA con ellos para prepararlos',
            'acontecimiento'=>'I Camporee de Conquistadores y Guías Mayores',
            'reconocimiento'=>'Deseamos iniciar un estilo de reconocimiento basado en el desempeño del club antes que en la comparación de los puntos con todos los clubes. Es decir, a partir de este Camporee, todo club que quiera alcanzar el primer lugar lo puede hacer, siempre y cuando alcance el porcentaje mínimo para lograrlo.
				La tabla de puntuación se dividirá en cuatro escalafones, que son:
				• Participación: Cuando el club alcanza entre 1 y 50% de la puntuación camporee y precamporee
				• Distinguido: Cuando el club alcanza entre 51 y 70% de la puntuación camporee y precamporee
				• Sobresaliente: Cuando el club alcanza entre 71 y 90% de la puntuación camporee y precamporee
				• HONORABLE: Cuando el club alcanza entre 91 y 100% de la puntuación camporee y precamporee. Este es el equivalente al Primer lugar, y el único que será premiado.',
            'observacion'=>'Para el mes de enero sostendremos una reunión con todos los directores de clubes de la Misión, para finiquitar detalles sobre el camporee. El Líder JA de zona les informará sobre el día y la hora.
			Mis muchachos!! Estamos cerca de Canaán! Personalmente, siento mucha expectativa por los pocos días que faltan. Al mirar todo el trabajo durante el año, no hago más que agradecer por todo el esfuerzo y toda la pasión que han desplegado todos ustedes, directores, por cada evento juvenil. Así como falta poco para ir a nuestro Camporee, pues… También falta poco para el Cielo. Preparémonos para este mayor evento. Falta poco, chicos. ¡Cristo pronto viene!
			Dios les bendiga, con todo mi cariño y afecto,',
            'categoria_id'=>'2'
        	]);
    }
}
