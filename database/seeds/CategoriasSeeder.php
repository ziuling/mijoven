<?php

use Illuminate\Database\Seeder;
use App\Categoria;
class CategoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $c1 = Categoria::create(['nombre'=>'Aventureros']);
        $c2 = Categoria::create(['nombre'=>'Conquistadores']);
        $c3 = Categoria::create(['nombre'=>'Guías Mayores']);
        $c4 = Categoria::create(['nombre'=>'Jóvenes']);
    }
}
