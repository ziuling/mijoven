<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detalle extends Model
{
    protected $table = 'detalles';
    protected $fillable = ['actividad_id', 'puntuacion_id', 'puntos'];
    

    public function actividad()
    {
    	return $this->belongsTo('App\Actividad','actividad_id');
    }
    
    public function puntuacion()
    {
        return $this->belongsTo('App\Puntuacion','puntuacion_id');
    }
}
