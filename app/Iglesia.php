<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iglesia extends Model
{
    protected $table = 'iglesias';

    protected $fillable = ['tipo_id', 'nombre','direccion', 'distrito_id', 'user_id'];
    
    public function tipo()
    {
    	return $this->belongsTo('App\Tipo','tipo_id');
    }
    public function distrito()
    {
    	return $this->belongsTo('App\Distrito','distrito_id');
    }
    public function club()
    {
    	return $this->hasMany('App\Club','club_id');
    }

    public function iglesia()
    {
      return $this->hasMany('App\Iglesia','iglesia_id');
    }
    public function miembro()
    {
       return $this->hasMany('App\Miembro','iglesia_id');
    }
    
    public function directiva()
   {
       return $this->hasMany('App\Directiva','iglesia_id');
   }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function pago()
   {
         return $this->hasMany('App\Pago','iglesia_id');
   }
   public function planificacion()
   {
       return $this->hasMany('App\Planificacion','iglesia_id');
   }
   public function puntuacion()
   {
       return $this->hasMany('App\Puntuacion','iglesia_id');
   }
   public function ministerio()
   {
       return $this->hasMany('App\Ministerio','iglesia_id');
   }
   public function informe()
   {
       return $this->hasMany('App\Informe','iglesia_id');
   }
}
