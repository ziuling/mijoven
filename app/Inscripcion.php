<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inscripcion extends Model
{
    //
    protected $table = 'inscripciones';

   // protected $dates = ['fecha'];
    //protected $dateFormat = 'Y-m-d';

    protected $fillable =['fecha','monto', 'monto_miembro', 'monto_acompanante', 'monto_nino', 'seguro', 'categoria_id'];

    public function categoria()
   {
   	   return $this->belongsTo('App\Categoria','categoria_id');
   }
	
}
