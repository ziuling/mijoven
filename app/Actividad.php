<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actividad extends Model
{

    protected $table = 'actividades';
    
    //protected $dates = ['f_inicio','f_final'];
    //protected $dateFormat = 'd/m/Y';

    protected $fillable = ['f_inicio', 'f_final', 'mes', 'nombre', 'descripcion', 'puntuacion', 'tipo_a_id', 'circular_id', 'user_id'];

    public function user()
    {
            return $this->belongsTo('App\User','user_id');
    }

    public function circular()
    {
    	return $this->belongsTo('App\Circular','circular_id');
    }

    public function plan()
    {
    	return $this->hasOne('App\Plan_sostenimiento','actividad_id');
    }

    public function informe()
    {
    	return $this->hasOne('App\Informe','actividad_id');
    }
    

    public function ministerio()
    {
        return $this->hasOne('App\Ministerio','actividad_id');
    }

    public function pago()
    {
        return $this->hasOne('App\Pago','actividad_id');
    }

    public function planificacion()
    {
    	return $this->hasOne('App\Planificacion','actividad_id');
    }

    public function detalles()
    {
    	return $this->hasMany('App\Detalle', 'actividad_id');
    }
   
    public function tipo()
    {
        return $this->belongsTo('App\Tipo_a','tipo_a_id');
    }
    public function scopefijas($query)
   {
      return $query->where('tipo_a_id', 1);
   } 
   public function scopegenerales($query)
   {
      return $query->where('tipo_a_id', 2);
   } 
   public function scopeprecamporee($query)
   {
      return $query->where('tipo_a_id', 3);
   } 

}
