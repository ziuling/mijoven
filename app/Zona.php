<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zona extends Model
{
    //
    protected $table='zonas';
    protected $fillable = ['nombre','user_id'];

    public function distritos()
    {
    	return $this->hasMany('App\Distrito', 'zona_id');

    }
    public function lider()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }
}
