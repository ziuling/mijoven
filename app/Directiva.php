<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Directiva extends Model
{

    protected $table = "directivas";
    protected $fillable = ['persona_id','club_id', 'iglesia_id', 'cargo_id', 'grupo_id', 't', 'logo'];

    public function club()
    {
    	return $this->belongsTo('App\Club','club_id');
    }
   
    public function iglesia()
    {
        return $this->belongsTo('App\Iglesia','iglesia_id');
    }

    public function cargo()
    {
    	return $this->belongsTo('App\Cargo','cargo_id');
    }

    public function persona()
    {
    	return $this->belongsTo('App\Persona','persona_id');
    }

    public function grupo()
    {
    	return $this->belongsTo('App\Grupo','grupo_id');
    }

    

}
