<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{

    protected $table = 'grupos';
   protected $fillable = ['nombre', 'direccion', 'club_id', 'iglesia_id', 'user_id'];

    public function club()
    {
    	return $this->belongsTo('App\Club','club_id');
    }

    public function iglesia()
    {
        return $this->belongsTo('App\Iglesia','iglesia_id');
    }
    public function usuario()
    {
        return $this->belongsTo('App\User','user_id');
    }
    public function directiva()
    {
    	return $this->hasOne('App\Directiva','grupo_id');
    }

    public function miembro()
    {
    	return $this->hasMany('App\Miembro', 'grupo_id');
    }
}
