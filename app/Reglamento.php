<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reglamento extends Model
{
   protected $table = 'reglamentos';

   protected $fillable = ['titulo', 'introduccion', 'reglamento', 'categoria_id'];

   public function categoria()
   {
   	   return $this->belongsTo('App\Categoria','categoria_id');
   }

   
}
