<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Act_modulo extends Model
{
	protected $table = 'act_modulo';

    public function informe_act()
	{
		return $this->hasOne('App\Informe_Act','actmod_id');
	}
    public function modulo()
    {
        return $this->belongsTo('App\Modulo','modulo_id');
    }

	public function scopem($query)
    {
        return $query->where('modulo_id', 1);
    }

    public function scopemm($query)
    {
        return $query->where('modulo_id', 2);
    }

    public function scopemmm($query)
    {
        return $query->where('modulo_id', 3);
    }

    public function scopemmmm($query)
    {
        return $query->where('modulo_id', 4);
    }

}
