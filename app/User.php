<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'email', 'password','rol_id', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function rol()
    {
        return $this->belongsTo('App\Rol');
    }

    public function actividades()
    {
        return $this->hasMany('App\Actividad','user_id');
    }
    public function tipos()
    {
        return $this->hasMany('App\Tipo_a','user_id');
    }
    public function circulares()
    {
        return $this->hasMany('App\Circular','user_id');
    }
    
    public function zona()
    {
        return $this->hasOne('App\Zona', 'user_id');
    }

    public function club()
    {
        return $this->hasOne('App\Club','user_id');
    }
    public function grupo()
    {
        return $this->hasOne('App\Grupo','user_id');
    }
    public function iglesia()
    {
         return $this->hasOne('App\Iglesia', 'user_id');
    }

    public function scopes($query)
    {
        return $query->where('rol_id', 5);
    }
}
