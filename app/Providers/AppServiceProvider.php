<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        \Blade::directive('rol', function ($roles){
            $roles = explode(",", $roles);
            $addcont = 'Auth::user()->rol->nombre == '.$roles[0];
            foreach ($roles as $key => $value) {
                if ($key>0) {
                    $addcont .= ' || Auth::user()->rol->nombre == ' . $value; 
                }
            }
            $conditional = "<?php if(". $addcont ."): ?>";
            return $conditional;
        });

        \Blade::directive('endrol', function (){
            return "<?php endif ?>";
        });


        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
