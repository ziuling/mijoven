<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Puntuacion extends Model
{
    protected $table = 'puntuaciones';
    protected $fillable = ['mes', 'club_id', 'categoria_id', 'circular_id','iglesia_id', 'totalm', 'totalg', 'total'];
    

    public function club()
    {
    	return $this->belongsTo('App\Club','club_id');
    }
    public function categoria()
    {
        return $this->belongsTo('App\Categoria','categoria_id');
    }
    public function iglesia()
    {
        return $this->belongsTo('App\Iglesia','iglesia_id');
    }
    public function detalles()
    {
        return $this->hasMany('App\Detalle','puntuacion_id');
    }
 
}
