<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Miembro_investidura extends Model
{

    protected $table = 'miembro_investidura';

    protected $fillable = ['miembro_id', 'investidura_id'];

    public function miembro()
    {
    	return $this->belongsTo('App\Miembro','miembro_id');
    }

    public function investidura()
    {
    	return $this->belongsTo('App\Investidura','investidura_id');
    }

}
