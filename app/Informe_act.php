<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Informe_act extends Model
{
    protected $table = 'informe_act';

	protected $fillable = ['actmod_id', 'informe_id'];

    public function actividad()
	{
		return $this->belongsTo('App\Act_modulo','actmod_id');
	}

	public function informe()
	{
		return $this->belongsTo('App\Informe','informe_id');
	}
}
