<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Informe extends Model
{

	protected $table = 'informes';

	protected $fillable = [
	'mes',
	'descripcion',
	'club_id',
	'iglesia_id',
	'actividad_id', 
	'modulo_id'];

	public function club()
	{
		return $this->belongsTo('App\Club','club_id');
	}

	public function iglesia()
	{
		return $this->belongsTo('App\Iglesia','iglesia_id');
	}

	public function actividad()
	{
		return $this->belongsTo('App\Actividad','actividad_id');
	}

	public function modulo()
	{
		return $this->belongsTo('App\Modulo','modulo_id');
	}
	public function informe_act()
    {
    	return $this->hasMany('App\Informe_act', 'informe_id');
    }

}
