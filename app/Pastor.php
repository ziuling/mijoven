<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pastor extends Model
{
    //
    protected $table = 'pastores';

    protected $fillable = [
        'nombre', 'cedula', 'correo','distrito_id'
    ];
    
    public function distrito()
    {
    	return $this->belongsTo('App\Distrito','distrito_id');
    }

}
