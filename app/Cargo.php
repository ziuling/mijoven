<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    //
    protected $table = 'cargos';
   protected $fillable = ['nombre', 'tipo'];


    public function directiva()
    {
    	return $this->hasMany('App\Directiva','cargo_id');
    }
}
