<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reglamento;

class ReglamentosCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Reglamento::with('categoria')->get()->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo'=>'required|string', 
            'introduccion'=>'required|string', 
            'reglamento'=>'required|string',
            'categoria_id'=>'required|numeric',
        ]);
        $re = Reglamento::create($request->only(['titulo', 'introduccion', 'reglamento','categoria_id']));
        return $re;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Reglamento::findOrFail($id);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'titulo'=>'required|string', 
            'introduccion'=>'required|string', 
            'reglamento'=>'required|string',
            'categoria_id'=>'required|numeric',
            ]);
        $reglamento= Reglamento::find($id);
        $reglamento->fill($request->only(['titulo', 'introduccion', 'reglamento','categoria_id']));
        $reglamento->save();
        return $reglamento;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Reglamento::destroy($id);
        
    }
}
