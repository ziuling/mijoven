<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Identidad;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;

use Illuminate\Support\Facades\Auth;

class IdentidadCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r)
    {
        if ($r->wantsJson()) {
            $identidades = Identidad::with('club')->where('club_id', auth()->user()->club->id)->get()->toJson();
            return $identidades;
            
        }
        
        $identidad = Identidad::all();
        return view('identidades.index', ['identidad'=>$identidad]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $i = new Identidad();
        $i->direccion = $request->direccion;
        $i->club_id = auth()->user()->club->id;
      //  dd(Input::hasFile('logo'));
        if (Input::hasFile('logo')){
            $file=Input::file('logo');
            $file->move(public_path().'/img/logos/', $file->getClientOriginalName());
            $i->logo=$file->getClientOriginalName();
        }
        

        $i->save();
        return $i;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Identidad::where('id', $id)->where('club_id', auth()->user()->club->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'direccion'=>'string',
            'club_id'=> 'numeric',
            ]);

        $i= Identidad::find($id);
        $i->direccion = $request ->direccion;
        $i->club_id = auth()->user()->club->id;
        
        if (Input::hasFile('logo')){
            $file=Input::file('logo');
            $file->move(public_path().'/img/logos/', $file->getClientOriginalName());
            $i->logo=$file->getClientOriginalName();
        }

        $i->save();
        return $i;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Identidad::destroy($id);
        
    }
}
