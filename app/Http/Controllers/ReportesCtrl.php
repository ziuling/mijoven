<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Circular;
use App\Rol;
use App\User;
use Illuminate\Support\Facades\Auth;

class ReportesCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->rol->id == 4){
           $categoria = Auth::user()->club->categoria_id;
           $user = Auth::user()->club->iglesia->distrito->zona->user_id;
           $entidad = Auth::user()->club->id;
           $club = Auth::user()->club->id;
        }else if(Auth::user()->rol->id == 5){
            $categoria = 4;
            $user = Auth::id();
            $entidad = Auth::user()->iglesia->id;
            $club = null;
        }
        else{
            $categoria = null;
            $user = null;
            $entidad = null;
            $club = null;
        }
        $secretario = User::where('rol_id', 5)->first();
        $zona = User::where('rol_id', 3);
        $id = $secretario->id;
        return view('consultas.index', [ 'categoria'=>$categoria, 'user'=>$user, 'zona'=>$zona, 'id'=>$id, 'user'=>$user, 'entidad'=>$entidad, 'club'=>$club]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
