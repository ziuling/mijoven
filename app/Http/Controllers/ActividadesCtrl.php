<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Actividad;
use App\Circular;
use App\Tipo_a;

use \Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use DB;

class ActividadesCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r)
    {
        if ($r->wantsJson()) {
            if(Auth::user()->rol_id == 5 || Auth::user()->rol_id == 3 ){
                $actividades = Auth::user()->actividades;
                return $actividades;
            }else{
                $actividades = Actividad::all();
                return $actividades;
            }
            
        }
        $actividad = Actividad::with('circular', 'tipo', 'user');
        
        return view('actividades.index', ['actividad'=>$actividad]);
    }
    /*
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //$actividades=DB::table('actividades')->get();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'mes'=>'string', 
            'nombre'=>'required|string',
            'descripcion'=>'string',
            'puntuacion'=>'required|numeric',
            'tipo_a_id'=>'numeric',
            //'circular_id'=>'numeric',
           
            ]);

        $a = Actividad::create($request->only(['mes', 'nombre','descripcion','puntuacion','tipo_a_id','circular_id']));
        
        if ($request->user()->rol->nombre == 'Secretario(a)') {
            $a->user_id = Auth::id();
        }else if ($request->user()->rol->nombre == 'Líder de Zona') {
            $a->user_id = Auth::id();
        }
        if($request->f_inicio) $a->f_inicio = Carbon::parse($request->f_inicio);
        if($request->f_final) $a->f_final = Carbon::parse($request->f_final);
        $a->save();
        
        return response()->json($request->all(), 200);
        //return response()->json($a, 200);
        //return redirect()->route('actividades.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->rol_id == 5 || Auth::user()->rol_id == 3 ){
            $actividades = Actividad::where('tipo_a_id',$id)->where('user_id', Auth::id())->get();
            return $actividades;
        }else{
            $actividades = Actividad::where('tipo_a_id',$id)->get();
            return $actividades;
        }    
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request, [
            'f_inicio'=>'date', 
            //'f_final'=>'date', 
            'mes'=>'string', 
            'nombre'=>'string',
            'descripcion'=>'string',
            'puntuacion'=>'numeric',
            'tipo_a_id'=>'numeric',
            'circular_id'=>'numeric',
            ]);
        
        $date1 = \Carbon\Carbon::parse($request->f_inicio);
        $date2 = \Carbon\Carbon::parse($request->f_final);
        $actividad= Actividad::find($id);
        $actividad->f_inicio = $date1;
        $actividad->f_final = $date2;
        $actividad->mes = $request->mes;
        $actividad->nombre = $request->nombre;
        $actividad->descripcion = $request->descripcion;
        $actividad->puntuacion = $request->puntuacion;
        $actividad->tipo_a_id = $request->tipo_a_id;
        $actividad->circular_id = $request->circular_id;
        $actividad->user_id = Auth::id();
        $actividad->save();
        return $actividad;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $actividad= Actividad::destroy($id);
        return $actividad;
        
    }
}
