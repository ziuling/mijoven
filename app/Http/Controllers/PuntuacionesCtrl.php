<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Puntuacion;
use App\Detalle;
use App\Categoria;
use App\Actividad;
use App\Club;
use App\Iglesia;
use Illuminate\Support\Facades\Input;

class PuntuacionesCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $r)
    {
        if ($r->wantsJson()) {
            $puntuaciones = Puntuacion::with('iglesia', 'club', 'categoria')->get()->toJson();
            return $puntuaciones;
        }
        $carbon = new \Carbon\Carbon();
        $fecha = $carbon->now();
        $actividades = Actividad::all(); 
        $fijas = Actividad::fijas()->get(); 
        $generales = Actividad::generales()->get(); 
        $precamporee = Actividad::precamporee()->get(); 
        return view('puntuaciones.index', ['fijas'=>$fijas, 'fecha'=>$fecha,  'precamporee'=>$precamporee, 'generales'=>$generales]);
        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //return view('puntuaciones.mensuales');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $puntuacion = new Puntuacion();
        $puntuacion->club_id = $request->club_id;
        $puntuacion->iglesia_id = $request->iglesia_id;
        $puntuacion->mes = $request->mes;
        $puntuacion->categoria_id = $request->categoria_id;
        $puntuacion->circular_id = $request->circular_id;
        $puntuacion->total = $request->total;
        $puntuacion->totalm = $request->totalm;
        $puntuacion->totalg = $request->totalg;

        $puntuacion->save();

        //dump($request->actividad_id);
        dump($request->all());
        $detalle= json_decode($request->detalle, true);
        $actividad_id = $detalle['actividad_id'];
        $puntos = $detalle['puntos'];
        $cont= 0;

        while($cont < count($actividad_id)){
            $detalle = new Detalle();
            $detalle->puntuacion_id= $puntuacion->id;
            $detalle->actividad_id= $actividad_id[$cont];
            $detalle->puntos= $puntos[$cont];
            $detalle->save();
            
            $cont=$cont + 1;
        }

      /* foreach ($datos  as $key => $value) {
          solo para almacenar varios registros en un mismo campo
        }*/
  
        return $puntuacion;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $puntuaciones = Puntuacion::where('categoria_id', $id)->get();
            return $puntuaciones;
    }

    /*
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
