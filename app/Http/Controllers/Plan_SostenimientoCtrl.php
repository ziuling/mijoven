<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plan_sostenimiento;

use Illuminate\Support\Facades\Auth;


class Plan_SostenimientoCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r)
    {
        if ($r->wantsJson()) {
            if(auth()->user()->rol->id == 4){
                return Plan_sostenimiento::with('club',  'actividad')->where('club_id', auth()->user()->club->id)->get();
            }
            else{
                return Plan_sostenimiento::with('club',  'actividad')->get();
            }
        } 
        $club = Auth::id();   
        return view('planes.index', ['club'=>$club]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crearPDF($datos, $plan, $p, $vistaurl){
        $data = $datos;
        $date = date('Y-m-d');
        $view = \View::make($vistaurl, compact('data', 'plan', 'date', 'p'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);

        return $pdf->stream('reporte.pdf');
       
    }

    public function crear_reporte($id, $mes){
        $vistaurl = "planes.pdf";
        $planes = Plan_sostenimiento::where('club_id', $id)->where('mes', $mes)->get();
        $plan = count($planes);
        $p = Plan_sostenimiento::where('club_id', $id)->where('mes', $mes)->first();
       // dd($planes);
       
        return $this->crearPDF($planes, $plan, $p, $vistaurl);
    }


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        var_dump($request->all());
        $this->validate($request, [
            'mes'=>'required|string',
            'general'=>'required|string',
            'inv'=>'numeric',
            'inv_rec'=>'numeric',
            'ganancia'=>'numeric',
            'total_fondo'=>'numeric',
            ]);
        $plan = new Plan_sostenimiento();
        
        if ($request->user()->rol->nombre == 'Secretario(a) de Club') {
            $plan->club_id = auth()->user()->club->id;
        }
        $plan->mes = $request->mes;
        $plan->general = $request->general;
        $plan->inv= $request->inv;
        $plan->inv_rec= $request->inv_rec;
        $plan->ganancia= $request->ganancia;
        $plan->total_fondo= $request->total_fondo;
        $plan->save();
        return $plan;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Plan_sostenimiento::findOrFail($id)->where('club_id', auth()->user()->club->id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'mes'=>'required|string',
            'general'=>'required|string',
            'mes_ant'=>'required|string',
            'inv'=>'numeric',
            'inv_rec'=>'numeric',
            'ganancia'=>'numeric',
            'total_fondo'=>'numeric',
            'actividad_id'=>'numeric',
            ]);

        $plan = Plan_sostenimiento::find($id);
        
        if ($request->user()->rol->nombre == 'Secretario(a) de Club') {
            $plan->club_id = auth()->user()->club->id;
        }
        $plan->mes = $request ->mes;
        $plan->general = $request ->general;
        $plan->mes_ant= $request ->mes_ant;
        $plan->inv= $request ->inv;
        $plan->inv_rec= $request ->inv_rec;
        $plan->ganancia= $request ->ganancia;
        $plan->total_fondo= $request ->total_fondo;
        $plan->actividad_id= $request ->actividad_id;
        $plan->save();
        return $plan;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Plan_sostenimiento::destroy($id);
        
    }
}
