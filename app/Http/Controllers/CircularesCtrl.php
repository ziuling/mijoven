<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Circular;
use App\Actividad;
use App\User;
use Illuminate\Support\Facades\Auth;

class CircularesCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r)
    {
        if ($r->wantsJson()) {
            if(Auth::user()->rol->id == 3 || Auth::user()->rol->id == 5){
                $circulares = Auth::user()->circulares;
            }else if(Auth::user()->rol->id == 2){
               $user = User::where('rol_id', 5)->first();
               $circulares = Circular::where('user_id', $user->id)->get();
            }else {
               $circulares = Circular::with('categoria', 'user')->get();
            }
            return $circulares;

        }    
        return view('Circulares.index');
        
    }

    

    public function crearPDF($datos, $actividades, $vistaurl, $tipo){
        $data = $datos;
        $usuario = User::where('rol_id', 2)->first();
        $lider = $usuario->nombre;
        $rol = $datos->user->rol_id;
        
        //dd($lider);
        $actividades = $actividades;
        $date = date('Y-m-d');
        $view = \View::make(utf8_encode($vistaurl), compact('data', 'lider', 'actividades', 'date',  'rol'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML(($view));

        if($tipo==1){
            return $pdf->stream('reporte.pdf');
        }
        if($tipo==2){
            return $pdf->download('reporte.pdf');
        }
    }

    public function crear_reporte($tipo, $id){
        $vistaurl = "circulares.pdf";
        $circulares = Circular::findOrFail($id);
        $actividades = Actividad::where('circular_id', $id)->get();
        return $this->crearPDF($circulares, $actividades, $vistaurl, $tipo);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo'=>'required|string', 
            'sub_titulo'=>'string', 
            'destinario'=>'string',
            'asunto'=>'string',
            'fecha'=>'required|string',
            'recomendacion'=>'string',
            'introduccion'=>'string',
            'reconocimiento'=>'string',
            'observacion'=>'string',
            'categoria_id'=>'required|numeric',
        ]);

        $c = Circular::create($request->only(['titulo', 'sub_titulo', 'destinario','asunto','fecha','recomendacion', 'introduccion', 'reconocimiento','observacion','categoria_id', 'estado']));
        if ($request->user()->rol->nombre == 'Secretario(a)') {
            $c->user_id = Auth::id();
       
        }else if ($request->user()->rol->nombre == 'Líder de Zona') {
            $c->user_id = Auth::id();
          
        }
        $c->save();

        return $c;
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Circular::where('categoria_id', $id)->where('user_id', Auth::id())->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       /* $this->validate($request, [
            'titulo'=>'required|string', 
            'sub_titulo'=>'string', 
            'destinario'=>'required|string',
            'asunto'=>'string',
            'fecha'=>'required|string',
            'recomendacion'=>'string',
            'introduccion'=>'string',
            'reconocimiento'=>'string',
            'observacion'=>'string',
            'categoria_id'=>'required|numeric',
        ]);*/
        $circular= Circular::find($id);
        $circular->fill($request->only(['titulo', 'sub_titulo', 'destinario','asunto','fecha','recomendacion','introduccion', 'reconocimiento','observacion','categoria_id', 'estado']));
        if ($request->user()->rol->nombre == 'Secretario(a)') {
            $circular->user_id = Auth::id();
        }else if ($request->user()->rol->nombre == 'Líder de Zona') {
            $circular->user_id = Auth::id();
        }
        $circular->save();
        return $circular;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $object = Circular::findOrFail($id);

        foreach ($object->actividades as $actividad) {
            $actividad->delete();
        }
        $object->delete();
        return $object->__toString();
    }
}
