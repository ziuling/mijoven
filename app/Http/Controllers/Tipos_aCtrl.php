<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tipo_a;

use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Auth;


class Tipos_aCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r)
    {
        if ($r->wantsJson()) {
            $tipos = Auth::user()->tipos;
            return $tipos;
        }  
        
        return view('tipos.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'tipo'=>'required|string', 
            'nombre'=>'string', 
            'total_puntuacion'=>'numeric',
        ]);
        $t = new Tipo_a();
        $t->tipo = $request->tipo;
        $t->nombre = $request->nombre;
        $t->total_puntuacion = $request->total_puntuacion;
        if ($request->user()->rol->id == 5) {
            $t->user_id = Auth::id();
       
        }else if ($request->user()->rol->id == 3) {
            $t->user_id = Auth::id();
          
        }
        $t->save();
        return $t;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Tipo_a::findOrFail($id);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tipo'=>'required|string', 
            'nombre'=>'string', 
            'total_puntuacion'=>'numeric', 
            ]);
        $t= Tipo_a::find($id);
        $t->fill($request->only(['tipo', 'nombre', 'total_puntuacion']));
        if ($request->user()->rol->nombre == 'Secretario(a)') {
            $t->user_id = Auth::id();
       
        }else if ($request->user()->rol->nombre == 'Líder de Zona') {
            $t->user_id = Auth::id();
          
        }
        $t->save();
        return $t;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Tipo_a::destroy($id);
        
    }
}
