<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Zona;
use App\Distrito;
use App\Pastor;
use App\Club;
use App\Iglesia;
use App\Inscripcion;
use App\Reglamento;
use App\Actividad;
use App\Circular;
use App\Grupo;
use App\Puntuacion;
use App\Miembro;
use App\Pago;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');

    }

    public function administracion()
    {
        $zonas = \App\Zona::with('lider')->get();
        $distritos = \App\Distrito::with('zona','pastor')->get();
        $pastores = \App\Pastor::all();
        $clubes = \App\Club::all();
        $iglesias = \App\Iglesia::with('distrito','tipo')->get();
        $inscripciones = \App\Inscripcion::all();
        $inscripcion = \App\Inscripcion::with('categoria');
        $reglamentos = \App\Reglamento::all();

        return view('informaciones.index',['zonas'=>$zonas,'distritos'=>$distritos,'pastores'=>$pastores,'iglesias'=>$iglesias, 'clubes'=>$clubes, 'inscripciones'=>$inscripciones, 'inscripcion'=>$inscripcion, 'reglamentos'=>$reglamentos]);
        
        //return view('informaciones.index');
    }

    
    /* function consultas()
    {
        $inscripciones = \App\Inscripcion::all();
        $circulares = \App\Circular::all();
        $miembros = \App\Miembro::all();
        $directivas = \App\Directiva::all();
        $grupos = \App\Grupo::all();
        $puntuaciones = \App\Puntuacion::all();
        $pagos = \App\Pago::all();

        return view('consultas.index',['directivas'=>$directivas, 'puntuacion'=>$puntuaciones, 'grupos'=>$grupos, 'circular'=>$circulares, 'inscripcion'=>$inscripciones, 'miembro'=>$miembros, 'pago'=>$pagos]);
    }*/


    public function direccionc()
    {
        $clubes = \App\Club::all();

        return view('directivas.direccionc',['clubes'=>$clubes]);
    }

    public function direcciong()
    {
        $grupos = \App\Grupo::all();

        return view('directivas.direcciong',['grupos'=>$grupos]);
    }


}
