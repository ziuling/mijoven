<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\User;

class UsersCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r)
    {
        if ($r->wantsJson()) {
            $users = User::with('rol')->get();
            $salida = array();
            if ($r->user()->rol->nombre == 'Administrador') {
                foreach ($users as $user) {
                    $rol =$user->rol->nombre ;
                    if ($rol == 'Administrador' || $rol == 'Líder Juvenil' || $rol =='Secretario(a)') {
                       array_push($salida, $user);
                    }
                }               
            } elseif ($r->user()->rol->nombre == 'Líder Juvenil') {
                foreach ($users as $user) {
                    $rol =$user->rol->nombre ;
                    if ($rol == 'Secretario(a) de Club' || $rol == 'Secretario(a) de Sociedad de Jóvenes' || $rol =='Líder de Zona') {
                       array_push($salida, $user);
                    }
                }   
            }   
            return $salida;
        }   
        $status = \App\User::where('users.status', '0' || '1')->get();
        $aux_status = array('1' => 'Activo','0' => 'Inactivo');
        return view('usuarios.index',['status'=> $status,'aux' => $aux_status, ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
         $rules = [
            'nombre' => 'required|string|max:40',
            'email'=>'unique:users|email|required|max:50',
            'password'=> 'required|alpha_num|min:6|max:10',
            'status'=> 'required|numeric',
            'rol_id'=> 'required|numeric',
         ];
 
        $this->validate($request, $rules);
        $user = new User();
        $user->nombre = $request ->nombre;
        $user->email = $request ->email;
        $user->password = bcrypt($request ->password);
        $user->status = $request ->status;
        $user->rol_id = $request ->rol_id;
        $user->save();
        
        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre'=>'required|string|min:10|max:40',
            'email'=>'required|email|min:6|max:50',
            'password'=> 'required|alpha_num|min:6|max:10',
            'status'=> 'required|numeric',
            'rol_id'=> 'required|numeric']);

        $user= User::find($id);
        $user->nombre = $request ->nombre;
        $user->email = $request ->email;
        if ($request->has('password')) {
            $user->password = bcrypt($request ->password);
        }
        $user->status = $request ->status;
        $user->rol_id = $request ->rol_id;
        $user->save();
        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return User::destroy($id);

        
    }
}
