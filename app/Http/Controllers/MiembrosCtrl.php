<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Miembro;
use App\Persona;
use App\Miembro_investidura;
use App\Miembro_leccion;
use App\Investidura;
use App\Leccion;
use \Carbon\Carbon;

use Illuminate\Support\Facades\Auth;


class MiembrosCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r)
    {
        if ($r->wantsJson()) {
            if(auth()->user()->rol_id == 6){
                return Miembro::with('persona', 'club', 'iglesia', 'grupo')->where('iglesia_id', auth()->user()->iglesia->id)->get();
            }
            else if(auth()->user()->rol_id == 4){
                return Miembro::with('persona', 'club', 'iglesia', 'grupo')->where('club_id', auth()->user()->club->id)->get();
            }else{
                return Miembro::all(); 
            }
        }
        
        $investiduras = Investidura::all();
        $lecciones = Leccion::all();

        return view('miembros.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dump($request->all());
        $p= json_decode($request->persona, true);
        dump($p['cedula']);
        
        $this->validate($request,[
            'grupo_id'=>'numeric',
            'curso'=>'string',
            'categoria'=>'string',
            'ministerio'=>'string',
            'tipo_sangre'=>'string',
            'necesidad'=>'string',
            'observacion'=>'string',
            'donante'=>'required|numeric',
            'voluntario'=>'required|numeric'
            ]);
        
        
        $persona = new Persona();
        $persona->cedula = $p['cedula'];
        $persona->nombre = $p['nombre'];
        $persona->telefono = $p['telefono'];
        $persona->direccion = $p['direccion'];
        $persona->correo = $p['correo'];
        //$date1 = Carbon::parse($p['fecha_nac']);
        //$persona->fecha_nac = $date1;
       // $persona->fecha_nac = $request->persona['fecha_nac'];
       // $persona->edad = Carbon::createFromDate($date1)->age; 
        $persona->save();

        $miembro = new Miembro();
        $miembro->persona_id = $persona->id;
        
        if ($request->user()->rol->nombre == 'Secretario(a) de Club') {
            $miembro->club_id = auth()->user()->club->id;
        }else{
            $miembro->iglesia_id = auth()->user()->iglesia->id;
        }
       
        $miembro->grupo_id = $request ->grupo_id;
       if ($miembro->grupo_id == null){
            $miembro->t = 'o';
        }else{
            $miembro->t = 'g';
        }
        $miembro->curso = $request ->curso;
        $miembro->categoria = $request ->categoria;
        $miembro->ministerio = $request ->ministerio;
        $miembro->tipo_sangre = $request ->tipo_sangre;
        $miembro->necesidad = $request ->necesidad;
        $miembro->observacion = $request ->observacion;
        $miembro->donante = $request ->donante;
        $miembro->voluntario = $request ->voluntario;
        $miembro->save();
        dump($request->all());
        $miembro_investidura= json_decode($request->miembro_investidura, true);
        $miembro_leccion= json_decode($request->miembro_leccion, true);
        $investidura_id = $miembro_investidura['investidura_id'];
        $leccion_id = $miembro_leccion['leccion_id'];
        $cont= 0;
        while($cont < count($investidura_id)){
            $miembro_investidura = new Miembro_investidura();
            $miembro_investidura->miembro_id= $miembro->id;
            $miembro_investidura->investidura_id= $investidura_id[$cont];
            $miembro_investidura->save();
            
            $cont=$cont + 1;
        }
        while($cont < count($leccion_id)){
            $miembro_leccion = new Miembro_leccion();
            $miembro_leccion->miembro_id= $miembro->id;
            $miembro_leccion->leccion_id= $leccion_id[$cont];
            $miembro_leccion->save();   
            $cont=$cont + 1;
        }

        return $miembro;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        if(auth()->user()->rol_id == 6){
            return Miembro::where('id',$id)->where('iglesia_id', auth()->user()->iglesia->id)->get();
        }
        else if(auth()->user()->rol_id == 4 ){
            return Miembro::where('id',$id)->where('club_id', auth()->user()->club->id)->get();
        }else{
            return Miembro::all();

        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'grupo_id'=>'numeric',
            'persona_id'=>'required|numeric',
            'curso'=>'string',
            'categoria'=>'string',
            'ministerio'=>'string',
            'tipo_sangre'=>'string',
            'necesidad'=>'string',
            'observacion'=>'string',
            'donante'=>'required|numeric',
            'voluntario'=>'required|numeric'
            ]);

        $miembro =Miembro::find($id);
        if ($request->user()->rol->nombre == 'Secretario(a) de Club') {
            $miembro->club_id = auth()->user()->club->id;
        }else{
            $miembro->iglesia_id = auth()->user()->iglesia->id;
        }
       
        $miembro ->persona_id = $request->persona_id;
        $miembro ->iglesia_id = $request->iglesia_id;
        $miembro ->grupo_id = $request->grupo_id;
        $miembro ->curso = $request->curso;
        $miembro ->categoria = $request->categoria;
        $miembro ->ministerio = $request->ministerio;
        $miembro ->tipo_sangre = $request->tipo_sangre;
        $miembro ->necesidad = $request->necesidad;
        $miembro ->observacion = $request->observacion;
        $miembro ->donante = $request->donante;
        $miembro ->voluntario = $request->voluntario;
        $miembro->save ();
        return $miembro;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Miembro::destroy($id);
        
    }
}
