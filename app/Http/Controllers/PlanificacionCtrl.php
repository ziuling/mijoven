<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Planificacion;
use App\Persona;
use App\Identidad;
use App\Club;
use \Carbon\Carbon;


use Illuminate\Support\Facades\Auth;

class PlanificacionCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r)
    {
        if ($r->wantsJson()) {
            if(auth()->user()->rol->id == 4){

               return Planificacion::with('club', 'iglesia')->where('club_id', auth()->user()->club->id)->get();

            }else if(auth()->user()->rol->id == 6) {

                return Planificacion::with('club', 'iglesia')->where('iglesia_id', auth()->user()->iglesia->id)->get();
            }
            return Planificacion::with('club', 'iglesia')->get();
            
        } 

        if(auth()->user()->rol->id == 4){
           $objeto = auth()->user()->club;
           $id = auth()->user()->club->id;
           $rol = auth()->user()->club->user->rol_id;
        }
        else{
           $objeto = auth()->user()->iglesia;
           $id = auth()->user()->iglesia->id;
           $rol = auth()->user()->iglesia->user->rol_id;
        }
        return view('planificaciones.index', ['id'=>$id, 'rol'=>$rol, 'objeto'=>$objeto]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crearPDF($datos, $objeto, $rol, $logo, $vistaurl){
        $data = $datos;
        $año_actual = date('Y');
        //dd($c);
        
       // dd($logo->logo);
        $meses  = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $año_siguiente = date("Y")+1;
        $view = \View::make($vistaurl, compact('data', 'meses', 'año_actual', 'logo', 'año_siguiente', 'club', 'rol', 'objeto'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper("letter","landscape"); 
      
        return $pdf->stream('reporte.pdf');
        
    }

    public function crear_reporte($tipo, $id){
        $vistaurl = "planificaciones.pdf";
       //dd($rol);
        if($tipo == 1){
            $objeto  = Club::where('id', $id)->first();
            $l = Identidad::where('club_id', $objeto->id)->first(); 
            if($l){
            $logo = $l->logo; 
            }
            $logo = 'mojoven.png';
            //dd($obj);
            $rol = $objeto->user->rol_id;

        }else if($tipo == 2){
            $objeto  = Iglesia::where('id', $id)->first();
            $rol = $objeto->user->rol_id; 
        }

        //dd();
        if( $rol == 4 ){
            $planificaciones = Planificacion::where('club_id', $id)->get();
            //dd($planificaciones);
            $plan = count($planificaciones);
        }else if($rol == 6){
            $planificaciones = Planificacion::where('iglesia_id', $id)->get();
            $plan = count($planificaciones);
        }

        return $this->crearPDF($planificaciones, $objeto, $rol, $logo, $vistaurl);
    }

    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'f_inicio'=>'date', 
            'f_final'=>'date', 
            'como'=>'required|string', 
            'que'=>'required|string',
            'objetivo'=>'required|string',
            'proposito'=>'required|string',
        ]);

        $date1 = \Carbon\Carbon::parse($request->f_inicio);
        $date2 = \Carbon\Carbon::parse($request->f_final);

        $planificacion = new Planificacion();
        $planificacion->f_inicio=$date1;
        $planificacion->f_final=$date2;
        $planificacion->mes=$request->mes;
        $planificacion->estado= $request->estado;
        $planificacion->responsable=$request->responsable;
        $planificacion->que=$request->que;
        $planificacion->como=$request->como;
        $planificacion->objetivo=$request->objetivo;
        $planificacion->proposito=$request->proposito;
       
        if ($request->user()->rol->id == 4) {
            $planificacion->club_id = auth()->user()->club->id;
        }else if($request->user()->rol->id == 6){
            $planificacion->iglesia_id = auth()->user()->iglesia->id;
        }
        $planificacion->save();
        return $planificacion;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(auth()->user()->rol_id == 6){
           return Planificacion::where('id',$id)->where('iglesia_id', auth()->user()->iglesia->id)->get();
        }else{
            return Planificacion::where('id',$id)->where('club_id', auth()->user()->club->id)->get();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'f_inicio'=>'date', 
            'f_final'=>'date',  
            'como'=>'required|string', 
            'que'=>'required|string',
            'objetivo'=>'required|string',
            'proposito'=>'required|string',
            'persona_id'=>'numeric',
            ]);

        
        
        $date1 = \Carbon\Carbon::parse($request->f_inicio);
        $date2 = \Carbon\Carbon::parse($request->f_final);

        $planificacion= Planificacion::find($id);
        $planificacion->f_inicio=$date1;
        $planificacion->f_final=$date2;
        $planificacion->mes=$request->mes;
        $planificacion->estado= $request->estado;
        $planificacion->responsable=$request->responsable;
        $planificacion->que=$request->que;
        $planificacion->como=$request->como;
        $planificacion->objetivo=$request->objetivo;
        $planificacion->proposito=$request->proposito;
        if ($request->user()->rol->id == 4) {
            $planificacion->club_id = auth()->user()->club->id;
        }else if($request->user()->rol->id == 6){
            $planificacion->iglesia_id = auth()->user()->iglesia->id;
        }
       
        $planificacion->save();
        return $planificacion;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Planificacion::destroy($id);
    }
}
