<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grupo;
use App\Ministerio;
use App\Club;
use App\Directiva;
use App\Miembro;
use App\Miembro_leccion;

use Illuminate\Support\Facades\Auth;

class GruposCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Grupo::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    public function crearPDF($directivas, $miembros, $grupo, $anciano, $lider, $lidera, $cantidad,  $vistaurl){
        $dir = $directivas;
        $mie = $miembros;
        if($grupo->club_id!=null){
           $rol = $grupo->club->user->rol_id;
        }else if($grupo->iglesia_id!=null){
           $rol = $grupo->iglesia->user->rol_id;
        }

        $date = date('Y-m-d');
        $lecciones = Miembro_leccion::all();
        $view = \View::make($vistaurl, compact('dir', 'mie', 'grupo', 'anciano', 'lider', 'lidera', 'date', 'lecciones', 'rol', 'cantidad'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper("A4","landscape");
      
        return $pdf->stream('reporte.pdf');
       
    }

    public function crear_reporte($tipo, $id){
        $vistaurl = "directivas.grupos";
            $grupo = Grupo::where('id', $id)->first();
            //dd($grupo);
           // $gru = count($grupo);
            $directivas = Directiva::where('grupo_id', $id)->get();
            $anciano = Directiva::where('grupo_id', $id)->where('cargo_id', 5)->first();
            $lider = Directiva::where('grupo_id', $id)->where('cargo_id', 6)->first();
            $lidera = Directiva::where('grupo_id', $id)->where('cargo_id', 7)->first();
            
           // dd($lider);
            //dd($anciano);
            $miembros = Miembro::where('grupo_id', $id)->get();
            $cantidad = count($miembros);

        return $this->crearPDF($directivas, $miembros, $grupo, $anciano, $lider, $lidera, $cantidad, $vistaurl);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre'=>'required|string|max:200',
            'direccion'=>'required|string|min:10|max:255',
            ]);

        $grupo = new Grupo();

        if ($request->user()->rol->nombre == 'Secretario(a) de Club') {
            $grupo->club_id = auth()->user()->club->id;
        }else{
            $grupo->iglesia_id = auth()->user()->iglesia->id;
        }

        $grupo->nombre = $request ->nombre;
        $grupo->direccion = $request ->direccion;
        $grupo->user_id = Auth::id();
        $grupo->save();
        return $grupo;
    }
     
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Grupo::findOrFail($id);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
           'nombre'=>'required|string|max:200',
            'direccion'=>'required|string|min:5|max:255',
            'club_id'=> 'numeric',
            'iglesia_id'=> 'numeric',]);
            
        $grupo= Grupo::find($id);
        $grupo->nombre = $request ->nombre;
        $grupo->direccion = $request ->direccion;
        $grupo->club_id = $request ->club_id;
        $grupo->iglesia_id = $request ->iglesia_id;
        $grupo->save();
        return $grupo;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Grupo::destroy($id);
        
    }
}
