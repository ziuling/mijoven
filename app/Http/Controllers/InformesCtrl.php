<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Informe;
use App\Directiva;
use App\Modulo;
use App\Act_modulo;
use App\Informe_act;
use App\Identidad;
use App\Iglesia;
use App\Club;
use DB;

use Illuminate\Support\Facades\Auth;

class InformesCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r)
    {
        if ($r->wantsJson()) {
            return Informe::with('modulo', 'club', 'iglesia')->get()->toJson();
        }
        $carbon = new \Carbon\Carbon();
        $fecha = $carbon->now();
        $fecha->SubMonth();
        $fecha->SubMonth();
        $mes = $fecha->format('F');
         if(auth()->user()->rol->id == 4){
           $objeto = auth()->user()->club;
           $id = auth()->user()->club->id;
           $rol = auth()->user()->club->user->rol_id;
        }
        else{
           $objeto = auth()->user()->iglesia;
           $id = auth()->user()->iglesia->id;
           $rol = auth()->user()->iglesia->user->rol_id;
        }
        return view('informes.index', ['fecha'=>$fecha, 'mes'=>$mes, 'objeto'=>$objeto, 'id'=>$id, "rol"=>$rol]);
    }

    public function historial()
    {
       if(auth()->user()->rol->id == 4){
           $objeto = auth()->user()->club;
           $id = auth()->user()->club->id;
           $rol = auth()->user()->club->user->rol_id;
        }
        else{
           $objeto = auth()->user()->iglesia;
           $id = auth()->user()->iglesia->id;
           $rol = auth()->user()->iglesia->user->rol_id;
        }
        return view('informes.historial', ['objeto'=>$objeto, 'id'=>$id, 'rol'=>$rol]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crearPDF($datos, $obj, $rol, $inf, $vistaurl){
        $data = $datos;
        $date = date('Y-m-d');

        $actividades = Informe_act::all();
        //dd($actividades);
        $actividad = Act_modulo::all();
        $modulos = Modulo::all();
        
        $view = \View::make($vistaurl, compact('data', 'date', 'inf', 'rol', 'obj',  'actividades', 'actividad', 'modulos'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);

        return $pdf->stream('reporte.pdf');
    }

    public function crear_reporte($tipo, $id, $mes){

        $vistaurl = "informes.pdf";
        if($tipo == 1){
            $obj  = Club::where('id', $id)->first();
            //dd($obj);
            $rol = $obj->user->rol_id;

        }else if($tipo == 2){
            $obj  = Iglesia::where('id', $id)->first();
            $rol = $obj->user->rol_id; 
        }

        //dd($directiva);
        if( $rol == 4 ){
            $informes = Informe::where('club_id', $id)->where('mes', $mes)->get();
            //dd($informes);
            $inf = count($informes);
        }else if($rol == 6){
            $informes = Informe::where('iglesia_id', $id)->where('mes', $mes)->get();
            $inf = count($informes);
        }

        return $this->crearPDF($informes, $obj, $rol, $inf, $vistaurl);
    }

    public function create()
    {
        //
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd( $request->all() );
            $this->validate($request,[
            'mes'=>'required|string',
            'modulo_id'=>'required|numeric',
            'informe_act.actmod_id'=>'required',
            'descripcion'=>'required|string',
            ]);
        
            $informe = new Informe();
            $informe->mes=$request->mes;
            $informe->descripcion=$request->descripcion;

            if ($request->user()->rol->id == 4) {
                $informe->club_id = auth()->user()->club->id;
            }else{
                $informe->iglesia_id = auth()->user()->iglesia->id;
            }
            $informe->modulo_id=$request->modulo_id;
            $informe->save();

            $actmod_id =$request->informe_act['actmod_id'];
            $cont = 0;

            while($cont < count($actmod_id)){
                $informe_act = new Informe_act();    
                $informe_act->informe_id= $informe->id;    
                $informe_act->actmod_id= $actmod_id[$cont];
                $informe_act->save();

                $cont=$cont+1;
            }

        return $informe;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(auth()->user()->club->id == null){
            $informe= Informe::where('modulo_id',$id)->where('iglesia_id', auth()->user()->iglesia->id)->get();
            return $informe;
        }
        else{

            $informe = Informe::where('modulo_id',$id)->where('club_id', auth()->user()->club->id)->get();
            return $informe;
        }
           
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Informe::destroy($id);
    }
        
}
