<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Pago;
use Illuminate\Support\Facades\Auth;


class PagosCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r)
    {
        if ($r->wantsJson()) {
            if(auth()->user()->rol->id == 6 ){
              return Pago::with('club', 'iglesia')->where('iglesia_id', auth()->user()->iglesia->id)->get();
            }
            else if(auth()->user()->rol->id == 4 ){
              return Pago::with( 'club', 'iglesia')->where('club_id', auth()->user()->club->id)->get();
            }
            else{
              $pagos = Pago::with('club', 'iglesia', 'actividad')->get()->toJson();
              return $pagos; 
            }
        }    
        return view('pagos.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function crearPDF($datos, $vistaurl, $tipo){
        $data = $datos;
        $date = date('Y-m-d');
        $view = \View::make($vistaurl, compact('data', 'date'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);

        if($tipo==1){
            return $pdf->stream('reporte.pdf');
        }
        if($tipo==2){
            return $pdf->download('reporte.pdf');
        }
    }

    public function crear_reporte($tipo, $id){

        $vistaurl = "pagos.pdf";
        $pagos = Pago::findOrFail($id);
        //dd($pagos);

        return $this->crearPDF($pagos, $vistaurl, $tipo);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'fecha'=>'required|date',
            'n_referencia'=>'required|string',
            'banco'=>'required|string',
            'monto'=>'required|int',
            'concepto'=>'string',
            'cantidad_m'=>'numeric',
            'cantidad_n'=>'numeric',
            'cantidad_a'=>'numeric',
            'cantidad_e'=>'numeric',
            'directiva'=>'numeric',
            'tipo'=>'required|string',
            'comprobante'=>'required',
            ]);
        $date = \Carbon\Carbon::parse($request->fecha);

        $pago = new Pago();
      
        if ($request->user()->rol->nombre == 'Secretario(a) de Club') {
            $pago->club_id = auth()->user()->club->id;
        }else{
            $pago->iglesia_id = auth()->user()->iglesia->id;
        }

        $pago->fecha = $date;
        $pago->n_referencia = $request ->n_referencia;

        if (Input::hasFile('planilla')){
            $file=Input::file('planilla');
            $file->move(public_path().'/img/pagos/', $file->getClientOriginalName());
            $pago->planilla=$file->getClientOriginalName();
        }

        $pago->banco = $request ->banco;
        $pago->monto= $request ->monto;
        $pago->concepto= $request ->concepto;

        if (Input::hasFile('comprobante')){
            $file=Input::file('comprobante');
            $file->move(public_path().'/img/pagos/', $file->getClientOriginalName());
            $pago->comprobante=$file->getClientOriginalName();
        }

        $pago->cantidad_m= $request ->cantidad_m;
        $pago->cantidad_n= $request ->cantidad_n;
        $pago->cantidad_a= $request ->cantidad_a;
        $pago->cantidad_e= $request ->cantidad_e;
        $pago->directiva= $request ->directiva;
        $pago->tipo= $request ->tipo;
        $pago->save();
        return $pago;
    }
    
    public function downloadFile($file){
      $pathtoFile = public_path().'\img\pagos/'.$file;
      return response()->download($pathtoFile);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(auth()->user()->rol_id == 6){
            return Pago::where('id',$id)->where('iglesia_id', auth()->user()->iglesia->id)->get();
        }
        else if(auth()->user()->rol_id == 4){
            return Pago::where('id',$id)->where('club_id', auth()->user()->club->id)->get();
        }
        else{
           return Pago::where('id',$id)->get();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Pago::destroy($id);
    }
}
