<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Persona;

class PersonasCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $personas= Persona::all();
        return $personas;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nombre'=>'string',
            'cedula'=>'string',
            'correo'=>'string',
            'telefono'=>'string',
            'direccion'=>'string',
            'estado_civil'=>'string',
            'fecha_nac'=>'date',
            ]);
        $date = \Carbon\Carbon::parse($request->fecha_nac);
        $persona = new Persona();
        $persona->nombre = $request ->nombre;
        $persona->cedula = $request ->cedula;
        $persona->correo = $request ->correo;
        $persona->telefono = $request ->telefono;
        $persona->direccion = $request ->direccion;
        $persona->estado_civil = $request ->estado_civil;
        $persona->fecha_nac = $date;
        $persona->save();
        return $persona;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Persona::findOrFail($id);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nombre'=>'string',
            'cedula'=>'string',
            'correo'=>'string',
            'telefono'=>'string',
            'direccion'=>'string',
            'estado_civil'=>'string',
            'fecha_nac'=>'date',
            ]);
        $date = \Carbon\Carbon::parse($request->fecha_nac);
        $persona = Persona::find($id);
        $persona->nombre = $request ->nombre;
        $persona->cedula = $request ->cedula;
        $persona->correo = $request ->correo;
        $persona->telefono = $request ->telefono;
        $persona->direccion = $request ->direccion;
        $persona->estado_civil = $request ->estado_civil;
        $persona->fecha_nac = $date;
        $persona->save();
        return $persona;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Persona::destroy($id);
        
    }
}
