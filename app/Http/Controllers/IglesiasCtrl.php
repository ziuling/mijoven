<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Iglesia;

class IglesiasCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Iglesia::with('distrito','tipo', 'user')->get()->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'nombre'=>'required|string|min:3|max:200',
           'direccion'=>'required|string|min:1|max:200',
           'distrito_id'=>'required|numeric',
           'tipo_id'=>'required|numeric',
           'user_id'=>'numeric'
           ]);
        $i = Iglesia::create($request->only(['nombre','direccion', 'distrito_id', 'tipo_id', 'user_id']));
        return $i;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Iglesia::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre'=>'required|string|min:3|max:200',
            'direccion'=>'required|string|min:10|max:200',
            'distrito_id'=>'required|numeric',
            'tipo_id'=>'required|numeric',
            'user_id'=>'required|numeric'
            ]);
        $iglesia= Iglesia::find($id);
        $iglesia->nombre = $request ->nombre;
        $iglesia->direccion = $request ->direccion;
        $iglesia->distrito_id = $request ->distrito_id;
        $iglesia->tipo_id = $request ->tipo_id;
        $iglesia->user_id = $request ->user_id;
        $iglesia->save();
        return $iglesia;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Iglesia::destroy($id);
    }
}
