<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ministerio;
use App\Iglesia;
use App\Club;
use Illuminate\Support\Facades\Auth;


class MinisteriosCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r)
    {
        if ($r->wantsJson()) {
            if(auth()->user()->rol->id == 6){
                return Ministerio::with('club', 'iglesia', 'actividad')->where('iglesia_id', auth()->user()->iglesia->id)->get();
            }
            else if(auth()->user()->rol->id == 4){
                return Ministerio::with('club', 'iglesia', 'actividad')->where('club_id', auth()->user()->club->id)->get();
            }
            else{
                return Ministerio::with('club', 'iglesia')->get()->toJson();
            }
        }    
        return view('ministerios.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crearPDF($datos, $vistaurl, $obj, $rol, $min){
        $data = $datos;
        $date = date('Y-m-d');
        $view = \View::make($vistaurl, compact('data', 'date', 'obj', 'rol', 'min'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);

        return $pdf->stream('reporte.pdf');
    }

    public function crear_reporte($tipo, $id, $mes){
        $vistaurl = "ministerios.pdf";
        if($tipo == 1){
            $obj  = Club::where('id', $id)->first();
            $rol = $obj->user->rol_id;

        }else if($tipo == 2){
            $obj  = Iglesia::where('id', $id)->first();
            $rol = $obj->user->rol_id; 
        }
        if( $rol == 4 ){
            $ministerios = Ministerio::where('club_id', $id)->where('mes', $mes)->get();
            $min = count($ministerios);
        }else if($rol == 6){
            $ministerios = Ministerio::where('iglesia_id', $id)->where('mes', $mes)->get();
            $min = count($ministerios);
        }

        return $this->crearPDF($ministerios, $vistaurl, $obj, $rol, $min);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'mes'=>'string',
            'dia'=>'numeric',
            'descripcion'=>'string',
            'nombre'=>'string',
            'actividad_id'=>'numeric'
            ]);

        $ministerio = new Ministerio();
     
        if ($request->user()->rol->nombre == 'Secretario(a) de Club') {
            $ministerio->club_id = auth()->user()->club->id;
        }else{
            $ministerio->iglesia_id = auth()->user()->iglesia->id;
        }
        $ministerio->mes = $request ->mes;
        $ministerio->dia = $request ->dia;
        $ministerio->descripcion = $request ->descripcion;
        $ministerio->nombre = $request ->nombre;
        $ministerio->actividad_id = $request ->actividad_id;
        $ministerio->save();
        return $ministerio;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         if(auth()->user()->club->id == null){
            return Ministerio::where('id',$id)->where('iglesia_id', auth()->user()->iglesia->id)->get();
        }
        else{
            return Ministerio::where('id',$id)->where('club_id', auth()->user()->club->id)->get();

        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'mes'=>'string',
            'descripcion'=>'string',
            'nombre'=>'string',
            'actividad_id'=>'numeric'
            ]);
        $ministerio = Ministerio::find($id);
        if ($request->user()->rol->nombre == 'Secretario(a) de Club') {
            $ministerio->club_id = auth()->user()->club->id;
        }else{
            $ministerio->iglesia_id = auth()->user()->iglesia->id;
        }
        if ($request->user()->rol->id == 4 ){
                $ministerio->user_id = Auth::id();
            }else {
                $ministerio->user_id = Auth::id();
            }
        $ministerio->mes = $request ->mes;
        $ministerio->dia = $request ->dia;
        $ministerio->descripcion = $request ->descripcion;
        $ministerio->nombre = $request ->nombre;
        $ministerio->actividad_id = $request ->actividad_id;
        $ministerio->save();
        return $ministerio;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Ministerio::destroy($id);
        
    }
}
