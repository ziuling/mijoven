<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pastor;

class PastoresCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Pastor::with('distrito')->get()->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['nombre'=>'required|string|min:3|max:200', 
            'cedula'=>'required|string|min:7|max:10', 
            'correo'=>'required|string|min:10|max:50',
            'distrito_id'=>'required|numeric'
        ]);
        $p = Pastor::create($request->only(['nombre', 'cedula', 'correo','distrito_id']));
        return $p;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Pastor::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre'=>'required|string|min:3|max:20', 
            'cedula'=>'required|string|min:7|max:8', 
            'correo'=>'required|string|min:10|max:30'
            ]);
        $pastor= Pastor::find($id);
        $pastor->fill($request->only(['nombre', 'cedula', 'correo','distrito_id']));
        $pastor->save();
        return $pastor;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Pastor::destroy($id);
    }
}
