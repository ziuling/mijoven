<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Directiva;
use App\Persona;
use App\Cargo;
use App\Club;
use App\Grupo;
use App\Miembro;
use Illuminate\Support\Facades\Auth;

class DirectivasCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(Request $r)
    {
        if ($r->wantsJson()) {
            if(auth()->user()->rol_id == 6){
                return Directiva::with('persona', 'cargo', 'club', 'iglesia', 'grupo')->where('iglesia_id', auth()->user()->iglesia->id)->get();
            }else if(auth()->user()->rol_id == 4){
                return Directiva::with('persona', 'cargo', 'club', 'iglesia', 'grupo')->where('club_id', auth()->user()->club->id)->get();
            }else{
                return Directiva::with('persona', 'cargo', 'club', 'iglesia', 'grupo')->get();
            }
        }
        return view('directivas.index');
        
    }

    public function crearPDF($directivas, $miembros, $nombre, $vistaurl, $tipo){
        $data = $directivas;
        $data1 = $miembros;
        $nombre = $nombre;
        $date = date('Y-m-d');
        $view = \View::make($vistaurl, compact('data', 'data1', 'nombre', 'date'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);

        if($tipo==1){
            return $pdf->stream('reporte.pdf');
        }
        if($tipo==2){
            return $pdf->download('reporte.pdf');
        }
    }

    public function crear_reporte($tipo, $id){
        $vistaurl = "directivas.clubes";
        $directivas = Directiva::where('club_id', $id);
        $miembros = Miembro::where('club_id', $id);
        $id = Directiva::where('club_id', $id)->first();
        $nombre = $id->club->nombre;
        //dd($planes);

        return $this->crearPDF($directivas, $miembros, $nombre, $vistaurl, $tipo);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dump($request->all());
        $p= json_decode($request->persona, true);
        dump($p['cedula']);
        
        $persona = new Persona();
        $persona->cedula = $p['cedula'];
        $persona->nombre = $p['nombre'];
        $persona->telefono = $p['telefono'];
        $persona->direccion = $p['direccion'];
        $persona->correo = $p['correo'];

        $persona->save();

        $directiva = new Directiva();
        $directiva->persona_id = $persona->id;
        
        if ($request->user()->rol->id == 4) {
            $directiva->club_id = auth()->user()->club->id;
        }else if($request->user()->rol->id == 6){
            $directiva->iglesia_id = auth()->user()->iglesia->id;
        }

        $directiva->cargo_id = $request ->cargo_id;
        $directiva->grupo_id = $request ->grupo_id;

        if ($directiva->grupo_id == null){
            $directiva->t = 'o';
        }else{
            $directiva->t = 'g';
        }
        $directiva->save();

        return $directiva;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(auth()->user()->club->id == null){
            return Directiva::where('id',$id)->where('iglesia_id', auth()->user()->iglesia->id)->get();
        }
        else{
            return Directiva::where('id',$id)->where('club_id', auth()->user()->club->id)->get();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'cargo_id'=>'required|numeric',
           // 'grupo_id'=>'numeric'
            ]);


        $directiva =Directiva::find($id);

        if ($request->user()->rol->nombre == 'Secretario(a) de Club') {
            $directiva->club_id = auth()->user()->club->id;
        }else{
            $directiva->iglesia_id = auth()->user()->iglesia->id;
        }
        
        $persona = Persona::find($directiva->persona_id);
        
        $persona->cedula = $request->persona['cedula'];
        $persona->nombre = $request->persona['nombre'];
        $persona->correo = $request->persona['correo'];
        $persona->telefono = $request->persona['telefono'];
        $persona->direccion = $request->persona['direccion'];
        $persona->save();
        
        $directiva->persona_id = $persona->id;
        $directiva ->cargo_id = $request->cargo_id;
        $directiva ->grupo_id = $request->grupo_id;
        $directiva->save ();

        
        return $directiva;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Directiva::destroy($id);
    }
}
