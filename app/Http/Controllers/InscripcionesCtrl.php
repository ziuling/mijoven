<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inscripcion;

class InscripcionesCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        

        return Inscripcion::with('categoria')->get()->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'seguro'=>'required|numeric',
            'monto'=>'required|numeric',
            'monto_miembro'=>'required|numeric',
            'monto_acompanante'=>'required|numeric',
            'monto_nino'=>'required|numeric',
            'categoria_id'=>'required|numeric']);
        $date = \Carbon\Carbon::parse($request->fecha);
        $i = Inscripcion::create($request->only(['seguro','monto', 'monto_miembro', 'monto_acompanante', 'monto_nino','categoria_id']));
       $i->fecha = $date;
        $i->save();
        return $i;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Inscripcion::where('categoria_id',$id)->get();
        //return Inscripcion::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'fecha'=>'required|date',
            'seguro'=>'required|numeric',
            'monto'=>'required|numeric',
            'monto_miembro'=>'required|numeric',
            'monto_acompanante'=>'required|numeric',
            'monto_nino'=>'required|numeric',
            'categoria_id'=>'required|numeric']);

        $date = \Carbon\Carbon::parse($request->fecha);

        $inscripcion = Inscripcion::find($id);
        $inscripcion->fecha = $date;
        $inscripcion->seguro = $request->seguro;
        $inscripcion->monto = $request->monto;
        $inscripcion->monto_miembro = $request->monto_miembro;
        $inscripcion->monto_acompanante = $request->monto_acompanante;
        $inscripcion->monto_nino = $request->monto_nino;
        $inscripcion->categoria_id = $request->categoria_id;
        $inscripcion->save();
        return $inscripcion;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Inscripcion::destroy($id);
    }
}
