<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Club;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;

class ClubesCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
        //var_dump(Club::with('categoria', 'iglesia', 'usuario')->get()->toJson());
        $club= Club::with('categoria', 'iglesia', 'user')->get()->toJson(); 
        return $club;
        //return view('club.index', ['clubes'=>$clubes, 'club'=>$club]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'nombre'=>'required|string|min:8|max:40',
            'telefono'=>'string|min:11|max:11',
            'categoria_id'=> 'required|numeric',
            'iglesia_id'=> 'required|numeric',
            'user_id'=> 'required|numeric',
            ]);

        $c = Club::create($request->only(['nombre', 'telefono', 'categoria_id', 'iglesia_id', 'user_id']));
        $c->save();
        return $c;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Club::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        //return view("club.edit", ["club"=>Club::findOrFail($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre'=>'required|string|min:8|max:40',
            'telefono'=>'string|min:11|max:11',
            'categoria_id'=> 'required|numeric',
            'iglesia_id'=> 'required|numeric',
            'user_id'=> 'required|numeric',
            ]);

        $club= Club::find($id);
        $club->nombre = $request ->nombre;
        $club->telefono = $request ->telefono;
        $club->categoria_id = $request ->categoria_id;
        $club->iglesia_id = $request ->iglesia_id;
        $club->user_id = $request ->user_id;

        $club->save();
        return $club;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Club::destroy($id);
    }
}
