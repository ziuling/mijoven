<?php

namespace App\Http\Middleware;

use Closure;

class ReporteMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::user()->rol->id == 4 || \Auth::user()->rol->id == 6 || \Auth::user()->rol->id == 2 || \Auth::user()->rol->id == 3 ){
           return $next($request);
        } else{
            return abort(403);
        }

    }
}
