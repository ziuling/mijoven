<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{

    protected $table= 'personas';

    protected $primaryKey= 'id';
    
    protected $fillable = [
    'cedula',
    'nombre',
    'telefono',
    'direccion',
    'correo',
    'estado_civil',
    'fecha_nac'];

    
    public function directiva()
    {
    	return $this->hasOne('App\Directiva','persona_id');
    }

    public function miembro()
    {
    	return $this->hasOne('App\Miembro','persona_id');
    }

    public function planificacion()
    {
    	return $this->hasOne('App\Planificacion','persona_id');
    }
}
