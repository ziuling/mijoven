<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ministerio extends Model
{
    protected $table = 'ministerios';

	protected $fillable = ['mes', 'dia', 'nombre', 'descripcion', 'club_id', 'iglesia_id', 'actividad_id'];

	public function club()
	{
		return $this->belongsTo('App\Club','club_id');
	}

	public function iglesia()
	{
		return $this->belongsTo('App\Iglesia','iglesia_id');
	}

	public function actividad()
	{
		return $this->belongsTo('App\Actividad','actividad_id');
	}
}
