<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    //
    protected $table = 'pagos';
    
    protected $fillable = ['n_referencia', 'fecha', 'banco', 'monto', 'concepto', 'comprobante', 'planilla', 'cantidad_m', 'cantidad_a', 'cantidad_n', 'cantidad_e',  'directiva', 'tipo',  'club_id', 'iglesia_id', 'actividad_id'];

    public function club()
	{
		return $this->belongsTo('App\Club','club_id');
	}

	public function iglesia()
	{
		return $this->belongsTo('App\Iglesia','iglesia_id');
	}

	public function actividad()
	{
		return $this->belongsTo('App\Actividad','actividad_id');
	}

}
