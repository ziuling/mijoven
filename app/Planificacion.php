<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Planificacion extends Model
{
    protected $table = 'planificaciones';

    protected $fillable = ['f_inicio', 'f_final', 'responsable', 'mes', 'estado', 'como', 'cuando', 'que', 'objetivos', 'propositos', 'club_id', 'iglesia_id', 'actividad_id'];

    public function persona()
    {
    	return $this->belongsTo('App\Persona','persona_id');
    }

    public function club()
    {
    	return $this->belongsTo('App\Club','club_id');
    }

    public function iglesia()
    {
        return $this->belongsTo('App\Iglesia','iglesia_id');
    }

    public function actividad()
    {
    	return $this->belongsTo('App\Actividad','actividad_id');
    }
    public function scopeE($query)
    {
        return $query->where('mes', 'enero');
    }
    public function scopeF($query)
    {
        return $query->where('mes', 'febrero');
    }
    public function scopeM($query)
    {
        return $query->where('mes', 'marzo');
    }
    public function scopeA($query)
    {
        return $query->where('mes', 'abril');
    }
    public function scopeMA($query)
    {
        return $query->where('mes', 'mayo');
    }
    public function scopeJ($query)
    {
        return $query->where('mes', 'junio');
    }
    public function scopeJU($query)
    {
        return $query->where('mes', 'julio');
    }
    public function scopeAg($query)
    {
        return $query->where('mes', 'agosto');
    }
    public function scopeS($query)
    {
        return $query->where('mes', 'septiembre');
    }
    public function scopeO($query)
    {
        return $query->where('mes', 'octubre');
    }
    public function scopeN($query)
    {
        return $query->where('mes', 'noviembre');
    }
    public function scopeD($query)
    {
        return $query->where('mes', 'diciembre');
    }
}
