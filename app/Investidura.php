<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investidura extends Model
{
    protected $table = 'investiduras';

    protected $fillable = ['nombre', 't'];

    public function miembroc()
    {
    	return $this->hasMany('App\Miembro_investidura','investidura_id');
    }

    public function scopeaventurero($query)
    {

        return $query->where('t', 'a');
    }
    public function scopeconquiguia($query)
    {

        return $query->where('t', 'c');
    }
}
