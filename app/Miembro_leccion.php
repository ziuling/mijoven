<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Miembro_leccion extends Model
{
    protected $table = 'miembro_lecciones';

    protected $fillable = ['miembro_id', 'leccion_id'];

    public function miembro()
    {
    	return $this->belongsTo('App\Miembro','miembro_id');
    }

    public function leccion()
    {
    	return $this->belongsTo('App\Miembro_leccion','leccion_id');
    }
}
