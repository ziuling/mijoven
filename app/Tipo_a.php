<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_a extends Model
{
    protected $table = 'tipos_a';

    protected $fillable = ['tipo', 'nombre', 'total_puntuacion', 'user_id'];

    public function actividad()
    {
    	return $this->hasMany('App\Actividad','tipo_a_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

}
