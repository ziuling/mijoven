<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Identidad extends Model
{
   protected $table = 'identidad';
   protected $fillable = ['logo', 'direccion', 'club_id'];

    public function club()
    {
    	return $this->belongsTo('App\Club','club_id');
    }

}
