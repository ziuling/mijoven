<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Distrito extends Model
{
    //
    protected $table='distritos';

    protected $fillable = ['nombre','zona_id'];

    public function zona()
    {
    	return $this->belongsTo('App\Zona', 'zona_id');
    }
    public function pastor()
    {
    	return $this->hasOne('App\Pastor','distrito_id');
    }

    public function iglesia()
    {
    	return $this->hasMany('App\Iglesia','distrito_id');
    }
}
