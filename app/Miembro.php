<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Miembro extends Model
{
    protected $table = 'miembros';

    protected $fillable = ['persona_id', 'club_id', 'iglesia_id', 'grupo_id', 'curso', 'categoria', 'investidura', 'ministerio', 'tipo_sangre', 'necesidad', 'observacion', 'donante', 'voluntario'];

    public function club()
    {
    	return $this->belongsTo('App\Club','club_id');
    }
    
    public function persona()
    {
        return $this->belongsTo('App\Persona','persona_id');
    }

    public function iglesia()
    {
        return $this->belongsTo('App\Iglesia','iglesia_id');
    }

    public function grupo()
    {
        return $this->belongsTo('App\Grupo','grupo_id');
    }

    public function miembroc()
    {
    	return $this->hasMany('App\Miembro_investidura','miembro_id');
    }

    public function miembrog()
    {
    	return $this->hasMany('App\Miembro_leccion','miembro_id');
    }
}
