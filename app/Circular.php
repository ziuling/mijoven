<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Circular extends Model
{
    //
    protected $table = 'circulares';

    protected $fillable = ['titulo', 'sub_titulo', 'destinario','asunto','fecha','recomendacion','introduccion', 'reconocimiento','observacion','categoria_id', 'user_id', 'tipo', 'estado'];

    public function actividades()
    {
    	return $this->hasMany('App\Actividad','circular_id');
    }

    public function user()
    {
            return $this->belongsTo('App\User','user_id');
    }

    public function categoria()
    {
    	return $this->belongsTo('App\Categoria','categoria_id');
    }
    
   


}
