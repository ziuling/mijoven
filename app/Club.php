<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{

   protected $table = 'clubes';

   protected $fillable = ['nombre', 'telefono', 'logo', 'direccion', 'categoria_id', 'iglesia_id', 'user_id'];

   public function  identidad()
   {
         return $this->hasOne('App\Identidad','club_id');
   }

   public function user()
   {
   	   return $this->belongsTo('App\User','user_id');
   }

   public function miembro()
   {
   	   return $this->hasMany('App\Miembro','club_id');
   }

   public function directiva()
   {
   	   return $this->hasMany('App\Directiva','club_id');
   }

   public function iglesia()
   {
   	   return $this->belongsTo('App\Iglesia','iglesia_id');
   }

   public function categoria()
   {
   	   return $this->belongsTo('App\Categoria','categoria_id');
   }

   public function plan()
   {
   	   return $this->hasMany('App\Plan_sostenimiento','club_id');
   }

   public function ministerio()
   {
         return $this->hasMany('App\Ministerio','club_id');
   }

   public function puntuaciones()
   {
   	   return $this->hasMany('App\Puntuacion','club_id');
   }

   public function grupo()
   {
   	   return $this->hasMany('App\Grupo','club_id');
   }

   public function planificaciones()
   {
   	   return $this->hasMany('App\Planificacion','club_id');
   }

   public function informes()
   {
   	   return $this->hasMany('App\Informe','club_id');
   }

   public function pago()
   {
         return $this->hasMany('App\Pago','club_id');
   }
   public function scopeaventureros($query)
   {
      return $query->where('categoria_id', 1);
   }
   public function scopeconquistadores($query)
   {
      return $query->where('categoria_id', 2);
   }
   public function scopeguias($query)
   {
      return $query->where('categoria_id', 3);
   }
   
}
