<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modulo extends Model
{
	protected $table = 'modulos';

	protected $fillable = ['nombre','descripcion'];

    public function informe()
    {
    	return $this->hasMany('App\Informe','modulo_id');
    }

    public function actividades()
    {
    	return $this->hasMany('App\Act_modulo', 'modulo_id');
    }


    
}
