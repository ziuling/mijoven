<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    //
    protected $table='categorias';
    protected $fillable = ['nombre'];
    
    

    public function club()
	{
		return $this->hasMany('App\Club','categoria_id');
	}
    
    public function circular()
    {
    	return $this->hasMany('App\Circular','categoria_id');
    }
    public function puntuaciones()
    {
        return $this->hasMany('App\Puntuacion','categoria_id');
    }
    public function inscripcion()
    {
    	return $this->hasMany('App\Inscripcion','categoria_id');
    }

    public function reglamento()
   {
       return $this->hasOne('App\Reglamento','categoria_id');
   }

   public function scopea($query)
    {

        return $query->where('id', 1);
    }
}
