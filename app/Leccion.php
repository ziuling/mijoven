<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leccion extends Model
{

	protected $table = 'lecciones';

    protected $fillable = ['numero'];

    public function miembrog()
    {
    	return $this->hasMany('App\Miembro_leccion','leccion_id');
    }
}
