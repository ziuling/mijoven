<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    //
    public function usuarios()
    {
    	return $this->hasOne('App\User', 'rol_id');
    }
}
