<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan_sostenimiento extends Model
{
	protected $table = 'plan_sostenimiento';
    
    protected $fillable = ['mes', 'general', 'mes_ant', 'inv', 'inv_rec', 'ganancia', 'total_fondo', 'club_id', 'actividad_id'];

    public function club()
    {
    	return $this->belongsTo('App\Club','club_id');
    }

    public function actividad()
    {
    	return $this->belongsTo('App\Actividad','actividad_id');
    }
    
}
