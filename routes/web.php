
<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Use App\Actividad;

Route::get('/', function () {
    return view('auth.login');
});
    Route::auth();


	Route::group(['middleware' => 'auth'], function () {
		
	    Route::group(['middleware' => 'secretarioc'], function () {
			Route::resource('directivas','DirectivasCtrl');
			Route::resource('miembros','MiembrosCtrl');
			Route::get('historial', 'InformesCtrl@historial');
			Route::resource('informes','InformesCtrl');
			Route::resource('identidades','IdentidadCtrl');
			Route::resource('ministerios','MinisteriosCtrl');
			Route::resource('planificaciones','PlanificacionCtrl');
			Route::resource('informes_act','Informe_actCtrl');
			Route::resource('lecciones','LeccionCtrl');
			Route::resource('investiduras','InvestidurasCtrl');
			Route::resource('modulos','ModulosCtrl');
			Route::resource('act_modulos','Act_modulosCtrl');
			Route::resource('asistencias','AsistenciasCtrl');
			Route::resource('cargos','CargosCtrl');
			//Route::resource('pagos', 'PagosCtrl');
	    });
	    Route::group(['prefix'=>'direcciong'],function (){
				Route::get('/','HomeController@direcciong');
				Route::resource('grupos','GruposCtrl');
		    });
	    Route::group(['middleware' => 'plan'], function () {
			Route::resource('planes','Plan_SostenimientoCtrl');
        });
		Route::resource('personas','PersonasCtrl');
	    Route::group(['middleware' => 'reporte'], function () {
            Route::resource('reportes','ReportesCtrl');
			Route::resource('consultas','ReportesCtrl@index');
			Route::get('circular/{tipo}/{id}', 'CircularesCtrl@crear_reporte');
			Route::get('grupo/{tipo}/{id}', 'GruposCtrl@crear_reporte');
			Route::get('directiva/{tipo}/{id}', 'DirectivasCtrl@crear_reporte');
			Route::get('reglamento/{tipo}/{id}', 'HomeController@crear_reporte');

        });
      
	    Route::group(['middleware' => 'mvnor'], function () {
			Route::resource('actividades','ActividadesCtrl');
			Route::resource('circulares','CircularesCtrl');
			Route::resource('tipos','Tipos_aCtrl');
			Route::resource('puntuaciones','PuntuacionesCtrl');
			Route::resource('detalles','DetallesCtrl');

			//Route::resource('pagos', 'PagosCtrl');
			Route::resource('miembros','MiembrosCtrl');
            
            Route::group(['prefix'=>'administracion'], function (){
				Route::get('/','HomeController@administracion');
				Route::resource('iglesias','IglesiasCtrl');
				Route::resource('pastores','PastoresCtrl');
				Route::resource('zonas','ZonasCtrl');
			    Route::resource('distritos','DistritosCtrl');
				Route::resource('clubes','ClubesCtrl');
				Route::resource('inscripciones','InscripcionesCtrl');
				Route::resource('reglamentos', 'ReglamentosCtrl');

		    });
        });

	    Route::resource('categorias','CategoriasCtrl');

		Route::resource('pago', 'PagosCtrl');
        Route::get('/download/{file}' , 'PagosCtrl@downloadFile');
        
	    Route::group(['middleware' => 'pdf'], function () {
            Route::get('planificacion/{tipo}/{id}', 'PlanificacionCtrl@crear_reporte');
            Route::get('pago/{tipo}/{id}', 'PagosCtrl@crear_reporte');
		 	Route::get('informe/{tipo}/{id}/{mes}', 'InformesCtrl@crear_reporte');
			Route::get('plan/{id}/{mes}', 'Plan_SostenimientoCtrl@crear_reporte');
			Route::get('grupo/{tipo}/{id}', 'GruposCtrl@crear_reporte');
			Route::get('ministerio/{tipo}/{id}/{mes}', 'MinisteriosCtrl@crear_reporte');
            
		});

        Route::group(['middleware' => 'usuario'], function () {
			Route::resource('usuarios','UsersCtrl');
			Route::resource('roles','RolsCtrl');
		});
		
	  
	    Route::group(['middleware' => 'administrador'], function () {
		    Route::get('Respaldo',function(){
				return view('Respaldo.index');
			});
			Route::get('restaurar',function(){
				return view('Respaldo.restaurar');
			});
        });
		
		Route::get('print',function(){
			return view('informes.print')->with('modulos',\App\Modulo::with('actividades')->get());
		});

		Route::get('/home', 'HomeController@index');
	    //Route::get('/{slug?}', 'HomeController@index');
	
	});
  


